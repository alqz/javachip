package com.samisaada.paskahousu.ui;

import java.awt.*;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import com.samisaada.paskahousu.ui.table.*;

public class GameTable extends JPanel {

    private Image background;
    private Image logo;
    private HandUI hand;
    private DeckUI deck;
    private PlayAreaUI playArea;
    private PlayerSet[] playersets;

    public GameTable() {
        super(new GridLayout(3,3));
        this.background = Images.BACKGROUND;
        this.logo = Images.LOGO;
        //this.setOpaque(true);
        this.setBackground(new Color((float)50/255, (float)205/255, (float)50/255));

        this.hand = new HandUI();
        this.deck = new DeckUI();
        this.playArea = new PlayAreaUI();
        this.playersets = new PlayerSet[5];

        this.addComponents();
    }

    private void addComponents() {
        this.add((this.playersets[1] == null) ? new JLabel("") : this.playersets[1]);
        this.add((this.playersets[2] == null) ? new JLabel("") : this.playersets[2]);
        this.add((this.playersets[3] == null) ? new JLabel("") : this.playersets[3]);
        this.add((this.playersets[0] == null) ? new JLabel("") : this.playersets[0]);
        this.add(this.playArea);
        this.add((this.playersets[4] == null) ? new JLabel("") : this.playersets[4]);
        this.add(this.deck);
        this.add(this.hand);
    }

    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        int width = this.getWidth();
        int height = this.getHeight();
        int imageW = this.background.getWidth(this);
        int imageH = this.background.getHeight(this);

        // Tile the image to fill our area.
        for (int x = 0; x < width; x += imageW) {
            for (int y = 0; y < height; y += imageH) {
                g.drawImage(this.background, x, y, this);
            }
        }

        // Logo
        int logoW = this.logo.getWidth(this);
        int logoH = this.logo.getHeight(this);
        int positionX = width/2-logoW/2;
        if (positionX < 0)
            positionX = 0;
        int positionY = height/2-logoH/2;
        if (positionY < 0)
            positionY = 0;
        g.drawImage(this.logo, positionX, positionY, this);
    }

}
