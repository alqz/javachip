/*****************************************************************************
 * 
 * Copyright 2010-2011 LE ROUX Thomas <thomas.leroux.identity@gmail.com>
 * 
 * This file is part of Oria.
 * 
 * Oria is free software: you can redistribute it and/or modify
 * it under the terms of the zlib license. See the COPYING file.
 * 
 *****************************************************************************/

package fr.oria.utils;

import fr.oria.music.MusicPlayerDaemon;
import fr.oria.sound.SoundDaemon;

/**
 * This class handle all the Daemon for Oria.
 * 
 * @author november-eleven
 * @version 0.1
 *
 */
public class OriaDaemon {

	private static OriaDaemon instance;
	
	private SoundDaemon mSound;
	private MusicPlayerDaemon mMusic;
	
	private OriaDaemon() {
		mSound = new SoundDaemon();
		mMusic = new MusicPlayerDaemon();
	}
	
	public static OriaDaemon getInstance() {
		if(instance == null) {
			instance = new OriaDaemon();
		}
		return instance;
	}
	
	public static Boolean loadInstance() {
		if(instance == null) {
			instance = new OriaDaemon();
		}
		return (instance != null);
	}
	
	public static SoundDaemon getSoundDaemon() {
		return getInstance().getSound();
	}
	
	public static MusicPlayerDaemon getMusicDaemon() {
		return getInstance().getMusic();
	}
	
	public SoundDaemon getSound() {
		return mSound;
	}
	
	/**
	 * Get the Music Player Daemon from {@link OriaDaemon}
	 * 
	 * @return The Music Player Daemon
	 */
	public MusicPlayerDaemon getMusic() {
		return mMusic;
	}
	
	public static void dispose() {
		getSoundDaemon().dispose();
		getMusicDaemon().dispose();
	}

}
