package org.rsbot.script.wrappers;

import org.rsbot.bot.Context;
import org.rsbot.bot.accessors.Client;
import org.rsbot.bot.accessors.RSGroundEntity;
import org.rsbot.bot.accessors.RSGroundObject;
import org.rsbot.script.concurrent.Task;
import org.rsbot.script.methods.Calculations;
import org.rsbot.script.methods.Game;

import java.awt.*;

/**
 * Represents an item on a tile.
 *
 * @author Timer
 */
public class GroundItem implements Locatable, Verifiable, Interactive, Targetable {
	private final Item groundItem;
	private final Tile location;

	public GroundItem(final Tile location, final Item groundItem) {
		this.location = location;
		this.groundItem = groundItem;
	}

	/**
	 * Gets the top model on the tile of this ground item.
	 *
	 * @return The top model on the tile of this ground item.
	 */
	public GameModel getModel() {
		final int x = location.getX() - Game.getBaseX();
		final int y = location.getY() - Game.getBaseY();
		final Client client = Context.get().client;
		final int plane = client.getPlane();
		final org.rsbot.bot.accessors.RSGround rsGround = client.getRSGroundArray()[plane][x][y];

		if (rsGround != null) {
			final RSGroundEntity obj = rsGround.getGroundObject();
			if (obj != null) {
				final org.rsbot.bot.accessors.Model model = ((RSGroundObject) rsGround.getGroundObject()).getModel();
				if (model != null) {
					return new AnimableModel(model, obj);
				}
			}
		}
		return null;
	}

	/**
	 * Performs the given action on this GroundItem.
	 *
	 * @param action The menu action to click.
	 * @return <tt>true</tt> if the action was clicked; otherwise <tt>false</tt>.
	 */
	public boolean interact(final String action) {
		return interact(action, null);
	}

	/**
	 * Performs the given action on this GroundItem.
	 *
	 * @param action The menu action to click.
	 * @param option The option of the menu action to click.
	 * @return <tt>true</tt> if the action was clicked; otherwise <tt>false</tt>.
	 */
	public boolean interact(final String action, final String option) {
		final GameModel model = getModel();
		if (model != null) {
			return model.interact(action, option);
		}
		return getLocation().interact(Task.random(0.45, 0.55), Task.random(0.45, 0.55), 0, action, option);
	}

	/**
	 * Gets this GroundItem's Item.
	 *
	 * @return This GroundItem's Item.
	 */
	public Item getItem() {
		return groundItem;
	}

	/**
	 * Gets this GroundItem's location.
	 *
	 * @return This GroundItem's location.
	 */
	public Tile getLocation() {
		return location;
	}

	/**
	 * Calculates if this GroundItem is on screen.
	 *
	 * @return <tt>true</tt> if on screen; otherwise <tt>false</tt>.
	 */
	public boolean isOnScreen() {
		final GameModel model = getModel();
		if (model == null) {
			return location.isOnScreen();
		} else {
			return Calculations.isPointOnScreen(model.getPoint());
		}
	}

	/**
	 * Returns a point on this GroundItem's model.
	 *
	 * @return A point on this GroundItem's model.
	 */
	public Point getPoint() {
		GameModel model = getModel();
		if (model != null) {
			return model.getPoint();
		}
		return getLocation().toScreen();
	}

	/**
	 * Finds if this GroundItem's model contains a given point.
	 *
	 * @param x The X coordinate of the point.
	 * @param y The Y coordinate of the point.
	 * @return <tt>true</tt> if the model contains the point; otherwise <tt>false</tt>.
	 */
	public boolean contains(int x, int y) {
		GameModel model = getModel();
		if (model != null) {
			return model.contains(x, y);
		}
		return getLocation().toScreen().distance(x, y) < Task.random(4, 9);
	}

	/**
	 * Finds if this GroundItem's model contains a given point.
	 *
	 * @param p The point to check.
	 * @return <tt>true</tt> if the model contains the point; otherwise <tt>false</tt>.
	 */
	public boolean contains(final Point p) {
		return contains(p.x, p.y);
	}

	public void draw(final Graphics render) {
		final Tile tile = getLocation();
		render.setColor(Color.cyan);
		if (getModel() != null) {
			getModel().draw(render);
		}
		tile.draw(render);
	}

	public boolean verify() {
		return groundItem != null && groundItem.verify();
	}
}
