package a.b.c;

import java.util.Arrays;
import java.util.List;

import a.b.c.builtin.BFlatString;
import a.b.c.builtin.BFlatVoid;

public class BFlatLibrary {
    public static String[] runtimeExposedArr = {"bflat_addClass", "bflat_addMethod", "bflat_getMethod",
	    "class_createInstance", "bflat_getClassFromName", "bflat_allocateClassPair", "bflat_registerClassPair",
	    "bflat_getMethod", "class_getName", "class_getIvar", "class_getMethod", "class_respondsTo"};
    public static String[] runtimeClassesArr = {"Class", "Method", "BFlatMethod", "Ivar", "BFlatIvar"};
    public static List<String> runtimeExposed = Arrays.asList(runtimeExposedArr);
    public static List<String> runtimeClasses = Arrays.asList(runtimeClassesArr);
    
    public static BFlatVoid print(BFlatString str)
    {
	System.err.println(str.getValue());
	return new BFlatVoid();
    }
    
    public static BFlatString typeof(Instance inst)
    {
	return new BFlatString(inst.isa.getName());
    }
    
    public static void eval(BFlatString expr)
    {
	
    }

}
