package com.mad.floorplans;

/**
 * // -------------------------------------------------------------------------
/**
 *  This class models a room.
 *
 *  @author Taylor O'Connor
 *  @version 2012.04.11
 */
public class Room
{
    private String name;

    private float x;
    private float y;

    /**
     *  This is the constructor for a room.
     *  @param name String the room name
     *  @param x String the ratio of x to the width.
     *  @param y String the ratio of y to the width.
     */
    public Room(String name, String x, String y) {
        this.name = name;
        this.x = Float.parseFloat(x);
        this.y = Float.parseFloat(y);
    }

    /**
     * Returns the name of the Room.
     * @return name String the name.
     */
    public String name()
    {
        return name;
    }

    /**
     * Returns the ratio of the x location to the width.
     * @return x float the x ratio.
     */
    public float x()
    {
        return x;
    }

    /**
     * Returns the ratio of the y location to the height.
     * @return y float the y ratio.
     */
    public float y()
    {
        return y;
    }

}
