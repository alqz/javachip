package oxen.worldparser;

import java.io.IOException;
import org.apache.xerces.parsers.SAXParser;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import oxen.game.Game;
import oxen.state.GameState;

/**
 * WorldParser coordinates all parsing functions for the game.
 * This extends thread because its so awesome that it takes a while. To be awesome.
 * Anyhoo, it's a thread because it hangs the GUI when it runs.
 * To parse a game world its simple. You create an instance of this class, then call setState,
 * providing an instance of Game. You then call run(). You must then wait "one ice age" (3 or 4 ice ages for
 * Samsung netbooks) for the parser to complete.
 * isParseCompleted() will return true when this is the case, you can wait for it by calling thread.sleep()
 * in a loop. Once parsing is done you can access the initial state by calling getInitialState(), yes, it's
 * that easy. You can then put the initial state somewhere and have fun with it.
 * Happy parsing.
 * @author James
 */
public final class WorldParser extends Thread {
    protected GameState startState;
    protected Game manager;
    protected boolean parseComplete = false;
    
    /**
     * Default constructor for objects of type WorldParser.
     * Just creates the parser.
     */
    public WorldParser() {
    }
    
    /**
     * Lets you set the <code>Game</code> used by this state to output errors. 
     * @param manager The instance of <code>Game</code> to be used by this class
     * for error reporting.
     */
    public final void setState(Game manager) {
        this.startState = null;
        this.manager = manager;
    }
    /**
     * This runs the parser. Its explained in the class comment.
     */
    @Override
    public void run() {
        // This is hard coded. Deal with it.
        String toParse = "/gameNew.xml";
        
        try {
            SAXParser sp = new SAXParser();
            Handler handler = new Handler();
            sp.setContentHandler(handler);
            sp.parse(new InputSource(getClass().getResourceAsStream(toParse)));
            ResolutionLayer resLayer = new ResolutionLayer();     
            startState = resLayer.resolveWorld(handler);
        }
        catch (SAXException e) {
            manager.getLogManager().logOutput(e.getMessage());
        }
        catch (IOException e) {
            manager.getLogManager().logOutput(e.getMessage());
        }
        catch (ResolutionException e) {
            manager.getLogManager().logOutput(e.getMessage());
        }
        parseComplete = true;
    }
    /**
     * Used to tell if the parse is finished.
     * @return Returns <code>true</code> if parsing is finished, <code>false</code> if not.
     */
    public boolean isParseCompleted() {
        return parseComplete;
    }
    /**
     * Used to access the parsed initial state.
     * @return A state containing the contents of the XML if parsing is finished.
     * Null if not.
     */
    public GameState getInitialState() {
        if(parseComplete)
            return startState;
        else
            return null;
    }
}
