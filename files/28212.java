package intranet.infra.repository.impl;

import intranet.domain.entities.Empresa;
import intranet.domain.repository.EmpresaRepository;
import intranet.infra.dao.EmpresaDAO;
import intranet.utils.IntranetException;

import java.util.List;

import lombok.extern.java.Log;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


/**
 * @author Felipe Balbino
 * @since 02/06/2011
 */
@Log
@Repository
public class EmpresaRepositorioImpl implements EmpresaRepository {
	
	private EmpresaDAO empresaDAO;
	
	@Autowired
	public EmpresaRepositorioImpl(EmpresaDAO empresaDAO) {
		this.empresaDAO = empresaDAO;
	}


	@Override
	public Empresa buscarPeloId(Long id) throws IntranetException {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public List<Empresa> buscarTodos() throws IntranetException {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public List<Empresa> getAllForIndex() throws IntranetException {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public Empresa getEmpresaById(Long id) throws IntranetException {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public void remover(Empresa t) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void adicionar(Empresa t) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void alterar(Empresa t) {
		// TODO Auto-generated method stub
		
	}
	
	
	
	
}
