/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package qse_sepm_ss12_07.domain;

/**
 *
 * @author Richy
 */
public class ProductFilter
{
	private boolean filterProductName;
	private boolean filterCategoryName;
	private boolean filterProductBarcode;
	private String searchText = "";

	public boolean isFilterCategoryName()
	{
		return filterCategoryName;
	}

	public void setFilterCategoryName(boolean filterCategoryName)
	{
		this.filterCategoryName = filterCategoryName;
	}

	public boolean isFilterProductName()
	{
		return filterProductName;
	}

	public void setFilterProductBarcode(boolean filterProductBarcode)
	{
		this.filterProductBarcode = filterProductBarcode;
	}

	public boolean isFilterProductBarcode()
	{
		return filterProductBarcode;
	}

	
	public void setFilterProductName(boolean filterProductName)
	{
		this.filterProductName = filterProductName;
	}
	
	public String getSearchText()
	{
		return searchText;
	}

	public void setSearchText(String searchText)
	{
		this.searchText = searchText;
	}
	
}
