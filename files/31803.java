package olympic.filters;

import olympic.util.GlobalConfiguration;
import olympic.util.ScreenConfiguration;

import static com.googlecode.javacv.cpp.opencv_core.*;
import static com.googlecode.javacv.cpp.opencv_imgproc.*;

/**
 * Copyright (C) 2012 Oliver
 * <p/>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
public class Green implements ImageFilter {

    private int SCALE = 1;

    public Green(ScreenConfiguration screenConfiguration) {

    }

    @Override
    public IplImage process(IplImage... image) {
        return null;
    }

    @Override
    public IplImage process(IplImage image) {
        IplImage convert = cvCreateImage(cvGetSize(image), IPL_DEPTH_8U, 3);
        if (image.nChannels() == 1)
            cvCvtColor(image, convert, CV_GRAY2RGB);
        else
            cvCvtColor(image, convert, CV_RGB2BGR);
        IplImage b = cvCreateImage(cvGetSize(convert), convert.depth(), 1),
                g = cvCreateImage(cvGetSize(convert), convert.depth(), 1),
                r = cvCreateImage(cvGetSize(convert), convert.depth(), 1);
        cvSplit(convert, r, g, b, null);
        cvZero(b);
        cvZero(r);
        cvMerge(b, g, r, null, convert);
        cvReleaseImage(r);
        cvReleaseImage(g);
        cvReleaseImage(b);
        return convert;
    }

    @Override
    public int scale() {
        if (GlobalConfiguration.config().scale != 0) {
            return GlobalConfiguration.config().scale;
        }
        return SCALE;
    }

    @Override
    public void start() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void end() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public IplImage transform(IplImage source) {
        return source;
    }
}
