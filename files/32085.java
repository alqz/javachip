/*
 Copyright 2006 - 2009 Under Dusken

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */

package org.pegadi.webapp.control;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class FileController extends AbstractController {

    private final Logger log = LoggerFactory.getLogger(getClass());

    private File resourceDir;

    protected ModelAndView handleRequestInternal(HttpServletRequest request,
                                                 HttpServletResponse response) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();

        String relPath = ServletRequestUtils.getStringParameter(request, "path");
        File file = new File(resourceDir, relPath);

        if(file.isDirectory() || file.getAbsolutePath().contains("../")
                || !file.exists()){
              throw new IllegalArgumentException(relPath + " does not exist, or you does not have access to it.");
        }
        map.put("file", file);
        log.info("returned file: " + file.getAbsolutePath());
        return new ModelAndView("fileView", map);
    }

    @Required
    public void setResourceDir(File resourceDir) {
        this.resourceDir = resourceDir;
    }
}
