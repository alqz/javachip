package com.snc.automation.autobot.command;

import com.snc.automation.autobot.AutoBot;
import com.snc.automation.autobot.Command;
import com.snc.automation.autobot.database.DatabaseFactory;
import com.snc.automation.autobot.database.DatabaseFactory.PairResult;

import java.util.Random;

public final class TomCommand extends Command {
	public static final CommandMeta META = new CommandMeta(
		"tom",
		"This is Tom! Flags: -w = Stores message",
		"[-w]",
		TomCommand.class
	);

	private static final Random sRandom = new Random();
	private static final String DB_NAME_PREFIX = "tom_command_";
	
	public TomCommand(AutoBot bot, String channel, String sender, String login, String hostname, String message) {
		super(bot, channel, sender, login, hostname, message);
	}	
	
	@Override
	public void run() {
		String str = getArgument(mMessage).trim();
		if (str.length() == 0) {
			mBot.sendMessage(mChannel, mSender + ": WHAT?!");
		} else if (str.startsWith("-w ")) {
			savePhrase(str.substring("-1 ".length()));
		} else {
			mBot.sendMessage(mChannel, mSender + ": " + getRandomMessage());
		}
	}
	
	private void savePhrase(String phrase) {
		DatabaseFactory db = DatabaseFactory.getInstance();
		db.write(db.createName(DB_NAME_PREFIX, phrase), phrase);
		mBot.sendMessage(mChannel, mSender + ": Tom's phrase stored.");
	}
	
	private String getRandomMessage() {
		DatabaseFactory db = DatabaseFactory.getInstance();
		int num = db.count(DB_NAME_PREFIX);
		if (num == 0) {
			throw new RuntimeException("Tom database empty!");
		}
		
		num = sRandom.nextInt(num);
		PairResult result = db.readNext(DB_NAME_PREFIX, num);
		if (result == null) {
			throw new RuntimeException("Tom database emtpy!");
		}
		
		return result.value;
	}
}
