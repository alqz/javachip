package at.jsug.vereinsverwaltung.core.dao;

import at.jsug.vereinsverwaltung.core.domain.MemberType;
import at.jsug.vereinsverwaltung.core.domain.Person;
import at.jsug.vereinsverwaltung.core.domain.PersonSettings;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import javax.inject.Inject;
import java.util.Date;
import java.util.List;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;

public class JpaPersonDaoTest
{

    @Inject
    PersonDao dao;

    @Inject
    MemberTypeDao memberTypeDao;

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testPersist() throws Exception {

        Person p = new Person();
        p.setFirstname("Andreas");
        p.setLastname("Tester");
        p.setEmail("tester@jsug.at");
        p.setCreationdate(new Date());
        p.setMemberSince(new Date());

        PersonSettings s = new PersonSettings();
        s.setOnMailinglist(false);
        s.setOnMemberlist(true);

        p.setSettings(s);

        MemberType type = memberTypeDao.find(2);

        p.setMemberType(type);

        dao.persist(p);

        System.out.println("p.getId() = " + p.getId());



        Person q = dao.find(p.getId());

        assertNotNull(p);
        assertNotNull(q);
        assertEquals("Firstname should be equal", p.getFirstname(), q.getFirstname());
        assertEquals("Lastname should be equal", p.getLastname(), q.getLastname());
        assertEquals("Email should be equal", p.getEmail(), q.getEmail());
        assertEquals("Settings should be equal", p.getSettings().isOnMailinglist(), q.getSettings().isOnMailinglist());
        assertEquals("Settings should be equal", p.getSettings().isOnMemberlist(), q.getSettings().isOnMemberlist());


        List<Person> result = dao.findAll();

        for(Person pers : result)
        {
            System.out.println("Name: " + pers.getFirstname() + " " + pers.getLastname());
            System.out.println("Settings: " + pers.getSettings().isOnMailinglist() + " | "  + pers.getSettings().isOnMemberlist());
            System.out.println("----");
        }
        
    }

    @Ignore
    @Test
    public void testEdit() throws Exception {
    }

    @Ignore
    @Test
    public void testRemove() throws Exception {
    }

    @Ignore
    @Test
    public void testFind() throws Exception {
    }

    @Test
    public void testFindAll() throws Exception {
        
        List<Person> result = dao.findAll();

        assertNotNull(result);
        assertTrue("result should not be empty", result.size() > 0);
    }

    @Test  @Ignore
    public void testFindRange() throws Exception {
    }
}
