package mas.worldview.sframesystem;

import java.awt.*;
import java.awt.image.BufferStrategy;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import layout.TableLayout;
import mas.worldview.sframesystem.rts.TextureFactory;

/**
 *
 * @author Christoph
 */
public class SFrameSystem {
    
    private final Dimension FRAME_SIZE = new Dimension(1024,768);
    private final int NBR_BUFFERS = 4;
    private final double[][] CONTENTPANE_CELLSIZE = new double[][]{{TableLayout.FILL},{TableLayout.FILL}};
    private DisplayMode[] BEST_DISPLAY_MODES = new DisplayMode[] {
        new DisplayMode(1280, 1024, 32, 0),
        new DisplayMode(1024, 768, 32, 0),
        new DisplayMode(800, 600, 32, 0),
        new DisplayMode(1280, 1024, 16, 0),
        new DisplayMode(1024, 768, 16, 0),
        new DisplayMode(800, 600, 16, 0),
        new DisplayMode(1280, 1024, 8, 0),
        new DisplayMode(1024, 768, 8, 0),
        new DisplayMode(800, 600, 8, 0)
    };
    
    private boolean fullscreen, done;
    private String template = "template0", title;
    private Frame window;
    private JFrame frame;
    private Container contentPane;
    private Component lastContent;
    private GraphicsDevice gd;
    private DisplayMode dm;
    private Thread renderingThread;
    private BufferStrategy bufferStrategy;
    
    /**
     * 
     * @param fullscreen states whether the programm runs in window or fullscreen mode.
     * @param gd is the graphics device the program shall be displayed on. If null, its the default screen.
     */
    public SFrameSystem(boolean fullscreen, GraphicsDevice graphicsDevice, DisplayMode displayMode, String title) {
        this.fullscreen = fullscreen;
        this.title      = title;
        this.done       = true;
        
        //If the user did not specify a screen device, take the default screen.
        if(graphicsDevice == null) {
            this.gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
        } else {
            this.gd = graphicsDevice;
        }
        
        //Figure out which BufferCapabilities are available.
        DisplayMode dspms[] = this.gd.getDisplayModes();
        boolean r1280 = false, r1024 = false, r800 = false;
        for(DisplayMode dspm : dspms) {
            if(dspm.getWidth() == 1280 && dspm.getHeight() == 1024) {
                r1280 = true;
                this.dm = dspm;
            } else if(dspm.getWidth() == 1024 && dspm.getHeight() == 768 && !r1280) {
                r1024 = true;
                this.dm = dspm;
            } else if(dspm.getWidth() == 800 && dspm.getHeight() == 600 && !r1280 && !r1024) {
                r800 = true;
                this.dm = dspm;
            }
        }
        
        try {
            TextureFactory.newInstance(template, "r"+this.dm.getWidth()+"x"+this.dm.getHeight());
        } catch (IOException ex) {
            Logger.getLogger(SFrameSystem.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        if(fullscreen) {
            assembleWindow();
        } else {
            assembleFrame();
        }
    }
    
    //PRIVATE METHOD//
    
    /**
     * Initializes the frame for window mode.
     */
    private void assembleFrame() {
        this.frame = new JFrame(title, gd.getDefaultConfiguration());
        this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.frame.setPreferredSize(FRAME_SIZE);
        this.frame.setSize(FRAME_SIZE);
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        this.frame.setLocation((screenSize.width-FRAME_SIZE.width)/2, (screenSize.height-FRAME_SIZE.height)/2);
        this.contentPane = this.frame.getContentPane();
        this.contentPane.setLayout(new TableLayout(CONTENTPANE_CELLSIZE));
    }
    
    /**
     * Initializes the window for fullscreen mode.
     */
    private void assembleWindow() {
        this.window = new Frame(gd.getDefaultConfiguration());
        this.window.setUndecorated(true);
        this.window.setResizable(false);
//        this.window.setAutoRequestFocus(true);
        this.window.setIgnoreRepaint(true);
        
        double[][] cellSize = new double[][]{{TableLayout.FILL},{TableLayout.FILL}};
        this.window.setLayout(new TableLayout(cellSize));
        this.contentPane = new Container();
        this.contentPane.setLayout(new TableLayout(CONTENTPANE_CELLSIZE));
        this.window.setLayout(new TableLayout(cellSize));
        this.window.add(this.contentPane, "0,0,0,0");
    }
    
    
    private DisplayMode getBestDisplayMode(GraphicsDevice device) {
        for (int x = 0; x < BEST_DISPLAY_MODES.length; x++) {
            DisplayMode[] modes = device.getDisplayModes();
            for (int i = 0; i < modes.length; i++) {
                if (modes[i].getWidth() == BEST_DISPLAY_MODES[x].getWidth()
                   && modes[i].getHeight() == BEST_DISPLAY_MODES[x].getHeight()
                   && modes[i].getBitDepth() == BEST_DISPLAY_MODES[x].getBitDepth()
                   ) {
                    return BEST_DISPLAY_MODES[x];
                }
            }
        }
        return null;
    }
    
    /**
     * Creates and returns the Thread object, which will controll the repaint.
     * @return 
     */
    private Thread initRenderThread() {
        return new Thread(new Runnable() {

            int circlePositionX = 0, circlePositionY = 0, fps = 30, renderCounter = 0, renderTime, fpsLimit = 30;
            long startTime, endTime, loopTime = (1000l/fps), minWaitTime = 5;

            @Override
            public void run() {
                Graphics g;
                while(!done) {
                    startTime = System.currentTimeMillis();

                    //Draw
                    if(window.getBufferStrategy()==null) {
                        window.createBufferStrategy(4);
                    }
                    
                    g = window.getBufferStrategy().getDrawGraphics();
                    window.paintComponents(g);
                    bufferStrategy.show();
                    g.dispose();

                    endTime = System.currentTimeMillis();

                    try {

                        renderTime += endTime-startTime;
                        renderCounter++;
                        
                        if(renderCounter == 50) {
                            if((renderTime/renderCounter) == 0) {
                                fps += 1;
                            } else {
                                fps = 1000*renderCounter/renderTime;
                            }
                            if(fps > fpsLimit) {
                                fps = fpsLimit;
                            }
                            loopTime = 1000/fps;
                            renderTime = 0;
                            renderCounter = 0;
                            //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//                            System.out.println("New FPS: "+fps+"\n");
                            //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
                        }
                        
                        if(endTime-startTime < loopTime) {
                            Thread.sleep(loopTime-(endTime-startTime));
                        } else {
                            //propose new loop time ?
                            //wait
                            Thread.sleep(minWaitTime);
                        }
                    } catch (InterruptedException ex) {
                        Logger.getLogger(Window.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        });
    }
    
    //PUBLIC METHODS//
    
    /**
     * Sets the content in the contentPane and optionally saves the last content.
     * @param content 
     */
    public void setContent(Component content, boolean storeLastContent) {
        this.stopRendering();
        this.contentPane.invalidate();
        //Get the first component and store it in lastContent.
        if(storeLastContent && contentPane.getComponentCount() > 0) {
            this.lastContent = this.contentPane.getComponent(0);
        } else {
            this.lastContent = null;
        }
        //Remove all components...
        this.contentPane.removeAll();
        //...and add the new content. The contantPane uses a one cell TableLayout.
        this.contentPane.add(content, "0,0,0,0");
        this.contentPane.validate();
        this.startRendering();
    }
    
    /**
     * Sets the template for the GUI elements.
     * @param templateNbr 
     */
    public void setTemplate(String template) {
        this.template = template;
    }
    
    /**
     * Sets the frame/window visible or invisible
     * @param isVisible 
     */
    public void setVisible() {
        if(fullscreen) {
            //If fullscreen and displayChange are supportet, set the window to fullscreen else initialize the frame.
            if(gd.isFullScreenSupported()) {
                gd.setFullScreenWindow(window);
                if(dm != null) {
                    gd.setDisplayMode(dm);
                }
                window.createBufferStrategy(NBR_BUFFERS);
                bufferStrategy = window.getBufferStrategy();
            } else {
                fullscreen = false;
                assembleFrame();
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        frame.setVisible(true);
                    }
                });
                return;
            }
            
            //Creating a rendering thread for fullscreen mode. All components should ignore repaint.
            this.renderingThread = initRenderThread();
            this.done            = false;
            this.renderingThread.start();
            
        } else {
            SwingUtilities.invokeLater(new Runnable() {

                @Override
                public void run() {
                    frame.setVisible(true);
                }
            });
        }
    }
    
    public void goPageBack() {
        this.setContent(lastContent, true);
    }
    
    public void stopRendering() {
        this.done = true;
    }
    
    public void startRendering() {
        if(this.renderingThread != null) {
            try {
                this.renderingThread.join();
            } catch (InterruptedException ex) {
                Logger.getLogger(SFrameSystem.class.getName()).log(Level.SEVERE, null, ex);
            }
            this.done = false;
            this.renderingThread = initRenderThread();
            this.renderingThread.start();
        } else {
            if(!fullscreen) {
                frame.repaint();
            }
        }
    }
    
    public void toggleFullscreenMode() {
        
    }
    //ACCESSOR METHODS//
    
    /**
     * Returns wheather fullscreen mode is actove or not.
     * @return true if fullscreen mode is activated, false otherwise;
     */
    public boolean isFullscreen() {
        return fullscreen;
    }
}
