package com.kanna;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;

public class DeleteTransactionsOnClickListener implements OnClickListener
{
	@Override
	public void onClick(View view)
	{
		DatabaseHelper dbHelper = new DatabaseHelper(view.getContext());
		dbHelper.deleteAllTransactions();
		Toast.makeText(view.getContext(), "All Transactions have been deleted", Toast.LENGTH_SHORT).show();
	}
}
