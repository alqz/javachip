package com.alibaba.crm.sun.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.crm.sun.domain.UrlItem;
import com.alibaba.crm.sun.persistence.UrlItemMapper;

@Service
public class UrlItemService {

    @Autowired
    private UrlItemMapper urlItemMapper;

    public List<UrlItem> loadAllUrlItem() {
        return urlItemMapper.selectAll();
    }

}
