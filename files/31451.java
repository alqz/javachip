package org.minecms.minecmslink.controllers;

import org.bukkit.plugin.Plugin;
import org.minecms.minecmslink.MineCMSLink;
import org.minecms.minecmslink.TopicEntry;

import de.cubeisland.ApiBukkit.ApiServer.Action;
import de.cubeisland.ApiBukkit.ApiServer.ApiController;
import de.cubeisland.ApiBukkit.ApiServer.ApiRequest;
import de.cubeisland.ApiBukkit.ApiServer.ApiResponse;
import de.cubeisland.ApiBukkit.ApiServer.Controller;

@Controller(name = "topic")
public class TopicController extends ApiController{
	MineCMSLink plugin;
	
	public TopicController(Plugin arg0) {
		super(arg0);
		this.plugin = MineCMSLink.instance;
	}
	
	@Action(name = "new", parameters =
	    {
	        "title",
	        "title_clean",
	        "author"
	    })
	public void newPost(ApiRequest request, ApiResponse response){
		String title = request.params.getString("title");
		String title_clean = request.params.getString("title_clean");
		String author = request.params.getString("author");
		plugin.topicQueue.add(new TopicEntry(title, title_clean, author, false));
	}
	
	@Action(name = "reply", parameters =
	    {
	        "title",
	        "title_clean",
	        "author"
	    })
	public void reply(ApiRequest request, ApiResponse response){
		String title = request.params.getString("title");
		String title_clean = request.params.getString("title_clean");
		String author = request.params.getString("author");
		plugin.topicQueue.add(new TopicEntry(title, title_clean, author, true));
	}
	
}
