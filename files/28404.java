package jCiv.client.gui;

import java.awt.Color;

import jCiv.Civ;
import jCiv.Progress;
import jCiv.client.Art;
import jCiv.client.Game;
import jCiv.map.JCivMap;

/**
 * 
 * Main class to handle the rendering of the game.
 * 
 * @author Tehsmash
 *
 */
public class Screen extends Bitmap {
	
	public Screen(int width, int height) {
		super(width, height);
	}
	
	public void render(Game game) {
		if(game.inMenu()) {
			game.menu.render(this);
		} else {
			switch(game.status) {
                case Game.CONNECTING:
					renderInfo("Connecting...");
                    break;
                case Game.RECEIVINGSERVERINFO:
                    renderInfo("Receiving Server Information...");
                    break;
                case Game.SENDINGCLIENTINFO:
                    renderInfo("Sending Client Information...");
                    break;
                default:
                    renderGame(game);
            }
		}
	}
	
	public void renderInfo(String info) {
		fill(Color.BLACK);
		render(info, 50, 400, Color.WHITE);
	}
	
	public void renderGame(Game game) {
		renderMap(game.map);
		
		render(Art.bottomBar, 0, height-205, 2);
		
		renderCivs(game.civs);
		renderProgress(game.progress);
		renderTradingCards();
		renderCivCards();
	}
		
	public void renderMap(JCivMap map) {
		
	}
	
	public void renderCivs(Civ[] civs) {
		
	}
	
	public void renderProgress(Progress progress) {
		
	}
	
	public void renderTradingCards() {
		
	}
	
	public void renderCivCards() {
		
	}
}
