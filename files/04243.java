package unity.controller.admin.test;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;

import com.google.appengine.repackaged.com.google.common.util.Base64;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

public class TestController extends Controller {

    @Override
    public Navigation run() throws Exception {

        String secretKey = "o9fIiltjKr7Ljd9yM+4M01eOQHepuU7JoeFSCELt";
        String policy_document =
            "{\"expiration\":\"2117-12-01T12:00:00.000Z\",\"conditions\":[{\"acl\":\"public-read\" },{\"bucket\":\"unitygames\"},[\"starts-with\",\"$key\",\"\"],[\"content-length-range\",0,20971520]]}";
        String policy =
            Base64.encode(policy_document.getBytes("UTF-8")).replaceAll(
                "\n",
                "");
        Mac hmac = Mac.getInstance("HmacSHA1");
        hmac.init(new SecretKeySpec(secretKey.getBytes("UTF-8"), "HmacSHA1"));
        String signature =
            Base64.encode(hmac.doFinal(policy.getBytes("UTF-8"))).replaceAll(
                "\n",
                "");

        requestScope("policy",policy);
        requestScope("signature",signature);
        
        
        return forward("test.jsp");
    }
}
