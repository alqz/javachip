package _3_.views;

import java.awt.Color;
import java.awt.Font;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;

import _3_.*;
import _3_.controllers.*;
import _3_.objects.*;


public class view_top_Settings extends extender_view_Top
{
	public Main_Frame					frame;
	
	public controller_top_Settings		controller;
	
	public JLabel						version, ingelogdAlsTextLabel, gebruikersnaamLabel;
	public JButton						logoutButton, terugButton;
	public object_ButtonWithMouseOver	exitButton, minimizeButton;
	
	
	public view_top_Settings( controller_top_Settings controller, Main_Frame frame )
	{
		super( frame, 300, 140 );
		this.controller = controller;
		this.frame = frame;
		
		object_LocalDatabase dbHelper = new object_LocalDatabase( false );
		ArrayList<String> accountInfo = dbHelper.getAccount();
		
		
		version = new JLabel( Main_Frame.APP_NAME+" v"+Main_Frame.VERSION );
		version.setForeground( Color.WHITE );
		version.setBounds( 50, 7, width-50-50, 20 );
		add( version );
		
		
		ingelogdAlsTextLabel = new JLabel( "Ingelogd als: " );
		ingelogdAlsTextLabel.setBounds( 10, 40, 75, 20 );
		ingelogdAlsTextLabel.setForeground( Color.WHITE );
		ingelogdAlsTextLabel.setFont( new Font( "Arial", Font.BOLD, 12 ) );
		add( ingelogdAlsTextLabel );
		
		gebruikersnaamLabel = new JLabel( accountInfo.get(1) );
		gebruikersnaamLabel.setBounds( ingelogdAlsTextLabel.getWidth()+ingelogdAlsTextLabel.getX()+10, 40, width-ingelogdAlsTextLabel.getWidth()-ingelogdAlsTextLabel.getX()-10, 20 );
		gebruikersnaamLabel.setForeground( Color.WHITE );
		gebruikersnaamLabel.setFont( new Font( "Arial", Font.BOLD, 12 ) );
		add( gebruikersnaamLabel );
		
		
		logoutButton = new JButton( new ImageIcon(ImageLoader.load("button_uitloggen.png")) );
		logoutButton.setBounds( (width-200)/2, height-26-26-10-5, 200, 26 );
		logoutButton.addActionListener( controller );
		add( logoutButton );
		
		terugButton = new JButton( new ImageIcon(ImageLoader.load("button_keerterug.png")) );
		terugButton.setBounds( (width-200)/2, height-26-10, 200, 26 );
		terugButton.addActionListener( controller );
		add( terugButton );
		
		
		exitButton = new object_ButtonWithMouseOver( "button_exit.png", "button_exit_mouseover.png" );
		exitButton.setBounds( width-18-5, 5, 15, 15 );
		exitButton.addActionListener( controller );
		add( exitButton );

		minimizeButton = new object_ButtonWithMouseOver( "button_minimize.png", "button_minimize_mouseover.png" );
		minimizeButton.setBounds( width-18-5-16, 5, 15, 15 );
		minimizeButton.addActionListener( controller );
		add( minimizeButton );
	}
	
	
	public controller_top_Settings getController()
	{
		return controller;
	}
}
