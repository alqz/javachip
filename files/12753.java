 /* *************************************
 * Written by: Dustin Farley      
 * CPU: Intel Atom N570 
 *      dual core @ 1.66 Ghz 
 * OS: Windows 7 Home Premium 32 bit 
 * Written in: Notepad 
 * Asks for the
 * number of sides of a 
 * dice and how many of them to 
 * roll. 
 * Date of last revision: 27-Mar-2012 
 *************************************/
package concord.monopoly;

public class Dice {

    private int total;

    /*
     * Rolls 'number' amount of 'sides' sided die and stores the total amount
     * rolled in 'total'.
     */
    public int rollDice(int sides, int number) {
        total = 0;

        while (number > 0) {
            total = total + (int) (Math.random() * sides) + 1;
            number--;
        }
        return total;
    }
}//class
