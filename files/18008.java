package _2_v200;

import javax.swing.JFrame;
import com.sun.awt.AWTUtilities;


public class Frame extends JFrame
{
	public Frame()
	{
		System.setProperty( "sun.awt.noerasebackground", "true" );
		setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
		
		setLocation( 400, 200 );
		setSize( 0, 0 );
		
		setUndecorated( true );
		AWTUtilities.setWindowOpaque( this, false );
		setLayout( null );
		
		
		MenuPanel panel  = new MenuPanel( this );
		add( panel );
		
		
		setVisible( true );
		createBufferStrategy( 4 );
	}
	
	
	public void createBufferStrategy( int buffers )
	{
		boolean succes = false;
		while( !succes )
		{
			try
			{
				super.createBufferStrategy( 4 ); // 4 buffers
				succes = true;
			}
			catch( Exception error )
			{
				succes = false;
			}
		}
	}
	
	
	public static void main( String[] args )
	{
		new Frame();
	}
}
