package com.mud.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

import com.mud.worldmodel.World;

/**
 * @author Jared DiCioccio <br>
 * <br>
 *         The Server ONLY makes and maintains client connections. It has no
 *         access to the World Models. Only the Controller has acc <br>
 * <br>
 * @param p
 *            : the port to bind to
 * @param sg
 *            : the server GUI reference, if the server's started from the GUI
 */
public class Server {

	public static Server server;
	private ServerSocket serverSocket;
	private String host;
	private int port;
	private static int defaultPort = 1555;
	public Controller sc;	
	private static int uniqueID;
	private boolean running;
	private SimpleDateFormat sdf;
	public HashMap<String, ClientThread> clientList;

	private Thread inputThread;

	public Server() {
		this(defaultPort);
	}

	public Server(int p) {
		port = p;
		sdf = new SimpleDateFormat("HH:mm:ss");
		sc = new Controller(this.server);
		clientList = new HashMap<String, ClientThread>();
		inputThread = new Thread(new InputWorker(this));
		inputThread.setDaemon(true);
		inputThread.setPriority(Thread.MIN_PRIORITY);
		inputThread.start();
	}

	/**
	 * @param msg
	 *            A string to print to console
	 */
	private void display(String msg) {
		System.out.println(msg);
	}

	/**
	 * Attempts to remove a ClientThread from the server's client list. First
	 * attempts to remove based on the hashed toString() of the clients socket
	 * If that fails, attempts to remove based on the player name associated
	 * with the socket.
	 * 
	 * @param ct
	 *            - the ClientThread to remove.
	 * @return - TRUE if a client is removed - FALSE if the ClienThread was not
	 *         found in the HashMap
	 */

	public boolean removeClient(ClientThread ct) {
		if (clientList.remove(ct.socket.toString()) == null) {
			if (clientList.remove(ct.player.name) == null) {
				return false;
			} else {
				return true;
			}
		} else {
			return true;
		}
	}

	/**
	 * @param ct
	 *            - the ClientThread to add to the server list
	 * @return - True if the ClientList contains the ClienThread post operation
	 */
	public boolean registerClient(ClientThread ct) {
		this.clientList.put(ct.player.name, ct);
		this.display(ct.player.name + " has joined.");
		return (clientList.containsKey(ct.player.name));
	}

	/**
	 * @param name
	 *            - The name to key the ClientThread with
	 * @param ct
	 *            - the ClientThread
	 * @return - True if the ClientList contains the ClienThread post operation.
	 */
	public boolean registerClient(String name, ClientThread ct) {
		this.clientList.put(name, ct);
		return (clientList.containsKey(name));
	}

	/**
	 * Will dump a list of users to server console
	 * 
	 */
	public void listUsers() {
		display(clientList.size() + " users connected:");
		Iterator<Entry<String, ClientThread>> iterator = clientList.entrySet()
				.iterator();
		while (iterator.hasNext()) {
			display(iterator.next().getKey());
		}
	}

	/**
	 * @param ct
	 *            - the ClientThread of the user to send the user list to
	 */
	public void listUsers(ClientThread ct) {
		msgUser(ct, clientList.size() + " users connected:");
		Iterator<Entry<String, ClientThread>> iterator = clientList.entrySet()
				.iterator();
		while (iterator.hasNext()) {
			msgUser(ct, iterator.next().getKey());
		}
	}

	/**
	 * @param msg
	 *            - sends a String message to all conencted clients
	 */
	public void msgAll(String msg) {
		for (ClientThread ct : server.clientList.values()) {
			ct.sendMessage(msg);
		}
	}

	/**
	 * @param user
	 *            - the user name of the player to send a message to
	 * @param msg
	 *            - the message to send (a String)
	 */
	public void msgUser(String user, String msg) {
		clientList.get(user).sendMessage(msg);
	}

	/**
	 * @param ct
	 *            - the ClientThread of the user to send a message to
	 * @param msg
	 *            - the message to send (a String)
	 */
	public void msgUser(ClientThread ct, String msg) {
		ct.sendMessage(msg);
	}

	/**
	 * @param user
	 *            - the user to remove
	 * @param clientThread
	 *            - the calling thread. If this is null, the server is forcing
	 *            the removal.
	 * @return true if the user is removed from the player list false if
	 *         something went wrong
	 */
	public boolean removeUser(String user, ClientThread clientThread) {
		display("Server attempting to remove " + user);
		ClientThread ct = clientList.get(user);
		if (clientThread == null) {
			ct.close();
		}
		if (clientList.remove(user) != null) {
			listUsers();
			return true;

		} else {
			return false;
		}
	}

	public void start() throws InterruptedException {
		running = true;
		ServerSocket serverSocket = null;
		World gameWorld = new World();
		try {
			display("Starting server...");
			serverSocket = new ServerSocket();
			serverSocket.bind(new InetSocketAddress("localhost", port));

			while (running) {

				display("Waiting for incoming connections...");
				Socket socket = null;
				try {
					socket = serverSocket.accept();
				} catch (IOException e) {
					e.printStackTrace();
				}
				// we create a new thread, and add it to the clientlist as a
				// temporary client keyed by the clientThread's toString()
				// method.
				// if we authenticate the user, we replace that entry with a
				// proper entry <User,ClientThread>
				ClientThread ct = new ClientThread(socket, sc, this);
				clientList.put(socket.toString(), ct);
				ct.start();
				listUsers();
			}
			serverSocket.close();

			Iterator<Entry<String, ClientThread>> iterator = clientList
					.entrySet().iterator();
			while (iterator.hasNext()) {
				ClientThread t = (ClientThread) iterator.next();
				t.close();
			}
		} catch (IOException e) {
			display("Exception starting server" + e);
			e.printStackTrace();
		}
	}

	public void stop() {
		display("Shutting down server");
		running = false;
		System.exit(0);
	}

	// Input class to handle server console input commands
	private class InputWorker implements Runnable {
		private Server server;
		private BufferedReader input;
		private String in;

		public InputWorker(Server _server) {
			this.server = _server;
			input = new BufferedReader(new InputStreamReader(System.in));
			server.display("Console input initialized.");
		}

		public void run() {
			while (server.running) {
				try {
					in = input.readLine();
					if (in.equals("exit")) {
						server.stop();
					} else {
						server.msgAll("[SERVER]: " + in);
					}

				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * @param args
	 *            Port number to run server on
	 * @throws InterruptedException
	 */
	public static void main(String[] args) throws InterruptedException {
		int portNumber = defaultPort;
		server = new Server(portNumber);
		server.start();
	}
}
