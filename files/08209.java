package oxen.commands.user;

import java.util.regex.Pattern;
import oxen.commands.Command;
import oxen.game.Game;
import oxen.game.GameEndException;

/**
 * Implements the QUIT command
 *
 * @author matt
 */
public class QuitCommand extends Command {

    /**
     * The constructor for class QuitCommand
     * @param manager 
     */
    public QuitCommand(Game manager) {
        super(manager, Pattern.compile("^(QUIT|GO AWAY|FOREVER ALONE|SCARPER|LEAVE NOW AND NEVER COME BACK)", Pattern.CASE_INSENSITIVE));
    }

    /**
     * Checks whether the user's instruction can be interpreted as a request to
     * quit the game
     *
     * @param input The user's input
     * @return boolean
     */
    @Override
    public boolean canRespondTo(String input) {
        return getPattern().matcher(input).matches();
    }

    /**
     * Quits the game
     *
     * @param input
     * @throws GameEndException
     */
    @Override
    public void respondTo(String input) throws GameEndException {
        throw new GameEndException();
    }

    /**
     * Gets a short one line description of this command
     *
     * @return String
     */
    @Override
    public String getHelpDescription() {
        return "QUIT - Quits the game.";
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
