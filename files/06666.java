package tk.blackwolf12333.grieflog.data.player;

import java.util.HashMap;

import org.bukkit.ChatColor;

import tk.blackwolf12333.grieflog.PlayerSession;
import tk.blackwolf12333.grieflog.data.BaseData;
import tk.blackwolf12333.grieflog.data.OldVersionException;
import tk.blackwolf12333.grieflog.utils.Events;

public abstract class BasePlayerData extends BaseData {

	Integer gamemode;
	String playerName;
	
	public Integer getGamemode() {
		return gamemode;
	}
	
	public void setGamemode(Integer gamemode) {
		this.gamemode = gamemode;
	}
	
	public String getPlayerName() {
		return playerName;
	}
	
	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}
	
	@Override
	public abstract void rollback();
	public abstract void tpto(PlayerSession who);
	
	@Override
	public BaseData applyColors(HashMap<String, ChatColor> colors) {
		this.time = colors.get("time") + time + ChatColor.WHITE;
		this.event.setEventName(colors.get("event") + event.getEventName() + ChatColor.WHITE);
		this.playerName = colors.get("player") + playerName + ChatColor.WHITE;
		this.worldName = colors.get("world") + worldName + ChatColor.WHITE;
		return this;
	}
	
	@Override
	public boolean isInWorldEditSelectionOf(PlayerSession player) {
		return false;
	}
	
	public static BasePlayerData loadFromString(String line) {
		try {
			if(line.contains(Events.JOIN.getEventName())) {
				return handlePlayerJoinData(line.split(" "));
			} else if(line.contains(Events.QUIT.getEventName())) {
				return handlePlayerQuitData(line.split(" "));
			} else if(line.contains(Events.COMMAND.getEventName())) {
				return handlePlayerCommandData(line.split(" "));
			} else if(line.contains(Events.GAMEMODE.getEventName())) {
				return handlePlayerChangedGamemodeData(line.split(" "));
			} else if(line.contains(Events.WORLDCHANGE.getEventName())) {
				return handlePlayerChangedWorldData(line.split(" "));
			}
		} catch(OldVersionException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	private static PlayerJoinData handlePlayerJoinData(String[] data) throws OldVersionException {
		try {
			String time = data[0] + " " + data[1];
			String playerName = data[3];
			Integer gamemode = Integer.parseInt(data[8]);
			String ipaddress = data[5];
			Integer x = Integer.parseInt(data[9].replace(",", ""));
			Integer y = Integer.parseInt(data[10].replace(",", ""));
			Integer z = Integer.parseInt(data[11].replace(",", ""));
			String world = data[13].trim();
			return new PlayerJoinData(time, playerName, gamemode, world, ipaddress, x, y, z);
		} catch(ArrayIndexOutOfBoundsException e) {
			throw new OldVersionException("Data was not successfully parsed because it came from an outdated file!");
		}
	}
	
	private static PlayerQuitData handlePlayerQuitData(String[] data) throws OldVersionException {
		try {
			String time = data[0] + " " + data[1];
			Integer x = Integer.parseInt(data[7].replace(",", ""));
			Integer y = Integer.parseInt(data[8].replace(",", ""));
			Integer z = Integer.parseInt(data[9].replace(",", ""));
			String world = data[11].trim();
			String playerName = data[3];
			Integer gamemode = Integer.parseInt(data[5]);
			return new PlayerQuitData(time, playerName, gamemode, world, x, y, z);
		} catch(ArrayIndexOutOfBoundsException e) {
			throw new OldVersionException("Data was not successfully parsed because it came from an outdated file!");
		}
	}
	
	private static PlayerCommandData handlePlayerCommandData(String[] data) throws OldVersionException {
		try {
			String time = data[0] + " " + data[1];
			String command = data[8];
			String playerName = data[4];
			String world = data[data.length - 1].trim();
			Integer gamemode = Integer.parseInt(data[6]);
			return new PlayerCommandData(time, playerName, gamemode, world, command);
		} catch(ArrayIndexOutOfBoundsException e) {
			throw new OldVersionException("Data was not successfully parsed because it came from an outdated file!");
		}
	}
	
	private static PlayerChangedGamemodeData handlePlayerChangedGamemodeData(String[] data) throws OldVersionException {
		try {
			String time = data[0] + " " + data[1];
			Integer newGamemode = Integer.parseInt(data[6]);
			String playerName = data[3];
			Integer gamemode = 0; // is not set in the data
			String worldName = data[data.length - 1].trim();
			return new PlayerChangedGamemodeData(time, playerName, gamemode, worldName, newGamemode);
		} catch(ArrayIndexOutOfBoundsException e) {
			throw new OldVersionException("Data was not successfully parsed because it came from an outdated file!");
		}
	}
	
	private static PlayerChangedWorldData handlePlayerChangedWorldData(String[] data) throws OldVersionException {
		try {
			String time = data[0] + " " + data[1];
			String from = data[6].trim();
			String to = data[8].trim();
			String playerName = data[4];
			Integer gamemode = 0; // is not set in the data
			return new PlayerChangedWorldData(time, playerName, gamemode, to, from);
		} catch(ArrayIndexOutOfBoundsException e) {
			throw new OldVersionException("Data was not successfully parsed because it came from an outdated file!");
		}
	}
}
