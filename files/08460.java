package oxen.game.components.actions;

import java.util.ArrayList;

/**
 * The target list stores the targets that an action can be performed on
 * 
 * Provides first() and last() methods to peek at the head and tail of an action
 * target list
 * @author matt
 */
public class TargetList extends ArrayList<ActionTarget> {

    

    public static class TargetDoesNotExistException extends Exception {

        public TargetDoesNotExistException() {
        }
    }
    
    public boolean empty() {
        return size() == 0;
    }
    
    public ActionTarget first() throws TargetDoesNotExistException {
        try {
            return get(0);
        } catch (IndexOutOfBoundsException e) {
            throw new TargetDoesNotExistException();
        }
    }
    
    public ActionTarget last() throws TargetDoesNotExistException {
        try {
            return get(size() - 1);
        } catch (IndexOutOfBoundsException e) {
            throw new TargetDoesNotExistException();
        }
    }
}
