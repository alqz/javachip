package me.cosban.suckchat;

import me.cosban.suckchat.commands.CensorCommand;
import me.cosban.suckchat.commands.ChannelCommand;
import me.cosban.suckchat.commands.EmoteCommand;
import me.cosban.suckchat.commands.IgnoreCommand;
import me.cosban.suckchat.commands.MuteCommand;
import me.cosban.suckchat.commands.PrivateMessageCommand;
import me.cosban.suckchat.commands.SpoofCommand;
import me.cosban.suckchat.commands.SuckChatCommand;
import me.cosban.suckchat.managers.HookManager;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class SuckChat extends JavaPlugin {
	private static SuckAPI api;
	private static HookManager hooks;
	
	public void onEnable() {
		api = new SuckAPI(this);
		hooks = HookManager.getManager(this);
		
		getAPI().getCensor().init();
		getAPI().getPluginManager().registerEvents(getAPI().getListener(), this);
		
		getAPI().getListener().init();
		
		getCommand("mute").setExecutor(new MuteCommand(this));
		getCommand("unmute").setExecutor(new MuteCommand(this));
		getCommand("muteip").setExecutor(new MuteCommand(this));
		getCommand("unmuteip").setExecutor(new MuteCommand(this));
		getCommand("message").setExecutor(new PrivateMessageCommand(this));
		getCommand("reply").setExecutor(new PrivateMessageCommand(this));
		getCommand("emote").setExecutor(new EmoteCommand(this));
		getCommand("spoof").setExecutor(new SpoofCommand(this));
		getCommand("channel").setExecutor(new ChannelCommand(this));
		getCommand("censor").setExecutor(new CensorCommand(this));
		getCommand("suckchat").setExecutor(new SuckChatCommand(this));
		getCommand("ignore").setExecutor(new IgnoreCommand(this));
		
		getServer().getConsoleSender().sendMessage("italics " + ChatColor.ITALIC.getChar());
		getServer().getConsoleSender().sendMessage("Bold " + ChatColor.BOLD.getChar());
		getServer().getConsoleSender().sendMessage("Magic " + ChatColor.MAGIC.getChar());
		
		if(api.getChannels().isEnabled()){
			for(Player p: this.getServer().getOnlinePlayers()){
				api.getPlayers().addPlayer(p);
				api.getChannels().getChannel("main").addEar(api.getPlayers().getPlayer(p));
				api.getChannels().getChannel("main").addMouth(api.getPlayers().getPlayer(p));
				p.sendMessage(ChatColor.DARK_AQUA + "You were forced into the" + ChatColor.WHITE + " main " + ChatColor.DARK_AQUA + "channel.");
			}
		}
		hooks.loadHooks();
		getAPI().getLogger().info("[SuckChat] Plugin Enabled");
	}

	public void onDisable() {
		getAPI().getLogger().info("[SuckChat] Plugin Disabled");
	}

	public static SuckAPI getAPI() {
		return api;
	}
	
	public static HookManager getHooks(){
		return hooks;
	}
}