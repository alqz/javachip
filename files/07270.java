/*******************************************************************************
 * Copyright (c) 2011 BreizhJUG
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as
 *     published by the Free Software Foundation, either version 3 of the
 *     License, or (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 * 
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.breizhjug.breizhcamp;

import java.util.List;

import org.breizhjug.breizhcamp.adapter.SessionListAdapter;
import org.breizhjug.breizhcamp.model.Room;
import org.breizhjug.breizhcamp.model.Session;
import org.breizhjug.breizhcamp.view.TabChildListActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.TextView;

public class RoomActivity extends TabChildListActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.room);
		
		Room room = getIntent().getParcelableExtra("room");
		
		TextView title = (TextView) findViewById(R.id.title);
		title.setText(room.name);
		
		List<Session> sessions = BreizhCampApplication.event.getSessionsByRoom(room);
		
		setListAdapter(new SessionListAdapter(this, sessions));
		
		getListView().setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> adapter, View view, int position, long id) {
				Intent intent = new Intent(RoomActivity.this, SessionActivity.class);
				intent.putExtra("session", (Session) adapter.getItemAtPosition(position));
				startChildActivity("session", intent);
			}
			
		});
		
	}

	
}
