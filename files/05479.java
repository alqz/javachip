package com.demo.ibatis.action;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.convention.annotation.Result;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.car.platform.core.action.BaseAction;
import com.demo.ibatis.dao.impl.TestUserDao;
import com.demo.ibatis.entity.TestUser;
import com.demo.ibatis.service.ITestUserService;
import com.demo.ibatis.service.impl.TestUserService;
import com.opensymphony.xwork2.ActionSupport;

@ParentPackage("demo")
@Namespace(value="/demo/curd")
public class IbatisAction extends BaseAction {
	
	private TestUser testUser;
	
	private List<TestUser> testUserList=new ArrayList<TestUser>();
	
	@Resource
	private ITestUserService testUserService;
	
	
	@Action(value = "queryTestUser", results = {
			@Result(name = LIST, location = "/demo/curd/list.jsp"), 
		   }
    ) 
	public String queryTestUser()
	{
		if(testUser==null)
			testUser=new TestUser();
		pager=testUserService.pagedQuery(testUser, pager);
		pager.setQueryString(String.format("testUser.userName=%s&testUser.email=%s&pager.pageSize=%s",testUser.getUserName()!=null?testUser.getUserName():"",testUser.getEmail()!=null?testUser.getEmail():"",pager.getPageSize()));
		//testUserList=testUserService.queryForList(testUser);
		
		return LIST;
	}
	
	
	@Action(value = "addTestUser", 
			results = {
				@Result(name = LIST, location = "/demo/curd/list.jsp"), 
			}
	)
	public String addTestUser()
	{
		testUserService.insert(testUser);	
		testUserList=testUserService.queryForList(null);
		
		return LIST;
	}
	
	@Action(value = "deleteTestUser", 
			results = {
				@Result(name = LIST, location = "/demo/curd/list.jsp"), 
			}
	)
	public String deleteTestUser()
	{
		testUserService.deleteByPrimarykey(testUser.getId());
		testUserList=testUserService.queryForList(null);
		
		return LIST;
	}
	
	@Action(value = "viewTestUser", 
			results = {
				@Result(name = VIEW, location = "/demo/curd/updateTestUser.jsp"), 
			}
	)
	public String viewTestUser()
	{
		testUser=testUserService.findByPrimarykey(testUser.getId());
		
		return VIEW;
	}
	
	@Action(value = "updateTestUser", 
			results = {
				@Result(name = VIEW, location = "/demo/curd/updateTestUser.jsp"), 
			}
	)
	public String updateTestUser()
	{
		testUserService.update(testUser);
		
		return VIEW;
	}

	public TestUser getTestUser() {
		return testUser;
	}

	public void setTestUser(TestUser testUser) {
		this.testUser = testUser;
	}

	public List<TestUser> getTestUserList() {
		return testUserList;
	}

	public void setTestUserList(List<TestUser> testUserList) {
		this.testUserList = testUserList;
	}
	
	
	
	
}
