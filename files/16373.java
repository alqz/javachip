package dart.network;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

import dart.Consts;
import dart.Consts.*;
import dart.gui.ErrorWindow;

/**
 *
 * @author yuting
 */
//sets the attributes in the packets. Also responsible for sending the packets
//Note: all the methods need to take in the packet!!
public class Handler {

	private Socket _socket;
	private PrintWriter _output;
	private Scanner _input;
	private networkPacket _sendingPacket,_receivedPacket;
	private boolean _gameOver;
	private int _quitIndex=-1;
	private String _quitName = "";
	public Handler(Socket socket) throws IOException
	{
		_socket = socket;
		_output = new PrintWriter(socket.getOutputStream());
		_input = new Scanner(socket.getInputStream());
		_sendingPacket = new networkPacket();//initialize the packet and set the values to default
		_receivedPacket = new networkPacket();
		reset(_sendingPacket);
		reset(_receivedPacket);
		_gameOver = false;
	}


	public void resetPacket(networkPacket packet,int playerIndex, long currentTime,int x,int y, AttrType towerUpgrade, TowerType towerPurchase,
			RunnerType sendRunner)
	{
		synchronized(this){
			packet.setPlayerIndex(playerIndex);
			packet.setCurrentTime(currentTime);
			packet.setX(x);
			packet.setY(y);
			packet.setTowerUpgrade(towerUpgrade);
			packet.setTowerPurchase(towerPurchase);
			packet.setSendRunner(sendRunner);
			
		}
	}

	public void reset(networkPacket packet){
		synchronized(this){
			packet.setCurrentTime(-1);
			packet.setX(-500);
			packet.setY(-500);
			packet.setTowerUpgrade(AttrType.NONE);
			packet.setTowerPurchase(TowerType.NONE);
			packet.setSendRunner(RunnerType.NONE);
			packet.setAttribute1Change(false);
			packet.setAttribute2Change(false);
			packet.setAttribute3Change(false);
		}
	}

	public void setPlayerIndex(int index)
	{
		synchronized(this){
			_receivedPacket.setPlayerIndex(index);
			_sendingPacket.setPlayerIndex(index);
		}
	}
	public int getPlayerIndex()
	{
		return _receivedPacket.getPlayerIndex();
	}
	public void sendRunner(RunnerType sendRunner)
	{
		synchronized(this){
			_sendingPacket.setSendRunner(sendRunner);
		}
	}

	public void upgradeTower(AttrType upgradeTower)
	{
		synchronized(this){
			_sendingPacket.setTowerUpgrade(upgradeTower);
		}
	}

	public void buyTower(TowerType tt)
	{
		synchronized(this){
			_sendingPacket.setTowerPurchase(tt);
		}
	}

	public RunnerType isSendingRunner()
	{
		return _receivedPacket.getSendRunner();
	}

	public TowerType isBuyingTower()
	{
		return _receivedPacket.getTowerPurchase();
	}
	public AttrType isUpgradingTower()
	{
		return _receivedPacket.getTowerUpgrade();
	}

	public String translatePacketToString()
	{
		synchronized(this){
			String message = "";
			message += Integer.toString(_sendingPacket.getPlayerIndex())+"/"+"";
			message += Long.toString(_sendingPacket.getCurrentTime())+"/"+"";
			message += Integer.toString(_sendingPacket.getX())+"/"+"";
			message += Integer.toString(_sendingPacket.getY())+"/"+"";
			message += _sendingPacket.getTowerUpgrade().name() + "/" + "";
			message += _sendingPacket.getTowerPurchase().name()+"/"+"";
			message += _sendingPacket.getSendRunner().name()+"/"+"";
		
			return message;
		}
	}

	public void translateStringToPacket(String packetReceived)
	{
		
		String[] str = packetReceived.split("/");
		Integer playerIndex = Integer.parseInt(str[0]);
		Long currentTime = Long.parseLong(str[1]);
		Integer x = Integer.parseInt(str[2]);
		Integer y = Integer.parseInt(str[3]);
		AttrType towerUpgrade = Consts.AttrType.valueOf(str[4]);
		TowerType towerPurchase = Consts.TowerType.valueOf(str[5]);

		RunnerType sendRunner = Consts.RunnerType.valueOf(str[6]);

		this.resetPacket(_receivedPacket,playerIndex, currentTime, x, y, towerUpgrade, towerPurchase, sendRunner);
		


	}

	//translate the network packet into the string and send it over the connection
	//while sending and resetting the packet, lock the packet
	public void sendPacket()
	{
		synchronized(this){

			String message = translatePacketToString();
		
			_output.println(message);
			_output.flush();
			reset(_sendingPacket);
		}
	}
	
	public void receivePacket()
	{
		String packetReceived = _input.nextLine();
		//if some player drops out in the middle of the game
		if(packetReceived.contains("EXIT GAME"))
		{
			//set the quit player's index
			String[] str = packetReceived.split("/");
			_quitName = str[2];
			_gameOver = true;
			

			
		}
		else{
			//need to check if the package is the right one, when players want to add to the game in the middle of the game
			if((!packetReceived.contains("NEWPLAYER"))&&(!packetReceived.contains("chat:" ))){
				translateStringToPacket(packetReceived);
			}
		}
	}

	public boolean gameOver()
	{
		return _gameOver;
	}
	public void resetGameOver()
	{
		_gameOver = false;
	}
	public int quitPlayer()
	{
		return _quitIndex;
	}
	public String quitPlayerName()
	{
		return _quitName;
	}
	public void resetQuitPlayerName()
	{
		_quitName ="";
	}
	public void setTowerLocation(int findBlockX, int findBlockY) {
		synchronized(this){
			_sendingPacket.setX(findBlockX);
			_sendingPacket.setY(findBlockY);
		}

	}

	public int getTowerLocationX() {
		return _receivedPacket.getX();


	}
	public int getTowerLocationY() {
		return _receivedPacket.getY();


	}


	public void setBuyTower(TowerType tt, int findBlockX, int findBlockY) {
		// TODO Auto-generated method stub
		synchronized(this)
		{
			_sendingPacket.setTowerPurchase(tt);
			_sendingPacket.setX(findBlockX);
			_sendingPacket.setY(findBlockY);

		}
	}
	
	public void setUpgradeTower(AttrType at, int x, int y) {
		synchronized(this)
		{
			_sendingPacket.setTowerUpgrade(at);
			_sendingPacket.setX(x);
			_sendingPacket.setY(y);
		}
	}


	public void setCurrentTime(long currentTimeMillis) {
		// TODO Auto-generated method stub
		synchronized(this)
		{
			_sendingPacket.setCurrentTime(currentTimeMillis);
		}
	}
	
	public long getCurrentTime()
	{
		return _receivedPacket.getCurrentTime();
	}
	
	public void sendExitGameSignal()
	{
		_output.println("EXIT GAME:"+this.getPlayerIndex());
		_output.flush();
		System.out.println("exit sent");
	}
}
