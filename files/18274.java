package org.rsbot.bot.event.impl;

import org.rsbot.script.internal.event.TextPaintListener;
import org.rsbot.script.methods.Game;
import org.rsbot.script.methods.Players;
import org.rsbot.util.StringUtil;

import java.awt.*;

public class TAnimation implements TextPaintListener {
	public int drawLine(final Graphics render, int idx) {
		int animation;
		if (Game.isLoggedIn()) {
			animation = Players.getLocal().getAnimation();
		} else {
			animation = -1;
		}
		StringUtil.drawLine(render, idx++, "Animation " + animation);
		return idx;
	}
}