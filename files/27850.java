package ucdavis.bqleclipse.internal.ui;

import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;

import ucdavis.bqleclipse.models.IQueryResponse;
import ucdavis.bqleclipse.models.IQueryResult;

public class BQLResultLabelProvider extends LabelProvider implements ITableLabelProvider {
	@Override
	public Image getColumnImage(Object element, int columnIndex) {
		return null;
	}

	@Override
	public String getColumnText(Object element, int columnIndex) {
		if (element instanceof IQueryResponse && columnIndex == 0) {
			IQueryResponse response = (IQueryResponse)element;
			return String.format(
					Messages.BQLResultContentProvider_bugssimilartotrace_label,
					response.getQuery().getLabel());
		} else if (element instanceof IQueryResult) {
			IQueryResult result = (IQueryResult)element;
			if (columnIndex == 0) {
				return result.getBugId();
			} else if (columnIndex == 1) {
				return result.getBugTitle();
			} else if (columnIndex == 2) {
				return result.getBugURL();
			}
		} else if (element instanceof MessageRow && columnIndex == 0) {
			return ((MessageRow)element).getMessage();
		}
		
		return ""; //$NON-NLS-1$
	}
}