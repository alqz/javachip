package com.symbol8.moneytracker.domain;

import java.text.SimpleDateFormat;

public class Account {
	
	public static final SimpleDateFormat PRINTABLE_DATE_FORMAT = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
	
	private String id;

	public Account() {
	}

	public Account(String id) {
		this();
		this.id = id;
	}

	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Account other = (Account) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	
	
}
