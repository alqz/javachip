package controllers;

import controllers.deadbolt.Deadbolt;
import controllers.deadbolt.Restrict;
import play.modules.router.Get;
import play.mvc.Controller;
import play.mvc.With;

@Restrict("admin")
@With({Deadbolt.class, Widgets.class})
public class Admin extends Controller {


    @Get("/admin")
    public static void index() {
        render();
    }


}