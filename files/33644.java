package ohtu.citationneeded.ui.commands;

import java.util.HashMap;
import java.util.Set;
import ohtu.citationneeded.storages.CitationStorage;
import ohtu.citationneeded.ui.IO;

public class CommandFactory {

    private final HashMap<String, Command> commands;
    private final CitationStorage citations;
    private final IO io;

    public CommandFactory(CitationStorage citations, IO io) {
        this.citations = citations;
        this.io = io;
        commands = new HashMap<String, Command>();
        commands.put("add", new AddCitation(io, citations));
        commands.put("list", new ListCitations(io, citations));
        commands.put("genbx", new GenerateBibTexCommand(io, citations));
        commands.put("remove", new RemoveCitation(io, citations));
        commands.put("tag", new Tag(io, citations));
    }

    public Command getCommand(String commandStr) {
        return commandStr == null ? null : commands.get(commandStr.toLowerCase());
    }

    public Set<String> getCommandStrings() {
        return commands.keySet();
    }

    @Override
    public String toString() {
        StringBuilder string = new StringBuilder();
        for (String key : commands.keySet()) {
            string.append(key);
            string.append(", ");
        }
        
        string.delete(string.length() - 2, string.length());

        return string.toString();
    }
}
