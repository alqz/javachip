/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sica.controller;

import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import com.sica.dao.ProcessoDependenteDAO;
import com.sica.entity.ProcessoDependente;
import java.util.List;

/**
 *
 * @author Acer
 */
@Resource
public class ProcessoDependenteController {

    private Result result;
    private Validator validator;
    private ProcessoDependenteDAO processoDependenteDAO;

    public ProcessoDependenteController(Result result,ProcessoDependenteDAO processoDependenteDAO,  Validator validator) {
        this.processoDependenteDAO = processoDependenteDAO;
        this.result = result;
        this.validator = validator;

    }

    @Path("/processoDependente/form/{processoDependente.id}")
    public void form(long idDependente) {
        result.include("idDependente", idDependente);
    }

    @Path("/processoDependente/edita/{processoDependente.id}")
    public void edita(ProcessoDependente processoDependente) {

        result.include("action", 1);
        result.include("processoDependente", processoDependenteDAO.findById(processoDependente));
        result.include("dependente", processoDependenteDAO.findDependenteById(processoDependenteDAO.findById(processoDependente).getIdDependente()));
        result.redirectTo("/processoDependente/form/" + processoDependente.getId());
    }

    @Path("/processoDependente/atualizar")
    public void atualizar(final ProcessoDependente processoDependente) {
        // Valida
        /*validator.checking(new Validations() {

            {
                that(processoDependente.getNumeroBoletimOcorrencia() != null, "processoDependente.numeroBoletimOcorrencia", "numeroBoletimOcorrencia.vazio");
            }
        });*/

        // Em caso de erro
        result.include("action");
        result.include("dependente", processoDependente);
        //validator.onErrorUsePageOf(this.getClass()).form(processoDependente.getId());

        // Adiciona
        processoDependenteDAO.edita(processoDependente);

        // Redireciona para a listagem
        //result.redirectTo(this.getClass()).lista();
    }

    @Path("/processoDependente/adiciona")
    public void adiciona(final ProcessoDependente processoDependente) {
       

        // Adiciona
        processoDependenteDAO.adiciona(processoDependente);

        // Redireciona para a listagem
        result.redirectTo(this.getClass()).lista();
    }

    @Path("/processoDependente/deleta/{processoDependente.id}")
    public void deleta(ProcessoDependente processoDependente) {
        processoDependenteDAO.deleta(processoDependente);
        // Redireciona para a listagem
        result.redirectTo(this.getClass()).lista();
    }
     public List<ProcessoDependente> lista(){
       return processoDependenteDAO.listaTodos();
    }
}
