package net.skcomms.dtc.client.view;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import net.skcomms.dtc.client.ApiRequestParameterViewObserver;
import net.skcomms.dtc.client.model.SearchHistory;
import net.skcomms.dtc.shared.ApiMeta;
import net.skcomms.dtc.shared.ApiParameter;
import net.skcomms.dtc.shared.ApiTestRequest;

import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONString;
import com.google.gwt.regexp.shared.MatchResult;
import com.google.gwt.regexp.shared.RegExp;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.data.RecordList;
import com.smartgwt.client.types.ListGridEditEvent;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.form.fields.ComboBoxItem;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.KeyPressEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressHandler;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridEditorContext;
import com.smartgwt.client.widgets.grid.ListGridEditorCustomizer;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;

public class ApiRequestParameterView {

  private int invalidRecordIdx;

  private static final ListGrid requestParameterGrid = new ListGrid();

  private ApiMeta apiMeta;

  private static final RegExp IP_PATTERN = RegExp.compile("[0-9]+.[0-9]+.[0-9]+.[0-9]+");

  private final List<ApiRequestParameterViewObserver> observers = new ArrayList<ApiRequestParameterViewObserver>();

  public ApiRequestParameterView() {
    this.setupGridProperties();
    ApiRequestParameterView.requestParameterGrid.setFields(this.createNameField(),
        this.createValueField());
  }

  public void addObserver(ApiRequestParameterViewObserver observer) {
    this.observers.add(observer);
  }

  private FormItem createComboBoxControl() {
    final ComboBoxItem cbItem = new ComboBoxItem();
    cbItem.addKeyPressHandler(this.createKeyPressHandler());
    cbItem.setType("comboBox");
    List<ApiParameter> ipOptions = this.apiMeta.getIpInfo().getOptions();
    List<String> ipList = new ArrayList<String>();

    for (ApiParameter param : ipOptions) {
      cbItem.setAttribute("key", param.getKey());
      ipList.add(param.getValue());
    }

    cbItem.setValueMap(ipList.toArray(new String[0]));
    return cbItem;
  }

  private FormItem createComboBoxItem(String key) {
    final ComboBoxItem cbItem = new ComboBoxItem();
    cbItem.setType("comboBox");
    cbItem.addKeyPressHandler(this.createKeyPressHandler());
    ApiParameter param = this.apiMeta.getParameter(key);

    LinkedHashMap<String, String> items = new LinkedHashMap<String, String>();
    JSONObject jsonObject = param.getJsonObject();
    for (String k : jsonObject.keySet()) {
      String val = ((JSONString) jsonObject.get(k)).stringValue();
      items.put(k, k + ":" + val);
    }

    cbItem.setValueMap(items);
    return cbItem;
  }

  protected FormItem createFormItem(RequestGridRecord record) {
    return record.getType().equals("text") ? this.createTextItem() : this.createComboBoxItem(record
        .getKey());
  }

  private KeyPressHandler createKeyPressHandler() {
    return new KeyPressHandler() {

      @Override
      public void onKeyPress(KeyPressEvent event) {
        if (event.getKeyName().equals("Enter")) {
          ApiRequestParameterView.requestParameterGrid.saveAllEdits();
          ApiRequestParameterView.this.fireEnterKeyPressed();
        }

      }
    };
  }

  private ListGridEditorCustomizer createListGridEditorCustomizer() {
    return new ListGridEditorCustomizer() {

      @Override
      public FormItem getEditor(ListGridEditorContext context) {
        if (context.getEditField().getName().equals("value")) {
          RequestGridRecord record = (RequestGridRecord) context.getEditedRecord();
          return record.getKey().equals("IP") ? ApiRequestParameterView.this
              .createComboBoxControl() : ApiRequestParameterView.this.createFormItem(record);
        }
        return context.getDefaultProperties();
      }
    };
  }

  private ListGridField createNameField() {
    ListGridField nameField = new ListGridField("key", "Name", 120);
    nameField.setCanEdit(false);
    nameField.setCanFilter(false);
    nameField.setCanSort(false);
    nameField.setCanReorder(false);
    nameField.setCanGroupBy(false);
    nameField.setCanHide(false);

    return nameField;
  }

  private TextItem createTextItem() {
    final TextItem textItem = new TextItem();
    textItem.addKeyPressHandler(new KeyPressHandler() {

      @Override
      public void onKeyPress(KeyPressEvent event) {
        if (event.getKeyName().equals("Enter")) {
          ApiRequestParameterView.requestParameterGrid.saveAllEdits();
          ApiRequestParameterView.this.fireEnterKeyPressed();
        }
      }
    });
    return textItem;
  }

  private ListGridField createValueField() {
    ListGridField valueField = new ListGridField("value", "Value", 180);
    valueField.setCanFilter(false);
    valueField.setCanSort(false);
    valueField.setCanReorder(false);
    valueField.setCanGroupBy(false);
    valueField.setCanHide(false);

    return valueField;
  }

  private void fireEnterKeyPressed() {
    for (ApiRequestParameterViewObserver observer : this.observers) {
      observer.onEnterKeyPressed();
    }
  }

  private void fixInvalidField(ListGridRecord record, String warnMessage) {
    this.invalidRecordIdx = ApiRequestParameterView.requestParameterGrid.getRecordIndex(record);
    SC.warn(warnMessage, new BooleanCallback() {

      @Override
      public void execute(Boolean value) {
        int recordIdx = ApiRequestParameterView.this.invalidRecordIdx;
        ApiRequestParameterView.requestParameterGrid.selectRecord(recordIdx);
      }

    });
  }

  public ApiTestRequest getApiTestRequest() {
    return this.apiMeta.createApiTestRequest(this.getRequestParameters());
  }

  public String getPath() {
    return (this.apiMeta == null) ? null : this.apiMeta.getPath();
  }

  public ListGrid getRequestGrid() {
    return ApiRequestParameterView.requestParameterGrid;
  }

  public Map<String, String> getRequestParameters() {
    Map<String, String> params = new LinkedHashMap<String, String>();
    for (ListGridRecord record : ApiRequestParameterView.requestParameterGrid.getRecords()) {
      params.put(record.getAttribute("key"), this.getSafeRecordValue(record));
    }
    return params;
  }

  private String getSafeRecordValue(ListGridRecord record) {
    String value = null;
    if (record.getAttribute("key").equals("IP")) {
      MatchResult match = ApiRequestParameterView.IP_PATTERN.exec(record.getAttribute("value"));
      if (match != null) {
        value = match.getGroup(0);
      }
    } else {
      value = record.getAttribute("value");
    }
    return value == null ? "" : value;
  }

  public void setApiMeta(ApiMeta apiMeta) {
    this.apiMeta = apiMeta;
  }

  private void setupGridProperties() {
    ApiRequestParameterView.requestParameterGrid.setWidth(300);
    ApiRequestParameterView.requestParameterGrid.setCanEdit(true);
    ApiRequestParameterView.requestParameterGrid.setCanResizeFields(false);
    ApiRequestParameterView.requestParameterGrid.setEditEvent(ListGridEditEvent.CLICK);
    ApiRequestParameterView.requestParameterGrid.setEditByCell(true);
    ApiRequestParameterView.requestParameterGrid.setShowAllRecords(true);
    ApiRequestParameterView.requestParameterGrid.setCanAutoFitFields(false);
    ApiRequestParameterView.requestParameterGrid.setBodyOverflow(Overflow.VISIBLE);
    ApiRequestParameterView.requestParameterGrid.setOverflow(Overflow.VISIBLE);
    ApiRequestParameterView.requestParameterGrid.setLeaveScrollbarGap(false);
    ApiRequestParameterView.requestParameterGrid.setCanCollapseGroup(false);
  }

  private void setupGridRecords() {
    List<ApiParameter> params = this.apiMeta.getParameters();
    List<RequestGridRecord> records = new ArrayList<RequestGridRecord>();

    for (ApiParameter param : params) {
      records.add(new RequestGridRecord(param));
    }
    records.add(new RequestGridRecord("IP", this.apiMeta.getInitIpText()));

    ApiRequestParameterView.requestParameterGrid.setData(records.toArray(new RequestGridRecord[0]));
    ApiRequestParameterView.requestParameterGrid.setEditorCustomizer(this
        .createListGridEditorCustomizer());
  }

  public void update() {
    this.setupGridRecords();
  }

  public void updateRequestParameters(SearchHistory dtcSearchHistory) {
    RecordList recordList = ApiRequestParameterView.requestParameterGrid.getRecordList();
    for (Record record : recordList.toArray()) {
      String newValue = dtcSearchHistory.getRequestParameter(record.getAttribute("key"));
      if (newValue != null) {
        if (record.getAttribute("CND") != null) {
          record.setAttribute("value", "");
        } else {
          record.setAttribute("value", newValue);
        }
      }
    }
    ApiRequestParameterView.requestParameterGrid.setData(recordList);
    ApiRequestParameterView.requestParameterGrid.setEditorCustomizer(this
        .createListGridEditorCustomizer());
    ApiRequestParameterView.requestParameterGrid.redraw();
  }

  private boolean validateRecord(ListGridRecord record) {
    if (record.getAttribute("key").toLowerCase().equals("query")
        && (record.getAttribute("value") == null)) {
      this.fixInvalidField(record, "Invalid Query");
      return false;
    } else if (record.getAttribute("key").equals("IP")) {
      if (ApiRequestParameterView.IP_PATTERN.exec(record.getAttribute("value")).getGroup(0) == null) {
        this.fixInvalidField(record, "Invalid IP value");
        return false;
      }
    }
    return true;
  }

  public boolean validateRequestData() {
    for (ListGridRecord record : ApiRequestParameterView.requestParameterGrid.getRecords()) {
      if (!this.validateRecord(record)) {
        return false;
      }
    }
    return true;
  }

}
