/**
 * Copyright 1999-2009 The Pegadi Team
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.pegadi.articlesearch;

import java.util.Date;
import java.text.SimpleDateFormat;

import org.pegadi.sqlsearch.ComparisonTerm;


/**
 * This class implements a search based on when the article was last saved.
 *
 * @author Eirik Bjorsnos <bjorsnos@underdusken.no>
 * @version $Revision$, $Date$
 */
public class LastSavedTerm extends ComparisonTerm {

    /**
     * The date the article was last saved
     */
    private Date lastSavedDate;

    /**
     * Constructor taking a compartion type and a date as arguments
     *
     * @param comparison    comparison type
     * @param lastSavedDate the date the article was saved on/before/after
     */
    public LastSavedTerm(int comparison, Date lastSavedDate) {
        setComparison(comparison);
        this.lastSavedDate = lastSavedDate;
    }

    /**
     * Returns the WHERE-clause
     *
     * @return WHERE-clause
     */
    public String whereClause() {

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

        String dateString = format.format(lastSavedDate);
        return "Article.lastSaved "
                + getOperator()
                + " "
                + dateString;
    }

    /**
     * Returns the names of the tables involved in this search.
     *
     * @return A String[] containing the involved tables.
     */
    public String[] getTables() {

        String[] ret = new String[1];
        ret[0] = "Article";
        return ret;
    }

}
