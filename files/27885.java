package com.car.platform.util;

import org.apache.commons.io.FileUtils; 
import org.apache.commons.io.filefilter.*; 
import org.apache.commons.logging.Log; 
import org.apache.commons.logging.LogFactory; 

import java.io.*; 

/**     
 * @ 版权所有，翻版必究     
 * 类名称：FileToolkit    
 * 类描述：    
 * 创建人：魏国洲  
 * 创建时间：Aug 5, 2011 2:46:01 PM       
 * 备注：    
 * @version
 */
public final class FileToolkit { 
        private static final Log log = LogFactory.getLog(FileToolkit.class); 


        /** 
         * 删除一个文件或者目录 
         * 
         * @param targetPath 文件或者目录路径 
         * @IOException 当操作发生异常时抛出 
         */ 
        public static void deleteFile(String targetPath) throws IOException { 
                File targetFile = new File(targetPath); 
                if (targetFile.isDirectory()) { 
                        FileUtils.deleteDirectory(targetFile); 
                } else if (targetFile.isFile()) { 
                        targetFile.delete(); 
                } 
        } 

        /** 
         * 移动文件或者目录,移动前后文件完全一样,如果目标文件夹不存在则创建。 
         * 
         * @param resFilePath 源文件路径 
         * @param distFolder    目标文件夹 
         * @IOException 当操作发生异常时抛出 
         */ 
        public static void moveFile(String resFilePath, String distFolder) throws IOException { 
                File resFile = new File(resFilePath); 
                File distFile = new File(distFolder); 
                if (resFile.isDirectory()) 
                { 
                	FileUtils.copyDirectory(resFile,distFile);
                	FileUtils.deleteDirectory(resFile);
                    //FileUtils.moveDirectoryToDirectory(resFile, distFile, true); 
                } else if (resFile.isFile()) { 
                	FileUtils.copyFileToDirectory(resFile, distFile);
                	FileUtils.forceDelete(resFile);
                    //FileUtils.moveFileToDirectory(resFile, distFile, true); 
                } 
        } 



        /** 
         * 读取文件或者目录的大小 
         * 
         * @param distFilePath 目标文件或者文件夹 
         * @return 文件或者目录的大小，如果获取失败，则返回-1 
         */ 
        public static long genFileSize(String distFilePath) { 
                File distFile = new File(distFilePath); 
                if (distFile.isFile()) { 
                        return distFile.length(); 
                } else if (distFile.isDirectory()) { 
                        return FileUtils.sizeOfDirectory(distFile); 
                } 
                return -1L; 
        }

        /** 
         * 判断一个文件是否存在 
         * 
         * @param filePath 文件路径 
         * @return 存在返回true，否则返回false 
         */ 
        public static boolean isExist(String filePath) { 
                return new File(filePath).exists(); 
        }

        /** 
         * 本地某个目录下的文件列表（不递归） 
         * 
         * @param folder ftp上的某个目录 
         * @param suffix 文件的后缀名（比如.mov.xml) 
         * @return 文件名称列表 
         */ 
        public static String[] listFilebySuffix(String folder, String suffix) { 
                IOFileFilter fileFilter1 = new SuffixFileFilter(suffix); 
                IOFileFilter fileFilter2 = new NotFileFilter(DirectoryFileFilter.INSTANCE); 
                FilenameFilter filenameFilter = new AndFileFilter(fileFilter1, fileFilter2); 
                return new File(folder).list(filenameFilter); 
        }

        /** 
         * 将字符串追加到指定文件(当指定的父路径中文件夹不存在时，会最大限度去创建，以保证保存成功！) 
         * @param res  原字符串 
         * @param filePath 文件路径 
         * @return 成功标记 
         */ 
        public static boolean string2File(String res, String filePath) { 
                boolean flag = true; 
                BufferedReader bufferedReader = null; 
                BufferedWriter bufferedWriter = null; 
                try { 
                        File distFile = new File(filePath); 
                        if (!distFile.getParentFile().exists()) 
                        	distFile.getParentFile().mkdirs(); 
                        
                        OutputStreamWriter out = new OutputStreamWriter(new FileOutputStream(filePath),"UTF-8");
                        out.write(res);
                        out.flush();
                        out.close();

                       /* bufferedReader = new BufferedReader(new StringReader(res)); 
                        bufferedWriter = new BufferedWriter(new FileWriter(distFile,true)); 
                        bufferedWriter=new BufferedWriter(new OutputStreamWriter(distFile) );
                        char buf[] = new char[1024];         //字符缓冲区 
                        int len; 
                        while ((len = bufferedReader.read(buf)) != -1) { 
                                bufferedWriter.write(buf, 0, len); 
                        } 
                        bufferedWriter.flush(); 
                        bufferedReader.close(); 
                        bufferedWriter.close(); */
                } catch (IOException e) { 
                        flag = false; 
                        e.printStackTrace(); 
                } 
                return flag; 
        } 
} 

