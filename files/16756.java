package domain;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the registered_on database table.
 * 
 */
@Embeddable
public class RegisteredOnPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="student_id")
	private Integer studentId;

	@Column(name="course_occasion_id")
	private Integer courseOccasionId;

    public RegisteredOnPK() {
    }
	public Integer getStudentId() {
		return this.studentId;
	}
	public void setStudentId(Integer studentId) {
		this.studentId = studentId;
	}
	public Integer getCourseOccasionId() {
		return this.courseOccasionId;
	}
	public void setCourseOccasionId(Integer courseOccasionId) {
		this.courseOccasionId = courseOccasionId;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof RegisteredOnPK)) {
			return false;
		}
		RegisteredOnPK castOther = (RegisteredOnPK)other;
		return 
			this.studentId.equals(castOther.studentId)
			&& this.courseOccasionId.equals(castOther.courseOccasionId);

    }
    
	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.studentId.hashCode();
		hash = hash * prime + this.courseOccasionId.hashCode();
		
		return hash;
    }
}