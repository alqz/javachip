package a.b.c.builtin;

public class BFlatString {
    private String value;
    
    public BFlatString(String value)
    {
	this.value = value;
    }
    
    public static BFlatString init(BFlatString value)
    {
	return value;
    }
    
    public BFlatString concat(BFlatString other)
    {
	return new BFlatString(this.value.concat(other.value));
    }
    
    public BFlatInteger indexOf(BFlatString other)
    {
	return new BFlatInteger(this.value.indexOf(other.value));
    }
    
    public BFlatBoolean endsWith(BFlatString other)
    {
	return new BFlatBoolean(this.value.endsWith(other.value));
    }
    
    public BFlatBoolean equals(BFlatString other)
    {
	return new BFlatBoolean(this.value.equals(other.value));
    }
    
    public BFlatString str()
    {
	return this;
    }
    
    public String getValue()
    {
	return this.value;
    }
    

}
