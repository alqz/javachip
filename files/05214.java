/*******************************************************************************
 * Copyright (c) 2011 BreizhJUG
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as
 *     published by the Free Software Foundation, either version 3 of the
 *     License, or (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 * 
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.breizhjug.breizhcamp.view;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.view.KeyEvent;

public class TabChildListActivity extends ListActivity {

	/**
	 * The primary purpose is to prevent systems before
	 * android.os.Build.VERSION_CODES.ECLAIR from calling their default
	 * KeyEvent.KEYCODE_BACK during onKeyDown.
	 */
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			// preventing default implementation previous to
			// android.os.Build.VERSION_CODES.ECLAIR
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	/**
	 * Overrides the default implementation for KeyEvent.KEYCODE_BACK so that
	 * all systems call onBackPressed().
	 */
	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			onBackPressed();
			return true;
		}
		return super.onKeyUp(keyCode, event);
	}

	/**
	 * If a Child Activity handles KeyEvent.KEYCODE_BACK. Simply override and
	 * add this method.
	 */
	public void onBackPressed() {
		TabGroupActivity parentActivity = getTabGroupActivity();
		parentActivity.onBackPressed();
	}
	
	public void startChildActivity(String id, Intent intent) {
		TabGroupActivity parentActivity = getTabGroupActivity();
		parentActivity.startChildActivity(id, intent);
	}
	
	public TabGroupActivity getTabGroupActivity() {
		Activity parent = getParent();
		while( parent != null && !(parent instanceof TabGroupActivity) ) {
			parent = parent.getParent();
		}
		return (TabGroupActivity) parent;
	}
	
	public Context getRootContext() {
		Activity context = this;
		Activity parent = getParent();
		while( parent != null ) {
			context = parent;
			parent = context.getParent();
		}
		return context;
	}
}
