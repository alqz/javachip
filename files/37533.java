package tw.idv.palatis.danboorugallery.utils;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import android.os.AsyncTask;
import tw.idv.palatis.danboorugallery.defines.D;
import tw.idv.palatis.danboorugallery.model.Post;


public abstract class PageCursor {
	private static boolean synchronous = true;

	private int req_thresh;
	

	public PageCursor() {
		this.req_thresh = 100; // Really, try to set your own default.
	}

	public class PostIterator implements Iterator<Post>, Iterable<Post> {
		private Lock requesting;
		
		// Parent page cursor who uses us
		private PageCursor page_cursor;

		/* I don't really want to invent a new class to constrain the methods used,
		 * so here are the intended methods: addLast(), takeFirst(), size()
		 *
		 * Pretty please do not use anything else, and if you do, add them here.
		 */
		private BlockingDeque<Post> post_queue;

		public class AsyncGetPage extends AsyncTask<Void, Integer, Integer> {
			@Override
			protected Integer doInBackground(Void... voids) {
					Integer added = 0;
					D.Log.v("Requesting on " + page_cursor);
					List<Post> post_list = page_cursor.getNextPage();
					assert post_list != null;
					if (post_list != null) { // FIXME?
						for (Post post : post_list) {
							assert post != null;
							post_queue.addLast(post);
							added++;
							publishProgress(added);
						}
					}
				return added;
			}

			@Override
			protected void onProgressUpdate(Integer... progress) {
				D.Log.v("Progress: " + progress[0]);
			}

			@Override
			protected void onPostExecute(Integer posts_added) {
				D.Log.v("PageCursor$AsyncGetPage::onPostExecute called, added " + posts_added);
				requesting.unlock();
			}
		}

		public PostIterator(PageCursor page_cursor) {
			this.requesting = new ReentrantLock();
			this.page_cursor = page_cursor;
			this.post_queue = new LinkedBlockingDeque<Post>();
			//D.Log.v("Constructor called.");
			if (synchronous) {
				for (Post post : page_cursor.getNextPage()) {
					post_queue.addLast(post);
				}
			} else {
				if (requesting.tryLock()) {
					(new AsyncGetPage()).execute();
				} else {
					D.Log.v("Failed to obtain reqlock");
				}
			}
		}

		public Post next() {
			if ((post_queue.size() < page_cursor.getRequestThreshold()))
			{
				if (synchronous)
				{
					for (Post post : page_cursor.getNextPage())
					{
						post_queue.addLast(post);
					}
				}
				else
				{
					if (requesting.tryLock())
					{
						(new AsyncGetPage()).execute();
					} else {
						D.Log.v("Failed to obtain reqlock");
					}
				}
			}
			
			if (post_queue.size() == 0) {
				requesting.lock();
				requesting.unlock();
			}
				
			Post res;
			try {
				D.Log.v("Popping (blocking call)...");
				res = post_queue.takeLast();
				D.Log.v("Popped post: " + (res == null ? "NULL" : res.toString()));
				return res;
			} catch (InterruptedException e) {
				D.Log.v(e.toString());
				D.Log.wtf(e);
				throw new RuntimeException(e);
			}


		}

		public boolean hasNext() {
			return page_cursor.hasNextPage() || (post_queue.size() > 0);   
		}

		public void remove() {
			throw new RuntimeException("");
		}

		@Override
		public Iterator<Post> iterator() {
			return this;
		}
	}

	public void setRequestThreshold(int post_count) {
		this.req_thresh = post_count;
	}

	public int getRequestThreshold() {
		return this.req_thresh;
	}

	abstract public boolean setPage(int page_num);

	abstract public boolean setTags(String tags);

	abstract public void setPostsPerReq(int limit);

	abstract protected boolean hasNextPage();

	abstract protected List<Post> getNextPage();

	public Iterable<Post> postIterator() {
		return new PostIterator(this);
	}

	abstract public void cancel();

}


