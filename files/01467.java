/*
 *    Copyright 2011-2012 University of Toronto
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.ut.biolab.medsavant.view.pedigree;


/**
 *
 * @author mfiume
 */
public class Pedigree {
    public static final String FIELD_GENDER = "GENDER";
    public static final String FIELD_HOSPITALID = "HOSPITAL";
    public static final String FIELD_MOM = "MOM";
    public static final String FIELD_DAD = "DAD";
    public static final String FIELD_PATIENTID = "PATIENT";
    public static final String FIELD_AFFECTED = "AFFECTED";
}
