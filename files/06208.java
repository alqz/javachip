/**
 * Copyright 1999-2009 The Pegadi Team
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * Event to inform listeners of a change in bricks.
 *
 * @author Håvard Wigtil <havardw at pvv.org>
 * @version $Revision$, $Date$
 */
package org.pegadi.games.tetris;

import java.util.EventObject;

public class BrickEvent extends EventObject {
    Brick current;
    Brick next;

    /**
     * Create a new event.
     */
    public BrickEvent(Object source, Brick current, Brick next) {
        super(source);
        this.current = current;
        this.next    = next;
    }


    public Brick getCurrent() {
        return current;
    }


    public Brick getNext() {
        return next;
    }
}