package ohtu.citationneeded.ui.commands;

import ohtu.citationneeded.citations.ArticleCitation;
import ohtu.citationneeded.storages.CitationStorage;
import ohtu.citationneeded.ui.IO;

class ArticleCitationGenerator extends BaseCitationGenerator {

    private String journal;
    private Integer volume;
    private Integer number;
    private String pages;

    public ArticleCitationGenerator(IO io, CitationStorage citations) {
        super(io, citations);
    }

    @Override
    public void run() {
        super.run();
        journal = getStringField("Journal");
        volume = getIntegerField("Volume");
        number = getIntegerField("Number");
        pages = getStringField("Pages");
        citation = new ArticleCitation(journal, volume, number, pages, authors, title, year, publisher, address);
    }
}
