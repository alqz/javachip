package qse_sepm_ss12_07.gui;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.event.*;
import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;
import java.util.ResourceBundle;
import javax.imageio.ImageIO;
import javax.swing.*;
import net.miginfocom.swing.MigLayout;
import org.apache.log4j.Logger;
import qse_sepm_ss12_07.controller.StockController;
import qse_sepm_ss12_07.domain.Product;
import qse_sepm_ss12_07.domain.ProductCategory;

/**
 *
 */
public class StockView extends JPanel implements IStockView
{

	private static Logger logger = Logger.getLogger(StockView.class);
	private StockController controller;
	private List<ProductCategory> categories;
	private JButton buttonBack;
	private JPanel containerPanel;
	private JPanel panelSearch;
	private StockComponentListener stockComponentListener = new StockComponentListener();
	private ResourceBundle labels = ResourceBundle.getBundle("yasl_strings");
	private int comWidth = 150;
	private int wrapCount = 4;
	private int width = 0;
	private JTextField textFieldSearch;
	private StockViewActionListeners stockActionListener = new StockViewActionListeners();
	private JScrollPane scrollPane;
	private JButton buttonScannBarCode;
	private JLabel labelScan;
	private JButton buttonPrintMissingStockShoppingList;
	private StockFilterMenuView stockFilterMenue;
	private JButton buttonCustomizeSearge;

	public StockView()
	{
		setLayout(new MigLayout("", "", "[][grow, fill]"));
		initialize();
	}

	public void setController(StockController controller)
	{
		this.controller = controller;
	}

	private void layoutCalculate()
	{

		width = this.getWidth();
		containerPanel.removeAll();

		wrapCount = ((int) (width / comWidth)) - 1;

		if (wrapCount <= 0)
		{
			wrapCount = 1;
		}

		containerPanel.setLayout(new MigLayout("wrap " + String.valueOf(wrapCount)));
	}

	private void initialize()
	{
		buttonCustomizeSearge = new JButton(labels.getString("customize_search"));
		buttonCustomizeSearge.addActionListener(stockActionListener);


		stockFilterMenue = new StockFilterMenuView();
		stockFilterMenue.setFilterOnOff(true, true, true);
		
		stockFilterMenue.addActionListener(new StockFilterActionListener());

		textFieldSearch = new JTextField();
		textFieldSearch.addActionListener(stockActionListener);
		textFieldSearch.addKeyListener(stockActionListener);

		textFieldSearch.add(stockFilterMenue);
		textFieldSearch.setComponentPopupMenu(stockFilterMenue);

		containerPanel = new JPanel(new MigLayout("wrap 4"));
		panelSearch = new JPanel(new MigLayout(""));

		addComponentListener(stockComponentListener);

		buttonBack = new JButton(labels.getString("back"));
		buttonBack.addActionListener(stockActionListener);

		buttonScannBarCode = new JButton(labels.getString("scan_Barcode"));
		buttonScannBarCode.addActionListener(stockActionListener);

		try
		{
			buttonCustomizeSearge.setIcon(new ImageIcon((ImageIO.read(new File(this.getClass().getResource("/images/" + labels.getString("image_custom_search")).getFile()))).getScaledInstance(13, 13, Image.SCALE_DEFAULT)));
			buttonBack.setIcon(new ImageIcon((ImageIO.read(new File(this.getClass().getResource("/images/" + labels.getString("arrow_rotate_right")).getFile())))));
			buttonScannBarCode.setIcon(new ImageIcon((ImageIO.read(new File(this.getClass().getResource("/images/" + labels.getString("imageCamera")).getFile()))).getScaledInstance(13, 13, Image.SCALE_DEFAULT)));
		}
		catch (IOException ex)
		{
			logger.error("Not able to read images from resources");
		}

		buttonBack.setVerticalTextPosition(SwingConstants.BOTTOM);
		buttonBack.setHorizontalTextPosition(SwingConstants.CENTER);

		labelScan = new JLabel();

		buttonPrintMissingStockShoppingList = new JButton("Print missing stock...");
		//labelScan.setVisible(true);
		buttonPrintMissingStockShoppingList.addActionListener(stockActionListener);



		panelSearch.add(buttonCustomizeSearge, "top,");
		panelSearch.add(textFieldSearch, "top, growx, push");



		scrollPane = new JScrollPane(containerPanel);
		scrollPane.setBorder(BorderFactory.createEmptyBorder());


		add(panelSearch, "top, push, growx");
		add(buttonScannBarCode, "wrap");
		//add(labelScan, "wrap, right");
		add(scrollPane, "grow");
		updateUI();
		//resize();
	}

	public void showCategories()
	{
		layoutCalculate();

		for (ProductCategory productCategory : controller.getAllCategorys())
		{
			CategoryComponent categoryComponent = new CategoryComponent(productCategory);
			containerPanel.add(categoryComponent, "");
		}
		containerPanel.updateUI();
		updateUI();
		//resize();
	}

	public void showProductsFromCategory(ProductCategory productCategory)
	{

		layoutCalculate();

		containerPanel.add(buttonBack, "center, grow");
		for (Product product : controller.getProductsByCategory(productCategory))
		{
			ProductComponent productComponent = new ProductComponent(product);
			containerPanel.add(productComponent, "");

		}
		containerPanel.updateUI();
		updateUI();
	}

	public void showSingleProducts(List<Product> products)
	{
		layoutCalculate();

		containerPanel.add(buttonBack, "center, grow");
		for (Product product : products)
		{
			ProductComponent productComponent = new ProductComponent(product);
			containerPanel.add(productComponent, "");

		}
		containerPanel.updateUI();
		updateUI();
	}

	public void resize()
	{
		Component[] components = containerPanel.getComponents();

		layoutCalculate();

		for (int i = 0; i < components.length; i++)
		{
			containerPanel.add(components[i], "");
		}
		containerPanel.updateUI();

	}

	public void scanBarCode()
	{
		controller.startStopReadingBarcode();
	}

	public void startScann()
	{
		add(labelScan, "");
		buttonScannBarCode.setText(labels.getString("stop_Scan_Barcode"));
	}

	public void stopScann()
	{
		remove(labelScan);
		buttonScannBarCode.setText(labels.getString("scan_Barcode"));
	}

	public void setWebcamImage(Image image)
	{
		labelScan.setIcon(new ImageIcon(image));
		updateUI();
	}

	private void search()
	{
		controller.searchProducts(stockFilterMenue.getStockFilter());
	}

	public void setBarcode(String barcode)
	{
		stockFilterMenue.setFilterOnOff(false, true, false);
		textFieldSearch.setText(barcode);
		stopScann();
		search();
		updateUI();
	}

	class StockViewActionListeners implements ActionListener, KeyListener
	{

		public void actionPerformed(ActionEvent ae)
		{
			if (ae.getSource() == buttonBack)
			{
				controller.showCategories();
				textFieldSearch.setText("");
			}
			if (ae.getSource() == buttonScannBarCode)
			{
				scanBarCode();
			}
			if (ae.getSource() == buttonCustomizeSearge)
			{
				stockFilterMenue.show(buttonCustomizeSearge, 5, 20);
			}
			if (ae.getSource() == textFieldSearch)
			{
				search();
			}
		}

		public void keyTyped(KeyEvent ke)
		{
		}

		public void keyPressed(KeyEvent ke)
		{
		}

		public void keyReleased(KeyEvent ke)
		{
			if (ke.getSource() == textFieldSearch)
			{
				search();
			}
		}
	}

	class CategoryComponent extends JComponent
	{

		private ProductCategory category;
		private JButton button;
		private JLabel label;
		private Image image;

		public CategoryComponent(ProductCategory category)
		{
			this.category = category;
			setLayout(new MigLayout("", "[grow, center]"));
			buildG();
		}

		private void buildG()
		{
			image = controller.getImage(category);

			image = image.getScaledInstance(50, 50, Image.SCALE_DEFAULT);

			button = new JButton();

			button.addActionListener(new ActionListener()
			{

				public void actionPerformed(ActionEvent ae)
				{
					controller.showProducts(category);
				}
			});

			button.setIcon(new ImageIcon(image, category.getName()));
			//button.setPreferredSize(new Dimension(30,30));

			label = new JLabel(category.getName());
			add(button, "span 2, wrap");
			add(label, "");

		}
	}

	class ProductComponent extends JComponent
	{

		private Product product;
		private JButton buttonPlus;
		private JButton buttonMinus;
		private JLabel labelImage;
		private JLabel labelName;
		private DoubleTextField textfieldValue;
		private Image image;
		private JLabel labelAmount;
		private boolean amountNotZero = false;
		DecimalFormat amountFormat;


		public ProductComponent(Product product)
		{
			this.product = product;
			amountFormat = new DecimalFormat("#.##");
			buildComponent();
		}

		private void buildComponent()
		{
			image = controller.getImage(product);

			image = image.getScaledInstance(50, 50, Image.SCALE_DEFAULT);

			labelAmount = new JLabel();

			setButtonPlusMinus();
			labelImage = new JLabel();
			labelImage.setIcon(new ImageIcon(image, product.getName()));

			refresh();

			textfieldValue = new DoubleTextField(1, 2);
			textfieldValue.setDoubleValue(product.getAmountPerProduct());
			textfieldValue.setMinimumSize(new Dimension(30, 25));
			textfieldValue.setPreferredSize(new Dimension(30, 25));
			labelName = new JLabel(product.getName());

			MigLayout layout = new MigLayout("", "[center]", "[center][center][center]");

			this.setLayout(layout);
			this.add(labelImage, "wrap");
			this.add(labelName, "center, wrap");
			this.add(labelAmount, "center, wrap");
			this.add(buttonPlus, "center, split 3");
			this.add(textfieldValue, "");
			this.add(buttonMinus, "");
			this.updateUI();
		}

		public void refresh()
		{
			this.product = controller.getProductByID(product);
			labelAmount.setText(labels.getString("amount") + String.valueOf(amountFormat.format(product.getStockAmount())) + " " + product.getProductCategory().getUnit().getName());

			if (product.getStockAmount() <= 0)
			{
				amountNotZero = false;
			}
			else
			{
				amountNotZero = true;
			}

			labelImage.setEnabled(amountNotZero);
			buttonMinus.setEnabled(amountNotZero);
		}

		private void setButtonPlusMinus()
		{
			buttonMinus = new JButton();
			buttonPlus = new JButton();

			try
			{
				buttonMinus.setIcon(new ImageIcon(ImageIO.read(new File(this.getClass().getResource("/images/" + labels.getString("imageMinus")).getFile()))));
				buttonPlus.setIcon(new ImageIcon(ImageIO.read(new File(this.getClass().getResource("/images/" + labels.getString("imagePlus")).getFile()))));
			}
			catch (IOException ex)
			{
				logger.error("Not able to Read Plus / Minus Images from resources");
			}

			buttonMinus.addActionListener(new ActionListener()
			{

				public void actionPerformed(ActionEvent ae)
				{
					controller.decreaseProduct(product, textfieldValue.getDoubleValue());
					refresh();
				}
			});

			buttonPlus.addActionListener(new ActionListener()
			{

				public void actionPerformed(ActionEvent ae)
				{
					controller.increaseProduct(product, textfieldValue.getDoubleValue());
					refresh();
				}
			});
		}
	}

	public String getSearchText()
	{
		return textFieldSearch.getText();
	}

	class StockComponentListener implements ComponentListener
	{

		public void componentResized(ComponentEvent ce)
		{
			resize();
		}

		public void componentMoved(ComponentEvent ce)
		{
			//throw new UnsupportedOperationException("Not supported yet.");
		}

		public void componentShown(ComponentEvent ce)
		{
			//throw new UnsupportedOperationException("Not supported yet.");
		}

		public void componentHidden(ComponentEvent ce)
		{
			//throw new UnsupportedOperationException("Not supported yet.");
		}
	}

	public class StockFilterActionListener implements ActionListener
	{

		public void actionPerformed(ActionEvent e)
		{
			stockFilterMenue.updateFilter();
		}
	}
}
