/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package qse_sepm_ss12_07.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.ResourceBundle;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenu;
import javax.swing.JPopupMenu;
import org.apache.log4j.Logger;
import qse_sepm_ss12_07.domain.RecipeFilter;

/**
 *
 * @author sbr
 */
public class RecipeFilterMenu extends JPopupMenu
{
	private static final Logger logger = Logger.getLogger(RecipeFilterMenu.class);	
	private ResourceBundle labels = ResourceBundle.getBundle("yasl_strings");
	
	private RecipeFilter filter;
	private JCheckBoxMenuItem onlyAvailable;
	private JCheckBoxMenuItem searchRecipeName;
	private JCheckBoxMenuItem searchProductName;
	private JCheckBoxMenuItem filterForMood;
	private JCheckBoxMenuItem filterForUserTags;
	
	public RecipeFilterMenu()
	{
		onlyAvailable = new JCheckBoxMenuItem(labels.getString("recipefiltermenu_onlyavailable"));
		onlyAvailable.setActionCommand("onlyavailable");
		searchRecipeName = new JCheckBoxMenuItem(labels.getString("recipefiltermenu_searchrecipename"));
		searchRecipeName.setActionCommand("recipename");
		searchProductName = new JCheckBoxMenuItem(labels.getString("recipefiltermenu_searchproductname"));
		searchProductName.setActionCommand("productname");
		filterForMood = new JCheckBoxMenuItem(labels.getString("recipefiltermenu_filterformood"));
		filterForMood.setActionCommand("mood");
		filterForUserTags = new JCheckBoxMenuItem(labels.getString("recipefiltermenu_filterforusertags"));
		filterForUserTags.setActionCommand("usertags");
		
		add(filterForUserTags);
		add(filterForMood);
		addSeparator();
		add(onlyAvailable);
		addSeparator();
		add(searchRecipeName);
		add(searchProductName);
		
		//this.addFocusListener(new RecipeFilterMenuActionListener());
	}
	
	public void addActionListener(ActionListener listener)
	{
		onlyAvailable.addActionListener(listener);
		searchRecipeName.addActionListener(listener);
		searchProductName.addActionListener(listener);
		filterForMood.addActionListener(listener);
		filterForUserTags.addActionListener(listener);
	}
	
	public void setFilter(RecipeFilter filter)
	{
		logger.trace("setFilter filter="+filter);
		this.filter = filter;
		filterForMood.setSelected(filter.isFilterForCurrentMood());
		onlyAvailable.setSelected(filter.isOnlyWhenAllProductsAvailable());
		searchProductName.setSelected(filter.isSearchForProductName());
		searchRecipeName.setSelected(filter.isSearchForProductListName());
		filterForUserTags.setSelected(filter.isFilterForUserTags());
	}
	
	public RecipeFilter getFilter()
	{
		return filter;
	}
	
	public void updateFilter()
	{
		logger.trace("updateFilter");
		filter.setFilterForCurrentMood(filterForMood.isSelected());
		filter.setOnlyWhenAllProductsAvailable(onlyAvailable.isSelected());
		filter.setSearchForProductName(searchProductName.isSelected());
		filter.setSearchForProductListName(searchRecipeName.isSelected());
		filter.setFilterForUserTags(filterForUserTags.isSelected());
	}
	
}
