/*****************************************************************************
 * 
 * Copyright 2010-2011 LE ROUX Thomas <thomas.leroux.identity@gmail.com>
 * 
 * This file is part of Oria.
 * 
 * Oria is free software: you can redistribute it and/or modify
 * it under the terms of the zlib license. See the COPYING file.
 * 
 *****************************************************************************/

package fr.oria.game.render.info;

import com.badlogic.gdx.Gdx;

public class DeltaInfoNode implements InfoNode {
		
	public String render() {
		return String.valueOf(Gdx.graphics.getDeltaTime());
	}

}
