package qse_sepm_ss12_07.service;

import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import qse_sepm_ss12_07.dao.*;
import qse_sepm_ss12_07.domain.*;

/**
 * abstract base class for product list classes (Recipe and ShoppingList)
 */
public abstract class ProductListService<T extends ProductList, U extends ProductListFilter> implements IProductListService<T, U>
{

	private static final Logger logger = Logger.getLogger(ProductListService.class);
	protected IProductListDao productListDao;
	protected IProductCategoryService productCategoryService;
	protected IProductService productService;
	protected IUnitService unitService;
	protected IProductListProductEntryDao productComponentDao;
	protected IProductListProductCategoryEntryDao productCategoryComponentDao;
	protected ILoginService loginService;
	protected ITaggingService taggingService;

	public void setTaggingService(ITaggingService taggingService)
	{
		this.taggingService = taggingService;
	}

	public void setLoginService(ILoginService loginService)
	{
		this.loginService = loginService;
	}

	public void setUnitService(IUnitService unitService)
	{
		logger.trace("setUnitService");
		this.unitService = unitService;
	}

	public void setProductListDao(IProductListDao dao)
	{
		logger.trace("setProductListDao");

		productListDao = dao;
	}

	public void setProductListProductEntryDao(IProductListProductEntryDao dao)
	{
		productComponentDao = dao;
	}

	public void setProductListProductCategoryEntryDao(IProductListProductCategoryEntryDao dao)
	{
		productCategoryComponentDao = dao;
	}

	public void setProductCategoryService(IProductCategoryService productCategoryService)
	{
		this.productCategoryService = productCategoryService;
	}

	public void setProductService(IProductService productService)
	{
		this.productService = productService;
	}

	public void addProductListEntry(ProductList list, ProductListEntry entry)
	{
		// #13086 - simbra, 27.06.2012: set productlist id when add new productEntry, because otherwise this do not work
		entry.setProductList(list);

		//first check if we already have the entry for that product
		for (ProductListEntry item : list.getProductListEntrys())
		{
			if (item.equals(entry))
			{
				entry = item;
				//if we find an entry just increase the amount
				item.setAmount(item.getAmount() + 1);
				break;
			}
		}

		updateProductListEntry(entry);

		list.addProductListEntry(entry);
	}

	public ProductListEntry updateProductListEntry(ProductListEntry entry)
	{
		try
		{			
			if (entry instanceof ProductListProductCategoryEntry)
			{
				ProductListProductCategoryEntry productCategoryEntry = (ProductListProductCategoryEntry) entry;
				ProductListProductCategoryEntryEntity entity = productCategoryEntry.getEntity();

				if (entity.getId() == -1)
				{
					productCategoryComponentDao.create(entity);
				}
				else
				{
					productCategoryComponentDao.update(entity);
				}
			}

			if (entry instanceof ProductListProductEntry)
			{
				ProductListProductEntry productEntry = (ProductListProductEntry) entry;
				ProductListProductEntryEntity entity = productEntry.getEntity();

				if (entity.getId() == -1)
				{
					productComponentDao.create(entity);
				}
				else
				{
					productComponentDao.update(entity);
				}
			}
		}
		catch (DaoException exception)
		{
			logger.error("could not add product list entry", exception);
		}
		
		return entry;
	}

	public void removeProductListEntry(ProductList list, ProductListEntry entry)
	{
		try
		{
			if (entry == null)
			{
				return;
			}
			
			entry.setAmount(entry.getAmount() - 1);

			if (entry.getAmount() <= 0)
			{
				list.removeProductListEntry(entry);
			}

			if (entry instanceof ProductListProductCategoryEntry)
			{
				ProductListProductCategoryEntryEntity entity = (ProductListProductCategoryEntryEntity) entry.getEntity();
				if (entity.getProductAmount() <= 0)
				{
					productCategoryComponentDao.delete(entity);
				}
				else
				{
					productCategoryComponentDao.update(entity);
				}
			}

			if (entry instanceof ProductListProductEntry)
			{
				ProductListProductEntryEntity entity = (ProductListProductEntryEntity) entry.getEntity();
				if (entity.getProductAmount() <= 0)
				{
					productComponentDao.delete(entity);
				}
				else
				{
					productComponentDao.update(entity);
				}
			}
		}
		catch (DaoException exception)
		{
			logger.error("could not remove product list entry", exception);
		}
	}

	protected ProductListEntity getProductListEntity(int id) throws DaoException
	{
		return productListDao.find(id);
	}

	protected void loadProductList(ProductList productList) throws DaoException, ServiceException
	{
		// read children
		List<ProductListProductCategoryEntryEntity> categoryComponentEntities = productCategoryComponentDao.findAllByProductList(productList.getEntity().getId());
		List<ProductListProductEntryEntity> productComponentEntities = productComponentDao.findAllByProductList(productList.getEntity().getId());

		// create and add children's business object
		productList.addProductListEntries(loadProductCategories(categoryComponentEntities));
		productList.addProductListEntries(loadProducts(productComponentEntities));
	}

	protected void saveProductList(ProductList productList) throws DaoException
	{
		if (productList.getEntity().getId() == -1)
		{
			productListDao.create(productList.getEntity());
		}
		else
		{
			productListDao.update(productList.getEntity());
		}

		for (ProductListEntry entry : productList.getProductListEntrys())
		{
			entry.getEntity().setProductListId(productList.getEntity().getId());

			if (entry instanceof ProductListProductCategoryEntry)
			{
				saveProductListProductCategoryEntry((ProductListProductCategoryEntry) entry);
			}
			if (entry instanceof ProductListProductEntry)
			{
				saveProductListProductEntry((ProductListProductEntry) entry);
			}
		}
	}

	protected void deleteProductList(ProductList productList) throws DaoException
	{
		productCategoryComponentDao.deleteByProductList(productList.getEntity());
		productComponentDao.deleteByProductList(productList.getEntity());
		productListDao.delete(productList.getEntity());
	}

	protected void saveProductListProductCategoryEntry(ProductListProductCategoryEntry entry) throws DaoException
	{
		if (entry.getEntity().getId() == -1)
		{
			productCategoryComponentDao.create(entry.getEntity());
		}
		else
		{
			productCategoryComponentDao.update(entry.getEntity());
		}
	}

	protected void saveProductListProductEntry(ProductListProductEntry entry) throws DaoException
	{
		if (entry.getEntity().getId() == -1)
		{
			productComponentDao.create(entry.getEntity());
		}
		else
		{
			productComponentDao.update(entry.getEntity());
		}
	}

	protected List<ProductListProductCategoryEntry> loadProductCategories(List<ProductListProductCategoryEntryEntity> categoryComponentEntities) throws ServiceException
	{
		List<ProductListProductCategoryEntry> list = new ArrayList<ProductListProductCategoryEntry>();

		for (ProductListProductCategoryEntryEntity entity : categoryComponentEntities)
		{
			ProductListProductCategoryEntry entry = new ProductListProductCategoryEntry(entity);
			try
			{
				entry.setProductCategory(productCategoryService.getProductCategoryByProductListEntry(entry));
			}
			catch(ServiceException se)
			{
				throw new ServiceException("Can not load category for productlist.", se);
			}
			list.add(entry);
		}
		return list;
	}

	protected List<ProductListProductEntry> loadProducts(List<ProductListProductEntryEntity> productComponentEntities) throws ServiceException
	{
		List<ProductListProductEntry> list = new ArrayList<ProductListProductEntry>();
		for (ProductListProductEntryEntity entity : productComponentEntities)
		{
			ProductListProductEntry entry = new ProductListProductEntry(entity);
			entry.setProduct(productService.getProductByProductListEntry(entry));
			list.add(entry);
		}
		return list;
	}

	protected String getUnitString(ProductListEntry entry) throws ServiceException
	{
		String unitString = "";
		if (entry instanceof ProductListProductCategoryEntry)
		{
			ProductListProductCategoryEntry categoryEntry = (ProductListProductCategoryEntry) entry;
			if (categoryEntry.getProductCategory().getUnit() == null)
			{
				categoryEntry.getProductCategory().setUnit(unitService.getUnitById(categoryEntry.getProductCategory().getEntity().getUnitId()));
				//logger.trace("getUnitString: id=" +  categoryEntry.getEntity().ge );
			}
			unitString = categoryEntry.getProductCategory().getUnit().getName();

		}
		if (entry instanceof ProductListProductEntry)
		{
			ProductListProductEntry productEntry = (ProductListProductEntry) entry;
			if (productEntry.getProduct().getProductCategory() == null)
			{
				productEntry.getProduct().setProductCategory(productCategoryService.getProductCategoryById(productEntry.getProduct().getEntity().getProductCategoryId()));
			}
			if (productEntry.getProduct().getProductCategory().getUnit() == null)
			{
				productEntry.getProduct().getProductCategory().setUnit(unitService.getUnitById(productEntry.getProduct().getProductCategory().getEntity().getUnitId()));

			}
			unitString = productEntry.getProduct().getProductCategory().getUnit().getName();
		}
		return unitString;
	}
	//protected List<ProductList>
}
