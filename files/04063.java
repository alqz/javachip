package tw.idv.palatis.danboorugallery.utils;

/*
 * This file is part of Danbooru Gallery
 *
 * Copyright 2011
 *   - Victor Tseng <palatis@gmail.com>
 *
 * Danbooru Gallery is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Danbooru Gallery is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Danbooru Gallery.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.util.ArrayList;
import java.util.List;

import tw.idv.palatis.danboorugallery.defines.D;
import tw.idv.palatis.danboorugallery.model.Post;
import tw.idv.palatis.danboorugallery.siteapi.SiteAPI;


public class LazyPostFetcher implements ILazyPostFetcher
{
	private PageCursor page_cursor	= null;

	public LazyPostFetcher() { }
	
	public LazyPostFetcher(PageCursor pc)
	{
		setPageCursor(pc);
	}

	public void setPageCursor(PageCursor pc)
	{
		page_cursor = pc;
	}
	
	public boolean setPage(int page)
	{
		page = (page < 1) ? 1 : page;
		page_cursor.setPage(page);
		return false; // used to be curPage == newPage; FIXME
	}

	public boolean setTags(String tags)
	{
		page_cursor.setTags(tags);
		return false; // used to be curTags == newTags; FIXME
	}

	public boolean setPageLimit(int limit)
	{
		page_cursor.setPostsPerReq(limit);
		return false;  // used to be curLimit == newLimit; FIXME
	}

	public boolean setRating(String rating)
	{
		return false; // FIXME
		/*
		boolean result = enclosure.rating.equals( rating );
		enclosure.rating = rating;
		if ( !result)
			reached_end = false;
		return !result;*/
	}

	public void fetchNextPage(LazyImageAdapter adapter)
	{
		D.Log.v("Getting a page.....");
		if (page_cursor == null) {
			//SiteAPI.readPreference(null);
			page_cursor = SiteAPI.getInstance().getPageCursor();
		}
		int i = 0;
		for (Post post : page_cursor.postIterator()) {
			if (post != null)
				adapter.addPost(post);
			i++;
			if (20 < i)
				break;
		}
		adapter.notifyDataSetChanged();
		D.Log.v("We now have " + adapter.getCount() + " items on-screen.");
	}

	public void cancel()
	{/*
		if (fetcher != null)
		{
			fetcher.cancel( true );
			fetcher = null;
		}*/
	}

	public boolean hasMorePost()
	{
		return true; // FIXME
	}
/*
	private static class AsyncPostFetcher
		extends AsyncTask <PageCursor, Integer, Integer>
	{
		LazyImageAdapter	adapter;
		LazyPostFetcher		fetcher;

		AsyncPostFetcher(LazyImageAdapter a, LazyPostFetcher f)
		{
			adapter = a;
			fetcher = f;
		}

		@Override
		protected Integer doInBackground(PageCursor... params)
		{
			int fetched_posts_count = 0;
			int skipped_posts_count = 0;

			for (PageCursor enclosure : params)
			{
				while (fetched_posts_count < enclosure.limit)
				{
					if (isCancelled())
						break;

					SiteAPI site_api = SiteAPI.getInstance();
					
					if (site_api == null)
						break;

					List <Post> posts = site_api.fetchPostsIndex( enclosure.page, enclosure.tags, enclosure.limit );
					if (posts == null)
						continue;
					List <Post> filtered = new ArrayList < Post >( posts.size() );
					for (Post post : posts)
					{
						if (enclosure.rating.contains( post.rating ))
							filtered.add( post );
						else
							++skipped_posts_count;

						if (isCancelled())
							break;
					}

					if (isCancelled())
						break;

					adapter.addPosts( filtered );
					publishProgress();
					fetched_posts_count += filtered.size();

					D.Log.d( "AsyncPostFetcher::doInBackground(): fetched + skipped / total: %d + %d / %d", fetched_posts_count, skipped_posts_count, fetched_posts_count + skipped_posts_count );
				}
			}
			
			return fetched_posts_count;
		}

		@Override
		protected void onProgressUpdate(Integer... values)
		{
			adapter.notifyDataSetChanged();
		}

		@Override
		protected void onCancelled()
		{
			fetcher.fetcher = null;
		}

		@Override
		protected void onPostExecute(Integer result)
		{
			fetcher.fetcher = null;
			adapter.notifyDataSetChanged();
		}
	}*/
}