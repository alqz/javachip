package me.cosban.suckchat.commands;

import me.cosban.suckchat.SuckChat;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class IgnoreCommand implements CommandExecutor {

	public IgnoreCommand(SuckChat instance) {}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (command.getName().equalsIgnoreCase("ignore")) {
			if(args.length < 1){
				sender.sendMessage(ChatColor.RED + "Fine, if you don't want to ignore anyone... just... LEAVE");
				return false;
			}

			for(String arg: args){
				if(SuckChat.getAPI().getPlayers().getPlayer((Player)sender).getIgnored().contains(SuckChat.getAPI().getPlayers().getPlayer(arg))){
					SuckChat.getAPI().getPlayers().getPlayer((Player)sender).rmIgnored(SuckChat.getAPI().getPlayers().getPlayer(arg));
					sender.sendMessage(arg + ChatColor.DARK_GRAY + " is no longer ignored!");
				} else {
					SuckChat.getAPI().getPlayers().getPlayer((Player)sender).addIgnored(SuckChat.getAPI().getPlayers().getPlayer(arg));
					sender.sendMessage(arg + ChatColor.DARK_GRAY + " is now ignored!");
				}
			}
		} return false;
	}
}
