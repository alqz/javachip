package com.snc.automation.autobot.command;

import com.snc.automation.autobot.AutoBot;
import com.snc.automation.autobot.Command;

public final class LoadcommandCommand extends Command {
	public static final CommandMeta META = new CommandMeta(
		"loadcommand",
		"Initializes and enables a command.",
		"",
		LoadcommandCommand.class
	);

	public LoadcommandCommand(AutoBot bot, String channel, String sender, String login, String hostname, String message) {
		super(bot, channel, sender, login, hostname, message);
	}	
	
	@Override
	public void run() {
		String commandName = getArgument(mMessage);
		mBot.loadCommand(commandName);
		StringBuffer output = new StringBuffer(mSender).append(": Command `" + commandName + "` loaded.");
		mBot.sendMessage(mChannel, output.toString());
	}
}
