/**
 * Copyright 1999-2009 The Pegadi Team
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.pegadi.storysketch.views;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Font;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import org.jgraph.JGraph;
import org.jgraph.graph.CellView;
import org.jgraph.graph.CellViewRenderer;

import org.pegadi.storysketch.cells.Person;
import org.pegadi.storysketch.cells.PersonCell;


// Define the Renderer for an PersonView
public class PersonRenderer implements CellViewRenderer {

    JPanel component = new JPanel();

    JLabel label = new JLabel("Navn");

    Face face;

    public PersonRenderer() {
        component.setOpaque(false);
        label.setHorizontalAlignment(SwingConstants.CENTER);
        label.setVerticalAlignment(SwingConstants.TOP);
        label.setFont(new Font("SansSerif", Font.PLAIN,10));

        face  = new Face();

        component.setLayout(new BorderLayout());
        component.add(face, BorderLayout.CENTER);
        component.add(label, BorderLayout.SOUTH);
    }

    public Component getRendererComponent(JGraph graph,
                                          CellView view,
                                          boolean sel,
                                          boolean focus,
                                              boolean preview) {

        PersonCell pc = (PersonCell) view.getCell();
        Person person = (Person) pc.getUserObject();


        label.setText("<html><center>" + person.getName() +"</center></html>");

        face.setMood(person.getMood());
        face.setAnonymous(person.isAnonymous());

        return component;
    }

}

