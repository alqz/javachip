package ohtu.citationneeded.citations;

import java.util.LinkedList;
import java.util.List;

public class ArticleCitation extends Citation {

    private String journal;
    private Integer volume;
    private Integer number;
    private String pages;
    public static final String citationType = "Article";

    public ArticleCitation(String journal, Integer volume, Integer number, String pages, List<Author> authors, String title, Integer year, String publisher, String address) {
        super(authors, title, year, publisher, address, citationType);
        this.journal = journal;
        this.volume = volume;
        this.number = number;
        this.pages = pages;
    }

    public String getJournal() {
        return journal;
    }

    public Integer getNumber() {
        return number;
    }

    public String getPages() {
        return pages;
    }

    public Integer getVolume() {
        return volume;
    }

    @Override
    public String toString() {
        StringBuilder string = new StringBuilder();

        string.append(super.toString());
        if (this.journal != null) string.append("Journal: ").append(this.journal).append("\n");
        if (this.volume != null) string.append("Volume: ").append(this.volume).append("\n");
        if (this.number != null) string.append("Number: ").append(this.number).append("\n");
        if (this.pages != null) string.append("Pages: ").append(this.pages).append("\n");

        return string.toString();
    }
}
