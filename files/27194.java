package net.skcomms.dtc.server.util;

import java.util.regex.Matcher;

import org.junit.Assert;
import org.junit.Test;

public class DtcHelperTest {

  @Test
  public void testEscapeHtml() {
    String html = "<div>test</div>";
    Assert.assertEquals("&lt;div&gt;test&lt;/div&gt;", DtcHelper.escapeHtml(html));
  }

  @Test
  public void testUrl() {
    Matcher m = DtcHelper.urlPattern
        .matcher("http://img.nate.com/kwshop/ad_img/nice/2012/09/E4/N20120920-002276.gif?1209201611");
    Assert.assertTrue(m.matches());
  }

  @Test
  public void testUrlAnchor() {
    String html1 = "http://www.naver.com";
    String html2 = "http://jump.emforce.co.kr/jumper52.jsp?1=1$$$?EKAMS=daum.52.600.119.1244014070018.157183$$$http://affiliate.gmarket.co.kr/?com=emforce&media=nate&ch=code31&url=http://search.gmarket.co.kr/search.aspx?keyword=%B2%C9%B9%E8%B4%DE&jaehuid=200003977";
    String html3 = "http://img.nate.com/kwshop/ad_img/nice/2012/09/E4/N20120920-002276.gif?1209201611";
    Assert.assertEquals(
        "<a href='http://www.naver.com' target='_blank'> http://www.naver.com </a>",
        DtcHelper.setAnchorToUrlValue(html1));
    Assert
        .assertEquals(
            "<a href='http://jump.emforce.co.kr/jumper52.jsp?1=1$$$?EKAMS=daum.52.600.119.1244014070018.157183$$$http://affiliate.gmarket.co.kr/?com=emforce&media=nate&ch=code31&url=http://search.gmarket.co.kr/search.aspx?keyword=%B2%C9%B9%E8%B4%DE&jaehuid=200003977' target='_blank'> http://jump.emforce.co.kr/jumper52.jsp?1=1$$$?EKAMS=daum.52.600.119.1244014070018.157183$$$http://affiliate.gmarket.co.kr/?com=emforce&media=nate&ch=code31&url=http://search.gmarket.co.kr/search.aspx?keyword=%B2%C9%B9%E8%B4%DE&jaehuid=200003977 </a>",
            DtcHelper.setAnchorToUrlValue(html2));
  }

}
