package webservice.jaxws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "annuleerBoeking", propOrder = { "id" })
public class AnnuleerBoeking {
    protected int id;

    public int getId() {
        return id;
    }

    public void setId(int value) {
        this.id = value;
    }
}