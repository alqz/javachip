package org.pegadi.disposal;

import org.pegadi.model.ArticleStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableModel;
import javax.swing.text.BadLocationException;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import java.awt.*;
import java.util.List;


public class DisposalCellRenderer implements TableCellRenderer{

    TableCellRenderer wrappedRenderer;
    PageTableModel model;


    private final Logger log = LoggerFactory.getLogger(getClass());

    public DisposalCellRenderer(TableCellRenderer wrappedRenderer, TableModel model) {
        this.wrappedRenderer = wrappedRenderer;
        this.model = (PageTableModel)model;
    }
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        Component component = wrappedRenderer.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);


        if(column == 2 || column == 3 || column == 4 || column == 5) {
            JTextArea textArea = new JTextArea((String) value);
            textArea.setBackground(component.getBackground());
            textArea.setForeground(component.getForeground());
            textArea.setFont(component.getFont());
            textArea.setLineWrap(true);
            textArea.setWrapStyleWord(true);
            textArea.setSize(table.getColumnModel().getColumn(column).getWidth(), 10000);
            textArea.setSize(textArea.getWidth(), textArea.getPreferredSize().height);

            if (textArea.getHeight() > table.getRowHeight(row)) {
                table.setRowHeight(row, textArea.getHeight());
            }
            textArea.setMargin(new Insets(3, 3, 0, 0));
            component = textArea;
        } else if(column == 6) {
            List<ArticleStatus> statuses = (List<ArticleStatus>) value;

            JTextPane pane = new JTextPane();
            pane.setBackground(component.getBackground());
            pane.setForeground(component.getForeground());
            pane.setFont(component.getFont());


            StyledDocument doc = pane.getStyledDocument();

            Style style = pane.addStyle("Style", null);

            try {
                for(int i = 0; i < statuses.size(); i++) {
                    if(i > 0) {
                        StyleConstants.setBackground(style, component.getBackground());
                        doc.insertString(doc.getLength(), " \n \n", style);
                    }
                    ArticleStatus status = statuses.get(i);
                    if(status.getColor().equals(Color.white)) {
                        StyleConstants.setBackground(style, pane.getBackground());
                    } else {
                        StyleConstants.setBackground(style, status.getColor());
                    }

                    String s = status.getName() + " (" + model.getPage(row).getArticles().get(i).getCurrentNumberOfCharacters() + " tegn)";
                    doc.insertString(doc.getLength(), s, style);

                }
            } catch (BadLocationException e) {
                log.error("error - something went wrong when showing a styled cell", e);
            }

            pane.setSize(table.getColumnModel().getColumn(column).getWidth(), 10000);
            pane.setSize(pane.getWidth(), pane.getPreferredSize().height);

            if(pane.getHeight() > table.getRowHeight(row)) {
                table.setRowHeight(row, pane.getHeight());
            }

            pane.setMargin(new Insets(3, 3, 0, 0));
            component = pane;
        }

        // If it's the page number, center it and make it bold
        if(column == 0) {
            ((JLabel)component).setHorizontalAlignment(JLabel.CENTER);
            component.setFont(component.getFont().deriveFont(component.getFont().getStyle() ^ Font.BOLD));
        }

        // Here I can check for stuff and paint the row to match it (maybe colour codes etc.)
        if(model.getPage(row).isAdOnPage()) {
            if(!isSelected)
                component.setBackground(Color.lightGray);
        } else {
            if(!isSelected)
                component.setBackground(null);
        }

        return component;
    }
}
