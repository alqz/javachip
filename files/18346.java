package com.wtf.andropet;


import android.app.Application;
import android.content.Context;
import android.util.DisplayMetrics;
import android.view.WindowManager;

public class AndropetApplication extends Application {
	/*** Andropet States ***/
	public static enum GameState {
		GAMESTATE_MAIN
	}
	
	public void onCreate() {
		super.onCreate();
		
		initGlobalVar();
	}

	private void initGlobalVar() {
		Global.applicationContext = this;
		
		DisplayMetrics metrics = new DisplayMetrics();
		((WindowManager)this.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getMetrics(metrics);
		
		Global.density = (int) metrics.density;
		Global.screenWidth = metrics.widthPixels;
		Global.screenHeight = metrics.heightPixels;
		
	}
}
