package qse_sepm_ss12_07.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import org.apache.log4j.Logger;
import qse_sepm_ss12_07.dao.DaoException;
import qse_sepm_ss12_07.dao.ISupplierDao;
import qse_sepm_ss12_07.dao.SupplierEntity;
import qse_sepm_ss12_07.domain.Supplier;

public class SupplierService implements ISupplierService
{

	private ISupplierDao dao;
	private Logger logger = Logger.getLogger(SupplierService.class);

	public void setSupplierDao(ISupplierDao dao)
	{
		this.dao = dao;
	}

	public void saveSupplier(Supplier supplier) throws ServiceException
	{
		try
		{
			if (supplier.getEntity().getId() != -1)
			{
				dao.update(supplier.getEntity());
			}
			else
			{
				dao.create(supplier.getEntity());
			}
		}
		catch (DaoException de)
		{
			logger.error("saveSupplier", de);
			throw new ServiceException(de);
		}
	}
	
	public void deleteSupplier(Supplier supplier) throws ServiceException
	{
		try
		{
			this.dao.delete(supplier.getEntity());
		} 
		catch (DaoException ex)
		{
			logger.error("saveSupplier", ex);
			throw new ServiceException(ex);
		}
	}
	
	public Supplier newSupplier()
	{
		Supplier supplier = new Supplier();
		supplier.setName("newSupplier");
		return supplier;
	}

	public List<Supplier> getAllSuppliers() throws ServiceException
	{
		try
		{
			List<SupplierEntity> list = dao.findAll();

			List<Supplier> suppliers = new ArrayList<Supplier>();

			for (SupplierEntity sup : list)
			{
				suppliers.add(new Supplier(sup));
			}

			return suppliers;
		}
		catch (DaoException de)
		{
			logger.error("getAllSuppliers", de);
			throw new ServiceException(de);
		}
	}

	public Supplier getSupplierById(int id) throws ServiceException
	{
		try
		{
			return new Supplier(dao.find(id));
		}
		catch (DaoException de)
		{
			logger.error("getSupplierById", de);
			throw new ServiceException(de);
		}
	}
}
