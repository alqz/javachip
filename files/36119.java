package de.baralc.springspielereien.featureflipper.flipper.decision;

import org.springframework.stereotype.Service;

@Service
public class SystemPropertyBasedDecisionMaker implements DecisionMaker {

  public static final String USE_MOCKS = "USE_MOCKS";

  public Decision makeDecision() {
    String useMocks = System.getProperty(USE_MOCKS);
    return useMocks != null && useMocks.equals("ja") ? Decision.MOCK : Decision.PRODUCTION;
  }
}
