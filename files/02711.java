package qse_sepm_ss12_07.gui;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.ResourceBundle;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import net.miginfocom.swing.MigLayout;
import org.apache.log4j.Logger;
import qse_sepm_ss12_07.controller.SelectImageController;

/**
 *
 */
public class SelectImageView extends JPanel implements ISelectImageView
{
	private static final Logger logger = Logger.getLogger(SelectImageController.class);
	private SelectImageController controller;
	private JButton buttonSelect;
	private JButton buttonBack;
	private JButton buttonSavePicasaImage;
	private JButton buttonSearchPicasaImage;
	private JPanel panelLocalImages;
	private JPanel panelPicasaImages;
	private ResourceBundle labels = ResourceBundle.getBundle("yasl_strings");
	private SelectImageActionListener selectImageActionListene = new SelectImageActionListener();
	private ImageShowComponent selectedLocalImageShowComponent;
	private ImageShowComponent selectedPicasaImageShowComponent;
	private NewMouseListener newMouseListener = new NewMouseListener();
	private JScrollPane scrollpaneLocal;
	private JScrollPane scrollpaneRemote;
	private String url;
	private JTextField textFieldAlbumId;
	private JLabel labelTextAlbumId;
	private JTextField textFieldUserId;
	private JLabel labelTextUserId;
	private JLabel labelTextLocalImages;
	private JLabel labelTextPicasaImages;

	public SelectImageView()
	{
		setLayout(new MigLayout("fill"));
		//super(new MigLayout("fill", "[grow,fill]"));
		buildGUI();
	}

	public void buildGUI()
	{
		labelTextAlbumId = new JLabel(labels.getString("select_image_album_id"));
		labelTextUserId = new JLabel(labels.getString("select_image_user_id"));

		textFieldAlbumId = new JTextField("5175973645689271105");
		textFieldUserId = new JTextField("101166462008495927299");

		labelTextLocalImages = new JLabel(labels.getString("select_image_local_images"));
		panelLocalImages = new JPanel(new MigLayout("fill"));
		panelLocalImages.setBackground(Color.white);

		labelTextPicasaImages = new JLabel(labels.getString("select_image_picasa_images"));
		panelPicasaImages = new JPanel(new MigLayout("fill"));
		panelPicasaImages.setBackground(Color.white);

		buttonBack = new JButton(labels.getString("back"));
		buttonBack.addActionListener(selectImageActionListene);
		
		buttonSelect = new JButton(labels.getString("select"));
		buttonSelect.addActionListener(selectImageActionListene);

		buttonSavePicasaImage = new JButton(labels.getString("select_image_save_picasa_image_button"));
		buttonSavePicasaImage.addActionListener(selectImageActionListene);

		buttonSearchPicasaImage = new JButton(labels.getString("select_image_search_picasa_image_button"));
		buttonSearchPicasaImage.addActionListener(selectImageActionListene);


		try
		{
			buttonSavePicasaImage.setIcon(new ImageIcon(ImageIO.read(new File(this.getClass().getResource("/images/" + labels.getString("imageArrowDown")).getFile()))));
			buttonSelect.setIcon(new ImageIcon(ImageIO.read(new File(this.getClass().getResource("/images/" + labels.getString("imageSelect")).getFile()))));
			buttonSearchPicasaImage.setIcon(new ImageIcon(ImageIO.read(new File(this.getClass().getResource("/images/" + labels.getString("imageSearch")).getFile()))));
			buttonBack.setIcon(new ImageIcon(ImageIO.read(new File(this.getClass().getResource("/images/" + labels.getString("arrow_rotate_right")).getFile()))));
		}
		catch (IOException ex)
		{
			logger.error("Not able to read Images");
		}

		scrollpaneLocal = new JScrollPane(panelLocalImages);
		scrollpaneRemote = new JScrollPane(panelPicasaImages);


		add(labelTextUserId, "split 2");
		add(textFieldUserId, "wrap, growx");
		add(labelTextAlbumId, "split 2");
		add(textFieldAlbumId, "wrap, growx");
		add(buttonSearchPicasaImage, "wrap");
		add(labelTextPicasaImages, "wrap");
		add(scrollpaneRemote, "top, grow, wrap");
		add(buttonSavePicasaImage, "center, wrap");
		add(labelTextLocalImages, "wrap");
		add(scrollpaneLocal, "top, grow, wrap");
		add(buttonSelect, "split 2");
		add(buttonBack, "");


	}

	public void setLocalImages(List<ImageIcon> imageIcons)
	{
		panelLocalImages.removeAll();		

		for (ImageIcon imageIcon : imageIcons)
		{
			ImageShowComponent component = new ImageShowComponent(imageIcon);
			component.addMouseListener(newMouseListener);
			component.setIsPicasaImage(false);
			panelLocalImages.add(component, "");
		}

		updateUI();
	}

	public void addPicasaImage(ImageIcon imageIcon)
	{		
		ImageShowComponent component = new ImageShowComponent(imageIcon);
		component.addMouseListener(newMouseListener);
		component.setIsPicasaImage(true);
		panelPicasaImages.add(component, "");

		updateUI();
	}

	public void setController(SelectImageController selectImageController)
	{
		this.controller = selectImageController;
	}

	public String getURLAsString()
	{
		url = "https://picasaweb.google.com/data/feed/api/user/" + textFieldUserId.getText() + "/albumid/" + textFieldAlbumId.getText();
		return url;
	}
		
	public class SelectImageActionListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			if (e.getSource() == buttonSelect)
			{
				controller.imageSelected(selectedLocalImageShowComponent);
			}
			if (e.getSource() == buttonBack)
			{
				controller.imageSelected(null);
			}
			if (e.getSource() == buttonSavePicasaImage)
			{
				controller.saveImageToImageFolder(selectedPicasaImageShowComponent);
			}
			if (e.getSource() == buttonSearchPicasaImage)
			{
				panelPicasaImages.removeAll();
				//url = textFieldAlbumURL.getText();
				controller.searchPicasaImages();
			}
		}
	}

	class NewMouseListener implements MouseListener
	{

		public void mouseClicked(MouseEvent me)
		{
			if (!((ImageShowComponent) me.getSource()).getIsPicasaImage())
			{
				if (selectedLocalImageShowComponent != null)
				{
					selectedLocalImageShowComponent.showBorder(false);
				}

				selectedLocalImageShowComponent = (ImageShowComponent) me.getSource();
				selectedLocalImageShowComponent.showBorder(true);
			}

			if (((ImageShowComponent) me.getSource()).getIsPicasaImage())
			{
				if (selectedPicasaImageShowComponent != null)
				{
					selectedPicasaImageShowComponent.showBorder(false);
				}

				selectedPicasaImageShowComponent = (ImageShowComponent) me.getSource();
				selectedPicasaImageShowComponent.showBorder(true);
			}

		}

		public void mousePressed(MouseEvent me)
		{
			//throw new UnsupportedOperationException("Not supported yet.");
		}

		public void mouseReleased(MouseEvent me)
		{
			//throw new UnsupportedOperationException("Not supported yet.");
		}

		public void mouseEntered(MouseEvent me)
		{
			//throw new UnsupportedOperationException("Not supported yet.");
		}

		public void mouseExited(MouseEvent me)
		{
			//throw new UnsupportedOperationException("Not supported yet.");
		}
	}
}
