package com.samisaada.paskahousu.engine;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class CardTest {

    public static Test suite() {
        return new TestSuite(CardTest.class.getDeclaredClasses());
    }

    public static class Suits extends TestCase {

        public void testHeartsSuitIsDefined() {
            Card.Suit suit = Card.Suit.HEARTS;
        }

        public void testDiamondsSuitIsDefined() {
            Card.Suit suit = Card.Suit.DIAMONDS;
        }

        public void testClubsSuitIsDefined() {
            Card.Suit suit = Card.Suit.CLUBS;
        }

        public void testSpadesSuitIsDefined() {
            Card.Suit suit = Card.Suit.SPADES;
        }

    }

    public static class Name extends TestCase {

        public void testAceIsDefined() {
            Card.Name name = Card.Name.ACE;
        }

        public void test2IsDefined() {
            Card.Name name = Card.Name.TWO;
        }

        public void test3IsDefined() {
            Card.Name name = Card.Name.THREE;
        }

        public void test4IsDefined() {
            Card.Name name = Card.Name.FOUR;
        }

        public void test5IsDefined() {
            Card.Name name = Card.Name.FIVE;
        }

        public void test6IsDefined() {
            Card.Name name = Card.Name.SIX;
        }

        public void test7IsDefined() {
            Card.Name name = Card.Name.SEVEN;
        }

        public void test8IsDefined() {
            Card.Name name = Card.Name.EIGHT;
        }

        public void test9IsDefined() {
            Card.Name name = Card.Name.NINE;
        }

        public void test10IsDefined() {
            Card.Name name = Card.Name.TEN;
        }

        public void testJackIsDefined() {
            Card.Name name = Card.Name.JACK;
        }

        public void testQueenIsDefined() {
            Card.Name name = Card.Name.QUEEN;
        }

        public void testKingIsDefined() {
            Card.Name name = Card.Name.KING;
        }

    }

    public static class CardCreation extends TestCase {

        public void testCreateAceOfSpades() {
            Card card = new Card(Card.Suit.SPADES, Card.Name.ACE);
        }

        public void testAceOfSpadesBelongsToSpades() {
            Card card = new Card(Card.Suit.SPADES, Card.Name.ACE);
            assertEquals(Card.Suit.SPADES, card.suit());
        }

        public void testAceOfHeartsBelongsToHearts() {
            Card card = new Card(Card.Suit.HEARTS, Card.Name.ACE);
            assertSame(Card.Suit.HEARTS, card.suit());
        }

        public void testAceOfDiamondsBelongsToDiamonds() {
            Card card = new Card(Card.Suit.DIAMONDS, Card.Name.ACE);
            assertSame(Card.Suit.DIAMONDS, card.suit());
        }

        public void testAceOfClubsBelongsToClubs() {
            Card card = new Card(Card.Suit.CLUBS, Card.Name.ACE);
            assertSame(Card.Suit.CLUBS, card.suit());
        }

        public void testAceOfSpadesIsAce() {
            Card card = new Card(Card.Suit.SPADES, Card.Name.ACE);
            assertSame(Card.Name.ACE, card.name());
        }

        public void testTwoOfSpadesIsTwo() {
            Card card = new Card(Card.Suit.SPADES, Card.Name.TWO);
            assertSame(Card.Name.TWO, card.name());
        }

        public void testThreeOfSpadesIsThree() {
            Card card = new Card(Card.Suit.SPADES, Card.Name.THREE);
            assertSame(Card.Name.THREE, card.name());
        }

        public void testFourOfSpadesIsFour() {
            Card card = new Card(Card.Suit.SPADES, Card.Name.FOUR);
            assertSame(Card.Name.FOUR, card.name());
        }

        public void testFiveOfSpadesIsFive() {
            Card card = new Card(Card.Suit.SPADES, Card.Name.FIVE);
            assertSame(Card.Name.FIVE, card.name());
        }

        public void testSixOfSpadesIsSix() {
            Card card = new Card(Card.Suit.SPADES, Card.Name.SIX);
            assertSame(Card.Name.SIX, card.name());
        }

        public void testSevenOfSpadesIsSeven() {
            Card card = new Card(Card.Suit.SPADES, Card.Name.SEVEN);
            assertSame(Card.Name.SEVEN, card.name());
        }

        public void testEightOfSpadesIsEight() {
            Card card = new Card(Card.Suit.SPADES, Card.Name.EIGHT);
            assertSame(Card.Name.EIGHT, card.name());
        }

        public void testNineOfSpadesIsNine() {
            Card card = new Card(Card.Suit.SPADES, Card.Name.NINE);
            assertSame(Card.Name.NINE, card.name());
        }

        public void testTenOfSpadesIsTen() {
            Card card = new Card(Card.Suit.SPADES, Card.Name.TEN);
            assertSame(Card.Name.TEN, card.name());
        }

        public void testJackOfSpadesIsJack() {
            Card card = new Card(Card.Suit.SPADES, Card.Name.JACK);
            assertSame(Card.Name.JACK, card.name());
        }

        public void testQueenOfSpadesIsQueen() {
            Card card = new Card(Card.Suit.SPADES, Card.Name.QUEEN);
            assertSame(Card.Name.QUEEN, card.name());
        }

        public void testKingOfSpadesIsKing() {
            Card card = new Card(Card.Suit.SPADES, Card.Name.KING);
            assertSame(Card.Name.KING, card.name());
        }

    }

    public static class CardComparing extends TestCase {

        public void testSameCards() {
            Card card = new Card(Card.Suit.SPADES, Card.Name.ACE);
            Card otherCard = new Card(Card.Suit.SPADES, Card.Name.ACE);
            assertTrue(card.equals(otherCard));
        }

        public void testDifferentCards() {
            Card card = new Card(Card.Suit.SPADES, Card.Name.ACE);
            Card otherCard = new Card(Card.Suit.SPADES, Card.Name.TWO);
            assertFalse(card.equals(otherCard));
        }

    }

}
