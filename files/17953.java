/**
 * 
 * ContactListAdapter.java
 *
 * 	Copyright $2011 Frederik Hahne
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package de.atomfrede.android.thwdroid.messages.adapter;

import org.jivesoftware.smack.RosterEntry;
import org.jivesoftware.smack.packet.Presence;

import android.app.Activity;
import android.view.*;
import android.widget.*;
import de.atomfrede.android.thwdroid.R;

public class ContactListAdapter extends ArrayAdapter<RosterEntry> {

	private Activity context;
	private RosterEntry[] rosterEntries;

	public ContactListAdapter(Activity context, RosterEntry[] entries) {
		super(context, R.layout.messages_roster_item_view, entries);
		this.context = context;
		this.rosterEntries = entries;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = context.getLayoutInflater();
		View contactListItemView = inflater.inflate(R.layout.messages_roster_item_view, null, true);

		TextView nameTextView = (TextView) contactListItemView.findViewById(R.id.contact_name_text_view);
		TextView userNameTextView = (TextView) contactListItemView.findViewById(R.id.jabber_id_text_view);
		ImageView presenceImageView = (ImageView) contactListItemView.findViewById(R.id.presence_indicator_image);

		nameTextView.setText(getName(position));
		userNameTextView.setText(getJabberId(position));
		presenceImageView.setImageResource(getStatusImage(position));
		return contactListItemView;
	}

	/**
	 * Returns the name for a roster entry at the given position. If not name is set the jabber id is returned.
	 * 
	 * @param position
	 * @return
	 */
	public String getName(int position) {
		String name = rosterEntries[position].getName();
		if (name == null)
			name = rosterEntries[position].getUser();
		return name;
	}

	/**
	 * Returns the jabberId for the rosterEntry at the given position
	 * 
	 * @param position
	 * @return
	 */
	public String getJabberId(int position) {
		return rosterEntries[position].getUser();
	}

	private int getStatusImage(int position) {
		int imageId = R.drawable.presence_offline;
		// try {
		// Presence presence = ThwStub.getInstance().connection.getRoster().getPresence(getJabberId(position));
		//
		// switch (presence.getType()) {
		// case available:
		// imageId = getPresenceModeImage(presence);
		// break;
		//
		// case unavailable:
		// imageId = R.drawable.presence_offline;
		// break;
		//
		// default:
		// break;
		// }
		// } catch (Exception e) {
		//
		// }
		return imageId;

	}

	private int getPresenceModeImage(Presence presence) {
		int imageId = R.drawable.presence_offline;
		if (presence.getMode() != null) {
			switch (presence.getMode()) {
			case available:
				imageId = R.drawable.presence_online;
				break;
			case away:
				imageId = R.drawable.presence_away;
				break;
			case dnd:
				imageId = R.drawable.presence_busy;
				break;
			case xa:
				imageId = R.drawable.presence_away;
				break;
			case chat:
				imageId = R.drawable.presence_online;
				break;
			default:
				break;
			}
		} else {
			// if the mode is null or empty this should be interpreted as
			// "online"
			// see also the XMPP Guide
			imageId = R.drawable.presence_online;
		}
		return imageId;
	}
}
