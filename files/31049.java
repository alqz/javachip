/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sica.entity;

import br.com.caelum.vraptor.ioc.Component;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 *
 * @author Ana Paula e Arthur
 */
@Entity
@Component
public class DesligamentoJuridico {
    @Id
    @GeneratedValue
    
    private long id;
    private long idAbrigada;
    private String descricaoDesligamento;


    public DesligamentoJuridico() {
    }

    public DesligamentoJuridico(long idAbrigada, String descricaoDesligamento, long id) {
        this.idAbrigada = idAbrigada;
        this.descricaoDesligamento = descricaoDesligamento;
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    
    public String getDescricaoDesligamento() {
        return descricaoDesligamento;
    }

    public void setDescricaoDesligamento(String descricaoDesligamento) {
        this.descricaoDesligamento = descricaoDesligamento;
    }

    public long getIdAbrigada() {
        return idAbrigada;
    }

    public void setIdAbrigada(long idAbrigada) {
        this.idAbrigada = idAbrigada;
    }

    @Override
    public String toString() {
        return "DesligamentoJuridico{" + "id=" + id + ", idAbrigada=" + idAbrigada + ", descricaoDesligamento=" + descricaoDesligamento + '}';
    }

    
    
    
}