/* File: Inception/World/Handler.java
 * Provides: Handler for registered Worlds.
 * Authors: Xaymar
 * Copyright: 2012 (c) Inception Plugin Team.
 * License: 
 *      Inception by Inception Plugin Team is licensed under a
 *      Creative Commons Attribution-ShareAlike 3.0 Unported
 *      License.
 */
package Inception.World;

import Inception.Main.Inception;
import java.io.File;
import org.bukkit.Bukkit;
import org.bukkit.World;

public class Handler {

    private World world;
    private World worldSyncTimeTo;
    private World worldUpper;
    private World worldLower;
    private File configFile;
    private Configuration configLink;
    private HandlerListener handlerListener;
    private HandlerTickTask handlerTickTask;
    private int handlerTickTaskId = -1;
//    private HashMap<Chunk, HashMap<BlockVector, Material>> chunkOverlapChangedBlocksType;
//    private HashMap<Chunk, HashMap<BlockVector, Byte>> chunkOverlapChangedBlocksData;

    // <editor-fold desc="Constructor and Destructor">
    public Handler(World objWorld) {
        this.world = objWorld;

        // Find configuration file and load it.
        this.configFile = new File(Inception.pluginInstance.getWorldDirectory() + File.separator + objWorld.getName() + ".yml");
        if (!this.configFile.exists()) {
            Inception.pluginInstance.getPluginZipFile().unzipPathAs("world-config.yml", this.configFile);
        }
        this.configLink = new Configuration(this.configFile, Inception.pluginInstance.getDefaultConfiguration());

        this.handlerListener = new HandlerListener(this);
        this.handlerTickTask = new HandlerTickTask(this);

        loadConfiguration();
    }

    public void destroyHandler() {
        saveConfiguration();
    }
    // </editor-fold>

    // <editor-fold desc="Configuration">
    public Configuration getConfiguration() {
        return this.configLink;
    }

    public void loadConfiguration() {
        this.configLink.copyFrom(Inception.pluginInstance.getDefaultConfiguration());
        this.configLink.load();

        if (this.handlerTickTaskId != -1) {
            Bukkit.getScheduler().cancelTask(this.handlerTickTaskId);
            this.handlerTickTaskId = -1;
        }

        if (this.configLink.isWorldEnabled()) {
            worldSyncTimeTo = Bukkit.getWorld(this.configLink.getWorldSyncTimeTo());
            worldUpper = Bukkit.getWorld(this.configLink.getUpperWorld());
            worldLower = Bukkit.getWorld(this.configLink.getLowerWorld());

            // Recreate TickTask
            if (this.configLink.getWorldDelayedTicks() > 0) {
                this.handlerTickTaskId = Bukkit.getScheduler().scheduleSyncRepeatingTask(Inception.pluginInstance, handlerTickTask, this.configLink.getWorldDelayedTicks(), this.configLink.getWorldDelayedTicks());
            }
        } else {
            worldSyncTimeTo = null;
            worldUpper = null;
            worldLower = null;
        }
    }

    public void loadDefaultConfiguration() {
        if (this.configFile.exists()) {
            this.configFile.delete();
        }
        Inception.pluginInstance.getPluginZipFile().unzipPathAs("world-config.yml", this.configFile);
        this.loadConfiguration();
    }

    public void saveConfiguration() {
        this.configLink.save();
    }
    // </editor-fold>

    // <editor-fold desc="World">
    public World getWorld() {
        return world;
    }

    public World getWorldSyncTimeTo() {
        return worldSyncTimeTo;
    }

    public World getUpperWorld() {
        if (worldUpper != Bukkit.getWorld(this.configLink.getUpperWorld())) {
            worldUpper = Bukkit.getWorld(this.configLink.getUpperWorld());
        }
        return worldUpper;
    }

    public World getLowerWorld() {
        return worldLower;
    }

    // </editor-fold>
//                //This contains all changed blocks in this world.
//                overlapCreateChunkMap();
//
//                /*
//                 * Create a Task that handles some values every x Ticks.
//                 */
//                if (handlerTickTask == null) {
//                    handlerTickTask = new HandlerTickTask(plugin, this);
//                }
//                if ((iWorldDelayedTicks > 0)
//                    && (((objUpperWorld != null) && (bUpperTeleportEnabled == true))
//                        || ((objLowerWorld != null) && (bLowerTeleportEnabled == true)))) {
//                    if (handlerTickTask != -1) {
//                        plugin.getServer().getScheduler().cancelTask(handlerTickTask);
//                    }
//                    handlerTickTask = plugin.getServer().getScheduler().scheduleSyncRepeatingTask(plugin, handlerTickTask, iWorldDelayedTicks, iWorldDelayedTicks);
//                    if (handlerTickTask == -1) {
//                        plugin.getLogger().warning("<" + world.getName() + "> Could not register synchronized repeating task. Entities can not be teleported!");
//                    } else {
//                        plugin.getLogger().info("<" + world.getName() + "> WorldHandler enabled.");
//                    }
//                }
//                if (handlerTickTask == -1) {
//                    plugin.getLogger().info("<" + world.getName() + "> Teleportation disabled.");
//                }
//            } else {
//                if (handlerTickTask != -1) {
//                    plugin.getServer().getScheduler().cancelTask(handlerTickTask);
//                    handlerTickTask = -1;
//                }
//                plugin.getLogger().info("<" + world.getName() + "> WorldHandler disabled.");
//            }
//        } catch (FileNotFoundException ex) {
//            plugin.getLogger().log(Level.SEVERE, null, ex);
//        } catch (IOException ex) {
//            plugin.getLogger().log(Level.SEVERE, null, ex);
//        } catch (InvalidConfigurationException ex) {
//            plugin.getLogger().log(Level.SEVERE, null, ex);
//        }
//    }
//
//    public World getWorld() {
//        return world;
//    }
//
//
//    public void destroy() {
//        if (chunkOverlapChangedBlocksType != null) {
//            for (Chunk chunk : chunkOverlapChangedBlocksType.keySet()) {
//                overlapUnloadChunk(chunk);
//            }
//            chunkOverlapChangedBlocksType.clear();
//            chunkOverlapChangedBlocksType = null;
//        }
//        if (chunkOverlapChangedBlocksData != null) {
//            for (Chunk chunk : chunkOverlapChangedBlocksData.keySet()) {
//                overlapUnloadChunk(chunk);
//            }
//            chunkOverlapChangedBlocksData.clear();
//            chunkOverlapChangedBlocksData = null;
//        }
//    }
//
//    private void overlapCreateChunkMap() {
//        destroy();
//        chunkOverlapChangedBlocksType = new HashMap<Chunk, HashMap<BlockVector, Material>>();
//        chunkOverlapChangedBlocksData = new HashMap<Chunk, HashMap<BlockVector, Byte>>();
//    }
//    private void overlapLoadChunk(Chunk chunk) {
//
//        HashMap<BlockVector, Material> changedBlocksType = new HashMap<BlockVector, Material>();
//        HashMap<BlockVector, Byte> changedBlocksData = new HashMap<BlockVector, Byte>();
//        chunkOverlapChangedBlocksType.put(chunk, changedBlocksType);
//        chunkOverlapChangedBlocksData.put(chunk, changedBlocksData);
//        if (bWorldIsEnabled == true) {
//            if ((bUpperOverlapEnabled == true) && (objUpperWorld != null) && (iUpperOverlapTo <= world.getMaxHeight()) && (iUpperOverlapFrom >= 0)) {
//                Chunk chunkUpper = objUpperWorld.getChunkAt(chunk.getX(), chunk.getZ());
//                if (chunkUpper != null) {
//                    boolean manualLoad = false;
//                    if (chunkUpper.isLoaded() == false) {
//                        //Quick & Dirty hack to prevent recursive calls due to infinite events...
//                        chunkOverlapChangedBlocksType.put(chunkUpper, new HashMap<BlockVector, Material>());
//                        chunkUpper.load(true);
//                        manualLoad = true;
//                    }
//                    for (int layer = 0; layer < iUpperOverlapLayers; layer++) {
//                        for (int x = 0; x < 16; x++) {
//                            for (int z = 0; z < 16; z++) {
//                                Block block = chunk.getBlock(x, iUpperOverlapTo - (iUpperOverlapLayers - 1) + layer, z);
//                                Block blockUpper = chunkUpper.getBlock(x, iUpperOverlapFrom + layer, z);
//                                if (block != null) {
//                                    if (blockUpper != null) {
//                                        if (block.getType() == Material.AIR) {
//                                            BlockVector pos = new BlockVector(x, iUpperOverlapTo - layer, z);
//                                            changedBlocksType.put(pos, block.getType());
//                                            changedBlocksData.put(pos, block.getData());
//                                            block.setType(blockUpper.getType());
//                                            block.setData(blockUpper.getData());
//                                        }
//                                    }
//                                }
//                            }
//                        }
//                    }
//                    if (manualLoad == true) {
//                        //Quick & Dirty hack to prevent recursive calls due to infinite events...
//                        chunkOverlapChangedBlocksType.remove(chunkUpper);
//                        chunkUpper.unload(false);
//                    }
//                }
//            }
//            if ((bLowerOverlapEnabled == true && objLowerWorld != null) && (iLowerOverlapTo >= 0) && (iLowerOverlapFrom <= 255)) {
//                Chunk chunkLower = objLowerWorld.getChunkAt(chunk.getX(), chunk.getZ());
//                if (chunkLower != null) {
//                    boolean manualLoad = false;
//                    if (chunkLower.isLoaded() == false) {
//                        //Quick & Dirty hack to prevent recursive calls due to infinite events...
//                        chunkOverlapChangedBlocksType.put(chunkLower, new HashMap<BlockVector, Material>());
//                        chunkLower.load(true);
//                        manualLoad = true;
//                    }
//                    for (int layer = 0; layer < iLowerOverlapLayers; layer++) {
//                        for (int x = 0; x < 16; x++) {
//                            for (int z = 0; z < 16; z++) {
//                                Block block = chunk.getBlock(x, iLowerOverlapTo + layer, z);
//                                Block blockLower = chunkLower.getBlock(x, iLowerOverlapFrom - (iLowerOverlapLayers - 1) + layer, z);
//                                if (block != null) {
//                                    if (blockLower != null) {
//                                        if (block.getType() == Material.AIR) {
//                                            BlockVector pos = new BlockVector(x, iLowerOverlapTo + layer, z);
//                                            changedBlocksType.put(pos, block.getType()); //This causes a java.lang.OutOfMemoryError. Figure out why.
//                                            changedBlocksData.put(pos, block.getData());
//                                            block.setType(blockLower.getType());
//                                            block.setData(blockLower.getData());
//                                        }
//                                    }
//                                }
//                            }
//                        }
//                    }
//                    if (manualLoad == true) {
//                        //Quick & Dirty hack to prevent recursive calls due to infinite events...
//                        chunkOverlapChangedBlocksType.remove(chunkLower);
//                        chunkLower.unload(false);
//                    }
//                }
//            }
//        }
//    }
//
//    private void overlapUnloadChunk(Chunk chunk) {
//        HashMap<BlockVector, Material> changedBlocksType = chunkOverlapChangedBlocksType.get(chunk);
//        HashMap<BlockVector, Byte> changedBlocksData = chunkOverlapChangedBlocksData.get(chunk);
//        for (BlockVector vec : changedBlocksType.keySet()) {
//            Block block = chunk.getBlock(vec.getBlockX(), vec.getBlockY(), vec.getBlockZ());
//            Material oldType = changedBlocksType.get(vec);
//            Byte oldData = changedBlocksData.get(vec);
//            block.setType(oldType);
//            block.setData(oldData);
//        }
//        chunkOverlapChangedBlocksType.remove(chunk);
//        chunkOverlapChangedBlocksData.remove(chunk);
//    }
//
//    private void overlapBlockPlace(Block block) {
//        Location localPosition = block.getLocation();
//        if (bUpperOverlapEnabled && (objUpperWorld != null)) {
//            if ((localPosition.getBlockY() <= iUpperOverlapTo) && (localPosition.getBlockY() > iUpperOverlapTo - iUpperOverlapLayers)) {
//                plugin.getWorldHandlers().get(objUpperWorld).replaceBlock(block.getType(), block.getData(), new BlockVector(block.getX(), iUpperOverlapTo - block.getY(), block.getZ()), false);
//                removeListedBlock(block.getChunk(), block.getLocation().toVector().toBlockVector());
//            }
//        }
//        if (bLowerOverlapEnabled && (objLowerWorld != null)) {
//            if ((localPosition.getBlockY() >= iLowerOverlapTo) && (localPosition.getBlockY() < iLowerOverlapTo + iLowerOverlapLayers)) {
//                plugin.getWorldHandlers().get(objLowerWorld).replaceBlock(block.getType(), block.getData(), new BlockVector(block.getX(), block.getY() - iLowerOverlapTo, block.getZ()), true);
//                removeListedBlock(block.getChunk(), block.getLocation().toVector().toBlockVector());
//            }
//        }
//    }
//
//    private void overlapBlockBreak(Block block) {
//        Location localPosition = block.getLocation();
//        if (bUpperOverlapEnabled && (objUpperWorld != null)) {
//            if ((localPosition.getBlockY() <= iUpperOverlapTo) && (localPosition.getBlockY() > iUpperOverlapTo - iUpperOverlapLayers)) {
//                plugin.getWorldHandlers().get(objUpperWorld).replaceBlock(Material.AIR, new Byte((byte) 0), new BlockVector(block.getX(), iUpperOverlapTo - block.getY(), block.getZ()), false);
//                removeListedBlock(block.getChunk(), block.getLocation().toVector().toBlockVector());
//            }
//        }
//        if (bLowerOverlapEnabled && (objLowerWorld != null)) {
//            if ((localPosition.getBlockY() >= iLowerOverlapTo) && (localPosition.getBlockY() < iLowerOverlapTo + iLowerOverlapLayers)) {
//                plugin.getWorldHandlers().get(objLowerWorld).replaceBlock(Material.AIR, new Byte((byte) 0), new BlockVector(block.getX(), block.getY() - iLowerOverlapTo, block.getZ()), true);
//                removeListedBlock(block.getChunk(), block.getLocation().toVector().toBlockVector());
//            }
//        }
//    }
//
//    /*
//     * External
//     */
//    public void chunkLoadEvent(ChunkLoadEvent event) {
//        if (bWorldIsEnabled && mapWorldOverlapTriggers.get("ChunkLoadUnload")) {
//            Chunk chunk = event.getChunk();
//            //Quick & Dirty hack to prevent recursive calls due to infinite events...
//            if (!chunkOverlapChangedBlocksType.containsKey(chunk)) {
//                overlapLoadChunk(chunk);
//            }
//        }
//
//    }
//
//    public void chunkUnloadEvent(ChunkUnloadEvent event) {
//        if (bWorldIsEnabled && mapWorldOverlapTriggers.get(Triggers.ChunkLoadUnload)) {
//            Chunk chunk = event.getChunk();
//            if (chunkOverlapChangedBlocksType.containsKey(chunk)) {
//                overlapUnloadChunk(chunk);
//            }
//        }
//    }
//
//    public void blockPlaceEvent(BlockPlaceEvent event) {
//        if (bWorldIsEnabled && mapWorldOverlapTriggers.get(Triggers.BlockPlace) && !event.isCancelled()) {
//            overlapBlockPlace(event.getBlockPlaced());
//
//        }
//    }
//
//    public void blockBreakEvent(BlockBreakEvent event) {
//        if (bWorldIsEnabled && mapWorldOverlapTriggers.get(Triggers.BlockBreak) && !event.isCancelled()) {
//            overlapBlockBreak(event.getBlock());
//        }
//    }
//
//    public void blockBurnEvent(BlockBurnEvent event) {
//        if (bWorldIsEnabled && mapWorldOverlapTriggers.get(Triggers.BlockBurn) && !event.isCancelled()) {
//            overlapBlockBreak(event.getBlock());
//        }
//    }
//
//    public void blockFadeEvent(BlockFadeEvent event) {
//        if (bWorldIsEnabled && mapWorldOverlapTriggers.get(Triggers.BlockFade) && !event.isCancelled()) {
//            overlapBlockBreak(event.getBlock());
//        }
//    }
//
//    public void blockFormEvent(BlockFormEvent event) {
//        if (bWorldIsEnabled && mapWorldOverlapTriggers.get(Triggers.BlockForm) && !event.isCancelled()) {
//            overlapBlockPlace(event.getBlock());
//        }
//    }
//
//    public void blockGrowEvent(BlockGrowEvent event) {
//        if (bWorldIsEnabled && mapWorldOverlapTriggers.get(Triggers.BlockGrow) && !event.isCancelled()) {
//            overlapBlockPlace(event.getBlock());
//        }
//    }
//
//    public void blockSpreadEvent(BlockSpreadEvent event) {
//        if (bWorldIsEnabled && mapWorldOverlapTriggers.get(Triggers.BlockSpread) && !event.isCancelled()) {
//            overlapBlockPlace(event.getBlock());
//        }
//    }
//
//    //Listed Temporary Blocks
//    private void removeListedBlock(Chunk chunk, BlockVector Position) {
//        HashMap<BlockVector, Material> changedBlocksType = chunkOverlapChangedBlocksType.get(chunk);
//        HashMap<BlockVector, Byte> changedBlocksData = chunkOverlapChangedBlocksData.get(chunk);
//        if ((changedBlocksType != null) && (changedBlocksData != null)) {
//            if (changedBlocksType.containsKey(Position)) {
//                changedBlocksType.remove(Position);
//                changedBlocksData.remove(Position);
//            }
//        }
//    }
//
//    public void replaceBlock(Material withMat, Byte withData, BlockVector Position, boolean cameFromAbove) {
//        Vector localPosition = Position.clone();
//        if (cameFromAbove) {
//            localPosition.setY(iUpperOverlapTo - (iUpperOverlapLayers - 1) + Position.getBlockY());
//        } else {
//            localPosition.setY(iLowerOverlapTo + (iLowerOverlapLayers - 1) - Position.getBlockY());
//        }
//
//        Block block = world.getBlockAt(localPosition.getBlockX(), localPosition.getBlockY(), localPosition.getBlockZ());
//        Chunk chunk = world.getChunkAt(block);
//        removeListedBlock(chunk, block.getLocation().toVector().toBlockVector());
//        block.setType(withMat);
//        block.setData(withData);
//    }
}