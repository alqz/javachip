package models.user;

import siena.*;

import java.util.List;

@Table("UserId")
public class UserId extends Model {
    @Id(Generator.AUTO_INCREMENT)
    public Long id;

    @NotNull
    public User user;

    @Unique("token")
    public String token;

    @NotNull
    public String provider;

    public static UserId find(String id, String provider) {
        return UserId.all(UserId.class).filter("token", id).filter("provider", provider).get();
    }

    public static List<UserId> findByUser(User user) {
        return UserId.all(UserId.class).filter("user", user).fetch();
    }
}
