package net.skcomms.dtc.client.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import net.skcomms.dtc.client.PersistenceManager;

import com.google.gwt.core.client.GWT;

public class RecentApiSelectionDao {

  public static final String PREFIX = "RecentApiSelectionDao.";

  private static final int MAX_PAGELIST_SIZE = 50;

  public static String combineKey(String path) {
    return RecentApiSelectionDao.PREFIX + path;
  }

  private static void sortByTimeDesc(List<RecentApiSelection> selections) {
    Collections.sort(selections, new Comparator<RecentApiSelection>() {

      @Override
      public int compare(RecentApiSelection o1, RecentApiSelection o2) {
        return (int) (-(o1.getSelectedTime().getTime() - o2.getSelectedTime().getTime()));
      }
    });
  }

  private List<RecentApiSelection> deserializeAllSelections() {
    List<RecentApiSelection> selections = new ArrayList<RecentApiSelection>();
    for (String key : PersistenceManager.getInstance().getItemKeys()) {
      if (key.startsWith(RecentApiSelectionDao.PREFIX)) {
        RecentApiSelection selectedPage = RecentApiSelection.deserialize(PersistenceManager
            .getInstance()
            .getItem(key));
        selections.add(selectedPage);
      }
    }
    return selections;
  }

  // FIXME 더 보편적인 반환타입을 사용할 수 없을까?
  public List<RecentApiSelectionRecord> getAllRecentApiSelectionRecords() {
    List<RecentApiSelectionRecord> records = new ArrayList<RecentApiSelectionRecord>();

    for (RecentApiSelection selection : this.getAllRecentApiSelections()) {
      // FIXME RecentApiSelection을 파라미터로 받아서 RecentApiSelectionRecord를 생성하는
      // 생성자/팩토리 메써드를 만들자.
      RecentApiSelectionRecord record = RecentApiSelectionRecord.create(selection.getPath(),
          selection.getSelectedTime());
      records.add(record);
    }
    return records;
  }

  public List<RecentApiSelection> getAllRecentApiSelections() {
    List<RecentApiSelection> selections = this.deserializeAllSelections();
    RecentApiSelectionDao.sortByTimeDesc(selections);
    this.removeOldSelections(selections);
    return selections;
  }

  public void persist(RecentApiSelection selection) {
    GWT.log("persist: " + RecentApiSelectionDao.combineKey(selection.getPath()));
    PersistenceManager.getInstance().setItem(
        RecentApiSelectionDao.combineKey(selection.getPath()), selection.serialize());
  }

  public void remove(String path) {
    GWT.log("Remove visited history:" + path);
    PersistenceManager.getInstance().removeItem(RecentApiSelectionDao.combineKey(path));
  }

  private void removeOldSelections(List<RecentApiSelection> selections) {
    for (int i = RecentApiSelectionDao.MAX_PAGELIST_SIZE; i < selections.size(); i++) {
      this.remove(selections.get(i).getPath());
    }

    if (selections.size() > RecentApiSelectionDao.MAX_PAGELIST_SIZE) {
      selections.subList(RecentApiSelectionDao.MAX_PAGELIST_SIZE, selections.size()).clear();
    }
  }
}
