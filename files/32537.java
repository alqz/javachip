
package hotel.jaxws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "addHotel", namespace = "http://hotel/")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "addHotel", namespace = "http://hotel/")
public class AddHotel {

    @XmlElement(name = "arg0", namespace = "")
    private hotel.Hotel arg0;

    /**
     * 
     * @return
     *     returns Hotel
     */
    public hotel.Hotel getArg0() {
        return this.arg0;
    }

    /**
     * 
     * @param arg0
     *     the value for the arg0 property
     */
    public void setArg0(hotel.Hotel arg0) {
        this.arg0 = arg0;
    }

}
