/* Flow
 * Copyright (C) 2012 Jnorr44
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

package com.github.JamesNorris.Flow.Mechanics;

import org.bukkit.block.*;
import org.bukkit.event.*;
import org.bukkit.event.block.BlockFromToEvent;

import com.github.JamesNorris.Flow.Flow;
import com.github.JamesNorris.Flow.Util.CommandUtil;

// Class for preventing streams from forming.

public class StreamPreventer implements Listener {
	public int flowcounter;
	private final Flow plugin;

	public StreamPreventer(final Flow instance) {
		plugin = instance;
	}

	@EventHandler(priority = EventPriority.HIGHEST) public void BFTE(final BlockFromToEvent event) {
		if (!event.isCancelled()) {
			final Block block = event.getToBlock();
			if (block.getTypeId() == 0) {
				final Block source = event.getBlock();
				final int sourcetype = source.getTypeId();
				for (final BlockFace f : new BlockFace[] {BlockFace.NORTH, BlockFace.SOUTH, BlockFace.EAST, BlockFace.WEST}) {
					final Block stream = block.getRelative(f);
					if (stream.getX() == source.getX()) {
						if (stream.getY() == source.getY()) {
							if (stream.getZ() == source.getZ()) {
								continue;
							}
						}
					}
					if (stream.getData() == 0x0) {
						if ((stream.getTypeId() == 8 && plugin.getConfig().getBoolean("enableWater") == true) || (stream.getTypeId() == 10 && plugin.getConfig().getBoolean("enableLava") == true)) {
							event.getToBlock().setTypeId(sourcetype);
							++CommandUtil.flowFixCount;
							event.setCancelled(true);
							break;
						}
					}
				}
			}
		}
	}
}
