package intranet.domain.services.impl.test;

import intranet.domain.entities.Regra;
import intranet.utils.IntranetException;
import intranet.utils.IntranetServiceTestCase;
import intranet.utils.SpringBeanFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author Gabriel Cardelli
 * @since Mar 4, 2012 - 12:23:25 PM
 *
 */
public class AlterarProfessorServiceTest extends IntranetServiceTestCase {

	@Override
	public void testSeServicoExiste() throws IntranetException {	
		assertNotNull(SpringBeanFactory.getBean("alterarProfessorService"));
	}

	
	@Override
	public void testFaltandoParamsNoMapa() throws IntranetException {
		Map<String,Object> params = new HashMap<String,Object>();
		
		params.put("email", "gabrielcastilho.cardelli@gmail.com");
		params.put("nome", "gabriel castilho de almeida cardelli");
		params.put("matricula", "11223344");
		params.put("regras",new ArrayList<Regra>());
		
		
		Map<String,Object> result = getServiceLocator().execute("alterarProfessorService",params);
		
		
		assertNotNull(result.get("error"));
		
	}

	@Override
	public void testServiceMapParamNulo() throws IntranetException {
		Map<String,Object> result = getServiceLocator().execute("alterarProfessorService",null);
		assertNotNull(result.get("error"));
	}

	
	@Override
	public void testFluxoCorreto() throws IntranetException {
		Map<String,Object> params = new HashMap<String,Object>();
		
		params.put("id", 1);
		params.put("login", "admin");
		params.put("email", "gabrielcastilho.cardelli@gmail.com");
		params.put("nome", "gabriel castilho de almeida cardelli");
		params.put("matricula", "11223344");
		params.put("regras",new ArrayList<Regra>());
		
		Map<String,Object> result = getServiceLocator().execute("alterarProfessorService",params);
		
		assertNotNull(result.get("message"));
		assertNull(result.get("error"));
		
	}

}
