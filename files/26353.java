package ucdavis.bqleclipse.internal.models;

import ucdavis.bqleclipse.models.ITraceElement;
import ucdavis.bqleclipse.models.ITraceSession;

public class JavaTraceElement extends TraceElement {
	public JavaTraceElement(int id, ITraceElement.TraceLevel level, String className,
			ITraceSession session) {
		super(id, level, className, session);
	}
	
	@Override
	public String getName() {
		return String.format("%s.main", className);
	}

	@Override
	public String getType() {
		return "Java";
	}
}
