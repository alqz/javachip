/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package searchingmodule;

import java.io.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import searchingmodule.factoryCreators.SearchingModule;
import searchingmodule.libraries.Library;
import sun.org.mozilla.javascript.internal.ast.CatchClause;

/**
 *
 * @author CFGMM
 */
public class MainModule {

    /**
     * @param args the command line arguments
     */
    public static class Article {

        public String title = new String("null");
        public String authors = new String("null");
        public String releaseDate = new String("null");
        public String releaseLoc = new String("null");
        public String docType = new String("null");
        public String publisher = new String("null");
        public String nrPagini = new String("0");
        public String issn = new String("null");
        public String isbn = new String("null");
    };
    public static boolean ieeeDone = false;
    public static boolean citeSeerDone = false;
    public static boolean worldCatDone = false;
    public static boolean dblpDone = false;
    
    private static ArrayList<Article> articles;

    private static String getTagValue(String tag, Element element) {
        NodeList nList = element.getElementsByTagName(tag).item(0).getChildNodes();
        Node nValue = (Node) nList.item(0);
        return nValue.getNodeValue().trim();
    }

    private static void extractData(String xmlFile)
            throws ParserConfigurationException, SAXException, IOException {
        try {
            DocumentBuilderFactory docBuilderFactory;
            docBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
            Document doc = docBuilder.parse(new File(xmlFile));

            doc.getDocumentElement().normalize();
            NodeList listOfBooks = doc.getElementsByTagName("BOOK");

            articles = new ArrayList<>();

            for (int i = 0; i < listOfBooks.getLength(); i++) {
                Node nNode = listOfBooks.item(i);
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element nElement = (Element) nNode;

                    Article article = new Article();

                    //Excracting data
                    article.title = getTagValue("title", nElement);
                    article.authors = getTagValue("authors", nElement);
                    article.releaseDate = getTagValue("releaseDate", nElement);
                    article.releaseLoc = getTagValue("releaseLoc", nElement);
                    article.docType = getTagValue("docType", nElement);
                    article.publisher = getTagValue("publisher", nElement);
                    article.nrPagini = getTagValue("nrPagini", nElement);
                    article.issn = getTagValue("ISSN", nElement);
                    article.isbn = getTagValue("ISBN", nElement);
                    articles.add(article);
                    System.out.println(article.authors);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private static void writeToFinalFile(String fromFile, OutputStream toFile) {
        try {
            FileInputStream fstream = new FileInputStream(fromFile);
            DataInputStream in = new DataInputStream(fstream);
            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            String line;
            
            while ((line = br.readLine()) != null) {
                toFile.write((line + "\n").getBytes("UTF-8"));
            }
            in.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private static void populateFinalResult() {
        OutputStream file = null;
        try {
            file = new FileOutputStream("SearchResults.xml");
        } catch (FileNotFoundException ex) {
            Logger.getLogger(MainModule.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            file.write(("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<SearchResults>\n").getBytes("UTF-8"));
            writeToFinalFile("outputIeee.xml", file);
            writeToFinalFile("outputCiteSeer.xml", file);
            writeToFinalFile("outputWorldCat.xml", file);
            writeToFinalFile("outputDBLP.xml", file);
            file.write("</SearchResults>\n".getBytes("UTF-8"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        MainModule libs = new MainModule();
        //Making sure the file is passed as an argument
        try {
            if (args[0].length() < 1) {
                throw new Exception("There is no argument, please pass the "
                        + "XML file as an argument, "
                        + "ex: searching_module.jar XMLFile.xml");
            }

            if (args.length > 1) {
                System.out.println("To many arguments, additional arguments "
                        + "will be ignored");
            }
            //Making sure it is actually an XML file
            if (!(args[0].endsWith(".xml") || args[0].endsWith(".XML"))) {
                throw new Exception("This is not an xml file, please pass the "
                        + "XML file as an argument, "
                        + "ex: searching_module.jar XMLFile.xml");
            }
        } catch (Exception exception) {
            System.out.print("Passing argument Exception: "
                    + exception.getMessage());
        }
        //Extracting Data
        try {
            extractData(args[0]);
        } catch (IOException ioException) {
            System.out.print("Input/Output Exception: "
                    + ioException.getMessage());
        } catch (ParserConfigurationException parsesException) {
            System.out.print("Parser Exception: "
                    + parsesException.getMessage());
        } catch (SAXException saxException) {
            System.out.print("SAX Exception: "
                    + saxException.getMessage());
        }

        //Launching the search operation
        libs.startSearch();
        //Wait for the threads to finish the search
        while (!(ieeeDone && citeSeerDone && worldCatDone && dblpDone)) {
            System.out.println();
        }
        //Writing the results into a single file "SearchResults.xml"
        populateFinalResult();
    }

    public void startSearch() {
        SearchingModule creator = new SearchingModule(articles);
    }
}
