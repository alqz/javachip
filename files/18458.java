package mas.worldview.sframesystem.rts.viewport.brushes;

import java.awt.Color;
import java.awt.event.*;
import java.awt.geom.Rectangle2D;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import layout.TableLayout;
import mas.worldcontroller.SimulationController;
import mas.worldmodel.Map;
import mas.worldmodel.objects.RectWorldObject;
import mas.worldmodel.objects.WorldObjectFactory;
import mas.worldview.sframesystem.rts.GeneralRTSView;
import mas.worldview.sframesystem.rts.viewport.Brush;
import mas.worldview.sframesystem.rts.viewport.ViewPort;
import mas.worldview.sframesystem.rts.viewport.drawcommands.DrawSelectionBoxCommand;

/**
 *
 * @author Christoph
 */
public class FixedSizeWorldObjectBrush extends Brush {
    
    private int worldObjectType;
    private double widthInCM, heightInCM;
    private boolean isScrolling;
    private Map map;
    private Rectangle2D.Double selectionBox;
    private MouseMotionListener mouseMotionListener;
    private MouseWheelListener mouseWheelListener;
    private MouseListener mouseListener;
    private DrawSelectionBoxCommand drawSelBoxComm;
    private JTextField x_JTF, y_JTF, w_JTF, h_JTF;
    
    public FixedSizeWorldObjectBrush(int worldObjectType, Map map, double widthInCM, double heightInCM) {
        this.widthInCM        = widthInCM;
        this.heightInCM       = heightInCM;
        this.worldObjectType  = worldObjectType;
        this.brushInfoPanel   = initBrushInfoBox();
        this.map              = map;
        this.isScrolling      = false;
    }
    
    //PRIVATE METHODS//
    
    private JPanel initBrushInfoBox() {
        JPanel infoPanel = new JPanel();
        infoPanel.setBackground(Color.RED);
        infoPanel.setLayout(new TableLayout(new double[][]{{0.67,0.33},{TableLayout.FILL}}));
        infoPanel.setOpaque(false);
        JPanel dataPanel = new JPanel();
        dataPanel.setBackground(Color.GREEN);
        dataPanel.setLayout(new TableLayout(new double[][]{{5, 15, 5, TableLayout.FILL, 5, 15, 5, TableLayout.FILL, 5},{10, 20, 5, 20, 5, 20, 5}}));
        dataPanel.setOpaque(false);
        infoPanel.add(dataPanel, "0,0,0,0");
        
        // 5 JLabel, 4 JTextField, 2 JButton
        JLabel x_JL = new JLabel("X:");
        dataPanel.add(x_JL, "1,1,1,1");
        x_JTF = new JTextField();
        x_JTF.setEditable(false);
        dataPanel.add(x_JTF, "3,1,3,1");
        JLabel y_JL = new JLabel("Y:");
        dataPanel.add(y_JL, "5,1,5,1");
        y_JTF = new JTextField();
        y_JTF.setEditable(false);
        dataPanel.add(y_JTF, "7,1,7,1");
        JLabel w_JL = new JLabel("W:");
        dataPanel.add(w_JL, "1,3,1,3");
        w_JTF = new JTextField("300.00");
        w_JTF.setEditable(false);
        dataPanel.add(w_JTF, "3,3,3,3");
        JLabel h_JL = new JLabel("H:");
        dataPanel.add(h_JL, "5,3,5,3");
        h_JTF = new JTextField("300.00");
        h_JTF.setEditable(false);
        dataPanel.add(h_JTF, "7,3,7,3");
        JLabel t_JL = new JLabel("Type: "+worldObjectType);
        dataPanel.add(t_JL, "1,5,5,5");
        
        return infoPanel;
    }
    
    //PROTECTED METHODS//
    
    @Override
    protected void addAllActionListener(final GeneralRTSView generalRTSView) {
        // Add actionlistener rsponsible for selection box update and world object placement.
        final ViewPort vp = generalRTSView.getViewPort();
        if(this.selectionBox == null) {
            this.selectionBox = new Rectangle2D.Double(0, 0, widthInCM/vp.getZoomFactor(), heightInCM/vp.getZoomFactor());
        }
        
        mouseListener = new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {}

            @Override
            public void mousePressed(MouseEvent e) {
                if(e.getButton() == MouseEvent.BUTTON3) {
                    isScrolling = true;
                }
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                if(e.getButton() == MouseEvent.BUTTON1) {
                    double zoomFactor = vp.getZoomFactor();
                    // Check for overlap
                    if(map.doesAreaFit((selectionBox.x*zoomFactor+vp.getVPX()), (selectionBox.y*zoomFactor+vp.getVPY()), selectionBox.width*zoomFactor, selectionBox.height*zoomFactor)) {
                        //TODO Enable resize frame
                        // For now finish placement.
                        RectWorldObject newObject = WorldObjectFactory.createRectWorldObject(worldObjectType, (selectionBox.x*zoomFactor+vp.getVPX()), (selectionBox.y*zoomFactor+vp.getVPY()), widthInCM, heightInCM);
                        map.addRectWorldObject(newObject);
                    }
                    
                    //TODO find a sufficient way for repainting in fullscreen and window mode
                    generalRTSView.repaint();
                }
                
                if(e.getButton() == MouseEvent.BUTTON3) {
                    isScrolling = false;
                }
            }

            @Override
            public void mouseEntered(MouseEvent e) {}

            @Override
            public void mouseExited(MouseEvent e) {}
        };
        
        mouseWheelListener = new MouseWheelListener() {
            @Override
            public void mouseWheelMoved(MouseWheelEvent e) {
                selectionBox.setRect(e.getX(), e.getY(), widthInCM/vp.getZoomFactor(), heightInCM/vp.getZoomFactor());
            }
        };
        
        mouseMotionListener = new MouseMotionListener() {
            Rectangle2D.Double mapInCM = vp.getMapSizeInCM();
            
            @Override
            public void mouseDragged(MouseEvent e) {
                if(isScrolling) {
                    selectionBox.setRect(e.getX(), e.getY(), selectionBox.width, selectionBox.height);
                }
            }

            @Override
            public void mouseMoved(MouseEvent e) {
                double zoomFactor = vp.getZoomFactor();
                double xPos = (e.getX()*zoomFactor+vp.getVPX());
                if(xPos < 0.0) {
                    xPos = 0.0;
                } else if(xPos > mapInCM.getWidth()) {
                    xPos = mapInCM.getWidth();
                }
                
                double yPos = (e.getY()*zoomFactor+vp.getVPY());
                if(yPos < 0.0) {
                    yPos = 0.0;
                } else if(yPos > mapInCM.getHeight()) {
                    yPos = mapInCM.getHeight();
                }
                
                x_JTF.setText(""+xPos);
                y_JTF.setText(""+yPos);
                
                selectionBox.setRect(e.getX(), e.getY(), selectionBox.width, selectionBox.height);
                if(SimulationController.Instance().doesAreaFit((selectionBox.x*vp.getZoomFactor()+vp.getVPX()), (selectionBox.y*vp.getZoomFactor()+vp.getVPY()), selectionBox.width*vp.getZoomFactor(), selectionBox.height*vp.getZoomFactor())) {
                    drawSelBoxComm.setBackgroundColor(Color.GREEN);
                    drawSelBoxComm.setBorderColor(Color.GREEN);
                } else {
                    drawSelBoxComm.setBackgroundColor(Color.RED);
                    drawSelBoxComm.setBorderColor(Color.RED);
                }
                //TODO find a sufficient way for repainting in fullscreen and window mode
                generalRTSView.repaint();
            }
        };
        
        vp.addMouseListener(mouseListener);
        vp.addMouseWheelListener(mouseWheelListener);
        vp.addMouseMotionListener(mouseMotionListener);
    }

    @Override
    protected void addAllDrawCommands(GeneralRTSView generalRTSView) {
        //Note: Keep DrawCommands in instance variables for removing.
        drawSelBoxComm = new DrawSelectionBoxCommand(generalRTSView.getViewPort(), selectionBox, Color.LIGHT_GRAY, Color.DARK_GRAY);
        generalRTSView.addDrawCommand(drawSelBoxComm);
    }

    @Override
    protected void removeAllActionListener(GeneralRTSView generalRTSView) {
        // Remove all ActionListeners
        ViewPort vp = generalRTSView.getViewPort();
        vp.removeMouseListener(mouseListener);
        vp.removeMouseWheelListener(mouseWheelListener);
        vp.removeMouseMotionListener(mouseMotionListener);
    }

    @Override
    protected void removeAllDrawCommands(GeneralRTSView generalRTSView) {
        // Remove all DrawCommands
        generalRTSView.removeDrawCommand(drawSelBoxComm);
    }
    
    //PUBLIC METHODS//
    
    //ACCESSOR METHODS//
    
}