package com.simpleorpg.common;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Properties;

import org.apache.log4j.Logger;

public class PropertiesLoader {
	private static PropertiesLoader instance = new PropertiesLoader();
	private HashMap<String, Properties> propertiesMap = new HashMap<String,Properties>();
	private Logger logger = Logger.getLogger(PropertiesLoader.class);
	
	private PropertiesLoader() {
	}
	
	public static PropertiesLoader getInstance() {
		return instance;
	}
	
	public Properties loadProperties(String propertiesFile) {
		 Properties properties = propertiesMap.get(propertiesFile);
		 
		 if (properties == null) {
			 properties = new Properties();
			 try {
				 InputStream in = getClass().getResourceAsStream("/" + propertiesFile);
				 properties.load(in);
				 propertiesMap.put(propertiesFile, properties);
			 } catch (IOException ex) {
				 logger.error("Unable to load " + propertiesFile, ex);
			 }
		 }
		 return properties;
	 }

}
