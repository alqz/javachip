package org.zeroturnaround.model;

import java.io.File;

public class Conversion {
  private File inputFile;

  public File getInputFile() {
    return inputFile;
  }

  public void setInputFile(File inputFile) {
    this.inputFile = inputFile;
  }
}
