/**
 * Copyright (c) 2011 Martin M Reed
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package net.hardisonbrewing.signingserver.service.icon;

public class Icons {

    public static final int APPLICATION_INDICATOR_SIZE = 64;

    public static final String LOGO_URL = "/blackberry_logo.svg";

    public static final String COLOR_UNKNOWN = "rgb(186,186,186)";
    public static final String COLOR_SUCCESS = "rgb(126,186,124)";
    public static final String COLOR_FAILURE = "rgb(186,88,88)";
}
