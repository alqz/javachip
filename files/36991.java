package com.snc.automation.autobot;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.regex.Pattern;

/**
 * Static contract for child classes:
 *   public static final CommandMeta META;
 */
public abstract class Command implements Runnable, LanguageConstants {
	public static final String TO = ": ";
	
	protected final AutoBot mBot;	
	protected final String mChannel;
	protected final String mSender;
	protected final String mLogin;
	protected final String mHostname;
	protected final String mMessage;
	
	public static final class CommandMeta {
		public static final long FLAG_BOTNET = 0x01;
		
		private final String mName;
		private final String mSynopsis;
		private final String mUsage;
		private final Class<? extends Command> mCommandClass;
		private final long mFlags;
		
		public CommandMeta(String name, String synopsis, String usage, Class<? extends Command> commandClass) {
			mName = name.toLowerCase();
			mSynopsis = synopsis;
			mUsage = usage;
			mCommandClass = commandClass;
			mFlags = 0;
		}
		
		public CommandMeta(String name, String synopsis, String usage, Class<? extends Command> commandClass, long flags) {
			mName = name.toLowerCase();
			mSynopsis = synopsis;
			mUsage = usage;
			mCommandClass = commandClass;
			mFlags = flags;
		}		

		public String getName() {
			return mName;
		}

		public String getSynopsis() {
			return mSynopsis;
		}

		public String getUsage() {
			return mUsage;
		}		

		public long getFlags() {
			return mFlags;
		}

		public boolean isFlagged(long flag) {
			return ((mFlags & flag) == flag);
		}
		
		public Command instantiate(AutoBot bot, String channel, String sender, String login, String hostname, String message) {
			Constructor<? extends Command> constructor;
			try {
				constructor = mCommandClass.getConstructor(
						AutoBot.class, String.class, String.class, String.class, String.class, String.class
				);
				
				return constructor.newInstance(bot, channel, sender, login, hostname, message);
			} catch (SecurityException e) {
				throw new RuntimeException(e);
			} catch (NoSuchMethodException e) {
				throw new RuntimeException(e);
			} catch (IllegalArgumentException e) {
				throw new RuntimeException(e);
			} catch (InstantiationException e) {
				throw new RuntimeException(e);
			} catch (IllegalAccessException e) {
				throw new RuntimeException(e);
			} catch (InvocationTargetException e) {
				throw new RuntimeException(e);
			}
		}
	}
	
	public static String getNextArgument(String message) {
    	int pos = message.indexOf(' ');
    	return message.substring(0, (pos == -1 ? message.length() : pos));		
	}
	
	public static String getArgument(String message) {
    	int pos = message.indexOf(' ');
    	if (pos == -1) {
    		return "";
    	}
    	
    	return message.substring(pos + 1);
	}
	

	public static String getArgument(String message, int argumentIndex) {
		int i = 0;
		int start = 0;
		int lastStart = 0;
		int end = 0;
		for ( ; i <= argumentIndex && end != -1; ++i) {
			lastStart = start;
			end = message.indexOf(' ', start);
			start = end + 1;
		}
		
		if (end == -1) {
			if (i <= argumentIndex)
				return null;
			
			end = message.length();
		}
		
		return message.substring(lastStart, end);
	}


	public static ArrayList<String> getArguments(String message) {
		ArrayList<String> result = new ArrayList<String>();
		Scanner sc = new Scanner(message);
		Pattern pattern = Pattern.compile(
			"\"[^\"]*\"" +
			"|'[^']*'" +
			"|[A-Za-z0-9']+"
	    	);
		String token;

		while ((token = sc.findInLine(pattern)) != null) {
			result.add(trimQuotes(token));
		}

		return result;
	}
	
	public static String trimQuotes(String text) {
		String result = text;	
		result = result.replaceAll("^\"|\"$", "");//double quotes gone
		result = result.replaceAll("^'|'$", "");//single quotes gone

		return result;
	}	

	public Command(AutoBot bot, String channel, String sender, String login, String hostname, String message) {
		mBot = bot;
		mChannel = channel;
		mSender = sender;
		mLogin = login;
		mHostname = hostname;
		mMessage = message;
	}

	public final AutoBot getBot() {
		return mBot;
	}

	public final String getChannel() {
		return mChannel;
	}

	public final String getSender() {
		return mSender;
	}

	public final String getLogin() {
		return mLogin;
	}

	public final String getHostname() {
		return mHostname;
	}

	public final String getMessage() {
		return mMessage;
	}
	
	public final StringBuffer toSender() {
		return new StringBuffer(mSender).append(TO);
	}
}
