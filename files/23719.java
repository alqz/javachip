package dart.tower;

import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;

import dart.Consts;
import dart.Player;
import dart.gui.ImageManager;
import dart.runner.Runner;

public class Tower extends Square{
	
//--------------------------------
//Fields	
//--------------------------------

	//Inherits posx and posy from square
	
	//Integer index to hold which players owns this tower
	protected Player owner;
	
	//cost to purchase the tower
	protected static double purchaseCost=10;
	
	//Variables to specify the current stats of the tower
	protected double damage=10,range=4,currentHealth=10,originalHealth=10;
	//Variables to specify the exponent by which the stats will upgrade
	protected double damageExp=1.4,rangeExp=1.15,reloadSpeedExp=0.9,healthExp=1.2;
	//Variables to specify the current cost to upgrade
	protected double damageCost=7,rangeCost=10,reloadSpeedCost=10,healthCost=5;
	//Variables to hold the exponent by which the cost is multiplied
	protected double damageCostExp=1.5,rangeCostExp=1.6,reloadSpeedCostExp=1.25,healthCostExp=1.2;
	
	
	//Long to hold how much time must elapse before the tower can attack again.
	protected long reloadSpeed=1000;
	//Long to hold the time the tower last fired
	protected long lastFired;
	
	//Lock onto a runner to keep the tower 
	protected Runner runnerLock;
	
	//The (radian) angle at which the tower's turret will be drawn on top of it
	protected double turretAngle;
	
	//The image for the turret.
	private Consts.TowerType _type;
	
//--------------------------------
//Constructors	
//--------------------------------
	
	//Basic constructor for a tower
	public Tower(int x, int y, Consts.TowerType type)
	{
		//Create a square
		super(x, y, Consts.TextureType.TOWER);
		
		//Specify that this space is not buildable
		this.setBuildable(false);
		
		//Specify that the tower is not walkable
		this.setWalkable(false);
		//Set the turret picture
		this._type=type;
		//Initialize the turretAngle to an arbitrary number
		this.turretAngle=0;
	}
	
	public Tower(int x, int y, Player p)
	{
		//Create a square
		super(x, y, Consts.TextureType.TOWER);
		
		//Specify that this space is not buildable
		this.setBuildable(false);
		
		//Specify that the tower is not walkable
		this.setWalkable(false);
		//Set the turret picture
		this._type=Consts.TowerType.BASIC;
		//Initialize the turretAngle to an arbitrary number
		this.turretAngle=0;
		
		this.owner=p;
	}
	
//--------------------------------
//Methods
//--------------------------------
	
	
//Getter Methods

	public Player getOwner() { return this.owner; }
	public int    getOwnerIndex()	{ return owner.getIndex(); }
	public int    getTeam()	 { return owner.getTeam(); }
	
	public Consts.TowerType getType() { return _type; }
	
	public static double getPurchaseCost()	{ return purchaseCost; }
	
	public double getDamage() { return this.damage; }
	public double  getRange() { return this. range; }
	public double  getArmor() { return this. currentHealth; }
	public long   getSpeed() { return this. reloadSpeed; }
	
	public double getDamageCost() { return this.damageCost; }
	public double  getRangeCost() { return this.rangeCost;  }
	public double  getSpeedCost() { return this.reloadSpeedCost;  }
	public double  getArmorCost() { return Math.floor(this.currentHealth*this.healthExp)/10.0;  }
	
	public double getTurretAngle()	{ return this.turretAngle;	}
	public Runner  getRunnerLock()	{ return this.runnerLock;	}
	public int getWidth() { return ImageManager.getResizedTowerTexture(_type).getWidth(); }
	public int getHeight() { return ImageManager.getResizedTowerTexture(_type).getHeight(); }
	
//Setter Methods

	//Takes in screen coordinates
	public void setTurretAngle(double x0, double y0, double x1, double y1)
	{
		this.turretAngle=Math.atan2(y0-y1,x0-x1);
	}
	
	//Set the runnerLock onto a specific runner
	public void setRunnerLock(Runner r)
	{
		this.runnerLock=r;
	}
	
	//Set the player owner of this tower
	public void setOwner(Player owner)
	{
		this.owner = owner;
	}
	
	public void setTurretAngle(Runner r)
	{
		turretAngle=Math.PI+(Math.atan2((double) (posY-r.getPrecisePosY()), (double) (posX-r.getPrecisePosX())));
	}
	
	
		//--------------------------------//
	
	public void deductHealth(int damage)
	{
		currentHealth-=damage;
	}

//Runner methods

	//Determine whether the tower can attack a runner yet
	public boolean canAttack(long time)
	{
		//If the tower's health is 0 or below, return false
		if(currentHealth<=0) return false;
		
		//If the tower has waited a sufficient amount of time, say yes
		if(time - lastFired > reloadSpeed) return true;
		//Otherwise say no.
		return false;
	}
	
	//Attacks the runner and returns whether the runner has died or not
	public boolean attackRunner(Runner r,long time)
	{
		boolean died; 
		
		//Make the tower face the runner
		setTurretAngle(r);
				
		//Deal damage to the runner and find out if it died or not
		
		died = r.takeDamage(getDamage());
		//Reset the time the tower last fired
		lastFired=time;
		
		//Return whether the runner died or not
		return died;			
	}
	
	//Calculates the distance between a runner and a tower and returns whether the runner is in range
	public boolean runnerIsWithinRange(Runner r)
	{
		//Calculate the difference between the x and y coordinates
		int x = posX - r.getPosX();
		int y = posY - r.getPosY();
		
		//Check if the fields are within range
		if( Math.sqrt( (double) (x*x+y*y) ) <= range) return true;
		else return false;
	}
	
	
//Upgrade Methods
	
	
	public void upgradeDamage()
	{
		//Upgrade the damage
		this.damage=Math.floor(this.damage*this.damageExp);
		//Increase the cost to upgrade next time.
		this.damageCost=Math.floor(this.damageCost*this.damageCostExp);
	}
	public void upgradeRange()
	{
		//Upgrade the range
		this.range=Math.floor(this.range*this.rangeExp*10.0)/10.0;
		
		//Increase the cost to upgrade next time.
		this.rangeCost=Math.floor(this.rangeCost*this.rangeCostExp);
	}
	public void upgradeSpeed()
	{
		//Upgrade the speed
		this.reloadSpeed=(long) Math.floor( ((double) this.reloadSpeed )*this.reloadSpeedExp);
		//Increase the cost to upgrade next time.
		this.reloadSpeedCost=Math.floor(this.reloadSpeedCost*this.reloadSpeedCostExp);
	}
	public void upgradeArmor()
	{
		//If the turret's out of armor
		if(this.currentHealth<=0)
		{
			//Reset the armor to 10 health
			this.currentHealth=10;
			
			//Set the cost to the current health times the health exponent;
			this.healthCost=this.currentHealth*this.healthExp;
			return;
		}
		
		//Upgrade the armor
		this.currentHealth=Math.floor(this.currentHealth*this.healthExp);
		//Increase the cost to upgrade next time.
		this.healthCost=Math.floor(this.healthCost*this.healthExp);
	}
	
//Paint Method
	
	@Override
	public void paint(Graphics2D g, int x, int y) {
		g.drawImage(ImageManager.getResizedTowerTexture(_type), x, y, null);
		
		BufferedImage resizedTurret = ImageManager.getResizedTurret(_type);
		AffineTransform at = new AffineTransform();
		at.rotate(turretAngle + Math.PI / 2, resizedTurret.getWidth() / 2, resizedTurret.getHeight() / 2);
		
		AffineTransformOp rotateOp = new AffineTransformOp(at, AffineTransformOp.TYPE_BILINEAR);
		BufferedImage rotatedTurret = rotateOp.filter(resizedTurret, null);
		
		//If the Tower can't shoot, don't draw the turret to show that it's out of order
		if(currentHealth>0) g.drawImage(rotatedTurret, x, y, null);
	}
}
