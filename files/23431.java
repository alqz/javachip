package mod_nw;
import net.minecraft.src.ItemBlock;
import net.minecraft.src.ItemStack;
public class ItemSakuraSapling extends ItemBlock 
{
    public ItemSakuraSapling(int par1)
    {
        super(par1);
        this.setMaxDamage(0);
        this.setHasSubtypes(true);
    }

    /**
     * Returns the metadata of the block which this Item (ItemBlock) can place
     */
    public int getMetadata(int par1)
    {
        return par1 ;
    }
    public int getIconFromDamage(int par1)
    {
        return mod_nw.dsakuraSapling.getBlockTextureFromSideAndMetadata(0, par1);
    }
	@Override
	public String getTextureFile() {
		return CommonProxy.BLOCK_PNG;
	}
	@Override
	public String getItemNameIS(ItemStack itemstack) {
		return getItemName() + "." + itemstack.getItemDamage();
	}
}
