package com.rpg.utils.collectors;

import com.rpg.config.Config;

/**
 * User: alexander
 * Date: 02.03.12
 * Time: 15:38
 */
public class ItemCollector extends CollectorImpl {
    private static final ItemCollector instance = new ItemCollector();

    private ItemCollector() {
    }

    public static ItemCollector get() {
        return instance;
    }

    public void itemsImport() {
        ICollector collector = new CollectorImpl();
        collector.importItems(Config.get().getItemsBasePath());
    }

    public String getItemName(int itemID) {
        return itemMap.get(itemID).getName();
    }

    public int getItemType(int itemID) {
        return itemMap.get(itemID).getType();
    }

    public int getItemGrade(int itemID) {
        return itemMap.get(itemID).getGrade();
    }

    public String getItemText(int itemID) {
        return itemMap.get(itemID).getText();
    }

    public int getItemMDefence(int itemID) {
        return itemMap.get(itemID).getmDefence();
    }

    public int getItemPDefence(int itemID) {
        return itemMap.get(itemID).getpDefence();
    }

    public int getItemPrice(int itemID) {
        return itemMap.get(itemID).getPrice();
    }
}


