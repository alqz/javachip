package algorithm;

import java.text.DecimalFormat;
import java.util.LinkedList;
import java.util.List;

/**
 * Repräsentiert den Status der Umgebung der Kamera
 */
public class RoomState {
	/**
	 * Liste der Körperobjekte die sich in der Umgebung befinden
	 */
	private List<Body> bodyList;
	/**
	 * falls während einer Suche Körperobjekte verfallen, werden sie erst nach
	 * der Suche gelöscht
	 */
	private boolean	lock = false;
	/**
	 * Ausgabeformat für Gleitkommazahlen
	 */
	private	DecimalFormat df = new DecimalFormat("#.##");

	/**
	 * Konstruktor; initialisiert Körperliste
	 */
	public RoomState() {
		this.bodyList = new LinkedList<Body>();
	}
	
	/**
	 * Fügt der Körperliste neues Objekt hinzu
	 * 
	 * @param newBody hinzuzufügender Körper
	 */
	public void addBody(Body newBody) {
		this.bodyList.add(newBody);
	}
	
	/**
	 * Gibt den Umgebungstatus als String wieder
	 */
	public String toString() {
		String out = "";
		for (Body body : this.bodyList) {
			out += 	df.format(body.getProbability()) + "% (" +
					df.format(body.getPos().x) + ", " +
					df.format(body.getPos().y) + ", " +
					df.format(body.getPos().z) + ")\n";
		}
		
		return out;
	}
	
	/**
	 * Triggert Verfall von Körperwahrscheinlichkeiten; bei 0% werden Körper
	 * gelöscht
	 */
	public void decay() {
		List<Body> decayed = new LinkedList<Body>();
		
		for (Body body : this.bodyList) {
			body.decay();
			if (body.getProbability() == 0) decayed.add(body);
		}
		
		while (this.isLocked()) {};
		this.bodyList.removeAll(decayed);
	}
	
	/**
	 * Ermöglicht das Sperren der Körperliste; wenn "false" können Körper
	 * gelöscht werden
	 * 
	 * @param lock Sperrung
	 */
	public void lock(boolean lock) {
		this.lock = lock;
	}

	/**
	 * löscht die Körperliste
	 */
	public void reset() {
		while (this.isLocked()) {};
		this.bodyList = new LinkedList<Body>();
	}
	
	public List<Body> getBodyList() {
		return this.bodyList;
	}

	public boolean isLocked() {
		return this.lock;
	}
}
