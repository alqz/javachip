/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package unb.sica.daos;

import br.com.caelum.vraptor.ioc.Component;
import org.hibernate.Session;
import unb.sica.entities.Abrigada;

/**
 *
 * @author caiquepeixoto
 */
@Component
public class AbrigadaDao extends GenericDao<Abrigada> {

    public AbrigadaDao(Session session) {
        super(session);
    }

}
