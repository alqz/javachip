package net.minecraft.src;

import java.util.List;
import java.util.Random;

public class BiomeGenSpace extends BiomeGenBase
{
    public BiomeGenSpace(int par1)
    {
        super(par1);
        spawnableCreatureList.clear();
        topBlock = (byte)0;
        fillerBlock = (byte)0;
        biomeDecorator.treesPerChunk = 0;
        biomeDecorator.deadBushPerChunk = 0;
        biomeDecorator.reedsPerChunk = 0;
        biomeDecorator.cactiPerChunk = 0;
    }
    
}