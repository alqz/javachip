package at.moma.gui.login;

import java.io.File;
import java.io.FileNotFoundException;

import net.miginfocom.swt.MigLayout;

import org.apache.log4j.Logger;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Layout;
import org.eclipse.swt.widgets.Text;

import at.moma.gui.GuiUtil;
import at.moma.gui.ISubWindow;
import at.moma.gui.SwitchSubWindowListener;
import at.moma.service.MessageService;
import at.moma.service.SettingsUtil;

public class LoginComposite extends Composite implements ISubWindow {

	private static Logger log = Logger.getLogger(LoginComposite.class);

	private MessageService mService = MessageService.instance();
	private Label lblUsername, lblPassword;
	private Text txtUsername, txtPassword;
	private Button btnLogin, btnNewUser, btnCancel;
	private Image splash;
	private Color white, grey;
	private Layout layout;

	public LoginComposite(Composite parent, int style) {
		super(parent, style);
		init();
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

	private void init() {
		log.debug("initiate widgets");
		layout = new MigLayout("wrap 2, right, insets dialog",
				"[right]r[fill]", "[]r[]u[]");
		setLayout(layout);

		splash = new Image(getDisplay(), "img" + File.separator + "splash.png");
		setBackgroundImage(splash);

		// colors
		white = getDisplay().getSystemColor(SWT.COLOR_WHITE);
		grey = getDisplay().getSystemColor(SWT.COLOR_TITLE_INACTIVE_FOREGROUND);

		try {
			// username
			lblUsername = new Label(this, SWT.RIGHT);
			lblUsername.setText(mService.getMessage("username"));

			lblUsername.setBackground(white);
			txtUsername = new Text(this, SWT.LEFT | SWT.BORDER);

			// passwort
			lblPassword = new Label(this, SWT.RIGHT);
			lblPassword.setText(mService.getMessage("password"));
			lblPassword.setBackground(white);
			txtPassword = new Text(this, SWT.LEFT | SWT.BORDER | SWT.PASSWORD);

			// new user
			btnNewUser = new Button(this, SWT.PUSH);
			btnNewUser.setText(mService.getMessage("new_user"));
			btnNewUser.setToolTipText(mService.getMessage("tooltip_new_user"));
			btnNewUser.setLayoutData("split 3, right, span");
			btnNewUser.addSelectionListener(new SwitchSubWindowListener(getShell(), this, CreateNewUserAccount.class.getName()));

			// cancel
			btnCancel = new Button(this, SWT.PUSH);
			btnCancel.setText(mService.getMessage("cancel"));
			btnCancel.addSelectionListener(new at.moma.gui.DisposeListener(this));

			// login
			btnLogin = new Button(this, SWT.PUSH);
			btnLogin.setText(mService.getMessage("login"));
			btnLogin.addSelectionListener(new LoginListener(this));

		} catch (FileNotFoundException e) {
			log.fatal("lang file not available");
			GuiUtil.switchSubWindow(getShell(), this, NoLangFile.class.getName());
		}

		// set the sequence of the different controls if you move the focus with tab
		 Control[] tabList = new Control[] {txtUsername, txtPassword, btnLogin, btnCancel, btnNewUser};
		 setTabList(tabList);
		 
		 if(!SettingsUtil.isEstablishedUser()){
			 disableLogin();
		 }
	}

	@Override
	public void freeResources() {
		splash.dispose();
	}
	
	public void disableLogin() {
		btnLogin.setEnabled(false);
		txtPassword.setEnabled(false);
		txtUsername.setEnabled(false);
		btnNewUser.setFocus();
		lblUsername.setForeground(grey);
		lblPassword.setForeground(grey);
	}
	
	protected String getPassword(){
		return txtPassword.getText();
	}
	
	protected String getUsername(){
		return txtUsername.getText();
	}
}
