/**
 *
 * 	Copyright $2011 Frederik Hahne
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package de.atomfrede.android.thwdroid.messages.activity;

import java.util.Collection;

import org.jivesoftware.smack.*;
import org.jivesoftware.smack.packet.Presence;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import de.atomfrede.android.thwdroid.messages.adapter.ContactListAdapter;
import de.usetable.model.ModelListActivity;

public class RosterActivity extends ModelListActivity implements RosterListener {

	public static String EXTRA_ROSTER_ITEM = "EXTRA_ROSTER_ITEM";
	ProgressDialog dialog;
	ContactListAdapter contactListAdapter;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// ConnectionUtil.getStub().getConnection().getRoster()
		// .addRosterListener(this);
		contactListAdapter = new ContactListAdapter(this, getRosterEntries());

		setListAdapter(contactListAdapter);
	}

	private RosterEntry[] getRosterEntries() {
		try {
			// return ThwStub.getInstance().connection.getRoster().getEntries().toArray(new RosterEntry[1]);
		} catch (Exception e) {

		}
		return null;
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		// startMuttiImgMessageComposing(this.getListAdapter().getItem(position)
		// .toString());
		startMuttiImgMessageComposing(contactListAdapter.getName(position));
	}

	// @Override
	// public boolean onCreateOptionsMenu(Menu menu) {
	// MenuInflater inflater = getMenuInflater();
	// inflater.inflate(R.menu.roster_menu, menu);
	// return true;
	// }

	// @Override
	// public boolean onOptionsItemSelected(MenuItem item) {
	// switch (item.getItemId()) {
	// case R.id.roster_menu_logout:
	// logout();
	// break;
	// }
	// return true;
	// }

	// @Override
	// public void onBackPressed() {
	// Intent goToHomescreen = new Intent(Intent.ACTION_MAIN);
	// goToHomescreen.addCategory(Intent.CATEGORY_HOME);
	// goToHomescreen.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
	// startActivity(goToHomescreen);
	// }

	private void startMuttiImgMessageComposing(String rosterEntry) {
		Intent composingIntent = new Intent(this, ComposeMessageActivity.class);
		composingIntent.putExtra(RosterActivity.EXTRA_ROSTER_ITEM, rosterEntry);
		// composingIntent.putExtra(ActionBarActivity.GD_ACTION_BAR_TITLE,
		// "Compose MuttiImg Message");
		startActivity(composingIntent);
	}

	@Override
	public void entriesAdded(Collection<String> arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void entriesDeleted(Collection<String> arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void entriesUpdated(Collection<String> arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void presenceChanged(Presence arg0) {
		// TODO Auto-generated method stub
		runOnUiThread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				contactListAdapter = new ContactListAdapter(RosterActivity.this, getRosterEntries());
				setListAdapter(contactListAdapter);
			}
		});

	}

	// @Override
	// public void onRefresh() {
	// // TODO Auto-generated method stub
	//
	// }
}
