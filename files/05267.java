/**
 * 
 */
package net.skcomms.dtc.shared;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONString;

/**
 * @author jujang@sk.com
 */
@SuppressWarnings("serial")
public class ApiMeta implements Serializable {

  private List<ApiParameter> params;

  private IpInfoModel ipInfo;

  private String encoding;

  private String path;

  private String appName;

  private String apiNumber;

  private String defaultProtocol;

  public ApiTestRequest createApiTestRequest(Map<String, String> values) {
    ApiTestRequest request = new ApiTestRequest();

    request.setPath(this.path);
    request.setEncoding(this.encoding);
    request.setAppName(this.appName);
    request.setApiNumber(this.apiNumber);
    request.setProtocol(this.getProtocol(values));
    request.setParameters(this.getApiParameters(values));

    return request;
  }

  public String getApiNumber() {
    return this.apiNumber;
  }

  private List<ApiParameter> getApiParameters(Map<String, String> values) {
    List<ApiParameter> result = new ArrayList<ApiParameter>();
    for (ApiParameter param : this.params) {
      ApiParameter p = new ApiParameter(param);
      p.setValue(values.get(param.getKey()));
      result.add(p);
    }
    result.add(new ApiParameter("IP", values.get("IP")));

    return result;
  }

  public String getAppName() {
    return this.appName;
  }

  public String getCndFieldName() {
    for (ApiParameter param : this.params) {
      if (param.isCndQuery()) {
        System.out.println("param,, : " + param.toString());
        return param.getKey();
      }
    }
    return null;
  }

  /**
   * @return
   */
  public String getEncoding() {
    return this.encoding;
  }

  public String getInitIpText() {
    if (!this.getIpInfo().getIpText().isEmpty()) {
      return this.getIpInfo().getIpText();
    } else if (this.getIpInfo().getOptions().size() > 0) {
      return this.getIpInfo().getOptions().get(0).getValue();
    } else {
      return "";
    }
  }

  /**
   * @return
   */
  public IpInfoModel getIpInfo() {
    return this.ipInfo;
  }

  public ApiParameter getParameter(String key) {
    for (ApiParameter param : this.params) {
      if (param.getKey().equals(key)) {
        return param;
      }
    }
    return null;
  }

  public List<ApiParameter> getParameters() {
    return this.params;
  }

  /**
   * @return
   */
  public String getPath() {
    return this.path;
  }

  private String getProtocol(Map<String, String> values) {
    ApiParameter portParam = this.getParameter("PORT");
    if (portParam.isObject()) {
      String portValue = values.get("PORT");
      JSONObject jsonObject = portParam.getJsonObject();
      JSONString jsonValue = (JSONString) jsonObject.get(portValue);
      return (jsonValue == null) ? this.defaultProtocol : jsonValue.stringValue();
    } else {
      return this.defaultProtocol;
    }
  }

  public void setApiNumber(String apiNumber) {
    this.apiNumber = apiNumber;
  }

  public void setAppName(String appName) {
    this.appName = appName;
  }

  public void setDefaultProtocol(String defaultProtocol) {
    this.defaultProtocol = defaultProtocol;
  }

  public void setEncoding(String encoding) {
    this.encoding = encoding;
  }

  public void setIpInfo(IpInfoModel ipInfo) {
    this.ipInfo = ipInfo;
  }

  public void setParameters(List<ApiParameter> params) {
    this.params = params;
  }

  public void setPath(String path) {
    this.path = path;
  }

  @Override
  public String toString() {
    return this.encoding + ", " + this.ipInfo.toString() + ", " + this.params.toString();
  }

}
