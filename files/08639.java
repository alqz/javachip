package com.troxellophilus.myplugin;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.*;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class MyPlugin extends JavaPlugin
{
	public ArrayList<Location> blockProtection = new ArrayList<Location>();
	public ArrayList<Shafter> shafterList = new ArrayList<Shafter>();
	
	// Runs when the plugin is enabled.
	public void onEnable()
	{
		// Designates a new MyListener to listen for events. MyListener is passed the main class of the plugin.
		getServer().getPluginManager().registerEvents(new MyListener(this), this);
		
		// Sets an executor for the command /rules. When the command is entered, this onCommand is run.
		getCommand("rules").setExecutor(new CommandExecutor(){
			public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
			{
				List<String> rules = getConfig().getStringList("rules");
				
				// For each item in rules, print that item to the player.
				for(String item : rules)
				{
					sender.sendMessage(item);
				}
				return true;
			}
		});
		
		// Get the config file information.
		getConfig().options().copyDefaults(true);
		saveConfig();
	}
	
	// Takes commands (/<command>) typed by a CommandSender and deals with them.
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
	{
		/**
		 * The /opuser command. First check if the sender is an instance of Player, then check to see if the
		 * sender has permission to use this command.
		 * 
		 * If sender has permission, check to see if the sender provided the correct # of arguments.
		 * 
		 * If the correct # of arguments are passed, get the server, get the player (getOfflinePlayer(name) finds
		 * both online and offline players rather than just online) and set that player to OP. Send a success message
		 * to both the sender and the OP'd player.
		 */
		if(cmd.getName().equalsIgnoreCase("opuser"))
		{
			if(sender instanceof Player)
			{
				// permCheck returns true if the provided player has the provided permission.
				if(!permCheck((Player)sender, "opuser.command"))
				{
					sender.sendMessage(ChatColor.RED+"No Permission");
					return true;
				}
			}
			
			if(args.length != 1 || args[0] == null)
			{
				sender.sendMessage(ChatColor.RED+"ERROR: /opuser [USERNAME]");
				return true;
			}
			
			getServer().getOfflinePlayer(args[0]).setOp(true);
			
			sender.sendMessage(args[0] + " was promoted to an OP!");
			sendMessage(args[0], "You have been promoted to an OP!");
		}
		
		/**
		 * The /tele command. First check if the sender is a player. If so, check permissions, if not
		 * return true (since non-players can't teleport). Then check the args. Find the location of the
		 * provided player, teleport them to the provided location using the teleport(location) method.
		 */
		if(cmd.getName().equalsIgnoreCase("tele"))
		{
			if(sender instanceof Player)
			{
				if(!permCheck((Player)sender, "tele.command"))
				{
					sender.sendMessage(ChatColor.RED+"No Permission");
					return true;
				}
			}
			else
			{
				return true;
			}
			
			// There should be 1 argument, and it should not be null. This /tele will only allow players
			// to teleport themselves to other players. Cannot teleport other players to other players.
			if(args.length != 1 || args[0] == null)
			{
				sender.sendMessage(ChatColor.RED+"ERROR: /tele [player]");
				return true;
			}
			
			Location l = Bukkit.getPlayer(args[0]).getLocation();
			
			Bukkit.getPlayer(sender.getName()).teleport(l);
			return true;
		}
		
		/**
		 * The /shafter command. First do appropriate checks of player and permissions. Shafter has multiple
		 * constructors, so it can be passed zero arguments or 3 arguments. If neither are passed, provide an error message.
		 */
		if(cmd.getName().equalsIgnoreCase("shafter"))
		{
			Shafter s;
			
			if(sender instanceof Player)
			{
				if (!permCheck((Player)sender, "shafter.command"))
				{
					sender.sendMessage(ChatColor.RED + "No permission");
					return true;
				}
			}

			if(args.length == 0)
			{
				s = new Shafter();
				
				// Need to set the item in the player's hand to a gold hoe. Note, this will replace
				// the item currently in the player's hand with a gold hoe.
				getServer().getPlayer(sender.getName()).setItemInHand(s.getHoe());
				
				// Add this shafter to the list so it can be found in MyListener.
				shafterList.add(s);
				
				// Send a confirmation message.
				sender.sendMessage(ChatColor.BLUE+"New Shafter size 3L 3W 3H");
			}
			else if(args.length == 3)
			{
				s = new Shafter(Integer.parseInt(args[0]), Integer.parseInt(args[1]), Integer.parseInt(args[2]));
				getServer().getPlayer(sender.getName()).setItemInHand(s.getHoe());
				shafterList.add(s);
				sender.sendMessage(ChatColor.BLUE+"New Shafter size " + s.getX() + "L " + s.getY() + "W " + s.getZ() + "H");
			}
			else
			{
				sender.sendMessage(ChatColor.RED+"ERROR: /shafter (l) (w) (h)");
			}
		}
		
		return true;
	}
	
	/**
	 * Method to simply send a message to any specified player.
	 * @param username String name of player to send message to.
	 * @param message String message to send.
	 */
	private void sendMessage(String username, String message)
	{
		Player player = getServer().getPlayerExact(username);
		
		if(player != null)
		{
			player.sendMessage(message);
		}
	}
	
	/**
	 * Checks to see if the provided player has the provided permission.
	 * @param player Player to check permission of.
	 * @param permission Permission to check.
	 * @return Returns true if the player has permission, else false.
	 */
	private boolean permCheck(Player player, String permission)
	{
		if(player.isOp() || player.hasPermission(permission)) return true;
		return false;
	}
}
