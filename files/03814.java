import org.tmatesoft.sqljet.core.table.SqlJetDb;
import org.tmatesoft.sqljet.core.SqlJetTransactionMode;
import org.tmatesoft.sqljet.core.table.ISqlJetTable;
import org.tmatesoft.sqljet.core.SqlJetException;
import org.tmatesoft.sqljet.core.table.ISqlJetCursor;

import java.io.File;
import java.util.Set;
import java.util.LinkedList;

public class Sqlite
{

	public static final String DB_NAME = "db.sqlite";

	private SqlJetDb db;
	private File dbFile;

	public Sqlite() 
	{
		this.dbFile = new File(DB_NAME);
	}

	private void openDatabase() throws Exception
	{
		this.db = SqlJetDb.open(dbFile, true);
	}
	private void openDatabaseWrite()
	{
		try {
			openDatabase();
			this.db.beginTransaction(SqlJetTransactionMode.WRITE);
		} catch (Exception e) {
			System.out.println(">>> Could not open the database <<<");
			e.printStackTrace();
		}
	}

	private void openDatabaseRead()
	{
		try {
			openDatabase();
			this.db.beginTransaction(SqlJetTransactionMode.READ_ONLY);
		} catch (Exception e) {
			System.out.println(">>> Could not open the database <<<"); e.printStackTrace();
		}
	}

	private void closeDatabase()
	{
		try {
			this.db.commit();
			this.db.close();
		} catch (Exception e) {
			System.out.println(">>> Could not close the database <<<");
			e.printStackTrace();
		}
	}

	public boolean createTable(String tableName, String sqlStatement)
	{
		openDatabaseWrite();
		try {
			Set<String> tables = db.getSchema().getTableNames();
			// if table already exists, return false
			if (tables.contains(tableName)) {
				return false;
			}
			db.createTable(sqlStatement);
			return true;
		} catch (SqlJetException e) {
			System.out.println(">>> Error while creating table <<<" + "\n" +
					"SQL Statement leading to this is: \n" +
					sqlStatement);
			return false;
		} finally {
			closeDatabase();
		}
	}

	public boolean dropTable(String tableName)
	{
		openDatabaseWrite();
		try {
			Set<String> tables = db.getSchema().getTableNames();
			// if table already exists, return false
			if (!tables.contains(tableName)) {
				return false;
			}
			db.dropTable(tableName);
			return true;
		} catch (SqlJetException e) {
			System.out.println(">>> Error while dropping table <<<");
			return false;
		} finally {
			closeDatabase();
		}
 	}

	public boolean createIndex(String indexName, String sqlStatement)
	{
		openDatabaseWrite();
		try {
			Set<String> indices = db.getSchema().getIndexNames();
			// if indices already exists, return false
			if (indices.contains(indexName)) {
				return false;
			}
			db.createIndex(sqlStatement);
			return true;
		} catch (SqlJetException e) {
			System.out.println(">>> Error while creating index <<<" + "\n" +
					"SQL Statement leading to this is: \n" +
					sqlStatement);
			return false;
		} finally {
			closeDatabase();
		}
	}

	public boolean dropIndex(String indexName)
	{
		openDatabaseWrite(); try {
			Set<String> indices = db.getSchema().getIndexNames();
			// if indices already exists, return false
			if (!indices.contains(indexName)) {
				return false;
			}
			db.dropIndex(indexName);
			return true;
		} catch (SqlJetException e) {
			System.out.println(">>> Error while dropping index <<<");
			return false;
		} finally {
			closeDatabase();
		}
	}
	public void tableInsert(String tableName, String sqlStatement, String sqlStatement2)
	{
		ISqlJetTable table;
		openDatabaseWrite();
		try {
			table = db.getTable(tableName);
			if (sqlStatement2 != null) {
				table.insert(sqlStatement, sqlStatement2);
			} else {
				table.insert(sqlStatement);
			}
		} catch (SqlJetException e) {
			System.out.println(">>> Error while inserting value to table " + tableName + " <<<" + "\n" +
					"SQL Statement leading to this is: \n" +
					sqlStatement);
		} finally {
			closeDatabase();
		}
	}
	public void tableInsert(String tableName, String sqlStatement) 
	{
		tableInsert (tableName, sqlStatement, null);
	}

	public String[][] getValues (String tableName, String indexName, String type, 
							String[] fields, String lookup, Object[] obj1, Object[] obj2,
							boolean deleteValue)
	{
		try {
			LinkedList<String[]> list = new LinkedList<String[]>();
			LinkedList<String> fieldResult; 

			openDatabaseWrite();
			ISqlJetCursor cursor;
			ISqlJetTable table 		= db.getTable(tableName);

			if (type.equalsIgnoreCase("order")) {
				cursor = table.order(indexName);
			} else if (type.equalsIgnoreCase("lookup")) {
				cursor = table.lookup(indexName, lookup);
			} else if (type.equalsIgnoreCase("open")) {
				cursor = table.open();
			} else {
				cursor = table.scope(indexName, obj1, obj2);
			}
			try {
				if (!cursor.eof()) {
					do {
						//System.out.println(cursor.getRowId());
				
						fieldResult = new LinkedList<String>();

						for (String temp : fields) {
						 //	System.out.println(cursor.getString(temp));
							fieldResult.add(cursor.getString(temp));
						}
						list.add(fieldResult.toArray(new String[0]));
						if (deleteValue) {
							cursor.delete();
						}
					} while(cursor.next());
				}
				return list.toArray(new String[0][0]);
			} finally {
				cursor.close();
				closeDatabase();
			}
		} catch (Exception e) {
			e.printStackTrace();		
			closeDatabase();
			return null;
		}
	}
	
//	public void deleteValues();
//	public void updateValues();
}
