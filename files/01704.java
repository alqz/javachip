package plugin.arcwolf.blockdoor.listeners;

import org.bukkit.block.Block;
import org.bukkit.craftbukkit.entity.CraftSheep;
import org.bukkit.entity.Monster;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityChangeBlockEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.ItemSpawnEvent;

import plugin.arcwolf.blockdoor.BlockDoor;
import plugin.arcwolf.blockdoor.Utils.SpecialCaseCheck;

public class BlockDoorEntityListener implements Listener {

    private BlockDoor plugin;

    public BlockDoorEntityListener() {
        plugin = BlockDoor.plugin;
    }

    @EventHandler
    public void onEntityExplode(EntityExplodeEvent event) {
        if (event.isCancelled())
            return;
        int x = event.getLocation().getBlockX();
        int y = event.getLocation().getBlockY();
        int z = event.getLocation().getBlockZ();
        String in_world = event.getLocation().getWorld().getName();

        BreakLoop:
        for(int i = x - 5; i != x + 5; i++)
            for(int j = y - 5; j != y + 5; j++)
                for(int k = z - 5; k != z + 5; k++) {
                    if (plugin.twostatedoorhelper.findCoordinates(i, j, k, in_world) != 2147483647) {
                        event.setCancelled(true);
                        break BreakLoop;
                    }
                }
    }

    @EventHandler
    public void onEntityChangeBlock(EntityChangeBlockEvent event) {
        if (event.isCancelled())
            return;
        if (event.getEntity() instanceof CraftSheep || event.getEntity() instanceof Monster) {
            int x = event.getBlock().getLocation().getBlockX();
            int y = event.getBlock().getLocation().getBlockY();
            int z = event.getBlock().getLocation().getBlockZ();
            String in_world = event.getBlock().getWorld().getName();
            if (plugin.twostatedoorhelper.findCoordinates(x, y, z, in_world) != 2147483647) {
                event.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onItemSpawn(ItemSpawnEvent event) { // for item popping & twostate doors (JIC method) "Just in Case"
        if (event.isCancelled())
            return;
        int blockId2 = event.getEntity().getItemStack().getTypeId();
        int blockID = event.getLocation().getBlock().getTypeId();
        if(blockId2 == 324) blockID = 64;
        else if(blockId2 == 330) blockID = 71;
        if (SpecialCaseCheck.isWatchedBlock(blockID)) {
            int x = event.getLocation().getBlockX();
            int y = event.getLocation().getBlockY();
            int z = event.getLocation().getBlockZ();
            String in_world = event.getLocation().getWorld().getName();
            int twoStateID = plugin.twostatedoorhelper.findCoordinates(x, y, z, in_world);
            Block hdoorBlock = plugin.hdoorhelper.findBlock(x, y, z, in_world);
            if (Math.abs(twoStateID) != 2147483647) {
                //System.out.println("TSID= " + twoStateID + " blockID= " + blockID);
                if (twoStateID >= 0) {
                    int stateID = plugin.twostatedoorhelper.firstStateFindCoordinates(plugin.datawriter.twostate.get(twoStateID), x, y, z);
                    //System.out.println("SID= " + stateID + " OpenClose= " + plugin.datawriter.twostate.get(twoStateID).isOpen + " BlockAT= " + plugin.getWorld(in_world).getBlockAt(x, y, z).getTypeId());
                    if (plugin.datawriter.twostate.get(twoStateID).stateOneContents.get(stateID).blockID == blockID && !plugin.datawriter.twostate.get(twoStateID).isOpen) {
                        //System.out.println("S1 X= " + x + " Y= " + y + " Z= " + z + " world= " + in_world + " blockID= " + blockID);
                        plugin.datawriter.twostate.get(twoStateID).stateOneContents.get(stateID).blockID = 0;
                    }
                    else if (plugin.datawriter.twostate.get(twoStateID).stateTwoContents.get(stateID).blockID == blockID && plugin.datawriter.twostate.get(twoStateID).isOpen) {
                        //System.out.println("S2 X= " + x + " Y= " + y + " Z= " + z + " world= " + in_world + " blockID= " + blockID);
                        plugin.datawriter.twostate.get(twoStateID).stateTwoContents.get(stateID).blockID = 0;
                    }
                }
                else {
                    twoStateID = Math.abs(twoStateID);
                    if (blockId2 == 324 || blockId2 == 330) {
                        int stateID = plugin.twostatedoorhelper.firstStateFindCoordinates(plugin.datawriter.twostate.get(twoStateID), x, y, z);
                        //System.out.println(stateID + " " + (plugin.datawriter.twostate.get(twoStateID).stateTwoContents.get(stateID).blockID == blockID) + " " + (plugin.getWorld(in_world).getBlockAt(x, y, z).getTypeId() == blockID));
                        if (plugin.datawriter.twostate.get(twoStateID).stateOneContents.get(stateID).blockID == blockID && !plugin.datawriter.twostate.get(twoStateID).isOpen) {
                            plugin.datawriter.twostate.get(twoStateID).stateOneContents.get(stateID).blockID = 0;
                        }
                        else if (plugin.datawriter.twostate.get(twoStateID).stateTwoContents.get(stateID).blockID == blockID && plugin.datawriter.twostate.get(twoStateID).isOpen) {
                            plugin.datawriter.twostate.get(twoStateID).stateTwoContents.get(stateID).blockID = 0;
                        }
                    }
                    event.setCancelled(true);
                }
            }
            if (hdoorBlock != null) {
                if (hdoorBlock.getTypeId() == blockID) {
                    event.setCancelled(true);
                }
            }
        }
    }
}