/*
 * JBoss, Home of Professional Open Source.
 * Copyright 2009, Red Hat Middleware LLC, and individual contributors
 * as indicated by the @author tags. See the copyright.txt file in the
 * distribution for a full listing of individual contributors.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package org.jboss.managed.bean.metadata.jbmeta;

import java.lang.reflect.Method;
import java.util.Arrays;

import org.jboss.managed.bean.metadata.MethodMetadata;

/**
 * MethodMetadataImpl
 *
 * @author Jaikiran Pai
 * @version $Revision: $
 */
public class MethodMetadataImpl implements MethodMetadata
{
   /** Method name */
   private String methodName;
   
   /** Method params */
   private String[] params;
   
   /**
    * The declaring class of the method
    */
   private String declaringClass;
   
   public MethodMetadataImpl(Method method)
   {
      this.methodName = method.getName();
      this.declaringClass = method.getDeclaringClass().getName();
      
      // set the params
      Class<?>[] methodParams = method.getParameterTypes();
      this.params = new String[methodParams.length];
      int i = 0;
      for (Class<?> methodParam : methodParams)
      {
         this.params[i++] = methodParam.getName();
      }
      
   }
   
   /**
    * {@inheritDoc}
    */
   @Override
   public String getMethodName()
   {
      return this.methodName;
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public String[] getMethodParams()
   {
      return this.params;
   }

   @Override
   public String getDeclaringClass()
   {
      return this.declaringClass;
   }

   /**
    * @see java.lang.Object#hashCode()
    */
   @Override
   public int hashCode()
   {
      final int prime = 31;
      int result = 1;
      result = prime * result + ((declaringClass == null) ? 0 : declaringClass.hashCode());
      result = prime * result + ((methodName == null) ? 0 : methodName.hashCode());
      result = prime * result + Arrays.hashCode(params);
      return result;
   }

   /**
    * @see java.lang.Object#equals(java.lang.Object)
    */
   @Override
   public boolean equals(Object obj)
   {
      if (this == obj)
         return true;
      if (obj == null)
         return false;
      if (!(obj instanceof MethodMetadataImpl))
         return false;
      MethodMetadataImpl other = (MethodMetadataImpl) obj;
      if (declaringClass == null)
      {
         if (other.declaringClass != null)
            return false;
      }
      else if (!declaringClass.equals(other.declaringClass))
         return false;
      if (methodName == null)
      {
         if (other.methodName != null)
            return false;
      }
      else if (!methodName.equals(other.methodName))
         return false;
      if (!Arrays.equals(params, other.params))
         return false;
      return true;
   }
   
}
