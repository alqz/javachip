package mas;

import java.util.LinkedList;

/**
 *
 * @author Christoph
 */
public abstract class Observable {
    private final LinkedList<Observer> observer;
    private boolean hasChanged;
    
    public Observable() {
        this.observer = new LinkedList<Observer>();
        this.clearChanged();
    }
    
    //PUBLIC METHODS//
    public final void setChanged() {
        this.hasChanged = true;
    }
    
    public final void clearChanged() {
        this.hasChanged = false;
    }
    
    public void addObserver(Observer observer) {
        if(!this.observer.contains(observer)) {
            this.observer.add(observer);
        }
    }
    
    public final void clearObservers() {
        this.observer.clear();
    }
    
    public boolean deleteObserver(Observer observer) {
        return this.observer.remove(observer);
    }
    
    public void notifyObservers(Object arg, int action) {
        if(hasChanged()) {
            for(Observer o : observer) {
                o.update(this, arg, action);
            }
            this.clearChanged();
        }
    }
    
    //ACCESSOR METHODS//
    public final boolean hasChanged() {
        return hasChanged;
    }
}
