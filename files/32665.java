package intranet.interfaces.struts.actions;

import intranet.domain.entities.Disciplina;
import intranet.domain.entities.DisciplinaLetiva;
import intranet.domain.entities.DisciplinaLetivaHorario;
import intranet.domain.entities.Horario;
import intranet.domain.entities.Professor;
import intranet.domain.enums.DiaSemanaEnum;
import intranet.infra.repository.impl.DisciplinaRepositorioImpl;
import intranet.infra.repository.impl.HorarioRepositorioImpl;
import intranet.infra.repository.impl.PessoaRepositorioImpl;
import intranet.infra.struts.IntranetAction;
import intranet.interfaces.params.impl.DisciplinaLetivaHorarioNovoParams;
import intranet.interfaces.params.impl.DisciplinaLetivaNovoParams;
import intranet.interfaces.params.impl.DisciplinaLetivaSearchParams;
import intranet.interfaces.params.impl.HorarioNovoParams;


import intranet.utils.FormUtil;
import intranet.utils.IntranetException;
import intranet.utils.SpringBeanFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.java.Log;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;






import com.opensymphony.xwork2.ActionSupport;

/**
 * 
 * @author Gabriel Cardelli
 * @since Feb 21, 2012 - 12:38:16 AM
 *
 */
@Log
@Controller
public class HorarioAction extends IntranetAction{

	private static final long serialVersionUID = 1L;
	
	@Getter @Setter private List<DisciplinaLetivaHorario> disciplinaLetivaHorario = new ArrayList<DisciplinaLetivaHorario>();
	@Getter @Setter private List<Horario> horarios = new ArrayList<Horario>();
	@Getter @Setter private List<Integer> anos  = new ArrayList<Integer>();
	@Getter @Setter private List<Integer> semestres = new ArrayList<Integer>();
	@Getter @Setter private List<DisciplinaLetiva> disciplinasLetivas = new ArrayList<DisciplinaLetiva>();
	@Getter @Setter private List<DiaSemanaEnum> diasSemana = new ArrayList<DiaSemanaEnum>();
	
		
	@Getter @Setter private Integer id;
	@Getter @Setter private Integer idDisciplinaLetiva;
	@Getter @Setter private DiaSemanaEnum diaSemana;
	@Getter @Setter private Integer horarioId;
	@Getter @Setter private Integer idHorario;
	
	@Getter @Setter @Autowired private Horario horario;
	@Getter @Setter @Autowired private HorarioRepositorioImpl horarioDAO;
	@Getter @Setter @Autowired private DisciplinaRepositorioImpl disciplinaDAO;
	@Getter @Setter @Autowired private DisciplinaLetiva disciplinaLetiva;
	@Getter @Setter @Autowired private HorarioNovoParams horarioNovoParams;
	@Getter @Setter @Autowired private DisciplinaLetivaHorarioNovoParams disciplinaLetivaHorarioNovoParams;

	public String prepare()  {
		//verificaAcessoNegado();
		
		try {
			setAnos(FormUtil.getAnosList(2));
			setSemestres(FormUtil.getSemestresList());
			horarios = 	horarioDAO.getAllHorarios();
		} catch ( IntranetException e ) {
			addActionMessage(e.getMessage());
	
		}
		
		return "novoList";
	
	}
	
	public String save() throws Exception{
		horarioDAO.save(horarioNovoParams.getHorario());
		addActionMessage("Horário criado com sucesso");
		return prepare();
	}
	
	public String delete() throws Exception{
		try{
		horarioDAO.delete( horarioDAO.getHorarioById( id ) );
		addActionMessage("Horário deletado com sucesso");			
	} catch (Exception e) {		
		addActionError("Não foi possivel deletar Horário, ocorreu um erro interno no Servidor");			
	}
		return prepare();
	}
	
	public String deleteDisciplinaLetivaHorario() throws Exception{
		try{
		DisciplinaLetivaHorario disciplinaLetivaHorario = horarioDAO.getDisciplinaLetivaHorarioByHorarioAndDisciplinaLetivaId(idDisciplinaLetiva,idHorario);
		horarioDAO.deleteDisciplinaLetivaHorario( disciplinaLetivaHorario );
		addActionMessage("Horário deletado com sucesso");			
	} catch (IntranetException e) {		
		addActionError("Não foi possivel deletar Horário, ocorreu um erro interno no Servidor");
	}
		return listarHorarioPorDisciplinaLetiva();
	}
	
	public String listarHorarioPorDisciplinaLetiva() throws Exception{
		try{
		disciplinaLetiva = disciplinaDAO.getDisciplinaLetivaById(idDisciplinaLetiva);
		setDiasSemana( Arrays.asList(DiaSemanaEnum.values()) );
		horarios = 	horarioDAO.getAllHorarios();
		disciplinaLetivaHorario = 	horarioDAO.getAllDisciplinaLetivaHorariosById(idDisciplinaLetiva);
		} catch (IntranetException e) {		
			addActionError("Não foi possivel listar Horários, ocorreu um erro interno no Servidor");		

		}
	return "listarHorarioDiscplinaLetiva";
	}
	
	
	public String saveHorariosEmDisciplinaLetiva() throws Exception{
		try{
				DisciplinaLetiva letiva = disciplinaDAO.getDisciplinaLetivaById(idDisciplinaLetiva);
				Horario horario = horarioDAO.getHorarioById( horarioId );
				DisciplinaLetivaHorario disciplinaLetivaHorario = new DisciplinaLetivaHorario();
				disciplinaLetivaHorario.getDisciplinaLetivaHorarioPK().setDiaSemana(diaSemana);
				disciplinaLetivaHorario.getDisciplinaLetivaHorarioPK().setDisciplinaLetiva(letiva);
				disciplinaLetivaHorario.getDisciplinaLetivaHorarioPK().setHorario(horario);
				
				if(horarioDAO.validationDisciplinaLetivaHorario(disciplinaLetivaHorario)){
					horarioDAO.saveDisciplinaLetivaHorario(disciplinaLetivaHorario);
					addActionMessage("Horario adicionado em disciplina letiva com sucesso");
				}else{
					addActionError("Não foi possivel Adicionar, este horário já existe nesta disciplina. ");		
				}
		} catch (Exception e) {		
			addActionError("Não foi possivel Adicionar, ocorreu um erro interno no Servidor");
			e.printStackTrace();
			throw new IntranetException(e.getMessage());
		}
		return listarHorarioPorDisciplinaLetiva();
	}

		
	
}
