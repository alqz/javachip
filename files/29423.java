package me.rodolffo.adminchat;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.logging.Logger;

import me.rodolffo.adminchat.AdminChatUtils;

public class AdminChat extends JavaPlugin{

	private Logger log;
	AdminChatUtils aut;

	@Override
	public void onLoad(){

		log = Logger.getLogger("Minecraft"); //We only need Bukkit's default logger

		aut = new AdminChatUtils();

		log.info("AdminChat version " + this.getDescription().getVersion() + " by Rodolffo loaded");
	}

	public void onUnload(){

		log.info("AdminChat version " + this.getDescription().getVersion() + " by Rodolffo unloaded");
	}

	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args){

		if(cmd.getName().equalsIgnoreCase("ach") || cmd.getName().equalsIgnoreCase("achat")){

			if(!sender.hasPermission("ac.see") && !sender.isOp()){

				sender.sendMessage(ChatColor.RED + "You don't have permission to use this!");
				return true;
			}

			if(args.length < 1)
			{
				return false; //The false return value sends the "usage" message defined in the plugin.yml
			}

			String msg = aut.arrayToString(args);
			String outputformat = ChatColor.BLUE + "**" + sender.getName() + ": " + msg;

			log.info("**" + sender.getName() + ": " + msg);

			for(Player player : Bukkit.getServer().getOnlinePlayers()){

				if(player.hasPermission("ac.see")){

						player.sendMessage(outputformat);
				}
			}

			return true;
		}

		return false;
	}
}