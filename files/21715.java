package org.rsbot.script.internal.task;

import org.rsbot.bot.Bot;
import org.rsbot.bot.Context;
import org.rsbot.bot.accessors.Client;
import org.rsbot.script.concurrent.Task;
import org.rsbot.script.internal.event.PaintListener;
import org.rsbot.script.methods.Mouse;
import org.rsbot.script.wrappers.Application;
import org.rsbot.script.wrappers.Targetable;

import java.awt.*;
import java.util.ArrayList;

public class TargetTask extends Task implements PaintListener {
	private final Targetable targetable;
	private final Application conditional;
	protected final ArrayList<ForceModifier> forceModifiers = new ArrayList<ForceModifier>(5);
	protected final Vector2D velocity = new Vector2D();
	private final Client client = Context.get().client;
	private final Bot bot = Context.get().bot;
	public boolean acceptedApply = false;
	public boolean running = false;
	private boolean bad = false;
	private final long start;

	public TargetTask(final Targetable targetable, final Application conditional) {
		this.targetable = targetable;
		this.conditional = conditional;
		this.running = true;
		this.start = System.currentTimeMillis();
	}

	public void run() {
		initForceModifiers();
		if (targetable.contains(client.getMouse().getPoint())) {
			Mouse.moveRandomly(10, 50);
		}
		while (running && (System.currentTimeMillis() - start < conditional.timeOut)) {
			final Point p = targetable.getPoint();
			if (p.x == -1 || p.y == -1) {
				try {
					Thread.sleep(random(50, 250));
				} catch (InterruptedException ignored) {
				}
				continue;
			}

			if (targetable.contains(client.getMouse().getPoint())) {
				sleep(random(25, 50));
				if (conditional.apply()) {
					acceptedApply = true;
					break;
				}
				bad = true;
			}

			final double deltaTime = random(8D, 10D) / 1000D;
			final Vector2D force = new Vector2D();
			for (ForceModifier modifier : forceModifiers) {
				Vector2D f = modifier.apply(deltaTime, p);
				if (f == null) {
					continue;
				}
				force.add(f);
			}

			if (Double.isNaN(force.xUnits) || Double.isNaN(force.yUnits)) {
				stop();
				return;
			}
			velocity.add(force.multiply(deltaTime));

			final Vector2D deltaPosition = velocity.multiply(deltaTime);
			if (deltaPosition.xUnits != 0 && deltaPosition.yUnits != 0) {
				int x = client.getMouse().getX() + (int) deltaPosition.xUnits;
				int y = client.getMouse().getY() + (int) deltaPosition.yUnits;
				if (!client.getCanvas().contains(x, y)) {
					switch (bot.composite.im.side) {
						case 1:
							x = 1;
							y = random(0, client.getCanvas().getHeight());
							break;
						case 2:
							x = random(0, client.getCanvas().getWidth());
							y = client.getCanvas().getHeight() + 1;
							break;
						case 3:
							x = client.getCanvas().getWidth() + 1;
							y = random(0, client.getCanvas().getHeight());
							break;
						case 4:
							x = random(0, client.getCanvas().getWidth());
							y = 1;
							break;
					}
				}
				bot.composite.im.hopMouse(x, y);
			}

			try {
				Thread.sleep((long) (deltaTime * 1000D / bot.composite.mouseMultiplier));
			} catch (InterruptedException e) {
				stop();
				return;
			}
		}
		stop();
	}

	@Override
	public void stop() {
		running = false;
	}

	public void initForceModifiers() {
		forceModifiers.add(new ForceModifier() {
			public Vector2D apply(double deltaTime, Point pTarget) {
				Vector2D force = new Vector2D();

				Vector2D toTarget = new Vector2D();
				toTarget.xUnits = pTarget.x - client.getMouse().getX();
				toTarget.yUnits = pTarget.y - client.getMouse().getY();
				if (toTarget.xUnits == 0 && toTarget.yUnits == 0) {
					return null;
				}

				double alpha = toTarget.getAngle();
				double acc = random(1500, 2000);
				force.xUnits = Math.cos(alpha) * acc;
				force.yUnits = Math.sin(alpha) * acc;

				return force;
			}
		});

		forceModifiers.add(new ForceModifier() {
			public Vector2D apply(double deltaTime, Point pTarget) {
				return velocity.multiply(-1);
			}
		});

		forceModifiers.add(new ForceModifier() {
			private int offset = random(300, 500);
			private double offsetAngle = -1;

			public Vector2D apply(double deltaTime, Point pTarget) {
				if (offsetAngle == -1) {
					offsetAngle = random(-Math.PI, Math.PI);
				}
				Vector2D toTarget = new Vector2D();
				toTarget.xUnits = pTarget.x - client.getMouse().getX();
				toTarget.yUnits = pTarget.y - client.getMouse().getY();
				if (offset > 0 && toTarget.getLength() > random(25, 55)) {
					Vector2D force = new Vector2D();
					force.xUnits = Math.cos(offsetAngle) * offset;
					force.yUnits = Math.sin(offsetAngle) * offset;
					offset -= random(0, 6);
					return force;
				}
				return null;
			}
		});

		forceModifiers.add(new ForceModifier() {
			// correction when close
			public Vector2D apply(double deltaTime, Point pTarget) {
				final Vector2D toTarget = new Vector2D();
				toTarget.xUnits = pTarget.x - client.getMouse().getX();
				toTarget.yUnits = pTarget.y - client.getMouse().getY();
				double length = toTarget.getLength();
				if (length < random(75, 125)) {
					Vector2D force = new Vector2D();

					final double speed = velocity.getLength();
					final double rh = speed * speed;
					final double s = toTarget.xUnits * toTarget.xUnits + toTarget.yUnits * toTarget.yUnits;
					if (s == 0) {
						return null;
					}
					double f = rh / s;
					f = Math.sqrt(f);
					final Vector2D adjustedToTarget = toTarget.multiply(f);

					force.xUnits = (adjustedToTarget.xUnits - velocity.xUnits) / (deltaTime);
					force.yUnits = (adjustedToTarget.yUnits - velocity.yUnits) / (deltaTime);

					final double v = 4D / length;
					if (v < 1D) {
						force = force.multiply(v);
					}
					if (length < 10) {
						force = force.multiply(0.5D);
					}
					return force;
				}
				return null;
			}
		});

		forceModifiers.add(new ForceModifier() {
			public Vector2D apply(double deltaTime, Point pTarget) {
				final int mouseX = client.getMouse().getX();
				final int mouseY = client.getMouse().getY();
				if (mouseX == pTarget.x && mouseY == pTarget.y && !bad) {
					velocity.xUnits = 0;
					velocity.yUnits = 0;
				}
				bad = false;
				return null;
			}
		});
	}

	public void onRepaint(final Graphics render) {
		targetable.draw(render);
	}

	interface ForceModifier {
		public Vector2D apply(double deltaTime, Point pTarget);
	}

	class Vector2D {
		public double xUnits;
		public double yUnits;

		public Vector2D sum(Vector2D vector) {
			final Vector2D out = new Vector2D();
			out.xUnits = xUnits + vector.xUnits;
			out.yUnits = xUnits + vector.yUnits;
			return out;
		}

		public void add(Vector2D vector) {
			xUnits += vector.xUnits;
			yUnits += vector.yUnits;
		}

		public Vector2D multiply(double factor) {
			final Vector2D out = new Vector2D();
			out.xUnits = xUnits * factor;
			out.yUnits = yUnits * factor;
			return out;
		}

		public double getLength() {
			return Math.sqrt(xUnits * xUnits + yUnits * yUnits);
		}

		public double getAngle() {
			return Math.atan2(yUnits, xUnits);
		}
	}
}