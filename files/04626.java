package qse_sepm_ss12_07.service;

import java.util.*;
import org.apache.log4j.Logger;
import qse_sepm_ss12_07.dao.*;
import qse_sepm_ss12_07.domain.*;

public class RecipeService extends ProductListService<Recipe, RecipeFilter> implements IRecipeService
{

	private static final Logger logger = Logger.getLogger(RecipeService.class);
	private IRecipeDao recipeDao;
	private IMissingStockService missingStockService;

	public void setRecipeDao(IRecipeDao dao)
	{
		logger.debug("setRecipeDao");
		this.recipeDao = dao;
	}

	public void setMissingStockService(IMissingStockService missingStockService)
	{
		this.missingStockService = missingStockService;
	}

	public List<Recipe> getAll() throws ServiceException
	{
		List<Recipe> recipes = new ArrayList<Recipe>();
		try
		{
			List<RecipeEntity> recipeEntities = recipeDao.findAll();

			for (RecipeEntity entity : recipeEntities)
			{
				ProductListEntity productListEntity = getProductListEntity(entity.getProductListId());
				Recipe recipe = new Recipe(productListEntity, entity);
				loadProductList(recipe);
				loadTags(recipe);
				recipes.add(recipe);
			}
		}
		catch (DaoException exception)
		{
			logger.error("could not load recipes", exception);
			throw new ServiceException("could not load recipes", exception);
		}

		return recipes;
	}

	public String getFullDescription(Recipe recipe) throws ServiceException
	{
		StringBuilder sb = new StringBuilder();
		sb.append("Name: ").append(recipe.getEntity().getName()).append("\n\n");
		sb.append("Ingredients: \n");
		for (ProductListEntry entry : recipe.getProductListEntrys())
		{
			sb.append("	").append(entry.getName()).append(", ").append(entry.getAmount()).append(" ").append(getUnitString(entry)).append("\n");
		}

		sb.append("Description: ").append(recipe.getDescription()).append("\n\n");

		if (recipe.getTags().size() > 0)
		{
			sb.append("Tags: \n");
			for (Tag tag : recipe.getTags())
			{
				sb.append("	* ").append(tag.getName()).append("\n");
			}
		}
		
		String missingStockText = missingStockService.getMissingStockTextBasedOnRecipe(recipe);
		if(!missingStockText.isEmpty())
		{
			sb.append("\nMissing Ingredients: \n");
			sb.append(missingStockText);
		}

		return sb.toString();
	}

	public void saveRecipe(Recipe recipe) throws ServiceException
	{
		try
		{
			//has to happen first because we need the productlist id
			saveProductList(recipe);
			recipe.getRecipeEntity().setProductListId(recipe.getEntity().getId());

			if (recipe.getRecipeEntity().getId() == -1)
			{
				recipeDao.create(recipe.getRecipeEntity());
			}
			else
			{
				recipeDao.update(recipe.getRecipeEntity());
			}
		}
		catch (DaoException exception)
		{
			throw new ServiceException("Could not save recipe", exception);
		}
	}

	public void deleteRecipe(Recipe recipe) throws ServiceException
	{
		try
		{
			deleteProductList(recipe);
			recipeDao.delete(recipe.getRecipeEntity());
		}
		catch (DaoException exception)
		{
			logger.error("Could not delete recipe", exception);
			throw new ServiceException("Could not delete recipe", exception);
		}
	}

	public List<Recipe> getAllByFilter(RecipeFilter filter) throws ServiceException
	{
		try
		{
			Set<Recipe> recipeSet = new HashSet<Recipe>();

			logger.trace("getAllRecipesByFilter: filter=" + filter);

			// find recipes by name
			if (filter.isSearchForProductListName())
			{
				recipeSet.addAll(getAllRecipesByName(filter.getSearchString()));
			}

			// find recipes by productname
			if (filter.isSearchForProductName())
			{
				recipeSet.addAll(getAllRecipesByProductName(filter.getSearchString()));
				recipeSet.addAll(getAllRecipesByCategoryName(filter.getSearchString()));
			}

			List<Recipe> recipes = new ArrayList<Recipe>(recipeSet);

			//find with actual mood
			if (filter.isFilterForCurrentMood())
			{
				removeRecipesWhereCurrentMoodIsMissing(recipes);
			}
			
			// find by user-tags
			if (filter.isFilterForUserTags())
			{
				removeRecipesWhereCurrentUserTagsAreMissing(recipes);
			}
			
			//all available check
			if (filter.isOnlyWhenAllProductsAvailable())
			{
				removeRecipesWhereStockIsMissing(recipes);
			}
			
			
			return recipes;
		}
		catch (ServiceException se)
		{
			logger.error("Could not get all recipes by filter", se);
			throw new ServiceException("Could not get all recipes by filter", se);
		}
	}

	private List<Recipe> getAllRecipesByProductName(String name) throws ServiceException
	{
		logger.trace("getAllRecipesByProductName: name=" + name);
		Set<Recipe> recipeSet = new HashSet<Recipe>();
		List<Product> products = productService.getProductsByName(name);
		for (Product product : products)
		{
			recipeSet.addAll(getAllRecipesByProduct(product));
		}
		return new ArrayList<Recipe>(recipeSet);
	}

	private List<Recipe> getAllRecipesByCategoryName(String name) throws ServiceException
	{
		logger.trace("getAllRecipesByCategoryName: name=" + name);
		Set<Recipe> recipeSet = new HashSet<Recipe>();
		List<ProductCategory> productCategories = productCategoryService.getAllProductCategoriesByName(name);
		for (ProductCategory productCategory : productCategories)
		{
			recipeSet.addAll(getAllRecipesByProductCategory(productCategory));
		}
		return new ArrayList<Recipe>(recipeSet);
	}

	private void removeRecipesWhereStockIsMissing(List<Recipe> recipes) throws ServiceException
	{
		logger.trace("removeRecipesWhereStockIsMissing: recipes=" + recipes);
		List<Recipe> missingProductRecipes = new ArrayList<Recipe>();
		for (Recipe recipe : recipes)
		{
			if (missingStockService.hasMissingStock(recipe))
			{
				missingProductRecipes.add(recipe);
			}
		}
		recipes.removeAll(missingProductRecipes);
	}

	private List<Recipe> getAllRecipesByName(String name) throws ServiceException
	{
		try
		{
			logger.trace("getAllRecipesByName: name=" + name);
			List<Recipe> recipes = new ArrayList<Recipe>();
			for (ProductListEntity entity : productListDao.findRecipesByName(name))
			{
				RecipeEntity recipeEntity = recipeDao.findByProductListId(entity.getId());
				Recipe newRecipe = new Recipe(entity, recipeEntity);
				loadProductList(newRecipe);
				loadTags(newRecipe);
				recipes.add(newRecipe);
			}
			return recipes;
		}
		catch (DaoException de)
		{
			logger.error("Could not get all recipes by name", de);
			throw new ServiceException("Could not get all recipes by name", de);
		}
	}

	private List<Recipe> getAllRecipesByProduct(Product product) throws ServiceException
	{
		try
		{
			logger.trace("getAllRecipesByProduct: product=" + product);
			List<Recipe> recipes = new ArrayList<Recipe>();

			List<ProductListProductEntryEntity> entries = productComponentDao.findAllByProduct(product.getEntity().getId());
			for (ProductListProductEntryEntity entry : entries)
			{
				ProductListEntity productListEntity = productListDao.find(entry.getProductListId());
				RecipeEntity recipeEntity = null;
				try
				{
					recipeEntity = recipeDao.findByProductListId(entry.getProductListId());
				}
				catch (EntityNotFoundDaoException se)
				{
					logger.info("No recipe existing for productListEntity: " + productListEntity);
				}
				if (recipeEntity != null)
				{
					Recipe toAdd = new Recipe(productListEntity, recipeEntity);
					loadProductList(toAdd);
					loadTags(toAdd);
					recipes.add(toAdd);
				}
			}

			return recipes;
		}
		catch (DaoException de)
		{
			logger.error("Could not get all recipes by product", de);
			throw new ServiceException("Could not get all recipes by product", de);
		}
	}

	private List<Recipe> getAllRecipesByProductCategory(ProductCategory productCategory) throws ServiceException
	{
		try
		{
			logger.trace("getAllRecipesByProductCategory: category=" + productCategory);
			List<Recipe> recipes = new ArrayList<Recipe>();

			List<ProductListProductCategoryEntryEntity> entries = productCategoryComponentDao.findAllByProductCategory(productCategory.getEntity().getId());
			for (ProductListProductCategoryEntryEntity entry : entries)
			{
				ProductListEntity productListEntity = productListDao.find(entry.getProductListId());
				RecipeEntity recipeEntity = null;
				try
				{
					recipeEntity = recipeDao.findByProductListId(entry.getProductListId());
				}
				catch (EntityNotFoundDaoException se)
				{
					logger.info("No recipe existing for productListEntity: " + productListEntity);
				}
				if (recipeEntity != null)
				{
					Recipe toAdd = new Recipe(productListEntity, recipeEntity);
					loadProductList(toAdd);
					loadTags(toAdd);
					recipes.add(toAdd);
				}
			}

			return recipes;
		}
		catch (DaoException de)
		{
			logger.error("Could not get all recipes by product", de);
			throw new ServiceException("Could not get all recipes by product", de);
		}
	}

	private void removeRecipesWhereCurrentMoodIsMissing(List<Recipe> recipes) throws ServiceException
	{
		logger.trace("removeRecipesWhereCurrentMoodIsMissing: recipes=" + recipes);
		MoodTag currentMood = loginService.getLoggedInUser().getUserMood();
		logger.trace(" Your current mood is: "+currentMood);
		List<Recipe> missingMoodRecipes = new ArrayList<Recipe>();
		for (Recipe recipe : recipes)
		{
						
			if(recipe.getTags()==null || recipe.getTags().isEmpty())
			{
				recipe.setTags(taggingService.getTags(recipe));
			}
			
			logger.trace("     --->>  "+ recipe.getTags());
			
			boolean containsTag = recipe.containsTag(currentMood);
			
			if (!containsTag)
			{
				missingMoodRecipes.add(recipe);
			}
		}

		recipes.removeAll(missingMoodRecipes);
	}
	
	private void removeRecipesWhereCurrentUserTagsAreMissing(List<Recipe> recipes) throws ServiceException
	{
		logger.trace("removeRecipesWhereCurrentMoodIsMissing: recipes=" + recipes);
		List<Tag> userTags = taggingService.getTags(loginService.getLoggedInUser());
		List<Recipe> missingUserTagRecipes = new ArrayList<Recipe>();
		for (Recipe recipe : recipes)
		{
			
			if(recipe.getTags()==null || recipe.getTags().isEmpty())
			{
				recipe.setTags(taggingService.getTags(recipe));
			}
			
			boolean containsTag = false;
			
			// iterate over user-tags and check if the recipe contains one of them
			Iterator<Tag> iterator = userTags.iterator();
			while (!containsTag && iterator.hasNext())
			{
				Tag nextTag = iterator.next();
				if(recipe.containsTag(nextTag))
				{
					containsTag = true;
				}
			}
			
			if (!containsTag)
			{
				missingUserTagRecipes.add(recipe);
			}
		}

		recipes.removeAll(missingUserTagRecipes);
	}
	
	private void loadTags(Recipe recipe) throws ServiceException
	{
		logger.trace("loadTags: "+recipe);
		recipe.setTags(taggingService.getTags(recipe));
	}
}
