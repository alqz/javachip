package dart;

import java.util.ArrayList;

import dart.runner.Runner;

public class Player {
	
//--------------------------------
//Fields
//--------------------------------

	//Int to hold the index (which player) this player is.
	private int index;
	
	//Int to hold which team the player belongs to
	private int team;
	
	//Player information
	private double money=1000.0,income=50,score=0;
	
	private long time=0, timeLastPaid;
	
	//Number of lives left on this player's team
	private int teamLives=25;
	
	String name = new String();
	
//--------------------------------
//Constructors
//--------------------------------

	public Player(int index, int team, String name)
	{
		//Request player index from the host
		this.index	= index;
		this.team	= team;
		this.name   = name;
	}

		
//--------------------------------
//Methods
//--------------------------------		
	
//Getter Methods
	
	public int getIndex()	{ return index;	}
	public int getTeam()	{ return team;	}
	public double getMoney()	{ return money; }
	public double getScore()	{ return score; }
	public long getTime()	{ return time;	}
	public long getTimeLastPaid() { return timeLastPaid; }
	public double getIncome()	{ return this.income; }
	public int getTeamLives()	{ return this.teamLives; }
	public String getName()		{ return this.name; }
	
//Setter Methods
	
	public void setMoney(double money)	{ this.money = money; }
	public void setScore(double score) { this.score = score; }
	public void setTime(long time)	{ this.time  = time; }
	public void setName(String name) { this.name = name; }
	
		//--------------------------------//
	
//Money methods	
	public void deductMoney(double money)		{ this.money-=money;   }
	public void payIncome()				{ this.money+=income;  timeLastPaid=time;}
	public void increaseIncome(double change)	{ this.income+=change; }
	
//Scoring methods
	
	//Take off one life for this player's team
	public void decrementLives()	{ this.teamLives--; }
	
	public void tallyRunners(ArrayList<Runner> deadRunners)
	{
		Runner r;
		//Loop through every runner
		while(deadRunners.size()>0)
		{
			//remove the lastmost runner on the list (quicker than the first and having to shift the whole list)
			r = deadRunners.remove(deadRunners.size()-1);
			
			//Increase the player's score
			money += r.getRewardMoney();
			
			//Increase the score for that player
			score++;
			
			System.out.println("Player money:"+money);
		}
	}
	
}