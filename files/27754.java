package qse_sepm_ss12_07.service;

import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import qse_sepm_ss12_07.dao.DaoException;
import qse_sepm_ss12_07.dao.ITagDao;
import qse_sepm_ss12_07.dao.TagEntity;
import qse_sepm_ss12_07.domain.*;

/**
 * implementation of the tagging service
 */
public class TaggingService implements ITaggingService 
{
	private Logger logger = Logger.getLogger(TaggingService.class);
	private ITagDao tagDao;
	
	public void setTagDao(ITagDao tagDao)
	{
		this.tagDao = tagDao;
	}

	public List<Tag> getTags(ITagable tagable) throws ServiceException
	{
		List<Tag> tags = new ArrayList<Tag>();
		
		if (tagable instanceof Product)
		{
			tags = getTags((Product) tagable);
		}
		
		if (tagable instanceof ProductCategory)
		{
			tags = getTags((ProductCategory) tagable);
		}
		
		if (tagable instanceof Recipe)
		{			
			tags = getTags((Recipe) tagable);
		}
		
		if (tagable instanceof User)
		{			
			tags = getTags((User) tagable);
		}
		
		tagable.setTags(tags);		
		return tags;
	}

	private List<Tag> getTags(Product product) throws ServiceException
	{
		List<Tag> tags = new ArrayList<Tag>();
		List<TagEntity> tagEntitys;

		try 
		{
			tagEntitys = tagDao.getTags(product.getEntity());

			for (TagEntity tag : tagEntitys) 
			{
				ProductTag productTag = new ProductTag(tag);
				productTag.setProduct(product);				
				tags.add(productTag);
			}
		} 
		catch (DaoException exception) 
		{
			logger.error("Could not get tags", exception);
			throw new ServiceException("could not get tags", exception);
		}

		return tags;
	}
	
	private List<Tag> getTags(User user) throws ServiceException
	{
		List<Tag> tags = new ArrayList<Tag>();
		List<TagEntity> tagEntitys;

		try 
		{
			tagEntitys = tagDao.getTags(user.getEntity());

			for (TagEntity tag : tagEntitys) 
			{
				UserTag userTag = new UserTag(tag);
				userTag.setUser(user);				
				tags.add(userTag);
			}
		} 
		catch (DaoException exception) 
		{
			logger.error("Could not get tags", exception);
			throw new ServiceException("could not get tags", exception);
		}

		return tags;
	}
	
	private List<Tag> getTags(ProductCategory productCategory) throws ServiceException
	{
		List<Tag> tags = new ArrayList<Tag>();

		try 
		{
			List<TagEntity> tagEntitys = tagDao.getTags(productCategory.getEntity());

			for (TagEntity tag : tagEntitys) 
			{
				ProductCategoryTag productCategoryTag = new ProductCategoryTag(tag);
				productCategoryTag.setProductCategory(productCategory);				
				tags.add(productCategoryTag);
			}
		} 
		catch (DaoException exception) 
		{
			logger.error("Could not get tags", exception);
			throw new ServiceException("could not get tags", exception);
		}
		
		return tags;
	}
	
	private List<Tag> getTags(Recipe recipe) throws ServiceException
	{
		List<Tag> tags = new ArrayList<Tag>();
		
		try
		{
			List<TagEntity> tagEntitys = tagDao.getTags(recipe.getRecipeEntity());

			for (TagEntity tag : tagEntitys)
			{
				RecipeTag recipeTag = new RecipeTag(tag);
				recipeTag.setRecipe(recipe);
				tags.add(recipeTag);
			}
		}
		catch (DaoException exception)
		{
			logger.error("Could not get tags", exception);
			throw new ServiceException("could not get tags", exception);
		}

		return tags;
	}

	public List<Tag> getAllAvailableTags() throws ServiceException
	{
		List<Tag> tags = new ArrayList<Tag>();
		
		try 
		{
			List<TagEntity> entities = tagDao.findAll();
			
			for(TagEntity entity : entities)
			{
				tags.add(new Tag(entity));
			}
		} 
		catch(DaoException exception)
		{
			logger.error("Could not get tags", exception);
			throw new ServiceException("could not get tags", exception);
		}
		
		return tags;
	}
	
	public List<MoodTag> getAllAvailableMoodTags() throws ServiceException
	{
		List<MoodTag> tags = new ArrayList<MoodTag>();
		
		try 
		{
			List<TagEntity> entities = tagDao.findAllMoods();
			
			for(TagEntity entity : entities)
			{
				tags.add(new MoodTag(entity));
			}
		} 
		catch(DaoException exception)
		{
			logger.error("Could not get tags", exception);
			throw new ServiceException("could not get tags", exception);
		}
		
		return tags;
	}
	
	public void addTag(ITagable tagable, Tag tag) throws ServiceException
	{
		try
		{	
			if (tagable.containsTag(tag)) //do not add a tag if he is already there
			{
				return;
			}
			
			
			saveTag(tag);
			
			
			if (tagable instanceof Product)
			{
				Product product = (Product) tagable;
				product.addTag(new ProductTag(tag.getEntity()));
				tagDao.link(tag.getEntity(), product.getEntity());
			}
			
			if (tagable instanceof ProductCategory)
			{
				ProductCategory productCategory = (ProductCategory) tagable;
				productCategory.addTag(new ProductCategoryTag(tag.getEntity(), productCategory));
				tagDao.link(tag.getEntity(), productCategory.getEntity());
			}
			
			if (tagable instanceof User)
			{
				User user = (User) tagable;
				user.addTag(new UserTag(tag.getEntity(), user));
				tagDao.link(tag.getEntity(), user.getEntity());
			}
			
			if (tagable instanceof Recipe)
			{
				Recipe recipe = (Recipe) tagable;
				recipe.addTag(new RecipeTag(tag.getEntity(), recipe));
				tagDao.link(tag.getEntity(), recipe.getRecipeEntity());
 			}
		} catch (DaoException exception)
		{
			logger.error("Could not add tag", exception);
			throw new ServiceException("could not add tag", exception);
		}
	}

	public void removeTag(ITagable tagable, Tag tag) throws ServiceException
	{
		try
		{
			if (!tagable.containsTag(tag))
			{
				return;
			}
			
			if (tag instanceof ProductTag)
			{
				ProductTag productTag = (ProductTag) tag;
				tagDao.unlink(productTag.getEntity(), productTag.getProduct().getEntity());
			}
			
			if (tag instanceof ProductCategoryTag)
			{
				ProductCategoryTag productCategoryTag = (ProductCategoryTag) tag;
				tagDao.unlink(productCategoryTag.getEntity(), productCategoryTag.getProductCategory().getEntity());
			}
						
			if (tag instanceof RecipeTag)
			{
				RecipeTag recipeTag = (RecipeTag) tag;
				tagDao.unlink(recipeTag.getEntity(), recipeTag.getRecipe().getRecipeEntity());
			}
			
			if (tag instanceof UserTag)
			{
				UserTag userTag = (UserTag) tag;
				tagDao.unlink(userTag.getEntity(), userTag.getUser().getEntity());
			}
			
			tagable.removeTag(tag);
		} 
		catch(DaoException exception)
		{
			logger.error("Could not remove tag", exception);
			throw new ServiceException("could not remove tag", exception);
		}
	}
	
	public void saveTag(Tag tag) throws ServiceException
	{
		TagEntity entity = tag.getEntity();
		
		try
		{
			if (entity.getId() == -1)
			{
				tagDao.create(entity);
			}
			else
			{
				tagDao.update(entity);
			}
		} catch (DaoException exception)
		{			
			logger.error("Could not save or create tag", exception);
			throw new ServiceException("could not save or create tag", exception);
		}
	}	
	
	public void deleteTag(Tag tag) throws ServiceException
	{
		try
		{
			tagDao.delete(tag.getEntity());
		}
		catch(DaoException exception)
		{
			logger.error("could not delete tag", exception);
			throw new ServiceException("could not delete tag", exception);
		}
	}
	
	public void loadMood(User user) throws ServiceException
	{
		try
		{
			List<TagEntity> tags = tagDao.findAllMoods();
			if(tags.size()>0)
			{
				user.setUserMood(new MoodTag(tags.get(0)));
			}
			else
			{
				user.setUserMood(new MoodTag(new TagEntity()));
			}
		}
		catch(DaoException de)
		{
			throw new ServiceException("Could not load user mood.",de);
		}
	}
}
