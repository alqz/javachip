package com.kelkoo.yatzee;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import com.kelkoo.yatzee.DiceLauncher;
import com.kelkoo.yatzee.Dices;
import com.kelkoo.yatzee.User;
import com.kelkoo.yatzee.Yatzee;

public class TestEndToEnd {

   // finishes when reach three throws
   @Test
   public void playWithCategoryOfOnesWithOneUserTwoTurns() throws Exception {
      DiceLauncher diceLauncher = mock(DiceLauncher.class);
      when(diceLauncher.launch()).
         thenReturn(new Dices(1, 1, 3, 3, 4)).
         thenReturn(new Dices(1, 6, 6)).
         thenReturn(new Dices(1, 2)).
         thenReturn(new Dices(1, 1, 2, 3, 4)).
         thenReturn(new Dices(1, 2, 4, 6)).
         thenReturn(new Dices(1, 2, 3));
      
      User user = new User();
      Yatzee yatzee = new Yatzee(diceLauncher, user);
      List<Integer> categories = Arrays.asList(1, 2);
      yatzee.startGame(categories);
      
      user.rollDices();
      user.selectDices(1, 1);
      user.rollDices();
      user.selectDices(1);
      user.rollDices();
      user.selectDices(1);
      user.selectCategory(1);
      assertThat("should not be finished", yatzee.isGameFinished(), is(false));
      assertThat(yatzee.score(), is(4));
      
      user.rollDices();
      user.selectDices(2);
      user.rollDices();
      user.selectDices(2);
      user.rollDices();
      user.selectDices(2);
      user.selectCategory(2);
      assertThat("should be finished", yatzee.isGameFinished(), is(true));
      assertThat(yatzee.score(), is(10));
      
   }

   /** TODO LIST */
   // manage multiple figures categories (1..6)
   // manage other categories (brelan, carre, full, petite suite, grande suite, yatzee, chance)
   // manage figures bonus
   // manage multiples users
   // implement interface with terminal
   // implement web interface
   // implement network gaming

}
