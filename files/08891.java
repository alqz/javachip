/**
 * 
 */
package com.oauth.wrap.api;

import java.net.HttpURLConnection;

import org.apache.log4j.Logger;

import com.oauth.wrap.model.AccessToken;
import com.oauth.wrap.model.Config;
import com.oauth.wrap.model.RefreshToken;
import com.oauth.wrap.model.Request;
import com.oauth.wrap.model.Response;
import com.oauth.wrap.model.Verifier;

/**
 * 
 * @author Oleksii Karpynskyi <okarpynskyi@gmail.com>
 * 
 */
public class Service {

    /**
     * 
     */
    private final Logger logger = Logger.getLogger(getClass());

    /**
     * 
     */
    private Api api;

    /**
     * 
     */
    private Config config;

    /**
     * OAuth WRAP Service Main Constructor
     * 
     * @param api
     * @param config
     */
    public Service(Api api, Config config) {
        this.api = api;
        this.config = config;
    }

    /**
     * @return the api
     */
    public Api getApi() {
        return api;
    }

    /**
     * @return the config
     */
    public Config getConfig() {
        return config;
    }

    /**
     * Get OAuth WRAP AccessToken Using Verification Code
     * 
     * @param verifier
     * @return {@link AccessToken} accessToken
     */
    public AccessToken getAccessToken(Verifier verifier) {

        Request request = new Request();

        request.setUrl(api.getAccessTokenUrl(verifier));
        request.setHttpVerb(api.getAccessTokenHttpVerb());
        request.addHeaders(api.getAccessTokenHeaders(verifier));
        request.addBodyParameters(api.getAccessTokenParameters(verifier));
        // request.addQuerystringParameters(api.getAccessTokenParameters(verifier));

        Response response = request.send();

        logger.debug(request);
        logger.debug(response);
        logger.debug("AccessToken: " + response.getBody());

        AccessToken accessToken = api.getAccessTokenExtractor().extract(response.getBody());

        /*
         * Response headers
         * 
         * 1) HTTP 200 OK
         * 
         * 2) HTTP/1.1 401 Unauthorized, WWW-Authenticate: WRAP
         * 
         * 3) HTTP 400 Bad Request
         */

        switch (response.getCode()) {
            case HttpURLConnection.HTTP_OK:
                if (!accessToken.isValidIncludingRefreshToken()) {
                    accessToken.setErrorReason("Access token was not parsed properly although the response is OK ");
                }
                break;
            case HttpURLConnection.HTTP_UNAUTHORIZED:
                if (!accessToken.isErrorReason()) {
                    accessToken.setErrorReason(String.format("Unauthorized = %d", response.getCode()));
                }
                break;
            case HttpURLConnection.HTTP_BAD_REQUEST:
            default:
                if (!accessToken.isErrorReason()) {
                    accessToken.setErrorReason(String.format("Bad Request (%d)", response.getCode()));
                }
        }
        logger.debug(accessToken);

        return accessToken;
    }

    /**
     * Get OAuth WRAP AccessToken Using RefreshToken
     * 
     * @param refreshToken
     * @return {@link AccessToken} accessToken
     */
    public AccessToken getAccessToken(RefreshToken refreshToken) {

        Request request = new Request();

        request.setUrl(api.getRefreshTokenUrl());
        request.setHttpVerb(api.getRefreshTokenHttpVerb());
        request.addHeaders(api.getRefreshTokenHeaders(refreshToken));
        request.addBodyParameters(api.getRefreshTokenParameters(refreshToken));
        // request.addQuerystringParameters(api.getRefreshTokenParameters(refreshToken));

        Response response = request.send();

        logger.debug(request);
        logger.debug(response);
        logger.debug("AccessToken(refreshed): " + response.getBody());

        AccessToken accessToken = api.getAccessTokenExtractor().extract(response.getBody());

        /*
         * Response headers
         * 
         * 1) HTTP 200 OK
         * 
         * 2) HTTP/1.1 401 Unauthorized, WWW-Authenticate: WRAP
         */

        switch (response.getCode()) {
            case HttpURLConnection.HTTP_OK:
                if (!accessToken.isValid()) {
                    accessToken.setErrorReason("Access token was not parsed properly although the response is OK ");
                }
                break;
            case HttpURLConnection.HTTP_UNAUTHORIZED:
            default:
                if (!accessToken.isErrorReason()) {
                    accessToken.setErrorReason(String.format("Unauthorized = %d", response.getCode()));
                }
                break;
        }
        logger.debug(accessToken);

        return accessToken;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "Service [api=" + api + ", config=" + config + "]";
    }

}
