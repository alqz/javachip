/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package qse_sepm_ss12_07.controller;

import org.junit.Test;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import qse_sepm_ss12_07.domain.Product;
import qse_sepm_ss12_07.domain.ProductCategory;
import qse_sepm_ss12_07.domain.User;
import qse_sepm_ss12_07.gui.*;
import qse_sepm_ss12_07.service.*;

/**
 *
 */
public class ProductCategoryControllerUnitTest
{

	/**
	 * Test of saveCategory method, of class ProductCategoryController.
	 */
	@Test
	public void testSaveCategory() throws ServiceException
	{
		IProductCategoryService productCategoryService = mock(IProductCategoryService.class);
		IEditCategoryView editCategoryView = mock(IEditCategoryView.class);
		IProductCategoryView productCateogryView = mock(IProductCategoryView.class);
		IBeanService beanService = mock(IBeanService.class);
		ITaggingController taggingController = mock(ITaggingController.class);		
		ITreeController treeController = mock(ITreeController.class);
		IUnitService unitService = mock(IUnitService.class);
		IProductService productService = mock(IProductService.class);
		ILoginService loginService = mock(ILoginService.class);
		ICategoryCompareView categoryCompareView = mock(ICategoryCompareView.class);
		
		when(beanService.getBean(ITaggingController.class)).thenReturn(taggingController);	
		
		ProductCategory productCategory = new ProductCategory();
		productCategory.setName("testCategory");
		productCategory.setMinimumStockAmount(3.0);
		productCategory.getEntity().setUnitId(0);
		productCategory.setImageFileName("bla");
		productCategory.getEntity().setIsDeleted(false);		

		ProductCategoryController controller = new ProductCategoryController();
		controller.setProductCategoryService(productCategoryService);
		controller.setBeanService(beanService);
		controller.setEditCategoryView(editCategoryView);
		controller.setProductCategoryView(productCateogryView);
		controller.setTreeController(treeController);
		controller.setUnitService(unitService);
		controller.setProductService(productService);
		controller.setLoginService(loginService);
		controller.setCategoryCompareView(categoryCompareView);
		
		User user = new User();
		
		when(loginService.getLoggedInUser()).thenReturn(user);
		
		controller.productCategorySelectionChanged(productCategory);						
		controller.productCategorySelectionChanged(null);
		
		verify(productCategoryService).saveProductCategory(productCategory);
	}

	/**
	 * Test of saveProduct method, of class ProductCategoryController.
	 */
	@Test
	public void testSaveProduct() throws ServiceException
	{
		IProductService productService = mock(IProductService.class);
		IEditProductView productView = mock(IEditProductView.class);
		IProductCategoryView productCategoryView = mock(IProductCategoryView.class);
		IBeanService beanService = mock(IBeanService.class);
		ITaggingController taggingController = mock(ITaggingController.class);
		ITreeController treeController = mock(ITreeController.class);
		IProductCategoryService productCategoryService = mock(IProductCategoryService.class);
		IPriceService priceService = mock(IPriceService.class);
		IProductCompareView productCompareView = mock(IProductCompareView.class);
        IRatingController ratingController = mock(IRatingController.class);
		
		when(beanService.getBean(ITaggingController.class)).thenReturn(taggingController);		
		
		Product product = new Product();

		ProductCategoryController controller = new ProductCategoryController();
		controller.setProductService(productService);
		controller.setBeanService(beanService);
		controller.setEditProductView(productView);
		controller.setProductCategoryView(productCategoryView);
		controller.setTreeController(treeController);
		controller.setProductCategoryService(productCategoryService);
		controller.setPriceService(priceService);
		controller.setProductCompareView(productCompareView);
        controller.setRatingController(ratingController);
		
		controller.productSelectionChanged(product);				
		controller.productSelectionChanged(null);
		verify(productService).saveProduct(product);
	}
	
	/**
	 * Test of deleteProduct method, of class ProductCategoryController.
	 */
	@Test
	public void testDeleteProduct() throws ServiceException
	{
		IProductService productService = mock(IProductService.class);
		IProductCategoryView productCategoryView = mock(IProductCategoryView.class);
		ITreeController treeController = mock(ITreeController.class);
		ITaggingController taggingController = mock(ITaggingController.class);
		IBeanService beanService = mock(IBeanService.class);
		IEditProductView editProductView = mock(IEditProductView.class);
		IProductCategoryService productCategoryService = mock(IProductCategoryService.class);
		IEditCategoryView editCategoryView = mock(IEditCategoryView.class);
		IPriceService priceService = mock(IPriceService.class);
		IProductCompareView productCompareView = mock(IProductCompareView.class);
		IRatingController ratingController = mock(IRatingController.class);
        
		when(beanService.getBean(ITaggingController.class)).thenReturn(taggingController);
		
		Product product = new Product();
		ProductCategory productCategory = new ProductCategory();
		product.setProductCategory(productCategory);
		productCategory.addProduct(product);

		ProductCategoryController controller = new ProductCategoryController();
		controller.setBeanService(beanService);
		controller.setProductService(productService);
		controller.setProductCategoryView(productCategoryView);
		controller.setTreeController(treeController);		
		controller.setEditProductView(editProductView);
		controller.setProductCategoryService(productCategoryService);
		controller.setEditCategoryView(editCategoryView);
		controller.setPriceService(priceService);
		controller.setProductCompareView(productCompareView);
        controller.setRatingController(ratingController);
		
		controller.productSelectionChanged(product);
		controller.deleteProduct(product);

		verify(productService).deleteProduct(product);
	}
	
	@Test
	public void testDeleteProductCategory() throws ServiceException
	{	
		IProductCategoryService productCategoryService = mock(IProductCategoryService.class);
		IEditCategoryView editCategoryView = mock(IEditCategoryView.class);
		IProductCategoryView productCateogryView = mock(IProductCategoryView.class);
		IBeanService beanService = mock(IBeanService.class);
		ITaggingController taggingController = mock(ITaggingController.class);		
		ITreeController treeController = mock(ITreeController.class);
		IUnitService unitService = mock(IUnitService.class);
		IProductService productService = mock(IProductService.class);
		ILoginService loginService = mock(ILoginService.class);
		ICategoryCompareView categoryCompareView = mock(ICategoryCompareView.class);
		
		when(beanService.getBean(ITaggingController.class)).thenReturn(taggingController);	
		
		ProductCategory productCategory = new ProductCategory();
		productCategory.setName("testCategory");
		productCategory.setMinimumStockAmount(3.0);
		productCategory.getEntity().setUnitId(0);
		productCategory.setImageFileName("bla");
		productCategory.getEntity().setIsDeleted(false);		

		Product prodcut = new Product();
		prodcut.setAmountPerProduct(1);
		prodcut.setBarcode("mumu");
		prodcut.setImageFileName("noImage");
		prodcut.setMinimumStockAmount(4);
		prodcut.setName("Foo");
		prodcut.setProductCategory(productCategory);
		prodcut.setStockAmount(12);
		
		productCategory.addProduct(prodcut);
		
		ProductCategoryController controller = new ProductCategoryController();
		controller.setProductCategoryService(productCategoryService);
		controller.setBeanService(beanService);
		controller.setEditCategoryView(editCategoryView);
		controller.setProductCategoryView(productCateogryView);
		controller.setTreeController(treeController);
		controller.setUnitService(unitService);
		controller.setProductService(productService);
		controller.setLoginService(loginService);
		controller.setCategoryCompareView(categoryCompareView);
		
		User user = new User();
		
		when(loginService.getLoggedInUser()).thenReturn(user);
		
		controller.productCategorySelectionChanged(productCategory);
		
		controller.deleteProductCategory();
		
		verify(productCategoryService).removeProductCategory(productCategory);
		
	}
}
