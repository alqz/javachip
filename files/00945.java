/**
 * 
 * DamageAccountListAdapter.java
 *
 * 	Copyright $2011 Frederik Hahne
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package de.atomfrede.android.thwdroid.damageaccount.adapter;

import java.util.*;

import android.app.Activity;
import android.view.*;
import android.widget.*;
import de.atomfrede.android.thwdroid.R;
import de.atomfrede.android.thwdroid.ui.ColorFilters;
import de.usetable.model.DamageAccount;

public class DamageAccountListAdapter extends ArrayAdapter<DamageAccount> {

	List<DamageAccount> damageAccounts;
	Activity context;
	boolean edit;
	HashMap<View, Integer> view_position;

	public DamageAccountListAdapter(Activity context, List<DamageAccount> objects, boolean edit) {
		super(context, R.layout.damage_account_list_item, objects);
		this.damageAccounts = objects;
		this.context = context;
		this.edit = edit;
		view_position = new HashMap<View, Integer>();
	}

	public List<DamageAccount> getDamageAccounts() {
		return damageAccounts;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = context.getLayoutInflater();
		View damageAccountListItemView;
		if (edit) {
			damageAccountListItemView = inflater.inflate(R.layout.damage_account_edit_list_item, null, true);
			ImageView deleteImageView = (ImageView) damageAccountListItemView.findViewById(R.id.delete_damage_account_image_view);
			deleteImageView.setColorFilter(ColorFilters.WHITE_CF);
			view_position.put(deleteImageView, position);
		} else {
			damageAccountListItemView = inflater.inflate(R.layout.damage_account_list_item, null, true);
		}

		TextView damageAccountTitle = (TextView) damageAccountListItemView.findViewById(R.id.damageAccountName);

		damageAccountTitle.setText(getDamageAccountTitle(position));

		TextView damagesCountTextView = (TextView) damageAccountListItemView.findViewById(R.id.damages_count);

		damagesCountTextView.setText(getDamageCount(position) + " Damages");
		return damageAccountListItemView;
	}

	private String getDamageAccountTitle(int position) {
		return damageAccounts.get(position).getName();
	}

	private int getDamageCount(int position) {
		if (damageAccounts.get(position).getDamageList() != null)
			return damageAccounts.get(position).getDamageList().size();
		return 0;
	}

	public int getPositionForView(View view) {
		return view_position.get(view);
	}

}
