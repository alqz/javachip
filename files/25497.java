package pl.edu.agh.cs;

public class OrderEvent {
	private String itemName;
    private double price;
	
    public OrderEvent(String itemName, double price) {
		super();
		this.itemName = itemName;
		this.price = price;
	}

	public String getItemName() {
		return itemName;
	}

	public double getPrice() {
		return price;
	}
}
