import java.util.*;
import java.math.*;
import java.io.*;
import cs1.Keyboard;

public class Battle {
  int rounds;
  Warrior player;
  Boss boss;
  int tmphp;
  int tmpmp;
  int tmpatk;
  int tmpdef;
  int tmpmag;
  int tmpres;
  int tmpspd;
  int tmpacc;
  
  public Battle(Warrior p, Boss b) {
    player = p;
    boss = b;
    int rounds = 0;
    tmphp = p.hp;
    tmpmp = p.mp;
    tmpatk = p.power;
    tmpdef = p.defense;
    tmpmag = p.magic;
    tmpres = p.resistance;
    tmpspd = p.speed;
    tmpacc = p.accuracy;
  }

  public int setPlayerMove() {
    int a = 0;
    int b = 0;
    int c = 0;
    int d = 0;
    int e = 0;
    int z = 0;
    while (z != -1) {
      if (a == 0) {
        System.out.println("Select: \n 1: Battle \n 2: Stats");
        a = Keyboard.readInt();
      }
      else if (a == 2) {
        String s = "";
        s = s + "HP: " + tmphp + "\n";
        s = s + "MP: " + tmpmp + "\n"; 
        s = s + "Power: " + tmpatk + "\n";
        s = s + "Defense: " + tmpdef + "\n";
        s = s + "Magic: " + tmpmag + "\n";
        s = s + "Resistance: " + tmpres + "\n";
        s = s + "Speed: " + tmpspd + "\n";
        s = s + "Accuracy: " + (tmpacc + 1) + "%";
        System.out.println(s);
        a = 0;
      }
      else if (a == 1) {
        System.out.println("Select: \n 1: Attack \n 2: Skills \n 3: Item \n 4: Return");
        b = Keyboard.readInt();
        if (b == 4) 
          a = 0;
        else if (b == 1) {
          e = 1;
          z = -1;
        }
        else if (b == 2) {
          System.out.println("Select: \n 1: Warrior \n 2: Fighter \n 3: Warlord \n 4: Savior \n 5: Return");
          c = Keyboard.readInt();
          if (c == 5) 
            a = 0;
          else if (c == 1) {
            System.out.println("Select: \n 1: Impair--5 MP \n 2: Dull--5 MP \n 3: Shockwave--10 MP \n 4: Return");
            d = Keyboard.readInt();
            if (d == 4) 
              a = 0;
            else if (d == 1 && player.haveImpair) {
              if (tmpmp < 5)
                System.out.println("Not enough MP!");
              else {
                e = 2;
                z = -1;
                tmpmp -= 5;
              }
            }
            else if (d == 2 && player.haveDull) {
              if (tmpmp < 5)
                System.out.println("Not enough MP!");
              else {
                e = 3;
                z = -1;
                tmpmp -= 5;
              }
            }
            else if (d == 3 && player.haveShockwave) {
              if (tmpmp < 10)
                System.out.println("Not enough MP!");
              else {
                e = 4;
                z = -1;
                tmpmp -= 10;
              }
            }
            else {
              System.out.println("Error: Not an available choice");
              a = 0;
            }
          }
          else if (c == 2) {
            System.out.println("Select: \n 1: Sunshine Uppercut--15 MP  \n 2: Dice Buff--10 MP \n 3: Fist of the Worlds: Kaio-ken--15 MP \n 4: Spirit Gun--15 MP \n 5: Return");
            d = Keyboard.readInt();
            if (d == 5) 
              a = 0;
            else if (d == 1 && player.haveSunUpper) {
              if (tmpmp < 15)
                System.out.println("Not enough MP!");
              else {
                e = 5;
                z = -1;
                tmpmp -= 15;
              }
            }
            else if (d == 2 && player.haveDice) {
              if (tmpmp < 10)
                System.out.println("Not enough MP!");
              else {
                e = 6;
                z = -1;
                tmpmp -= 10;
              }
            }
            else if (d == 3 && player.haveKaioken) {
              if (tmpmp < 15)
                System.out.println("Not enough MP!");
              else {
                e = 7;
                z = -1;
                tmpmp -= 15;
              }
            }
            else if (d == 4 && player.haveSpiritGun) {
              if (tmpmp < 15)
                System.out.println("Not enough MP!");
              else {
                e = 8;
                z = -1;
                tmpmp -= 15;
              }
            }
            else {
              System.out.println("Error: Not an available choice");
              a = 0;
            }
          }
          else if (c == 3) {
            System.out.println("Select: \n 1: Maximum Cannon--20 MP \n 2: Super Saiyan--25 MP \n 3: Spiraling Sphere: Rasengan--20 MP \n 4: Spirit Shotgun--25 MP \n 5: Turtle Devastation Wave: Kamehameha--30 MP \n 6: Return");
            d = Keyboard.readInt();
            if (d == 6) 
              a = 0;
            else if (d == 1 && player.haveMaxCannon) {
              if (tmpmp < 20)
                System.out.println("Not enough MP!");
              else {
                e = 9;
                z = -1;
                tmpmp -= 20;
              }
            }
            else if (d == 2 && player.haveSuperSaiyan) {
              if (tmpmp < 25)
                System.out.println("Not enough MP!");
              else {
                e = 10;
                z = -1;
                tmpmp -= 25;
              }
            }
            else if (d == 3 && player.haveRasengan) {
              if (tmpmp < 20)
                System.out.println("Not enough MP!");
              else {
                e = 11;
                z = -1;
                tmpmp -= 20;
              }
            }
            else if (d == 4 && player.haveSpiritShotgun) {
              if (tmpmp < 25)
                System.out.println("Not enough MP!");
              else {
                e = 12;
                z = -1;
                tmpmp -= 25;
              }
            }
            else if (d == 5 && player.haveKamehameha) {
              if (tmpmp < 30)
                System.out.println("Not enough MP!");
              else {
                e = 13;
                z = -1;
                tmpmp -= 30;
              }
            }
            else {
              System.out.println("Error: Not an available choice");
              a = 0;
            }
          }
          else if (c == 4) {
            System.out.println("Select: \n 1: Maximum Ingram--30 MP \n 2: Super Saiyan 2--40 MP \n 3: Sunshine Counter--30 MP \n 4: Spirit Wave--25 MP \n 5: Like a Hurricane: Hayate no Gotoku--35 MP \n 6: Falcon Punch--50 MP \n 7: Return");
            d = Keyboard.readInt();
            if (d == 7) 
              a = 0;
            else if (d == 1 && player.haveMaxIngram) {
              if (tmpmp < 30)
                System.out.println("Not enough MP!");
              else {
                e = 14;
                z = -1;
                tmpmp -= 30;
              }
            }
            else if (d == 5 && player.haveHnG) {
              if (tmpmp < 35)
                System.out.println("Not enough MP!");
              else {
                e = 15;
                z = -1;
                tmpmp -= 35;
              }
            }
            else if (d == 3 && player.haveSunCounter) {
              if (tmpmp < 30)
                System.out.println("Not enough MP!");
              else {
                e = 16;
                z = -1;
                tmpmp -= 30;
              }
            }
            else if (d == 4 && player.haveSpiritWave) {
              if (tmpmp < 25)
                System.out.println("Not enough MP!");
              else {
                e = 17;
                z = -1;
                tmpmp -= 25;
              }
            }
            else if (d == 2 && player.haveSuperSaiyan2) {
              if (tmpmp < 40)
                System.out.println("Not enough MP!");
              else {
                e = 18;
                z = -1;
                tmpmp -= 40;
              }
            }
            else if (d == 6 && player.haveFalconPunch) {
              if (tmpmp < 50)
                System.out.println("Not enough MP!");
              else {
                e = 19;
                z = -1;
                tmpmp -= 50;
              }
            }
            else {
              System.out.println("Error: Not an available choice");
              a = 0;
            }
          }
          else {
              System.out.println("Error: Not an available choice");
              a = 0;
            }
        }
        else if (b == 3) {
          System.out.println("Select: \n 1: HP Potions: " + player.hppotions + "\n 2: MP Potions: " + player.mppotions + "\n 3: Return");
          c = Keyboard.readInt();
          if (c == 3) 
            a = 0;
          else if (c == 1) {
            if (player.hppotions <= 0) 
              System.out.println("No HP Potions available for use!");
            else {
              e = 20;
              z = -1;
            }
          }
          else if (c == 2) {
            if (player.mppotions <= 0)
              System.out.println("No MP Potions available for use!");
            else {
              e = 21;
              z = -1;
            }
          }
          else {
              System.out.println("Error: Not an available choice");
              a = 0;
            }
        }
        else {
          System.out.println("Error: Not an available choice");
          a = 0;
        }
      }
      else {
        System.out.println("Error: Not an available choice");
        a = 0;
      }
    }
    return e;
  }
  
  public int playerattack() {
    Random g = new Random();
    double dmgmultiplier = ((100.0 - g.nextInt(100 - player.accuracy)) / 100) * tmpatk / boss.defense;
    int dmg = (int)(dmgmultiplier * tmpatk);
    boss.hp -= dmg;
    return dmg;
  }
  public int impair() {
    boss.defense *= 0.85;
    return boss.defense;
  }
  public int dull() {
    boss.power *= 0.85;
    return boss.power;
  }
  public int shockwave() {
    Random g = new Random();
    double dmgmultiplier = ((120.0 - g.nextInt(100 - player.accuracy)) / 100) * (tmpatk) / (boss.defense / 1.5);
    int dmg = (int)(dmgmultiplier * tmpatk);
    boss.hp -= dmg;
    return dmg;
  }
  public int sunshineupper() {
    Random g = new Random();
    double dmgmultiplier = ((130.0 - g.nextInt(100 - player.accuracy)) / 100) * (1.5 * tmpatk) / (boss.defense / 1.25);
    int dmg = (int)(dmgmultiplier * tmpatk);
    boss.hp -= dmg;
    return dmg;
  }
  public int spiritgun() {
    Random g = new Random();
    double dmgmultiplier = ((135.0 - g.nextInt(100 - player.accuracy)) / 100) * (2 * tmpmag) / (boss.resistance / 1.5);
    int dmg = (int)(dmgmultiplier * tmpatk);
    boss.hp -= dmg;
    return dmg;
  }
  public int maximumcannon() {
    Random g = new Random();
    double dmgmultiplier = ((150.0 - g.nextInt(100 - player.accuracy)) / 100) * (1.5 * tmpatk) / (boss.defense / 1.25);
    int dmg = (int)(dmgmultiplier * tmpatk);
    boss.hp -= dmg;
    return dmg;
  }
  public int rasengan() {
    Random g = new Random();
    double dmgmultiplier = ((140.0 - g.nextInt(100 - player.accuracy)) / 100) * (3 * tmpmag) / (boss.defense / 2);
    int dmg = (int)(dmgmultiplier * tmpatk);
    boss.hp -= dmg;
    return dmg;
  }
  public int spiritshotgun() {
    Random g = new Random();
    double dmgmultiplier = ((145.0 - g.nextInt(100 - player.accuracy)) / 100) * (3 * tmpmag) / (boss.resistance);
    int dmg = (int)(dmgmultiplier * tmpatk);
    boss.hp -= dmg;
    return dmg;
  }
  public int kamehameha() {
    Random g = new Random();
    double dmgmultiplier = ((200.0 - g.nextInt(100 - player.accuracy)) / 100) * (3 * tmpmag) / (boss.defense / 1.5);
    int dmg = (int)(dmgmultiplier * tmpatk);
    boss.hp -= dmg;
    return dmg;
  }
  public int maximumingram() {
    Random g = new Random();
    double dmgmultiplier = ((180.0 - g.nextInt(100 - player.accuracy)) / 100) * (2 * tmpatk) / boss.defense;
    int dmg = (int)(dmgmultiplier * tmpatk);
    boss.hp -= dmg;
    return dmg;
  }
  public int hayate() {
    Random g = new Random();
    double dmgmultiplier = ((150.0 - g.nextInt(100 - player.accuracy)) / 100) * (3 * tmpmag) / (boss.defense / 2.5);
    int dmg = (int)(dmgmultiplier * tmpatk);
    boss.hp -= dmg;
    tmpspd *= 1.25;
    return dmg;
  }
  public int scounter() {
    Random g = new Random();
    double dmgmultiplier = ((100.0 - g.nextInt(100 - player.accuracy)) / 100) * 1.5 * (player.hp - tmphp) / boss.defense;
    int dmg = (int)(dmgmultiplier * tmpatk);
    boss.hp -= dmg;
    return dmg;
  }
  public int spiritwave() {
    Random g = new Random();
    double dmgmultiplier = ((175.0 - g.nextInt(100 - player.accuracy)) / 100) * (3 * tmpmag) / (boss.resistance / 2);
    int dmg = (int)(dmgmultiplier * tmpatk);
    boss.hp -= dmg;
    return dmg;
  }
  public int falconpunch() {
    Random g = new Random();
    double dmgmultiplier = ((200.0 - g.nextInt(100 - player.accuracy)) / 100) * (2.5 * tmpatk) / (boss.defense / 1.5);
    int dmg = (int)(dmgmultiplier * tmpatk);
    boss.hp -= dmg;
    return dmg;
  }
  public void kaioken() {
    tmpatk *= 1.1;
    tmpspd *= 1.1;
  }
  public void supersaiyan() {
    tmpatk *= 1.2;
    tmpspd *= 1.3;
  }
  public void supersaiyan2() {
    tmpatk *= 1.3;
    tmpspd *= 1.5;
  }
  public String dice() {
    Random g = new Random();
    int a = g.nextInt(5);
    String s = "";
    if (a == 0) {
      tmpatk *= 1.3;
      s = "Power";
    }
    else if (a == 1) {
      tmpdef *= 1.3;
      s = "Defense";
    }
    else if (a == 2) {
      tmpmag *= 1.3;
      s = "Magic";
    }
    else if (a == 3) {
      tmpres *= 1.3;
      s = "Resistance";
    }
    else if (a == 4) {
      tmpspd *= 1.3;
      s = "Speed";
    }
  return s;
  }
  
  public void playerperformaction(int e) {
    if (e == 1) {
        System.out.println("You attacked! \n You dealt " + playerattack() + " damage");
    }
    else if (e == 2) {
      System.out.println("You used Impair! " + boss.name + "'s defense was lowered to " + impair());
    }
    else if (e == 3) {
      System.out.println("You used Dull! " + boss.name + "'s attack was lowered to " + dull());
    }
    else if (e == 4) {
      System.out.println("You used Shockwave! \n You dealt " + shockwave() + " damage");
    }
    else if (e == 5) {
      System.out.println("You used Sunshine Uppercut! \n You dealt " + sunshineupper() + " damage");
    }
    else if (e == 6) {
      System.out.println("You used Dice Buff! Your " + dice() + " rose!");
    }
    else if (e == 7) {
      kaioken();
      System.out.println("You used Kaio-Ken! Your Power and Speed rose!");
    }
    else if (e == 8) {
      System.out.println("You used Spirit Gun! \n You dealt " + spiritgun() + " damage");
    }
    else if (e == 9) {
      System.out.println("You used Maximum Cannon! \n You dealt " + maximumcannon() + " damage");
    }
    else if (e == 10) {
      supersaiyan();
      System.out.println("You used Super Saiyan! Your Power and Speed sharply rose!");
    }
    else if (e == 11) {
      System.out.println("You used Rasengan! \n You dealt " + rasengan() + " damage");
    }
    else if (e == 12) {
      System.out.println("You used Spirit ShotGun! \n You dealt " + spiritshotgun() + " damage");
    }
    else if (e == 13) {
      System.out.println("You used Kamehameha! \n You dealt " + kamehameha() + " damage");
    }
    else if (e == 14) {
      System.out.println("You used Maximum Ingram! \n You dealt " + maximumingram() + " damage");
    }
    else if (e == 15) {
      System.out.println("You used Hayate No Gotoku! \n You dealt " + hayate() + " damage. Your Speed increased!");
    }
    else if (e == 16) {
      System.out.println("You used Sunshine Counter! \n You dealt " + scounter() + " damage");
    }
    else if (e == 17) {
      System.out.println("You used Spirit Wave! \n You dealt " + spiritwave() + " damage");
    }
    else if (e == 18) {
      supersaiyan2();
      System.out.println("You used Super Saiyan 2! Your Power and Speed immensely rose!");
    }
    else if (e == 19) {
      System.out.println("You used Falcon Punch! \n You dealt " + falconpunch() + " damage");
    }
    else if (e == 20) {
      player.hppotions--;
      tmphp += (player.hp * 0.25);
      System.out.println("Used HP Potion! Recovered " + (player.hp * 0.25) + " HP");
    }
    else if (e == 21) {
      player.mppotions--;
      tmpmp += (player.mp * 0.3);
      System.out.println("Used MP Potion! Recovered " + (player.mp * 0.3) + " MP");
    }
  }
  
  public void enemyperformaction() {
    Random g = new Random();
    int f = g.nextInt(10);
    if (f < 4) {
      tmphp -= boss.attack(tmpdef);
      System.out.println(boss.name + " attacked! \n" + boss.name + " dealt " + boss.attack(tmpdef) + " damage");
    }
    else if (f < 6) 
      System.out.println(boss.name + " rested!" + boss.name + "'s " + boss.buff() + " rose!");
    else {
      tmphp -= boss.skill(tmpdef);
      System.out.println(boss.name + " used an ability! \n" + boss.name + " dealt " + boss.skill(tmpdef) + " damage");
    }
  }
                 
  public boolean sequence() {
    int z = 0;
    while (z != -1) {
      int e = setPlayerMove(); //set the player's next move
      if (tmpspd >= boss.speed || e == 20 || e == 21) { //checks which side goes first
        playerperformaction(e); //player performs the chosen action
        if (boss.hp <= 0) {
          System.out.println("You won the battle!");
          return true;
        }
        enemyperformaction(); //enemy performs a randomly chosen action
        if (tmphp <= 0) {
          System.out.println("You lost!");
          return false;
        }
      }
      else {
        enemyperformaction();
        if (tmphp <= 0) {
          System.out.println("You lost!");
          return false;
        }
        playerperformaction(e);
        if (boss.hp <= 0) {
          System.out.println("You won the battle!");
          return true;
        }
      }
      if (tmpmp < (int)(player.mp * 0.9)) {
        tmpmp += (int)(player.mp * 0.05);
        System.out.println("Recovered " + (int)(player.mp * 0.05) + " MP");
      }
      System.out.println("Player HP: " + tmphp);
      System.out.println("Player MP: " + tmpmp);
      System.out.println("Boss HP: " + boss.hp);
      rounds++;
    }
 return false;
  }
}