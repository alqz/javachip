package oxen.game.components.room;

import java.io.Serializable;
import oxen.game.components.conditions.ConditionList;

/**
 *
 * @author matt
 */
@SuppressWarnings("serial")
public class Door implements Serializable {

    private ConditionList conditions;
    private Direction direction;
    private String roomId;

    /**
     * Constructor for class Door
     * @param direction
     * @param toRoomId 
     */
    public Door(Direction direction, String toRoomId) {
        this.direction = direction;
        this.roomId = toRoomId;
    }

    /**
     * Get the id of the room
     * @return 
     */
    public String getRoomId() {
        return roomId;
    }

    /**
     * get the conditions attached to the door
     * @return 
     */
    public ConditionList getConditions() {
        return conditions;
    }

    /**
     * Set the conditions attached to the door
     * @param conditions 
     */
    public void setConditions(ConditionList conditions) {
        this.conditions = conditions;
    }

    /**
     * Get the direction in which the door is place in the room
     * @return 
     */
    public Direction getDirection() {
        return direction;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
