/**
 * Copyright 1999-2009 The Pegadi Team
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * A server for publications.
 *
 * @author Jørgen Binningsbø <jb@underdusken.no>
 * @author Håvard Wigtil <havardw at pvv.org>
 */


package org.pegadi.server.publication;

import org.pegadi.model.Publication;
import org.pegadi.server.PublicationServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

public class PublicationServerImpl implements PublicationServer {

    private JdbcTemplate template;
    private PublicationRowMapper mapper;

    private final Logger log = LoggerFactory.getLogger(getClass());

    public PublicationServerImpl() {
        mapper = new PublicationRowMapper();
    }

    public void setDataSource(DataSource dataSource) {
        template = new JdbcTemplate(dataSource);
    }

    /**
     *  returns one publication
     */
    public Publication getPublicationByID(int ID) {
        return template.query("SELECT * FROM Publication WHERE ID=?", mapper, ID).get(0);
    }

    /**
     * Returns all years which have publications.
     *
     * @return an array of <code>int</code>'s
     */
    public List<Integer> getYearsWithPublications() {   // never used?
        List<Publication> pubs = template.query("SELECT DISTINCT year(releasedate) FROM Publication", mapper);
        List<Integer> res = new ArrayList<Integer>();
        Calendar cal = new GregorianCalendar();
        for(Publication pub : pubs) {
            cal.setTime(pub.getReleaseDate());
            res.add(cal.get(Calendar.YEAR));
        }
        return res;
    }

    /**
     * returns all the publications scheduled for releasing a given year
     */
    public List<Publication> getPublicationsByYear(int year) {
            return template.query("SELECT * FROM Publication WHERE year(releasedate)=? ORDER BY releasedate DESC",
                    mapper, year);
    }

    /**
     * this method returns an array of Publications.
     * Active publications are defined as having a release-date higher
     * than the current date.
     */
    public List<Publication> getActivePublications() {
            return template.query("SELECT * FROM Publication WHERE releasedate > CURRENT_DATE ORDER BY releasedate ASC",
                    mapper);
    }


    /**
     * Saves or updates a publication in persistent storage. If the ID is less than or equals to zero,
     * it is created, else it is updated.
     *
     * @return ID of created publication
     */
    public int saveOrUpdatePublication(final Publication pub) {
        if(pub.getId() > 0) {
            template.update("UPDATE Publication SET name=?, releasedate=?, printdate=?, deadlineText=?, description=?, number=? WHERE ID=?",
                pub.getName(),
                new java.sql.Date(pub.getReleaseDate() != null ? pub.getReleaseDate().getTime() : new java.util.Date().getTime()),
                new java.sql.Date(pub.getPrintDate() != null ? pub.getPrintDate().getTime() : new java.util.Date().getTime()),
                new java.sql.Date(pub.getDeadlineText() != null ? pub.getDeadlineText().getTime() : new java.util.Date().getTime()),
                pub.getDescription(), pub.getNumber(), pub.getId());
        }
        else {
            final String insertSql = "INSERT INTO Publication (name, releasedate, printdate, deadlineText, description, number) VALUES (?, ?, ?, ?, ?, ?)";
            KeyHolder keyHolder = new GeneratedKeyHolder();

            template.update(
                    new PreparedStatementCreator() {
                        public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                            PreparedStatement stmt = connection.prepareStatement(insertSql, new String[] {"ID"});
                            stmt.setString(1, pub.getName());
                            stmt.setDate(2, new java.sql.Date(pub.getReleaseDate() != null ? pub.getReleaseDate().getTime() : new java.util.Date().getTime()));
                            stmt.setDate(3, new java.sql.Date(pub.getPrintDate() != null ? pub.getPrintDate().getTime() : new java.util.Date().getTime()));
                            stmt.setDate(4, new java.sql.Date(pub.getDeadlineText() != null ? pub.getDeadlineText().getTime() : new java.util.Date().getTime()));
                            stmt.setString(5, pub.getDescription() != null ? pub.getDescription() : "");

                            Calendar cal = Calendar.getInstance();
                            cal.setTime(pub.getReleaseDate() != null ? pub.getReleaseDate() : new java.util.Date());
                            stmt.setInt(6, getPublicationsByYear(cal.get(Calendar.YEAR)).size()+1);

                            return stmt;
                        }
                    }, keyHolder );
            pub.setId(keyHolder.getKey().intValue());
        }
        return pub.getId();
    }

    private class PublicationRowMapper implements ParameterizedRowMapper<Publication> {
        
        public Publication mapRow(ResultSet rs, int i) throws SQLException {
            Publication pub = new Publication(rs.getInt("ID"));
            pub.setName(rs.getString("name"));
            pub.setDescription(rs.getString("description"));
            pub.setReleaseDate(rs.getDate("releasedate"));
            pub.setPrintDate(rs.getDate("printdate"));
            pub.setDeadlineText(rs.getDate("deadlineText"));
            pub.setNumber(rs.getInt("number"));
            return pub;
        }
    }


}
