package calendar;

import java.util.ArrayList;

/**
* @author Adam Barrell
* @date 24/02/2012
*
* @brief
* This class will hold a collection of Setting objects.
*/
public class SettingsCollection extends Collection<Setting> {

   /**
	* Constructor creates a new ArrayList to hold only objects
	* of type Setting.
	*/
    public SettingsCollection() {
        m_data = new ArrayList<Setting>();
    }
    
   /**
    * Sets the value of an existing setting only if the name
    * matches that of the setting. If a match is not found,
    * a new setting object is created and added to the collection array.
    * @param name
    * @param value
    */
    public boolean Set(String name, String value) {
        for(int i = 0; i < m_data.size(); i++) {
            if(m_data.get(i).GetName().equals(name)) {
                m_data.get(i).SetValue(value);
                return true;
            }
        }
        Setting data = new Setting(name, value);
        m_data.add(data);
        return true;
    }
    
   /**
    * Gets an the value of an existing setting only if
    * there exists a setting with parameter name.
    * @param name The name of the setting.
    * @return The value of the matched setting.
    */
    public String Get(String name) {
       for(int i = 0; i < m_data.size(); i++) {
            if(m_data.get(i).GetName().equals(name)) {
                return m_data.get(i).GetValue();
            }
        }
       throw new IllegalArgumentException("The Setting "+name+
    		   " is not defined!");
   	}
   
   /**
    * Checks if a setting exists which matches parameter name.
    * @param name The name of the setting.
    * @return Returns true if match found, false otherwise.
    */
   public boolean Exists(String name) {
       for(int i = 0; i < m_data.size(); i++) {
            if(m_data.get(i).GetName().equals(name)) {
                return true;
            }
        }
       return false;
   }
   
  /**
   * Gets the value of a setting as an integer only if
   * a setting matches the parameter name.
   * @param name The name of the setting.
   * @return Returns matched setting value as an integer.
   */
   public int GetInt(String name) {
       for(int i = 0; i < m_data.size(); i++) {
            if(m_data.get(i).GetName().equals(name)) {
                return m_data.get(i).GetValueInt();
            }
        }
       throw new IllegalArgumentException("The Setting "+name+
    		   " is not defined!");
   }
   
   
  /**
   * Gets the value of a setting as a float only if
   * a setting matches the parameter name.
   * @param name The name of the setting.
   * @return Returns matched setting value as a float.
   */
   public double GetFloat(String name) {
       for(int i = 0; i < m_data.size(); i++) {
            if(m_data.get(i).GetName().equals(name)) {
                return m_data.get(i).GetValueFloat();
            }
        }
       throw new IllegalArgumentException("The Setting "+name+
    		   " is not defined!");
   }
   
   public static void main(String args[]) {
	   
	   SettingsCollection collection = new SettingsCollection();
	   
	   /* Test Set(name, value) */
	   collection.Add(new Setting("Language", "EN"));
	   collection.Set("Language", "ES");
	   if (collection.Get("Language")=="ES") {
		   System.out.println("Pass: Setter method succeeded");
	   } else {
		   System.out.println("Fail: Setter method failed");
	   }
	   // Test setting a setting value to a setting that does not exist
	   collection.Set("NotExists", "5");
	   if (collection.Get("NotExists")=="5") {
		   System.out.println("Pass: New setting created when setting a " +
		   		"value "+
				   "to a setting that does not exist");
	   } else {
		   System.out.println("New setting did not get made");
	   }
	   
	   // Test Get(name) with name that does not exist
	   try {
		   collection.Get("InvalidName");
		   System.out.println("Fail: InvalidName setting should not exist!");
	   } catch (IllegalArgumentException e) {
		   System.out.println("Pass: "+e.getMessage());
	   }
	   
	   // Test Exists(name) with name that does not exist
	   if (collection.Exists("InvalidName")) {
		   System.out.println("Fail: The setting InvalidName should " +
		   		"not exist!");
	   } else {
		   System.out.println("Pass: The setting InvalidName does not exist");
	   }
	   
	   // Test Exists(name) with name that does exist
	   if (collection.Exists("Language")) {
		   System.out.println("Pass: The setting Language does exist");
	   } else {
		   System.out.println("Fail: The setting Language should not exist");
	   }
	   
	   // Test GetInt(name) with name that does exist
	   Setting intVolume = new Setting("IntVolume", 5);
	   collection.Add(intVolume);
	   if (collection.GetInt("IntVolume")==5) {
		   System.out.println("Pass: The setting IntVolume does exist" +
		   		" and equal 5");
	   } else {
		   System.out.println("Fail: The setting IntVolume may not " +
		   		"exist or equal 5");
	   }
	   
	   // Test GetInt(name) with name that does not exist
	   try {
		   collection.GetInt("NextIntVolume");
		   System.out.println("Fail: An integer has been returned from " +
		   		"a setting that "+
				   "does not exist!");
	   } catch (IllegalArgumentException e) {
		   System.out.println("Pass: " +e.getMessage());
	   }
	   
	   // Test GetFloat(name) with name that does exist
	   Setting floatVolume = new Setting("FloatVolume", 5.5);
	   collection.Add(floatVolume);
	   if (collection.GetFloat("FloatVolume")==5.5) {
		   System.out.println("Pass: The setting FloatVolume does exist " +
		   		"and equal 5.5");
	   } else {
		   System.out.println("Fail: The setting FloatVolume may not " +
		   		"exist or equal 5.5");
	   }
	   
	   // Test GetFloat(name) with name that does not exist
	   try {
		   collection.GetInt("NextFloatVolume");
		   System.out.println("Fail: A float value has been returned " +
		   		"from a setting that "+
				   "does not exist!");
	   } catch (IllegalArgumentException e) {
		   System.out.println("Pass: " +e.getMessage());
	   }
   }
}
