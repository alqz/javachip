package com.calypso.bildroid;

import java.io.IOException;
import java.text.DateFormat;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.SQLException;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

public class NewsListActivity extends Activity {

	/* VARIABLES */

	// to use in subsequent calls to the log methods.
	private static final String TAG = "NewsListActivity";
	
	private static final String PREFS_NEWS = "NewsPrefsFile";
	private Button menuButton;
	private Button aboutButton;
	private Button refreshButton;
	private ListView lv;
	private ProgressDialog progressDialog;
	private CheckForUpdates searchThread;
	private NewsDatabase db;
	private Article[] myArr;

	OnClickListener listener = new OnClickListener() {

		public void onClick(View arg0) {
			Toast toast = Toast.makeText(getApplicationContext(),
					"Not implemented yet!", Toast.LENGTH_SHORT);
			toast.show();
		}

	};

	/* METHODS */

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.news_main);

		Refresh();

		// find menu button in the UI by its ID
		menuButton = (Button) findViewById(R.id.newsListMenuButton);
		aboutButton = (Button) findViewById(R.id.newsListAboutButton);
		refreshButton = (Button) findViewById(R.id.newsListRefreshButton);

		// register a callback to be invoked when this view is clicked
		menuButton.setOnClickListener(listener);
		aboutButton.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				AlertDialog.Builder builder = new AlertDialog.Builder(NewsListActivity.this);
				builder.setMessage(
						"In this section, BilDroid project aims to build a faster and more comfortable way for users to read BilNews. I would appreciate it if you send any suggestion or bug to me (hcelaloner@gmail.com).")
						.setCancelable(true)
						.setPositiveButton("ok",
								new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int id) {
								dialog.cancel();
							}
						});
				AlertDialog alert = builder.create();
				alert.show();
			}
		});

		refreshButton.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				Refresh();
			}
		});

		lv = (ListView) findViewById(R.id.newsList);
		lv.setTextFilterEnabled(true);

		lv.setOnItemClickListener(new OnItemClickListener() {

			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Intent openDetailedView = new Intent(NewsListActivity.this,
						ArticleDetailedView.class);

				if (myArr[position].getTitle() != null) {
					openDetailedView.putExtra("title",
							myArr[position].getTitle());
				}

				if (myArr[position].getContent() != null) {
					openDetailedView.putExtra("content", myArr[position].getContent());
				}

				if (myArr[position].getImageURL() != null) {

					openDetailedView.putExtra("imageURL", myArr[position]
							.getImageURL().toString());
				}

				if (myArr[position].getArticleURL() != null) {
					openDetailedView.putExtra("url",
							myArr[position].getArticleURL());
				}

				startActivity(openDetailedView);

			}
		});

	}

	@Override
	protected void onStart() {
		super.onStart();
		// The activity is about to become visible.
	}

	@Override
	protected void onResume() {
		super.onResume();
		// The activity has become visible (it is now "resumed").
	}

	@Override
	protected void onPause() {
		super.onPause();
		// Another activity is taking focus (this activity is about to be
		// "paused").
	}

	@Override
	protected void onStop() {
		super.onStop();
		// The activity is no longer visible (it is now "stopped")
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		// The activity is about to be destroyed.
	}

	private void Refresh() {

		// create progress dialog
		progressDialog = new ProgressDialog(NewsListActivity.this);
		progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		progressDialog.setMessage("Fetching latest articles...");
		progressDialog.setIndeterminate(true);
		progressDialog.show();

		// instantiate a NewsDBAdapter
		db = new NewsDatabase(this);

		try {

			// open a connection to the DB, and create a table if one does not
			// yet exist.
			db.open();

		} catch (SQLException e) {
			Log.e(TAG, "Error during opening database");
		}

		// start the progress thread
		searchThread = new CheckForUpdates();
		searchThread.start();		
	}

	/**
	 * boolean isConnected()
	 * 
	 * Tests network connectivity for Wi-Fi and mobile. It determines whether
	 * these network interfaces are available (that is, whether network
	 * connectivity is possible) and/or connected (that is, whether network
	 * connectivity exists and if it is possible to establish sockets and pass
	 * data
	 * 
	 * http://developer.android.com/training/basics/network-ops/connecting.html
	 * 
	 * @return true - if Wi-Fi and mobilenetwork interfaces are available and/or
	 *         connected
	 */
	private boolean isConnected() {
		// This code snippet taken from
		// http://developer.android.com/training/basics/network-ops/managing.html
		ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = connMgr
				.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		boolean isWifiConn = networkInfo.isConnected();
		networkInfo = connMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
		boolean isMobileConn = networkInfo.isConnected();

		if (isWifiConn || isMobileConn) {
			return true;
		}
		return false;
	}

	private class CheckForUpdates extends Thread {

		// to use in subsequent calls to the log methods.
		private static final String INNER_CLASS_TAG = "CheckForUpdates";

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Thread#run()
		 */
		@Override
		public void run() {
			super.run();

			if (isConnected()) {
				// If there is a network connection
				Log.i(INNER_CLASS_TAG, "Network Connection is found ");

				// load saved preferences
				SharedPreferences prefs = getSharedPreferences(PREFS_NEWS,
						MODE_PRIVATE);

				// get the publication date from BilNews website
				Document doc = null;

				try {
					doc = Jsoup.connect(
							"http://www.bilkent.edu.tr/~bilnews/index.html")
							.get();
				} catch (IOException e) {
					Log.e(INNER_CLASS_TAG, "Connection Error ");
				}

				if (doc != null) {
					Element content = doc.getElementById("header");
					Elements links = content.getElementsByTag("p");

					String newsInfo = links.first().text().trim();

					String[] result = newsInfo.split("\\s");
					String publishDate = result[result.length - 3] + " "
							+ result[result.length - 2] + " "
							+ result[result.length - 1];

					try {
						// get a DateFormat instance for formatting and parsing
						// dates
						DateFormat df = DateFormat.getDateInstance();

						if (df.parse(publishDate).after(
								df.parse(prefs.getString("Date",
										"April 24, 2012")))) {

							// send an INFO log message.
							Log.i(INNER_CLASS_TAG,
									"There is a new publish date on website: "
											+ publishDate);

							// send an INFO log message.
							Log.i(INNER_CLASS_TAG,
									"Program will start to fetch new data");

							FetchNews();

							// set a new Date to Preferences.
							SharedPreferences.Editor editor = prefs.edit();
							editor.putString("Date", publishDate);

							// we must commit the preferences or they won't be
							// saved!
							editor.commit();
							Log.i(INNER_CLASS_TAG,
									"The date which is stored in SharedPreferences is updated to "
											+ publishDate);

						}
					} catch (java.text.ParseException e) {
						Log.e(INNER_CLASS_TAG,
								"An error occured during date parsing."
										+ " (Method: checkUpdatesorNews)");
					} // end of try-catch block

				} // end of "if (doc != null)" block

			} // end of "if (isConnected())" block

			myArr = db.getAllArticles();

			lv.post(new Runnable() {
				public void run() {
					lv.setAdapter(new ListAdapter(NewsListActivity.this, myArr , null));
				}
			});

			db.close();

			// dismiss the dialog, remove it from the screen
			progressDialog.dismiss();

		}// end of run()

		private void FetchNews() {

			for (int i = 1; i < 3; i++) {

				// Store a null reference in the variable doc
				Document doc = null;

				// fetch the specified URL and parse to a HTML DOM
				try {
					doc = Jsoup.connect(
							"http://www.bilkent.edu.tr/~bilnews/index.html")
							.get();
				} catch (IOException e) {
					e.printStackTrace();
				}

				if (doc != null) {
					// fetch elements in sidemenu
					Elements sidemenu = doc.select("div.sidemenu");

					// select html links from sidemenu
					Elements links = sidemenu.get(i).select("a[href$=.html]");

					// iterate over all links
					for (Element link : links) {
						// instantiate an article object using link
						Article aNews = new Article(link.attr("abs:href"));

						// insert instantiated article object to database
						db.insertArticle(aNews);
					}
				}

			}
		} // end of FetchNews()

	}
}
