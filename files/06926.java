
package webservice.jaxws;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import webservice.domain.Transport;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getTransportTResponse", propOrder = {
    "_return"
})
public class GetTransportTResponse {

    @XmlElement(name = "return", nillable = true)
    protected List<Transport> _return;

    public List<Transport> getReturn() {
        if (_return == null) {
            _return = new ArrayList<Transport>();
        }
        return this._return;
    }

}
