package org.pegadi.server.article;

import org.pegadi.model.ArticleType;
import org.pegadi.server.ArticleTypeServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * User: jan-preben
 * Date: Sep 21, 2010
 */
@Service
public class ArticleTypeServerImpl implements ArticleTypeServer {

    private final Logger log = LoggerFactory.getLogger(getClass());
    private JdbcTemplate template;
    private ArticleTypeRowMapper articleTypeMapper = new ArticleTypeRowMapper();

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.template = new JdbcTemplate(dataSource);
    }

    /**
     * Returns the articletype with the given ID
     *
     * @param ID the ID of the articletype
     * @return an articletype, or null if no articletype found
     */
    public ArticleType getArticleType(int ID) {
        if (ID == 0) {
            return null;
        }
        try {
            return template.queryForObject("SELECT * FROM ArticleType WHERE ID=?", articleTypeMapper, ID);
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }


    /**
     * Returns all available article types.
     *
     * @return an array of <code>ArticleType</code>s.
     */
    public List<ArticleType> getArticleTypes() {
        return template.query("SELECT * FROM ArticleType", articleTypeMapper);
    }

    

    private class ArticleTypeRowMapper implements ParameterizedRowMapper<ArticleType> {

        public ArticleType mapRow(ResultSet rs, int rowNum) throws SQLException {
            ArticleType articleType = new ArticleType(rs.getInt("ID"));
            articleType.setDescription(rs.getString("description"));
            articleType.setTagname(rs.getString("tagname"));
            articleType.setTemplate(rs.getString("template"));
            articleType.setName(rs.getString("name"));
            return articleType;
        }
    }
}
