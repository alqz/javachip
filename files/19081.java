package c301.w11.View;

import java.io.File;

import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.widget.TabHost;

/**
 * This activity class is used to implement the tab selection user interface.
 */
public class SkinTrackerActivity extends TabActivity 
{
	
	/**
	 * Called when the activity is starting. The tabbed activity options are:
	 * 
	 * <ul>
	 * <li>		Photo Log: 	Display the photos in chronological order.
	 * <li>		Tag Log:   	Display the list of tag groups.
	 * <li>		Comparison:	Display the comparison analysis.
	 * <li>		Search:		Display the search box.
	 * </ul>
	 * 
	 * @param savedInstanceState If the activity is being re-initialized after previously being 
	 * shut down then this Bundle contains the data it most recently supplied in 
	 * onSaveInstanceState(Bundle). Note: Otherwise it is null. 
	 */
    public void onCreate(Bundle savedInstanceState) 
    {
    	String folder = Environment.getExternalStorageDirectory().getAbsolutePath() + "/skin";
        File folderF = new File(folder);
        if (!folderF.exists())
        {
            folderF.mkdir();
        }
        
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        TabHost tabHost = getTabHost();
        TabHost.TabSpec spec;
        Intent intent;

        intent = new Intent().setClass(this, PhotoLogActivity.class);
        spec = tabHost.newTabSpec("photolog").setIndicator("Log").setContent(intent);
        tabHost.addTab(spec);
        
        intent = new Intent().setClass(this, TagActivity.class);
        spec = tabHost.newTabSpec("tag").setIndicator("Tag").setContent(intent);
        tabHost.addTab(spec);
        
        intent = new Intent().setClass(this, SearchActivity.class);
        spec = tabHost.newTabSpec("search").setIndicator("Search").setContent(intent);
        tabHost.addTab(spec);

        tabHost.setCurrentTab(0);
    }
}