package com.wtf.andropet.Class;

import java.util.Map;
import java.util.TreeMap;

import com.wtf.andropet.Global;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Picture;
import android.graphics.Point;
import android.graphics.RectF;

public class AndropetGraphicObject {
	public class Sprite {
		private AndropetGraphicObject.DrawingOrder order;
		
		private Bitmap resultImageBitmap;
		private int maskBitmapRes;
		private int fillBitmapRes;
		private Picture fillPicture;
		private int fillColor;
		
		private Map<Point, RectF> specialFrameRectByFrameNumber; 
		private int frameCol, frameRow;
		
		private boolean _needGenerateResultImage;
		
		public Sprite(int frameCol, int frameRow) {
			this.order = DrawingOrder.ORDER_DEFAULT;
			
			this.frameCol = (frameCol < 1 ? 1 : frameCol);
			this.frameRow = (frameRow < 1 ? 1 : frameRow);
			
			this.resultImageBitmap = null;

			this.maskBitmapRes = 0;
			this.fillBitmapRes = 0;
			this.fillColor = Color.TRANSPARENT;
		}
		
		private Bitmap getMaskBitmap() {
			return BitmapFactory.decodeResource(Global.applicationContext.getResources(), this.maskBitmapRes, AndropetGraphicObject.BITMAPOPT_MASK);
		}

		public void setMaskBitmap(int maskBitmapResourceId) {
			this.maskBitmapRes = maskBitmapResourceId;
			
			_needGenerateResultImage = true;
		}

		private Bitmap getFillBitmap() {
			return BitmapFactory.decodeResource(Global.applicationContext.getResources(), this.fillBitmapRes);
		}

		public void setFillBitmap(int fillBitmapResourceId) {
			this.fillBitmapRes = fillBitmapResourceId;
			
			_needGenerateResultImage = true;
		}

		public void setFillPicture(Picture fillPicture) {
			this.fillPicture = fillPicture;
			
			_needGenerateResultImage = true;
		}

		public void setFillColor(int fillColor) {
			this.fillColor = fillColor;
		}

		public void setDrawingOrder(AndropetGraphicObject.DrawingOrder order) {
			this.order = order;
		}

		public void setSpecialFrameRect(Point frameIndex, RectF frameRect) {
			if (this.specialFrameRectByFrameNumber == null) {
				this.specialFrameRectByFrameNumber = new TreeMap<Point, RectF>();
			}
			
			this.specialFrameRectByFrameNumber.put(frameIndex, frameRect);
		}
		
		private RectF getFrameRect(Point frameIndex) {
			RectF _displayRectF = this.specialFrameRectByFrameNumber.get(frameIndex);
			
			if (_displayRectF != null) {
				return _displayRectF;
			} else {
				float spriteWidth = this.resultImageBitmap.getWidth() / this.frameCol;
				float spriteHeight = this.resultImageBitmap.getHeight() / this.frameRow;
				
				return new RectF(spriteWidth * frameIndex.x, spriteHeight * frameIndex.y, spriteWidth, spriteHeight);
			}
		}
		
		private RectF getResultImageRect() {
			Bitmap _fillBitmapBounds = BitmapFactory.decodeResource(Global.applicationContext.getResources(), this.fillBitmapRes, AndropetGraphicObject.BITMAPOPT_BOUNDSONLY);
			Bitmap _maskBitmapBounds = BitmapFactory.decodeResource(Global.applicationContext.getResources(), this.maskBitmapRes, AndropetGraphicObject.BITMAPOPT_BOUNDSONLY);
			
			RectF _returnRect = new RectF();
			
			if (this.order == DrawingOrder.ORDER_DEFAULT) {
				_returnRect.union(_fillBitmapBounds.getWidth(), _fillBitmapBounds.getHeight());
				_returnRect.union(_maskBitmapBounds.getWidth(), _maskBitmapBounds.getHeight());
				_returnRect.union(this.fillPicture.getWidth(), this.fillPicture.getHeight());
			} else if (this.order == DrawingOrder.ORDER_VECTOR_ONLY) {
				_returnRect.union(this.fillPicture.getWidth(), this.fillPicture.getHeight());
			} else {
				return null;
			}

			return _returnRect;
		}
		
		private Bitmap getSpriteResult() {
			if (_needGenerateResultImage) {
				generateResultImage();
			}
			
			return null;
		}

		private void generateResultImage() {
			if (this.resultImageBitmap != null) {
				this.resultImageBitmap.recycle();
			}
			RectF _resultImageRect = getResultImageRect();
			this.resultImageBitmap = Bitmap.createBitmap((int) _resultImageRect.width(), (int) _resultImageRect.height(), Bitmap.Config.ARGB_8888);
			
			Canvas _resultCanvas = new Canvas(this.resultImageBitmap);
			
			if (this.order == DrawingOrder.ORDER_DEFAULT) {
				// Draw Color
				drawColorOnResultImage(_resultCanvas);
				// Draw Vector
				drawPictureOnResultImage(_resultCanvas);
				// Draw Bitmap
				drawFillBitmapOnResultImage(_resultCanvas);
				// Draw Mask
				drawMaskBitmapOnResultImage(_resultCanvas);
				
			} else if (this.order == DrawingOrder.ORDER_VECTOR_ONLY) {
				drawPictureOnResultImage(_resultCanvas);
			}
			
			_needGenerateResultImage = false;
		}
		
		private void drawColorOnResultImage(Canvas resultImageCanvas) {
			if (this.fillColor != Color.TRANSPARENT) {
				resultImageCanvas.drawColor(fillColor);
			}
		}
		
		private void drawPictureOnResultImage(Canvas resultImageCanvas) {
			if (this.fillPicture != null) {
				resultImageCanvas.drawPicture(this.fillPicture);
			}
		}
		
		private void drawFillBitmapOnResultImage(Canvas resultImageCanvas) {
			Bitmap _fillBitmap = getFillBitmap();
			if (_fillBitmap != null) {
				resultImageCanvas.drawBitmap(_fillBitmap, 0, 0, null);
			}
		}
		
		private void drawMaskBitmapOnResultImage(Canvas resultImageCanvas) {
			Bitmap _maskBitmap = getMaskBitmap();
			if (_maskBitmap != null) {
				resultImageCanvas.drawBitmap(_maskBitmap, 0, 0, null);
			}
		}
	}
	
	public enum DrawingOrder {
		/** Drawing order is as the following: color, vector graphic, bitmap, mask bitmap **/
		ORDER_DEFAULT, 
		ORDER_VECTOR_ONLY
	}
	
	private static final BitmapFactory.Options BITMAPOPT_MASK;
	private static final BitmapFactory.Options BITMAPOPT_BOUNDSONLY;
	
	static {
		BITMAPOPT_MASK = new BitmapFactory.Options();
		BITMAPOPT_MASK.inPreferredConfig = Config.ALPHA_8;
		
		BITMAPOPT_BOUNDSONLY = new BitmapFactory.Options();
		BITMAPOPT_BOUNDSONLY.inJustDecodeBounds = true;
	};
	
	public AndropetGraphicGeometry displayProperty;
	private AndropetGraphicObject.Sprite sprite;
	private Point displayFrameIndex;
	
	public AndropetGraphicObject() {
		this.displayProperty = new AndropetGraphicGeometry();
		this.sprite = null;
		
		this.displayFrameIndex = new Point(0, 0);
	}
	
	public Point getFrameIndex() {
		return this.displayFrameIndex;
	}

	public void setFrameIndex(int frameCol, int frameRow) {
		this.displayFrameIndex.set(frameCol, frameRow);
	}

	public void setSprite(AndropetGraphicObject.Sprite sprite) {
		this.sprite = sprite;
	}

	public void onDraw(Canvas canvas) {
		sprite.getSpriteResult();
	}
}
