package mod_nw;

import net.minecraft.src.EntityPlayer;
import net.minecraft.src.EnumAction;
import net.minecraft.src.Item;
import net.minecraft.src.ItemStack;
import net.minecraft.src.PotionEffect;
import net.minecraft.src.World;

public class ItemCBucketMilk extends Item
{
    public int k ;
    public ItemCBucketMilk(int par1)
    {
        super(par1);
        setMaxStackSize(1);
        this.k = 0;
    }
    public ItemCBucketMilk(int par1, int type)
    {
        super(par1);
        setMaxStackSize(1);
        this.k = type;
    }

    public ItemStack onFoodEaten(ItemStack par1ItemStack, World par2World, EntityPlayer par3EntityPlayer)
    {
        par1ItemStack.stackSize--;

        if (!par2World.isRemote)
        {
            if (this.k == 0)
            {
                par3EntityPlayer.clearActivePotions();
            }

            if (this.k == 1 || this.k == 2)
            {
                //par3EntityPlayer.clearActivePotions();
                par3EntityPlayer.addPotionEffect(new PotionEffect(1, 500, 3));
                par3EntityPlayer.addPotionEffect(new PotionEffect(8, 500, 1));
                par3EntityPlayer.addPotionEffect(new PotionEffect(17, 2000, 1));
                par3EntityPlayer.addPotionEffect(new PotionEffect(19, 2000, 1));
                par3EntityPlayer.addPotionEffect(new PotionEffect(9, 3000, 1));
                par3EntityPlayer.addPotionEffect(new PotionEffect(15, 1200, 1));
                //blindness
            }

            if (this.k == 3)
            {
                par3EntityPlayer.addPotionEffect(new PotionEffect(1, 300, 3));
                par3EntityPlayer.addPotionEffect(new PotionEffect(8, 300, 3));
                par3EntityPlayer.addPotionEffect(new PotionEffect(17, 10, 1));
                par3EntityPlayer.addPotionEffect(new PotionEffect(19, 10, 1));
            }
        }

        if (par1ItemStack.stackSize <= 0)
        {
            if (this.k == 0 || this.k == 2)
            {
                return new ItemStack(mod_nw.cbucketItem);
            }

            /*if (this.k == 3)
            {
                return new ItemStack(mod_nw.krujkaBeeremp, 1);
            }*/
            else
            {
                return new ItemStack(Item.bucketEmpty);
            }
        }
        else
        {
            return par1ItemStack;
        }
	//	return par1ItemStack;
    }

    /**
     * How long it takes to use or consume an item
     */
    public int getMaxItemUseDuration(ItemStack par1ItemStack)
    {
        return 32;
    }

    /**
     * returns the action that specifies what animation to play when the items is being used
     */
    public EnumAction getItemUseAction(ItemStack par1ItemStack)
    {
        return EnumAction.drink;
    }

    /**
     * Called whenever this item is equipped and the right mouse button is pressed. Args: itemStack, world, entityPlayer
     */
    public ItemStack onItemRightClick(ItemStack par1ItemStack, World par2World, EntityPlayer par3EntityPlayer)
    {
        par3EntityPlayer.setItemInUse(par1ItemStack, getMaxItemUseDuration(par1ItemStack));
        return par1ItemStack;
    }
	@Override
	public String getTextureFile() {
		return CommonProxy.ITEMS_PNG;
	}
	
}
