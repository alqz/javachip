package com.ParadigmLearningSystems;

public class PLSActivityArea extends PLSActivity {
	
	private String type;
	private PLSActivity[] activities;
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public PLSActivity[] getActivities() {
		return activities;
	}
	public void setActivities(PLSActivity[] activities) {
		this.activities = activities;
	}
	
	

}
