package intranet.interfaces.params.impl;

import intranet.domain.entities.Evento;
import intranet.interfaces.params.EventoParams;
import intranet.security.SecurityUtils;
import intranet.utils.SpringBeanFactory;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

import lombok.Data;
import lombok.extern.java.Log;

import org.springframework.stereotype.Component;

/**
 * 
 * @author Gabriel Cardelli
 * @since Feb 21, 2012 - 12:43:43 AM
 *
 */
@Component(value="eventoNovoParams")
@Data 
@Log
public class EventoNovoParams implements EventoParams {

	private String titulo;
	private String coordenacao;
	
	private Calendar dataInicio;
	private Calendar dataFim;
	private Date horaInicio;
	private Date horaFim;
	
	private String local;
	private String publicoAlvo;
	private String observacoes;
	
	private SecurityUtils securityUtils;
	
	
	
	
	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return false;
	}
	
	public Evento getEvento(){
		Evento evento = (Evento) SpringBeanFactory.getBean("evento");
			evento.setTitle(titulo);
			evento.setCoordenacao(coordenacao);
			
			evento.setDataInicio(dataInicio);
			evento.setDataFim(dataFim);
			evento.setHoraInicio(horaInicio);
			evento.setHoraFim(horaFim);
			
			evento.setLocal(local);
			evento.setPublicoalvo(publicoAlvo);
			evento.setObs(observacoes);
			evento.setDatahoraPublicacao(Calendar.getInstance());
		return evento;
	}

	
	public void setHoraInicio(String horaInicio) {
		 DateFormat formatter = new SimpleDateFormat("hh:mm:ss");  
		 try {
			 this.horaInicio = (Date)formatter.parse(horaInicio);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void setHoraFim(String horaFim) {
		 DateFormat formatter = new SimpleDateFormat("hh:mm:ss");  
		 try {
			this.horaFim = (Date)formatter.parse(horaFim);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  
	}

	@Override
	public Map<String, Object> getParamsMap() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void clean() {
		// TODO Auto-generated method stub
		
	}
}
