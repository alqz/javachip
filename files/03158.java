/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mas.worldmodel.entity;

/**
 *
 * @author christopher_hh
 */
public class WayPoint {
    private double x;
    private double y;

    public WayPoint(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }
}
