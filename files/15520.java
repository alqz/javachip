package intranet.infra.repository.impl;

import intranet.domain.entities.Vaga;
import intranet.domain.repository.VagaRepository;
import intranet.infra.dao.VagaDAO;
import intranet.utils.IntranetException;

import java.util.Collection;
import java.util.List;

import lombok.extern.java.Log;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


/**
 * @author Felipe Balbino
 * @since 02/06/2011
 */
@Log
@Repository
public class VagaRepositorioImpl implements VagaRepository {
	
	private VagaDAO vagaDAO;
	
	
	@Autowired
	public VagaRepositorioImpl(VagaDAO vagaDAO) {
	super();
	this.vagaDAO = vagaDAO;
}

	@Override
	public Collection<Vaga> buscarTodos() throws IntranetException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Vaga buscarPeloId(Long id) throws IntranetException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Vaga> getAllValidos() throws IntranetException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Vaga> getAllForIndex() throws IntranetException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Vaga getVagaById(Long idVaga) throws IntranetException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Vaga> getVagasByIdAluno(Integer idAluno)
			throws IntranetException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void remover(Vaga t) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void adicionar(Vaga t) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void alterar(Vaga t) {
		// TODO Auto-generated method stub
		
	}


	

}
