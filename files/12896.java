package mas.worldview.sframesystem.rts;

import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import javax.swing.JLabel;
import javax.swing.JPanel;
import layout.TableLayout;
import mas.worldcontroller.SimulationController;
import mas.worldview.sframesystem.CustomButton;
import mas.worldview.sframesystem.rts.viewport.DrawCommand;
import mas.worldview.sframesystem.rts.viewport.ViewPort;

/**
 *
 * @author Christoph
 */
//NOTE: Might support different templates.
public class GeneralRTSView extends JPanel {

    private ViewPort viewPort;
    private MiniMap minimap;
    private JPanel popup;
    private InfoBox infobox;
    private GridMenu mainGridMenu, currentGridMenu;
    private boolean isFullscreen;
    
    public GeneralRTSView(boolean isFullscreen) {
        super();
        this.isFullscreen = isFullscreen;
        init(isFullscreen);
    }
    
    //PRVATE METHODS//
    
    private void init(boolean isFullscreen) {
        double[][] cellSize = new double[][]{{0.25,0.5,0.25},{0.75,0.05,0.20}};
        this.setLayout(new TableLayout(cellSize));
        
        //Add the minimap on the bottom left
        this.minimap = new MiniMap();
        this.minimap.setLayout(new TableLayout(new double[][]{{TableLayout.FILL,7},{7,TableLayout.FILL}}));
        this.minimap.setBackground(Color.BLUE);
        this.add(minimap, "0,1,0,2");
        
        //Add the gridMenu on the bottom right
        this.currentGridMenu = this.mainGridMenu = new GridMenu();
        this.currentGridMenu.setBackground(Color.GREEN);
        this.add(currentGridMenu, "2,1,2,2");
        
        //Add the infobox on the bottom center
        this.infobox = new InfoBox();
        this.infobox.setBackground(Color.YELLOW);
        this.add(infobox, "1,2,1,2");
        
        //Add the infobox. Let it fill out the rest.
        this.viewPort = new ViewPort(isFullscreen);
        this.viewPort.setSize(100, 100);
        this.viewPort.setBackground(Color.RED);
        this.add(viewPort, "0,0,2,1");
        
        //Add the popup box. Let it lay behind everything.
        this.popup = new JPanel();
        this.popup.setLayout(new TableLayout(new double[][]{{TableLayout.FILL},{TableLayout.FILL}}));
        this.popup.setOpaque(false);
    }
    
    //PUBLIC METHODS//
    
    public void reset() {
        init(isFullscreen);
    }

    /**
     * Adds a button with the given Icon on the defined position in the gridMenu.
     * If there is already a button, the old buton will be removed. It throws
     * a IndexOutOfBoundsException if the give position lays outside the grid.
     * @param icon The image which will be displayed on the button.
     * @param xPos The index of the column the button will be in.
     * @param yPos The index of the row the button will be in.
     * @param actionListener The ActionListener, which will be added to the button.
     * @throws IndexOutOfBoundsException 
     */
    public CustomButton addIconButton(BufferedImage icon, int xPos, int yPos, ActionListener actionListener) throws IndexOutOfBoundsException {
        return mainGridMenu.addIconButton(icon, xPos, yPos, actionListener);
    }

    /**
     * Adds a button with the given Icon on the defined position in the gridMenu.
     * If there is already a button, the old buton will be removed. It throws
     * a IndexOutOfBoundsException if the give position lays outside the grid.
     * @param icon The image which will be displayed on the button.
     * @param tooltip The tooltip of the button.
     * @param xPos The index of the column the button will be in.
     * @param yPos The index of the row the button will be in.
     * @param actionListener The ActionListener, which will be added to the button.
     * @throws IndexOutOfBoundsException 
     */
    public CustomButton addIconButton(BufferedImage icon, String tooltip, int xPos, int yPos, ActionListener actionListener) throws IndexOutOfBoundsException {
        return mainGridMenu.addIconButton(icon, tooltip, xPos, yPos, actionListener);
    }

    /**
     * Adds a button with the given character on the defined position in the gridMenu.
     * If there is already a button, the old buton will be removed. It throws
     * a IndexOutOfBoundsException if the give position lays outside the grid.
     * @param character The character which will be displayed on the button.
     * @param xPos The index of the column the button will be in.
     * @param yPos The index of the row the button will be in.
     * @param actionListener The ActionListener, which will be added to the button.
     * @throws IndexOutOfBoundsException 
     */
    public CustomButton addCharacterButton(char character, int xPos, int yPos, ActionListener actionListener) throws IndexOutOfBoundsException {
        return mainGridMenu.addCharacterButton(character, xPos, yPos, actionListener);
    }

    /**
     * Adds a button with the given character on the defined position in the gridMenu.
     * If there is already a button, the old buton will be removed. It throws
     * a IndexOutOfBoundsException if the give position lays outside the grid.
     * @param character The character which will be displayed on the button.
     * @param tooltip The tooltip of the button.
     * @param xPos The index of the column the button will be in.
     * @param yPos The index of the row the button will be in.
     * @param actionListener The ActionListener, which will be added to the button.
     * @throws IndexOutOfBoundsException 
     */
    public CustomButton addCharacterButton(char character, String tooltip, int xPos, int yPos, ActionListener actionListener) throws IndexOutOfBoundsException {
        return mainGridMenu.addCharacterButton(character, tooltip, xPos, yPos, actionListener);
    }
    
    public CustomButton removeButtonOn(int posX, int posY) {
        return currentGridMenu.removeButtonOn(posX, posY);
    }
    
    /**
     * Adds the given DrawCommand to the command chain of the view port.
     * The command is added to the end of the command chain.
     * That means that the last added command will be draw on the top layer.
     * @param newDrawCommand 
     */
    public void addDrawCommand(DrawCommand newDrawCommand) {
        this.viewPort.addDrawCommand(newDrawCommand);
    }
    
    /**
     * Removes the given DrawCommand from the command chain.
     * @param newDrawCommand 
     * @return True if the object was successfully removed, false otherwise.
     */
    public boolean removeDrawCommand(DrawCommand drawCommand) {
        return this.viewPort.removeDrawCommand(drawCommand);
    }
    
    public void removeAllDrawCommands() {
        this.viewPort.removeAllDrawCommands();
    }
    
    /**
     * Sets the map size. Needs to be called before the rendering begins.
     * @param widthInMeter Width of the map in meter.
     * @param heightInMeter Height of the map in meter.
     */
    public void setMapSize(double widthInMeter, double heightInMeter) {
        this.viewPort.setMapSize(widthInMeter, heightInMeter);
    }
    
    /**
     * Makes the popup panel visible. The popub box lays over the entire interface.
     */
    public void showPopupPanel() {
        this.invalidate();
        this.add(popup, "0,0,2,0");
        this.setComponentZOrder(popup, 0);
        this.popup.setOpaque(false);
        this.validate();
        if(!isFullscreen) {
            repaint();
        }
    }
    
    /**
     * Sets the given JPanel as the content of the popup.
     * The panel will be resized to the entire screen/window size.
     */
    public void setPopupContent(JPanel content) {
        this.popup.invalidate();
        if(this.popup.getComponentCount() > 0) {
           this.popup.removeAll();
        }
        this.popup.add(content, "0,0,0,0");
        this.popup.validate();
        if(!isFullscreen) {
            repaint();
        }
    }
    
    /**
     * Makes the popup panel invisible.
     */
    public void hidePopupPanel() {
        this.invalidate();
        this.remove(popup);
        this.validate();
        if(!isFullscreen && this.getParent() != null) {
            this.getParent().repaint();
        }
    }
    
    //ACCESSOR METHODS//
    
    /**
     * Returns the popup panel;
     * @return 
     */
    public JPanel getPopupBox() {
        return popup;
    }
    
    /**
     * Returns the currently used ViewPort object.
     * @return ViewPort object.
     */
    public ViewPort getViewPort() {
        return this.viewPort;
    }
    
    /**
     * Adds the given JPanel to the info box panel. If there is already another panel, it gets removed first.
     * @param content A JPanel which will be the new content of the info box panel.
     */
    public void setInfoBoxContent(JPanel content) {
        this.infobox.setContent(content);
    }
    
    public JPanel getDefaultInfoPanel() {
        return this.infobox.getDefaultInfo();
    }
    
    /**
     * Removes all content from the info box and adds the default info content.
     */
    public void setDefaultInfo() {
        this.infobox.setDefaultInfo();
    }
    
    /**
     * Removes the current grid menu and adds the given to the generals rts view.
     * @param newGridMenu The new grid menu, which will be displayed.
     */
    public void setGridMenu(GridMenu newGridMenu) {
        this.invalidate();
        this.currentGridMenu.deactivateAllBrushes(this);
        this.remove(this.currentGridMenu);
        this.currentGridMenu = newGridMenu;
        this.add(currentGridMenu, "2,1,2,2");
        this.setComponentZOrder(this.currentGridMenu, 1);
        this.validate();
        
        // Find a sufficient way to repaint in both window and fullscreen mode.
        this.repaint();
    }
    
    /**
     * Removes the current grid menu and displays the default grid menu.
     */
    public void setMainGridMenu() {
        this.setGridMenu(mainGridMenu);
    }
    
    /**
     * Returns the main or default grid menu object.
     * @return 
     */
    public GridMenu getMainGridMenu() {
        return mainGridMenu;
    }

    public void setMinimapContent(JPanel content) {
        this.minimap.invalidate();
        this.minimap.removeAll();
        this.minimap.add(content, "0,1,0,1");
        this.minimap.validate();
        repaint();
    }
    
    //INNER CLASSES//
    
    private class InfoBox extends JPanel {
        
        private TexturePaint background;
        private JPanel defaultInfo;
        private int borderWidth;
        private String walkedDistanceGuards_S, walkedDistanceIntruders_S, currentTimeStep_S;
        private JLabel walkedDistanceGuards_JL, walkedDistanceIntruders_JL, currentTimeStep_JL;
        
        public InfoBox() {
            BufferedImage temp = TextureFactory.instance().getPanelBackgroundImage();
            this.background = new TexturePaint(temp, new Rectangle(0, 0, temp.getWidth(), temp.getHeight()));
            this.setLayout(new TableLayout(new double[][]{{TableLayout.FILL},{7,TableLayout.FILL}}));
            this.walkedDistanceGuards_S = "Walked distance of Guards: ";
            this.walkedDistanceIntruders_S = "Walked distance of Intruders: ";
            this.currentTimeStep_S = "Current Timestep: ";
            this.defaultInfo = new JPanel();
            this.defaultInfo.setLayout(new GridLayout(3, 1));
            this.walkedDistanceGuards_JL = new JLabel(walkedDistanceGuards_S+"0m");
            this.walkedDistanceIntruders_JL = new JLabel(walkedDistanceIntruders_S+"0m");
            this.currentTimeStep_JL = new JLabel(currentTimeStep_S+"0s");
            this.defaultInfo.add(walkedDistanceGuards_JL);
            this.defaultInfo.add(walkedDistanceIntruders_JL);
            this.defaultInfo.add(currentTimeStep_JL);
            this.defaultInfo.setOpaque(false);
            this.add(defaultInfo, "0,1,0,1");
            this.borderWidth = 7;
        }
        
        public void setContent(JPanel content) {
            if(content != null) {
                this.invalidate();
                if(this.getComponentCount() > 0) {
                    this.removeAll();
                }
                this.add(content, "0,1,0,1");
                this.validate();
                if(!isFullscreen) {
                    repaint();
                }
            }
        }
        
        public JPanel getDefaultInfo() {
            return defaultInfo;
        }
        
        public void setDefaultInfo() {
            setContent(defaultInfo);
        }
        
        @Override
        public void paintComponent(Graphics g) {
            super.paintComponent(g);
            Graphics2D g2d = (Graphics2D)g;
            g2d.setPaint(background);
            g2d.fillRect(0, 0, this.getWidth(), this.getHeight());
            
            Composite tempComposite = g2d.getComposite();
            Polygon topBorder   = new Polygon(new int[]{0, this.getWidth(), this.getWidth(), 0}, new int[]{0, 0, borderWidth, borderWidth}, 4);
            g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER,0.5f)); // draw transparent background
            g2d.setColor(Color.BLACK);
            g2d.fill(topBorder);
            g2d.setComposite(tempComposite);
            
            walkedDistanceGuards_JL.setText(walkedDistanceGuards_S+(Math.round(SimulationController.Instance().getWorld().getWalkedDistanceGuards())/100.0)+"m");
            walkedDistanceIntruders_JL.setText(walkedDistanceIntruders_S+(Math.round(SimulationController.Instance().getWorld().getWalkedDistanceIntruders())/100.0)+"m");
            currentTimeStep_JL.setText(currentTimeStep_S+SimulationController.Instance().getWorld().getNrbOfCalculatedSteps()+"s");
        }
    }
}
