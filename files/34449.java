/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package qse_sepm_ss12_07.dao;

/**
 *
 */
public class ProductEntity
{
	private int id;
	private int productCategoryId;
	private String name = "";
	private String barCode = "";
	private double minimumStockAmount;
	private double amountPerProduct;
	private double stockAmount;
	private boolean isDeleted;
	private String imageFileName;

	public ProductEntity()
	{
		id = -1;
		name = "Product";
		minimumStockAmount = 0;
		amountPerProduct = 1;
		stockAmount = 0;
		isDeleted = false;
		imageFileName = "noImage.png";
	}

	public double getAmountPerProduct()
	{
		return amountPerProduct;
	}

	public void setAmountPerProduct(double amountPerProduct)
	{
		this.amountPerProduct = amountPerProduct;
	}

	public String getBarCode()
	{
		return barCode;
	}

	public void setBarCode(String barCode)
	{
		this.barCode = barCode;
	}

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public double getMinimumStockAmount()
	{
		return minimumStockAmount;
	}

	public void setMinimumStockAmount(double minimumStockAmount)
	{
		this.minimumStockAmount = minimumStockAmount;
	}

	public String getName()
	{
		return name;
	}

	public String getImageFileName()
	{
		return imageFileName;
	}

	public void setImageFileName(String imageFileName)
	{
		this.imageFileName = imageFileName;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public int getProductCategoryId()
	{
		return productCategoryId;
	}

	public void setProductCategoryId(int productCategory)
	{
		this.productCategoryId = productCategory;
	}

	public boolean getIsDeleted()
	{
		return isDeleted;
	}

	public double getStockAmount()
	{
		return stockAmount;
	}

	public void setStockAmount(double stockAmount)
	{
		this.stockAmount = stockAmount;
	}

	@Override
	public boolean equals(Object object)
	{
		if (object instanceof ProductEntity)
		{
			ProductEntity product = (ProductEntity) object;
			if (id == product.id)
			{
				return true;
			}
		}
		return false;
	}
}
