package _2_v212;

import javax.swing.*;
import java.awt.*;


public class Panel_Panel2 extends Extends_TransitionPanel
{
	private Panel_Menu menuPanel;
	
	
	public Panel_Panel2( Panel_Menu panel )
	{
		super( 150 );
		this.menuPanel = panel;
		
		JButton button = new JButton( "0,0 button" );
		button.setBounds( 0, 0, 150, 30 );
		add( button );
		
		JLabel label = new JLabel( "Dit is een nog een test!" );
		label.setBounds( 50, 55, 150, 30 );
		add( label );
	}
	
	
	public void paintComponent( Graphics g )
	{
		super.paintComponent( g );
		
		g.setColor( Color.CYAN );
		g.fillRect( 0, 0, getWidth(), getHeight() );
	}
}
