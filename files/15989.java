package airport;

import javax.naming.directory.InvalidAttributeValueException;

import queue.LevelOneQueue;
import queue.LevelTwoQueue;
import simulation.Simulation;

/**
 * The Class Location.
 * 
 * @filename Location.java
 * 
 * @author Group 12
 * @version April 2012 #001
 */
public abstract class Location {
	protected String locationName;
	
	private Queue queue;
	
	/**
	 * Instantiates a new location.
	 *
	 * @param locationName The name of this location
	 * @param maxSize The maximum number of aircraft permitted to be in this location at any one time.
	 * Expected value: A positive integer, else null. If null, location considered to not have a limit.
	 */
	public Location(String locationName, Integer maxSize) {
		this.locationName = locationName;
		
		if( Simulation.getQueueType() == 1) {
			this.queue = new LevelOneQueue(maxSize);
		}
		else if( Simulation.getQueueType() == 2) {
			this.queue = new LevelTwoQueue(maxSize);
		}
	}
	
	/**
	 * Gets the location name.
	 *
	 * @return the location name
	 */
	public String getLocationName() {
		return locationName;
	}

	/**
	 * Gets the max size.
	 *
	 * @return the max size
	 */
	public int getMaxSize() {
		return queue.getMaxSize();
	}
	
	/**
	 * Gets the size.
	 *
	 * @return the size
	 */
	public int getSize() {
		return queue.getSize();
	}
	
	

	/**
	 * Adds the.
	 *
	 * @param aircraft the aircraft
	 * @throws InvalidAttributeValueException the invalid attribute value exception
	 */
	public void add(Aircraft aircraft) throws InvalidAttributeValueException  {
		queue.add(aircraft);
	}
	
	/**
	 * Removes the.
	 *
	 * @param aircraft the aircraft
	 */
	public void remove(Aircraft aircraft) {
		queue.remove(aircraft);
	}
	
	/**
	 * Gets an aircraft from this location.
	 *
	 * @param i the i
	 * @return the aircraft
	 */
	public Aircraft get(int i) {
		return queue.get(i);
	}

	/**
	 * Gets the first aircraft in queue.
	 *
	 * @return the first aircraft in queue
	 */
	public Aircraft getFirstAircraftInQueue() {
		return queue.getFirstAircraftInQueue();
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public abstract String toString();
	
	/**
	 * Increment time in use.
	 */
	public abstract void incrementTimeInUse();
}
