package plugin.arcwolf.blockdoor.Doors;

// Block Data for the open state of a two state door object

public class DoorSecondState {

    public int x, y, z, blockID;
    public byte offset;

    public DoorSecondState(int in_x, int in_y, int in_z, int in_blockID, byte in_offset) {
        x = in_x;
        y = in_y;
        z = in_z;
        blockID = in_blockID;
        offset = in_offset;
    }

    public DoorSecondState(String in_string) {
        String[] split = in_string.split(",");
        if (split.length == 5) {
            blockID = Integer.parseInt(split[0]);
            offset = Byte.valueOf(split[1]);
            x = Integer.parseInt(split[2]);
            y = Integer.parseInt(split[3]);
            z = Integer.parseInt(split[4]);
        }
    }
}