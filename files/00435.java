package c301.w11.Model.test;

import android.test.AndroidTestCase;
import c301.w11.Model.Photo;

public class PhotoTest extends AndroidTestCase {
	public Photo testPic;

	@Override
	protected void setUp() throws Exception {
		testPic.setId(888);
		testPic.setDescription("Picture Test");
		testPic.setTag("Picture Tag Test");
		testPic.setUri("Picture URI Test");
	}
	
	public void testgetId() {
		long id = 80;
		
		assertTrue(testPic.getId() == 888);
		
		testPic.setId(id);
		
		assertTrue(testPic.getId() == 80);
	}
	
	public void testsetId() {
		long id = 80;
		
		assertTrue(testPic.getId() == 888);
		
		testPic.setId(id);
		
		assertTrue(testPic.getId() == 80);
	}
	
	public void testgetDescription() {
		String testString = "Test string";
		
		assertTrue(testPic.getDescription().equals("Picture Test"));
		
		testPic.setDescription(testString);
		
		assertTrue(testPic.getDescription().equals(testString));
	}
	
	public void testsetDescription() {
		String testString = "Test string";
		
		assertTrue(testPic.getDescription().equals("Picture Test"));
		
		testPic.setDescription(testString);
		
		assertTrue(testPic.getDescription().equals(testString));
	}

	public void testgetTag() {
		String testString = "Test tag string";
		
		assertTrue(testPic.getTag().equals("Picture Tag Test"));
		
		testPic.setTag(testString);
		
		assertTrue(testPic.getTag().equals(testString));
	}
	
	public void testsetTag() {
		String testString = "Test tag string";
		
		assertTrue(testPic.getTag().equals("Picture Tag Test"));
		
		testPic.setTag(testString);
		
		assertTrue(testPic.getTag().equals(testString));
	}
	
	public void testgetUri() {
		String testString = "Test uri string";
		
		assertTrue(testPic.getUri().equals("Picture URI Test"));
		
		testPic.setUri(testString);
		
		assertTrue(testPic.getUri().equals(testString));
	}
	
	public void testsetUri() {
		String testString = "Test uri string";
		
		assertTrue(testPic.getUri().equals("Picture URI Test"));
		
		testPic.setUri(testString);
		
		assertTrue(testPic.getUri().equals(testString));
	}
	
	public void testtoString() {
		
		String compare = "PhotoId: 888.  Description: Picture Test.  Tag: Picture Tag Test.  Uri: Picture URI Test";
		
		assertTrue(testPic.toString().equals(compare));
		
	}
}
