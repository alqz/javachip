package com.wisc.app;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.util.Log;

import com.parse.*;

import com.wisc.app.R;

public class PoLLandActivity extends Activity {
	
	static final String TAG = "Login";
	static final int DIALOG_FORGOT_PASS = 0;
	
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signin);
        
        Parse.initialize(this, "5gKKA1CRQCPcjQSWv9lwJ598BJGgPMcqfd4VR3hW", 
        		"d4O0Jn0PHdPm6ZfdlXF4tbrd4unDI6a860T5YtkT");
        
        ParseUser currentUser = ParseUser.getCurrentUser();
        if (currentUser != null){
        	if(currentUser.getBoolean("emailVerified")){
        		Intent i = new Intent(PoLLandActivity.this, HomeScreenActivity.class);
        		startActivity(i);
        	} else {
        		Toast.makeText(this, "You must verify your e-mail to log in.", Toast.LENGTH_SHORT).show();
        		ParseUser.logOut();
        	}
        }
        
        Button loginB = (Button) findViewById(R.id.signinButton);
        loginB.setOnClickListener(new OnClickListener() {
        	
		    public void onClick(View v) {
		    	
		        EditText usernameET = (EditText) findViewById(R.id.username);
		        String username = usernameET.getText().toString();
		        
		        EditText passwordET = (EditText) findViewById(R.id.password);
		        String password = passwordET.getText().toString();
		        
		        Log.i(TAG, username + " " + password);
		        ParseUser.logInInBackground(username, password, new LogInCallback() {
		        	public void done(ParseUser user, ParseException e) {
		        		if (user != null){
		                	if(user.getBoolean("emailVerified")){
			        			//Pass intent to homescreen
		                		
		                		Intent i = new Intent(PoLLandActivity.this, HomeScreenActivity.class);
		                		startActivity(i);
		                	} else {
		                		Toast.makeText(PoLLandActivity.this, "You must verify your e-mail to log in.", Toast.LENGTH_SHORT).show();
		                	}
		        		}
		        		else {
		        			//Login Failed
		        			Toast.makeText(PoLLandActivity.this, "Login Failed: " + e.getMessage(), Toast.LENGTH_SHORT).show();
		        			Log.e(TAG, e.getMessage());
		        		}
		        			
		        	}
		        });
		    }
		});
        
        /*
         * Goes to Create Account activity
         */
        Button createAccountB = (Button) findViewById(R.id.createAccountButton);
        createAccountB.setOnClickListener(new OnClickListener() {
        	
		    public void onClick(View v) {
		    	
	        	Intent i = new Intent(com.wisc.app.PoLLandActivity.this, com.wisc.app.CreateAccountActivity.class);
		    	startActivity(i);
		    }
		});
        
        TextView forgotPasswordTV = (TextView) findViewById(R.id.forgotPassword);
        forgotPasswordTV.setOnClickListener(new OnClickListener() {
        	
		    public void onClick(View v) {
		    	showDialog(DIALOG_FORGOT_PASS);
		    }
		});
        
    }
    @Override
    protected void onStart() {
        super.onStart();
        // The activity is about to become visible.
        
        ParseUser currentUser = ParseUser.getCurrentUser();
        if (currentUser != null){
        	if(currentUser.isAuthenticated()){
        		//Intent to home
        	} else {
        		Toast.makeText(this, "You must verify your e-mail to log in.", Toast.LENGTH_SHORT).show();
        	}
        }
    }
    
    protected Dialog onCreateDialog(int id){
    	Log.i(TAG, "Creating custom dialog");
    	Dialog dialog = null;
    	switch(id) {
    	case DIALOG_FORGOT_PASS:
    		AlertDialog.Builder builder;
	    	AlertDialog alertDialog;
	
	    	Context mContext = getApplicationContext();
	    	LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE);
	    	final View layout = inflater.inflate(R.layout.forgotpassdialog,
	    	                               (ViewGroup) findViewById(R.id.layout_root));
	
	    	builder = new AlertDialog.Builder(this);
	    	builder.setTitle("Enter email to reset password.")
	    		   .setView(layout)
	    	       .setPositiveButton("OK", new DialogInterface.OnClickListener() {
	    	    	   
	    	    	   final EditText email = (EditText)layout.findViewById(R.id.forgot_email);
						
						public void onClick(DialogInterface dialog, int which) {
							
							ParseUser.requestPasswordResetInBackground(
									email.getText().toString(),
									new RequestPasswordResetCallback() {
										public void done(ParseException e) {
											if (e == null) {
												Toast.makeText(PoLLandActivity.this, "Password reset email successfuly sent!",	Toast.LENGTH_SHORT).show();
											} else {
												Log.e(TAG, e.getMessage());
												Toast.makeText(PoLLandActivity.this, "Password reset email send was unsuccessful!",	Toast.LENGTH_SHORT).show();
											}
										}
							});
							dialog.dismiss();
						}
				    })
				    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
						
						public void onClick(DialogInterface dialog, int which) {
							dialog.cancel();
						}
					});
	    	alertDialog = builder.create();
	    	return alertDialog;
    	}
    	
    	return dialog;
    }
}