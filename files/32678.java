package webservice.domain;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "klant", propOrder = { "klant_id", "adres_id", "voornaam", "tussenvoegsel", "achternaam", "geslacht", "email", "geboorteDatum", "bsn" })
public class Klant {

	private int klant_id;
	private int adres_id;
    @XmlElement(required = true)
	private String voornaam;
    @XmlElement(required = true)
	private String tussenvoegsel; //!
    @XmlElement(required = true)
	private String achternaam;
    @XmlElement(required = true)
	private String geslacht;
    @XmlElement(required = true)
	private String email;		  //!
    @XmlElement(required = true)
    @XmlSchemaType(name = "date")
	private Date geboorteDatum;    //!
    @XmlElement(required = true)
	private int bsn;
	
	public Klant() {
		
	}

	public Klant(int klant_id, int adres_id, String voornaam, String tussenvoegsel, String achternaam, String geslacht, String email, Date geboorteDatum, int bsn) {
		this.klant_id = klant_id;
		this.adres_id = adres_id;
		this.voornaam = voornaam;
		this.tussenvoegsel = tussenvoegsel;
		this.achternaam = achternaam;
		this.geslacht = geslacht;
		this.email = email;
		this.geboorteDatum = geboorteDatum;
		this.bsn = bsn;
	}

	public int getKlant_id() {
		return klant_id;
	}

	public void setKlant_id(int klant_id) {
		this.klant_id = klant_id;
	}

	public int getAdres_id() {
		return adres_id;
	}

	public void setAdres_id(int adres_id) {
		this.adres_id = adres_id;
	}

	public String getVoornaam() {
		return voornaam;
	}

	public void setVoornaam(String voornaam) {
		this.voornaam = voornaam;
	}

	public String getTussenvoegsel() {
		return tussenvoegsel;
	}

	public void setTussenvoegsel(String tussenvoegsel) {
		this.tussenvoegsel = tussenvoegsel;
	}

	public String getAchternaam() {
		return achternaam;
	}

	public void setAchternaam(String achternaam) {
		this.achternaam = achternaam;
	}

	public String getGeslacht() {
		return geslacht;
	}

	public void setGeslacht(String geslacht) {
		this.geslacht = geslacht;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getGeboorteDatum() {
		return geboorteDatum;
	}

	public void setGeboorteDatum(Date geboorteDatum) {
		this.geboorteDatum = geboorteDatum;
	}

	public int getBsn() {
		return bsn;
	}

	public void setBsn(int bsn) {
		this.bsn = bsn;
	}	
}
//	public Klant(int i, String vn, String tv, String an, String em, String tn, String mn, String gs, Date gd) {
//		id = i;
//		voornaam = vn;
//		tussenvoegsel = tv;
//		achternaam = an;
//		email = em;
//		telefoonnummer = tn;
//		mobielNummer = mn;
//		geslacht = gs;
//		geboorteDatum = gd;
//	}