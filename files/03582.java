package intranet.interfaces.taglibs;

import intranet.domain.entities.Curso;
import intranet.domain.entities.Disciplina;
import intranet.domain.entities.Regra;

import java.util.List;


public class IntranetCoreTags {
	
	public static String contains(List<Regra> lista,Object object){
		
		
		for (Regra rule : lista){
			if (rule.getNome().equals(object.toString())){
				return "selected";
			}
		}
		
		return null;
		
	}
	
public static String containsDisciplina(List<Disciplina> lista,Object object){
		
		
		for (Disciplina rule : lista){
			if (rule != null && object != null){
				if (rule.getNome().equals(object.toString())){
					return "selected";
				}
			}
		}
		
		return null;
		
	}

public static String containsCurso(List<Curso> lista,Object object){
	
	
	for (Curso rule : lista){
		if (rule != null && object != null){
		if (rule.getNome().equals(object.toString())){
			return "selected";
		}
		}
	}
	
	return null;
	
}
	
}
