/*
 Copyright 2006 - 2010 Under Dusken

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
package org.pegadi.publisher.io;

import org.junit.Ignore;
import org.junit.Test;

import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.util.ArrayList;
import java.util.List;

import static junit.framework.Assert.assertEquals;

/**
 * @author Marvin B. Lillehaug <lillehau@underdusken.no>
 */
public class CRLFFilterOutputStreamTest {


    @Test
    public void testWriteWindowsFromWindows() throws IOException, InterruptedException {
        final PipedInputStream inputStream = new PipedInputStream();;
        PipedOutputStream outputStream = new PipedOutputStream(inputStream);
        CRLFFilterOutputStream filterOutputStream = new CRLFFilterOutputStream(outputStream, CRLFFilterOutputStream.Mode.WINDOWS);

        final List<Integer> output = new ArrayList<Integer>();
        Thread t = Reader(inputStream, output);

        filterOutputStream.write(12);
        filterOutputStream.write(13);
        filterOutputStream.write(10);
        filterOutputStream.write(13);
        filterOutputStream.write(10);
        filterOutputStream.write(11);
        filterOutputStream.close();

        t.join();
        checkOutputWin(output);
    }

    @Test
    public void testWriteMacToWindows() throws IOException, InterruptedException {
        final PipedInputStream inputStream = new PipedInputStream();;
        PipedOutputStream outputStream = new PipedOutputStream(inputStream);
        CRLFFilterOutputStream filterOutputStream = new CRLFFilterOutputStream(outputStream, CRLFFilterOutputStream.Mode.WINDOWS);

        final List<Integer> output = new ArrayList<Integer>();
        Thread t = Reader(inputStream, output);

        filterOutputStream.write(12);
        filterOutputStream.write(10);
        filterOutputStream.write(10);
        filterOutputStream.write(11);
        filterOutputStream.close();

        t.join();
        checkOutputWin(output);
    }

    @Test
    public void testWriteUnixToWindows() throws IOException, InterruptedException {
        final PipedInputStream inputStream = new PipedInputStream();;
        PipedOutputStream outputStream = new PipedOutputStream(inputStream);
        CRLFFilterOutputStream filterOutputStream = new CRLFFilterOutputStream(outputStream, CRLFFilterOutputStream.Mode.WINDOWS);

        final List<Integer> output = new ArrayList<Integer>();
        Thread t = Reader(inputStream, output);

        filterOutputStream.write(12);
        filterOutputStream.write(13);
        filterOutputStream.write(13);
        filterOutputStream.write(11);
        filterOutputStream.close();

        t.join();
        checkOutputWin(output);
    }

    @Test
    public void testWriteWindowsToMac() throws IOException, InterruptedException {
        final PipedInputStream inputStream = new PipedInputStream();;
        PipedOutputStream outputStream = new PipedOutputStream(inputStream);
        CRLFFilterOutputStream filterOutputStream = new CRLFFilterOutputStream(outputStream, CRLFFilterOutputStream.Mode.MAC);

        final List<Integer> output = new ArrayList<Integer>();
        Thread t = Reader(inputStream, output);

        filterOutputStream.write(12);
        filterOutputStream.write(13);
        filterOutputStream.write(10);
        filterOutputStream.write(13);
        filterOutputStream.write(10);
        filterOutputStream.write(11);
        filterOutputStream.close();

        t.join();
        checkOutputMac(output);
    }

    @Test
    @Ignore
    public void testWriteWindowsToUnix() throws IOException, InterruptedException {
        final PipedInputStream inputStream = new PipedInputStream();;
        PipedOutputStream outputStream = new PipedOutputStream(inputStream);
        CRLFFilterOutputStream filterOutputStream = new CRLFFilterOutputStream(outputStream, CRLFFilterOutputStream.Mode.UNIX);

        final List<Integer> output = new ArrayList<Integer>();
        Thread t = Reader(inputStream, output);

        filterOutputStream.write(12);
        filterOutputStream.write(13);
        filterOutputStream.write(10);
        filterOutputStream.write(13);
        filterOutputStream.write(10);
        filterOutputStream.write(11);
        filterOutputStream.close();

        t.join();
        checkOutputUnix(output);
    }

    @Test
    public void testWriteMacToUnix() throws IOException, InterruptedException {
        final PipedInputStream inputStream = new PipedInputStream();;
        PipedOutputStream outputStream = new PipedOutputStream(inputStream);
        CRLFFilterOutputStream filterOutputStream = new CRLFFilterOutputStream(outputStream, CRLFFilterOutputStream.Mode.UNIX);

        final List<Integer> output = new ArrayList<Integer>();
        Thread t = Reader(inputStream, output);

        filterOutputStream.write(12);
        filterOutputStream.write(10);
        filterOutputStream.write(10);
        filterOutputStream.write(11);
        filterOutputStream.close();

        t.join();
        checkOutputUnix(output);
    }

    @Test
    public void testWriteMacToMac() throws IOException, InterruptedException {
        final PipedInputStream inputStream = new PipedInputStream();;
        PipedOutputStream outputStream = new PipedOutputStream(inputStream);
        CRLFFilterOutputStream filterOutputStream = new CRLFFilterOutputStream(outputStream, CRLFFilterOutputStream.Mode.MAC);

        final List<Integer> output = new ArrayList<Integer>();
        Thread t = Reader(inputStream, output);

        filterOutputStream.write(12);
        filterOutputStream.write(10);
        filterOutputStream.write(10);
        filterOutputStream.write(11);
        filterOutputStream.close();

        t.join();
        checkOutputMac(output);
    }

    @Test
    public void testWriteUnixToMac() throws IOException, InterruptedException {
       final PipedInputStream inputStream = new PipedInputStream();;
        PipedOutputStream outputStream = new PipedOutputStream(inputStream);
        CRLFFilterOutputStream filterOutputStream = new CRLFFilterOutputStream(outputStream, CRLFFilterOutputStream.Mode.MAC);

        final List<Integer> output = new ArrayList<Integer>();
        Thread t = Reader(inputStream, output);

        filterOutputStream.write(12);
        filterOutputStream.write(13);
        filterOutputStream.write(13);
        filterOutputStream.write(11);
        filterOutputStream.close();

        t.join();
        checkOutputMac(output);
    }

    @Test
    public void testWriteUnixToUnix() throws IOException, InterruptedException {
       final PipedInputStream inputStream = new PipedInputStream();;
        PipedOutputStream outputStream = new PipedOutputStream(inputStream);
        CRLFFilterOutputStream filterOutputStream = new CRLFFilterOutputStream(outputStream, CRLFFilterOutputStream.Mode.UNIX);

        final List<Integer> output = new ArrayList<Integer>();
        Thread t = Reader(inputStream, output);

        filterOutputStream.write(12);
        filterOutputStream.write(13);
        filterOutputStream.write(13);
        filterOutputStream.write(11);
        filterOutputStream.close();

        t.join();
        checkOutputUnix(output);
    }

    private void checkOutputWin(List<Integer> output) {
        Integer[] a = output.toArray(new Integer[output.size()]);
        assertEquals("Wrong length", 6, a.length);
        assertEquals("Wrong int", new Integer(12), a[0]);
        assertEquals("Wrong int", new Integer(13), a[1]);
        assertEquals("Wrong int", new Integer(10), a[2]);
        assertEquals("Wrong int", new Integer(13), a[3]);
        assertEquals("Wrong int", new Integer(10), a[4]);
        assertEquals("Wrong int", new Integer(11), a[5]);
    }

    private void checkOutputMac(List<Integer> output) {
        Integer[] a = output.toArray(new Integer[output.size()]);
        assertEquals("Wrong length", 4, a.length);
        assertEquals("Wrong int", new Integer(12), a[0]);
        assertEquals("Wrong int", new Integer(10), a[1]);
        assertEquals("Wrong int", new Integer(10), a[2]);
        assertEquals("Wrong int", new Integer(11), a[3]);
    }

    private void checkOutputUnix(List<Integer> output) {
        Integer[] a = output.toArray(new Integer[output.size()]);
        assertEquals("Wrong length", 4, a.length);
        assertEquals("Wrong int", new Integer(12), a[0]);
        assertEquals("Wrong int", new Integer(13), a[1]);
        assertEquals("Wrong int", new Integer(13), a[2]);
        assertEquals("Wrong int", new Integer(11), a[3]);
    }

    private Thread Reader(final PipedInputStream inputStream, final List<Integer> output) {
        Thread reader = new Thread(){
            @Override
            public void run() {
                super.run();
                int read;
                try {
                    while ((read = inputStream.read()) != -1){
                        output.add(read);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
        reader.start();
        return reader;
    }
}
