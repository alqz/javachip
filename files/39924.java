package net.skcomms.dtc.client;

import net.skcomms.dtc.client.controller.ActivityIndicatorController;
import net.skcomms.dtc.client.controller.ApiRequestParameterController;
import net.skcomms.dtc.client.controller.ApiSelectionController;
import net.skcomms.dtc.client.controller.ApiTestController;
import net.skcomms.dtc.client.controller.HelpController;
import net.skcomms.dtc.client.controller.SearchHistoryController;
import net.skcomms.dtc.client.controller.UrlCopyController;
import net.skcomms.dtc.client.model.ApiMetaDao;
import net.skcomms.dtc.client.model.ApiNodeDao;
import net.skcomms.dtc.client.model.ApiTestModel;
import net.skcomms.dtc.client.model.SearchHistoryDao;
import net.skcomms.dtc.client.view.ApiRequestParameterView;
import net.skcomms.dtc.client.view.ApiSelectionView;
import net.skcomms.dtc.client.view.ApiTestView;
import net.skcomms.dtc.client.view.HelpButtonView;
import net.skcomms.dtc.client.view.HelpPageView;
import net.skcomms.dtc.client.view.NavigationBarView;
import net.skcomms.dtc.client.view.UrlCopyButtonView;
import net.skcomms.dtc.client.view.UrlCopyDialogBoxView;
import net.skcomms.dtc.client.view.UserSignInView;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.Window;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class DtcArdbeg implements EntryPoint {

  private ApiNodeDao apiNodeDao;

  private ApiSelectionController apiSelectionController;

  private ApiSelectionView apiSelectionView;

  private final ApiTestView apiTestView = new ApiTestView();

  private final UserSignInView usernameSubmissionManager = new UserSignInView();

  private final ActivityIndicatorController activityIndicatorController = new ActivityIndicatorController();

  private final ApiRequestParameterView apiRequestParameterView = new ApiRequestParameterView();;

  private NavigationBarView navigationBarView;

  /**
   * @return
   */
  public String getHref() {
    return Window.Location.getHref();
  }

  private void initializeApiSelection() {

    this.apiSelectionController = new ApiSelectionController();
    this.apiSelectionView = new ApiSelectionView();
    this.apiNodeDao = new ApiNodeDao();
    this.apiSelectionController.initialize(this.apiNodeDao, this.apiSelectionView,
        this.navigationBarView, ApiMetaDao.getInstance());
  }

  private void initializeApiTest() {
    ApiTestModel apiTestModel = new ApiTestModel();
    ApiTestController apiTestController = new ApiTestController();
    apiTestController
    .initialize(this.apiTestView, this.apiSelectionView, apiTestModel,
        ApiMetaDao.getInstance(), this.apiRequestParameterView);
    apiTestController.addObserver(this.activityIndicatorController);

    ApiRequestParameterController apiRequestParameterController = new ApiRequestParameterController(
        this.apiRequestParameterView, ApiMetaDao.getInstance(),
        this.apiSelectionView.getApiListView());
    apiRequestParameterController.setInitialApiParameters(Window.Location.getParameterMap());
    this.apiTestView.wireSubView(this.apiRequestParameterView);

    SearchHistoryDao searchHistoryDao = new SearchHistoryDao();
    SearchHistoryController searchHistoryController = new SearchHistoryController();
    searchHistoryController.initialize(this.apiRequestParameterView, this.apiTestView,
        searchHistoryDao, ApiMetaDao.getInstance(), apiTestModel);

  }

  protected void initializeComponents() {
    this.initializeNavigationBar();

    this.initializeApiSelection();
    this.initializeApiTest();

    this.initializeUrlCopy();
    this.usernameSubmissionManager.initialize();

    this.initializeHelpPage();
  }

  private void initializeHelpPage() {
    HelpButtonView button = new HelpButtonView();
    HelpPageView page = new HelpPageView();

    HelpController controller = new HelpController();
    controller.initialize(page, button);
  }

  private void initializeNavigationBar() {
    this.navigationBarView = new NavigationBarView();
    this.navigationBarView.initialize(ApiMetaDao.getInstance());
  }

  private void initializeUrlCopy() {
    UrlCopyButtonView button = new UrlCopyButtonView();
    UrlCopyDialogBoxView dialogBox = new UrlCopyDialogBoxView();
    UrlCopyController controller = new UrlCopyController();
    controller.initialize(this.getHref(), this.apiRequestParameterView, button, dialogBox);
  }

  @Override
  public void onModuleLoad() {
    this.initializeComponents();
    this.setUpCloseHandler();
  }

  private void setUpCloseHandler() {
    Window.addWindowClosingHandler(new Window.ClosingHandler() {

      @Override
      public void onWindowClosing(Window.ClosingEvent closingEvent) {
        closingEvent.setMessage("뭔가 불길한 일이 일어날 것 같습니다...");
      }
    });
  }
}
