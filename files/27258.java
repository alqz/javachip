/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package qse_sepm_ss12_07.domain;

import qse_sepm_ss12_07.dao.RatingEntity;

/**
 *
 */
public class Rating {
    private RatingEntity entity;
    private User user;
    private Boolean editable = false;
    
    public Rating(RatingEntity entity)
    {
        this.entity = entity;
    }
    
	public Rating()
	{
		entity = new RatingEntity();
	}
	
	public RatingEntity getEntity()
	{
		return entity;
	}
	
    public User getUser()
	{
		return user;
	}

	public void setUser(User user)
	{
        this.user = user;
		entity.setUserID(user.getEntity().getId());
	}
    
    public int getRating()
	{
		return entity.getRating();
	}

	public void setRating(int rating)
	{
		entity.setRating(rating);
	}
    
    public String getFeedback()
	{
		return entity.getFeedback();
	}

	public void setFeedback(String feedback)
	{
		entity.setFeedback(feedback);
	}
    
    public Boolean getEditable()
	{
		return editable;
	}

	public void setEditable(Boolean editable)
	{
		this.editable = editable;
	}
}
