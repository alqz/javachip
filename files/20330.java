package com.wisc.app;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.util.Log;

import com.parse.*;

public class HomeScreenActivity extends Activity{
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	
        super.onCreate(savedInstanceState);
        setContentView(R.layout.homescreen);
        
        Button createPoll = (Button)findViewById(R.id.createPollButton);
        createPoll.setOnClickListener( new OnClickListener() {
        	
        	public void onClick(View v){
        		
        		Intent i = new Intent(com.wisc.app.HomeScreenActivity.this, com.wisc.app.CreatePollActivity.class);
        		startActivity(i);
        		
        	}
        });
        
        Button logout = (Button)findViewById(R.id.logoutButton);
        logout.setOnClickListener( new OnClickListener() {
        	
        	public void onClick(View v) {
        		ParseUser.logOut();
        		Intent i = new Intent(com.wisc.app.HomeScreenActivity.this, com.wisc.app.PoLLandActivity.class);
        		startActivity(i);
        	}
        	
        });
        
        Button browse = (Button)findViewById(R.id.browseButton);
        browse.setOnClickListener( new OnClickListener() {
        	public void onClick(View v) {
        		Intent i = new Intent(com.wisc.app.HomeScreenActivity.this, com.wisc.app.ListPollsActivity.class);
        		startActivity(i);
        	}
        });
        
        Button userProfile = (Button)findViewById(R.id.userProfButton);
        userProfile.setOnClickListener(new OnClickListener() {
        	public void onClick(View v) {
        		Intent i = new Intent(com.wisc.app.HomeScreenActivity.this, com.wisc.app.UserProfileActivity.class);
        		startActivity(i);
        	}
        });
        
        
    }
}