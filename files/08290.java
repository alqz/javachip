/**
 * CS425 Spring 2012
 *
 * @author jesse
 * @date Apr 28, 2012
 */
package org.jlyo.theatreman.servlet;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jlyo.theatreman.UnAuthorizedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Provider;
import com.google.inject.Singleton;

@Singleton
public class ServletAdapter extends HttpServlet
{
	private static final Logger _logger = LoggerFactory.getLogger(ServletAdapter.class);

	private static final long serialVersionUID = 8528495447567928710L;
	private final Provider<? extends TheatreServlet> theatreServlet;

	public ServletAdapter(final Provider<? extends TheatreServlet> theatreServlet)
	{
		super();
		this.theatreServlet = theatreServlet;
	}

	@Override
	protected void doGet(final HttpServletRequest request, final HttpServletResponse response)
	{
		try
		{
			response.setHeader("Content-Type", "text/html");
			this.theatreServlet.get().doGet(request, response);
		}
		catch (final Exception e)
		{
			handleException(e, response);
		}
	}

	@Override
	protected void doPost(final HttpServletRequest request, final HttpServletResponse response)
	{
		try
		{
			this.theatreServlet.get().doPost(request, response);
		}
		catch (final Exception e)
		{
			handleException(e, response);
		}
	}

	private void handleException(final Exception exception, final HttpServletResponse response)
	{
		try
		{
			try
			{
				throw exception;
			}
			catch (final UnAuthorizedException e)
			{
				response.setHeader("WWW-Authenticate", "Basic realm=\"theatreman\"");
				response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
			}
			catch (final SQLException e)
			{
				if (e.getMessage().contains("table or view does not exist")
						|| e.getMessage().contains("invalid table name"))
					response.sendError(HttpServletResponse.SC_NOT_FOUND);
				else
				{
					_logger.warn(e.getMessage(), e);
					response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
					e.printStackTrace(response.getWriter());
				}
			}
			catch (final RuntimeException e)
			{
				_logger.error(e.getMessage(), e);
				final ThreadDeath threadDeath = new ThreadDeath();
				threadDeath.initCause(e);
				throw threadDeath;
			}
			catch (final Exception e)
			{
				_logger.warn(e.getMessage(), e);
				response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
				e.printStackTrace(response.getWriter());
			}
		}
		catch (final IOException e)
		{
			_logger.error(e.getMessage(), e);
		}
	}
}
