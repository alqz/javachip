package utils;


public class Utils {

    public static boolean isNotBlank(String arg) {
        if (arg != null && arg.length() > 0) {
            return true;
        }
        return false;
    }

    public static boolean isBlank(String arg) {
        if (arg == null || arg.length() == 0) {
            return true;
        }
        return false;
    }
}
