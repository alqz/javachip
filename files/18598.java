package plugin.arcwolf.blockdoor.commands;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import plugin.arcwolf.blockdoor.BlockDoor;
import plugin.arcwolf.blockdoor.BlockDoor.Types;
import plugin.arcwolf.blockdoor.Link;
import plugin.arcwolf.blockdoor.Triggers.BlockTrigger;
import plugin.arcwolf.blockdoor.Triggers.MyTrigger;
import plugin.arcwolf.blockdoor.Triggers.RedTrig;
import plugin.arcwolf.blockdoor.Utils.BlockDoorSettings;
import plugin.arcwolf.blockdoor.Utils.DataWriter;
import plugin.arcwolf.blockdoor.Zones.Zone;

public class LinkCommands {

    private BlockDoor plugin;
    private DataWriter dataWriter;

    public LinkCommands() {
        plugin = BlockDoor.plugin;
        dataWriter = plugin.datawriter;
    }

    public boolean commands(String[] split, Player player, BlockDoorSettings settings) {
        if (settings.friendlyCommand.equals("stateSelected-Uzone")) {
            Zone uzone = plugin.zonehelper.findZone("UZONE", split[1].toLowerCase(), player.getName(), split[2], player.getWorld().getName());
            if (uzone != null) {
                try {
                    int state = Integer.parseInt(split[5]);
                    if (state == 1) {
                        int id2 = plugin.singlestatedoorhelper.findDoor(split[3].toLowerCase(), player.getName(), player.getWorld().getName());
                        if (id2 != -1) {
                            if (split[4].equalsIgnoreCase("toggle") || split[4].equalsIgnoreCase("open") || split[4].equalsIgnoreCase("close")) {
                                int linkIndex = plugin.zonehelper.findLink(uzone, split[3].toLowerCase(), player.getName(), "ONESTATE");
                                if (linkIndex == -1) {
                                    uzone.links.add(new Link(split[3].toLowerCase(), player.getName(), split[4].toLowerCase(), "ONESTATE"));
                                    dataWriter.saveDatabase();
                                    player.sendMessage("Added Selected Entity zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' triggering on: '" + ChatColor.GREEN + split[2] + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[3].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[4].toLowerCase());
                                    return true;
                                }
                                else {
                                    if (split[4].equalsIgnoreCase("TOGGLE") || split[4].equalsIgnoreCase("T"))
                                        uzone.links.get(linkIndex).linkType = Types.TOGGLE;
                                    else if (split[4].equalsIgnoreCase("ON") || split[4].equalsIgnoreCase("OPEN"))
                                        uzone.links.get(linkIndex).linkType = Types.OPEN;
                                    else if (split[4].equalsIgnoreCase("OFF") || split[4].equalsIgnoreCase("CLOSE"))
                                        uzone.links.get(linkIndex).linkType = Types.CLOSE;
                                    else {
                                        player.sendMessage(ChatColor.RED + "Not a valid link type: " + ChatColor.WHITE + "OPEN, CLOSE, TOGGLE");
                                        return false;
                                    }
                                    uzone.links.get(linkIndex).link_name = split[3].toLowerCase();
                                    uzone.links.get(linkIndex).link_creator = player.getName();
                                    uzone.links.get(linkIndex).doorType = "ONESTATE";
                                    dataWriter.saveDatabase();
                                    player.sendMessage("Updated Selected Entity zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' triggering on: '" + ChatColor.GREEN + split[2] + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[3].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[4].toLowerCase());
                                    return true;
                                }
                            }
                            else {
                                player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <zonename> <trigger> <doorname> <type> <doorType>");
                                player.sendMessage(ChatColor.BLUE + "Ex: /dlink uzone zonename johndoe doorname toggle 1");
                                return true;
                            }
                        }
                        else {
                            player.sendMessage(ChatColor.RED + "The door - " + ChatColor.WHITE + split[3] + ChatColor.RED + " was not found");
                            return true;
                        }
                    }
                    else if (state == 2) {
                        int id2 = plugin.twostatedoorhelper.findDoor(split[3].toLowerCase(), player.getName(), player.getWorld().getName());
                        if (id2 != -1) {
                            if (split[4].equalsIgnoreCase("toggle") || split[4].equalsIgnoreCase("open") || split[4].equalsIgnoreCase("close")) {
                                int linkIndex = plugin.zonehelper.findLink(uzone, split[3].toLowerCase(), player.getName(), "TWOSTATE");
                                if (linkIndex == -1) {
                                    uzone.links.add(new Link(split[3].toLowerCase(), player.getName(), split[4].toLowerCase(), "TWOSTATE"));
                                    dataWriter.saveDatabase();
                                    player.sendMessage("Added Selected Entity zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' triggering on: '" + ChatColor.GREEN + split[2] + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[3].toLowerCase() + ChatColor.WHITE + "' - TwoState - " + ChatColor.GREEN + split[4].toLowerCase());
                                    return true;
                                }
                                else {
                                    if (split[4].equalsIgnoreCase("TOGGLE") || split[4].equalsIgnoreCase("T"))
                                        uzone.links.get(linkIndex).linkType = Types.TOGGLE;
                                    else if (split[4].equalsIgnoreCase("ON") || split[4].equalsIgnoreCase("OPEN"))
                                        uzone.links.get(linkIndex).linkType = Types.OPEN;
                                    else if (split[4].equalsIgnoreCase("OFF") || split[4].equalsIgnoreCase("CLOSE"))
                                        uzone.links.get(linkIndex).linkType = Types.CLOSE;
                                    else {
                                        player.sendMessage(ChatColor.RED + "Not a valid link type: " + ChatColor.WHITE + "OPEN, CLOSE, TOGGLE");
                                        return false;
                                    }
                                    uzone.links.get(linkIndex).link_name = split[3].toLowerCase();
                                    uzone.links.get(linkIndex).link_creator = player.getName();
                                    uzone.links.get(linkIndex).doorType = "TWOSTATE";
                                    dataWriter.saveDatabase();
                                    player.sendMessage("Updated Selected Entity zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' triggering on: '" + ChatColor.GREEN + split[2] + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[3].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[4].toLowerCase());
                                    return true;
                                }
                            }
                            else {
                                player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <zonename> <trigger> <doorname> <type> <doorType>");
                                player.sendMessage(ChatColor.BLUE + "Ex: /dlink uzone zonename johndoe doorname toggle 1");
                                return true;
                            }
                        }
                        else {
                            player.sendMessage(ChatColor.RED + "The door - " + ChatColor.WHITE + split[3] + ChatColor.RED + " was not found");
                            return true;
                        }
                    }
                    else if (state == 3) {
                        int id3 = plugin.hdoorhelper.findDoor(split[3].toLowerCase(), player.getName(), player.getWorld().getName());
                        if (id3 != -1) {
                            if (split[4].equalsIgnoreCase("toggle") || split[4].equalsIgnoreCase("open") || split[4].equalsIgnoreCase("close")) {
                                int linkIndex = plugin.zonehelper.findLink(uzone, split[3].toLowerCase(), player.getName(), "HYBRIDSTATE");
                                if (linkIndex == -1) {
                                    uzone.links.add(new Link(split[3].toLowerCase(), player.getName(), split[4].toLowerCase(), "HYBRIDSTATE"));
                                    dataWriter.saveDatabase();
                                    player.sendMessage("Added Selected Entity zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' triggering on: '" + ChatColor.GREEN + split[2] + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[3].toLowerCase() + ChatColor.WHITE + "' - HybridState - " + ChatColor.GREEN + split[4].toLowerCase());
                                    return true;
                                }
                                else {
                                    if (split[4].equalsIgnoreCase("TOGGLE") || split[4].equalsIgnoreCase("T"))
                                        uzone.links.get(linkIndex).linkType = Types.TOGGLE;
                                    else if (split[4].equalsIgnoreCase("ON") || split[4].equalsIgnoreCase("OPEN"))
                                        uzone.links.get(linkIndex).linkType = Types.OPEN;
                                    else if (split[4].equalsIgnoreCase("OFF") || split[4].equalsIgnoreCase("CLOSE"))
                                        uzone.links.get(linkIndex).linkType = Types.CLOSE;
                                    else {
                                        player.sendMessage(ChatColor.RED + "Not a valid link type: " + ChatColor.WHITE + "OPEN, CLOSE, TOGGLE");
                                        return false;
                                    }
                                    uzone.links.get(linkIndex).link_name = split[3].toLowerCase();
                                    uzone.links.get(linkIndex).link_creator = player.getName();
                                    uzone.links.get(linkIndex).doorType = "HYBRIDSTATE";
                                    dataWriter.saveDatabase();
                                    player.sendMessage("Updated Selected Entity zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' triggering on: '" + ChatColor.GREEN + split[2] + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[3].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[4].toLowerCase());
                                    return true;
                                }
                            }
                            else {
                                player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <zonename> <trigger> <doorname> <type> <doorType>");
                                player.sendMessage(ChatColor.BLUE + "Ex: /dlink uzone zonename johndoe doorname toggle 1");
                                return true;
                            }
                        }
                        else {
                            player.sendMessage(ChatColor.RED + "The Hybrid door - " + ChatColor.WHITE + split[3] + ChatColor.RED + " was not found");
                            return true;
                        }
                    }
                } catch (Exception e) {
                    player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <zonename> <trigger> <doorname> <type> <doorType>");
                    player.sendMessage(ChatColor.BLUE + "Ex: /dlink uzone zonename johndoe doorname toggle 1");
                    return true;
                }
            }
            else {
                player.sendMessage(ChatColor.RED + "The Selected Entity zone - " + ChatColor.WHITE + split[1] + " triggered by: " + split[2] + ChatColor.RED + " was not found");
                return true;
            }
        }
        else if (settings.friendlyCommand.equals("singleState-Uzone")) {
            Zone uzone = plugin.zonehelper.findZone("UZONE", split[1].toLowerCase(), player.getName(), split[2], player.getWorld().getName());
            if (uzone != null) {
                try {
                    int id2 = plugin.singlestatedoorhelper.findDoor(split[3].toLowerCase(), player.getName(), player.getWorld().getName());
                    if (id2 != -1) {
                        if (split[4].equalsIgnoreCase("toggle") || split[4].equalsIgnoreCase("open") || split[4].equalsIgnoreCase("close")) {
                            int linkIndex = plugin.zonehelper.findLink(uzone, split[3].toLowerCase(), player.getName(), "ONESTATE");
                            if (linkIndex == -1) {
                                uzone.links.add(new Link(split[3].toLowerCase(), player.getName(), split[4].toLowerCase(), "ONESTATE"));
                                dataWriter.saveDatabase();
                                player.sendMessage("Added Selected Entity zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' triggering on: '" + ChatColor.GREEN + split[2] + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[3].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[4].toLowerCase());
                                return true;
                            }
                            else {
                                if (split[4].equalsIgnoreCase("TOGGLE") || split[4].equalsIgnoreCase("T"))
                                    uzone.links.get(linkIndex).linkType = Types.TOGGLE;
                                else if (split[4].equalsIgnoreCase("ON") || split[4].equalsIgnoreCase("OPEN"))
                                    uzone.links.get(linkIndex).linkType = Types.OPEN;
                                else if (split[4].equalsIgnoreCase("OFF") || split[4].equalsIgnoreCase("CLOSE"))
                                    uzone.links.get(linkIndex).linkType = Types.CLOSE;
                                else {
                                    player.sendMessage(ChatColor.RED + "Not a valid link type: " + ChatColor.WHITE + "OPEN, CLOSE, TOGGLE");
                                    return false;
                                }
                                uzone.links.get(linkIndex).link_name = split[3].toLowerCase();
                                uzone.links.get(linkIndex).link_creator = player.getName();
                                uzone.links.get(linkIndex).doorType = "ONESTATE";
                                dataWriter.saveDatabase();
                                player.sendMessage("Updated Selected Entity zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' triggering on: '" + ChatColor.GREEN + split[2] + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[3].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[4].toLowerCase());
                                return true;
                            }
                        }
                        else {
                            player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <zonename> <trigger> <doorname> <type> <doorType>");
                            player.sendMessage(ChatColor.BLUE + "Ex: /dlink uzone zonename johndoe doorname toggle 1");
                            return true;
                        }
                    }
                    else {
                        player.sendMessage(ChatColor.RED + "The door - " + ChatColor.WHITE + split[3] + ChatColor.RED + " was not found");
                        return true;
                    }
                } catch (Exception e) {
                    player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <zonename> <trigger> <doorname> <type> <doorType>");
                    player.sendMessage(ChatColor.BLUE + "Ex: /dlink uzone zonename johndoe doorname toggle 1");
                    return true;
                }
            }
            else {
                player.sendMessage(ChatColor.RED + "The Selected Entity zone - " + ChatColor.WHITE + split[1] + " triggered by: " + split[2] + ChatColor.RED + " was not found");
                return true;
            }
        }
        else if (settings.friendlyCommand.equals("stateSelected-Trigger")) {
            BlockTrigger trigger = (BlockTrigger) plugin.triggerhelper.findTrigger("TRIGGER", split[1].toLowerCase(), player.getName(), player.getWorld().getName());
            if (trigger != null) {
                try {
                    int state = Integer.parseInt(split[4]);
                    if (state == 1) {
                        int id2 = plugin.singlestatedoorhelper.findDoor(split[2].toLowerCase(), player.getName(), player.getWorld().getName());
                        if (id2 != -1) {
                            if (split[3].equalsIgnoreCase("toggle") || split[3].equalsIgnoreCase("open") || split[3].equalsIgnoreCase("close")) {
                                int linkIndex = plugin.triggerhelper.findLink(trigger, split[2].toLowerCase(), player.getName(), "ONESTATE");
                                if (linkIndex == -1) {
                                    trigger.links.add(new Link(split[2].toLowerCase(), player.getName(), split[3].toLowerCase(), "ONESTATE"));
                                    dataWriter.saveDatabase();
                                    player.sendMessage("Added trigger '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                                else {
                                    if (split[3].equalsIgnoreCase("TOGGLE") || split[3].equalsIgnoreCase("T"))
                                        trigger.links.get(linkIndex).linkType = Types.TOGGLE;
                                    else if (split[3].equalsIgnoreCase("ON") || split[3].equalsIgnoreCase("OPEN"))
                                        trigger.links.get(linkIndex).linkType = Types.OPEN;
                                    else if (split[3].equalsIgnoreCase("OFF") || split[3].equalsIgnoreCase("CLOSE"))
                                        trigger.links.get(linkIndex).linkType = Types.CLOSE;
                                    else {
                                        player.sendMessage(ChatColor.RED + "Not a valid link type: " + ChatColor.WHITE + "OPEN, CLOSE, TOGGLE");
                                        return false;
                                    }
                                    trigger.links.get(linkIndex).link_name = split[2].toLowerCase();
                                    trigger.links.get(linkIndex).link_creator = player.getName();
                                    trigger.links.get(linkIndex).doorType = "ONESTATE";
                                    dataWriter.saveDatabase();
                                    player.sendMessage("Updated trigger '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                            }
                            else {
                                player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <triggername> <doorname> <type> <doorType>");
                                player.sendMessage(ChatColor.BLUE + "Ex: /dlink trigger triggername doorname toggle 1");
                                return true;
                            }
                        }
                        else {
                            player.sendMessage(ChatColor.RED + "The door - " + ChatColor.WHITE + split[2] + ChatColor.RED + " was not found");
                            return true;
                        }
                    }
                    else if (state == 2) {
                        int id2 = plugin.twostatedoorhelper.findDoor(split[2].toLowerCase(), player.getName(), player.getWorld().getName());
                        if (id2 != -1) {
                            if (split[3].equalsIgnoreCase("toggle") || split[3].equalsIgnoreCase("open") || split[3].equalsIgnoreCase("close")) {
                                int linkIndex = plugin.triggerhelper.findLink(trigger, split[2].toLowerCase(), player.getName(), "TWOSTATE");
                                if (linkIndex == -1) {
                                    trigger.links.add(new Link(split[2].toLowerCase(), player.getName(), split[3].toLowerCase(), "TWOSTATE"));
                                    dataWriter.saveDatabase();
                                    player.sendMessage("Added trigger '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - TwoState - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                                else {
                                    if (split[3].equalsIgnoreCase("TOGGLE") || split[3].equalsIgnoreCase("T"))
                                        trigger.links.get(linkIndex).linkType = Types.TOGGLE;
                                    else if (split[3].equalsIgnoreCase("ON") || split[3].equalsIgnoreCase("OPEN"))
                                        trigger.links.get(linkIndex).linkType = Types.OPEN;
                                    else if (split[3].equalsIgnoreCase("OFF") || split[3].equalsIgnoreCase("CLOSE"))
                                        trigger.links.get(linkIndex).linkType = Types.CLOSE;
                                    else {
                                        player.sendMessage(ChatColor.RED + "Not a valid link type: " + ChatColor.WHITE + "OPEN, CLOSE, TOGGLE");
                                        return false;
                                    }
                                    trigger.links.get(linkIndex).link_name = split[2].toLowerCase();
                                    trigger.links.get(linkIndex).link_creator = player.getName();
                                    trigger.links.get(linkIndex).doorType = "TWOSTATE";
                                    dataWriter.saveDatabase();
                                    player.sendMessage("Updated trigger '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                            }
                            else {
                                player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <triggername> <doorname> <type> <doorType>");
                                player.sendMessage(ChatColor.BLUE + "Ex: /dlink trigger triggername doorname toggle 1");
                                return true;
                            }
                        }
                        else {
                            player.sendMessage(ChatColor.RED + "The Two State door - " + ChatColor.WHITE + split[2] + ChatColor.RED + " was not found");
                            return true;
                        }
                    }
                    else if (state == 3) {
                        int id3 = plugin.hdoorhelper.findDoor(split[2].toLowerCase(), player.getName(), player.getWorld().getName());
                        if (id3 != -1) {
                            if (split[3].equalsIgnoreCase("toggle") || split[3].equalsIgnoreCase("open") || split[3].equalsIgnoreCase("close")) {
                                int linkIndex = plugin.triggerhelper.findLink(trigger, split[2].toLowerCase(), player.getName(), "HYBRIDSTATE");
                                if (linkIndex == -1) {
                                    trigger.links.add(new Link(split[2].toLowerCase(), player.getName(), split[3].toLowerCase(), "HYBRIDSTATE"));
                                    dataWriter.saveDatabase();
                                    player.sendMessage("Added trigger '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - HybridState - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                                else {
                                    if (split[3].equalsIgnoreCase("TOGGLE") || split[3].equalsIgnoreCase("T"))
                                        trigger.links.get(linkIndex).linkType = Types.TOGGLE;
                                    else if (split[3].equalsIgnoreCase("ON") || split[3].equalsIgnoreCase("OPEN"))
                                        trigger.links.get(linkIndex).linkType = Types.OPEN;
                                    else if (split[3].equalsIgnoreCase("OFF") || split[3].equalsIgnoreCase("CLOSE"))
                                        trigger.links.get(linkIndex).linkType = Types.CLOSE;
                                    else {
                                        player.sendMessage(ChatColor.RED + "Not a valid link type: " + ChatColor.WHITE + "OPEN, CLOSE, TOGGLE");
                                        return false;
                                    }
                                    trigger.links.get(linkIndex).link_name = split[2].toLowerCase();
                                    trigger.links.get(linkIndex).link_creator = player.getName();
                                    trigger.links.get(linkIndex).doorType = "HYBRIDSTATE";
                                    dataWriter.saveDatabase();
                                    player.sendMessage("Updated trigger '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                            }
                            else {
                                player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <triggername> <doorname> <type> <doorType>");
                                player.sendMessage(ChatColor.BLUE + "Ex: /dlink trigger triggername doorname toggle 1");
                                return true;
                            }
                        }
                        else {
                            player.sendMessage(ChatColor.RED + "The Hybrid State door - " + ChatColor.WHITE + split[2] + ChatColor.RED + " was not found");
                            return true;
                        }
                    }
                    else {
                        player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <triggername> <doorname> <type> <doorType>");
                        player.sendMessage(ChatColor.BLUE + "Ex: /dlink trigger triggername doorname toggle 1");
                        return true;
                    }
                } catch (Exception e) {
                    player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <triggername> <doorname> <type> <doorType>");
                    player.sendMessage(ChatColor.BLUE + "Ex: /dlink trigger triggername doorname toggle 1");
                    return true;
                }
            }
            else {
                player.sendMessage(ChatColor.RED + "The trigger - " + ChatColor.WHITE + split[1] + ChatColor.RED + " was not found");
                return true;
            }
        }
        else if (settings.friendlyCommand.equals("singleState-Trigger")) {
            BlockTrigger trigger = (BlockTrigger) plugin.triggerhelper.findTrigger("TRIGGER", split[1].toLowerCase(), player.getName(), player.getWorld().getName());
            if (trigger != null) {
                int id2 = plugin.singlestatedoorhelper.findDoor(split[2].toLowerCase(), player.getName(), player.getWorld().getName());
                if (id2 != -1) {
                    if (split[3].equalsIgnoreCase("toggle") || split[3].equalsIgnoreCase("open") || split[3].equalsIgnoreCase("close")) {
                        int linkIndex = plugin.triggerhelper.findLink(trigger, split[2].toLowerCase(), player.getName(), "ONESTATE");
                        if (linkIndex == -1) {
                            trigger.links.add(new Link(split[2].toLowerCase(), player.getName(), split[3].toLowerCase(), "ONESTATE"));
                            dataWriter.saveDatabase();
                            player.sendMessage("Added trigger '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                            return true;
                        }
                        else {
                            if (split[3].equalsIgnoreCase("TOGGLE") || split[3].equalsIgnoreCase("T"))
                                trigger.links.get(linkIndex).linkType = Types.TOGGLE;
                            else if (split[3].equalsIgnoreCase("ON") || split[3].equalsIgnoreCase("OPEN"))
                                trigger.links.get(linkIndex).linkType = Types.OPEN;
                            else if (split[3].equalsIgnoreCase("OFF") || split[3].equalsIgnoreCase("CLOSE"))
                                trigger.links.get(linkIndex).linkType = Types.CLOSE;
                            else {
                                player.sendMessage(ChatColor.RED + "Not a valid link type: " + ChatColor.WHITE + "OPEN, CLOSE, TOGGLE");
                                return false;
                            }
                            trigger.links.get(linkIndex).link_name = split[2].toLowerCase();
                            trigger.links.get(linkIndex).link_creator = player.getName();
                            trigger.links.get(linkIndex).doorType = "ONESTATE";
                            dataWriter.saveDatabase();
                            player.sendMessage("Updated trigger '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                            return true;
                        }
                    }
                    else {
                        player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <triggername> <doorname> <type> <doorType>");
                        player.sendMessage(ChatColor.BLUE + "Ex: /dlink trigger triggername doorname toggle 1");
                        return true;
                    }
                }
                else {
                    player.sendMessage(ChatColor.RED + "The door - " + ChatColor.WHITE + split[2] + ChatColor.RED + " was not found");
                    return true;
                }
            }
            else {
                player.sendMessage(ChatColor.RED + "The trigger - " + ChatColor.WHITE + split[1] + ChatColor.RED + " was not found");
                return true;
            }
        }
        else if (settings.friendlyCommand.equals("stateSelected-MyTrigger")) {
            MyTrigger mytrigger = (MyTrigger) plugin.triggerhelper.findTrigger("MYTRIGGER", split[1].toLowerCase(), player.getName(), player.getWorld().getName());
            if (mytrigger != null) {
                try {
                    int state = Integer.parseInt(split[4]);
                    if (state == 1) {
                        int id2 = plugin.singlestatedoorhelper.findDoor(split[2].toLowerCase(), player.getName(), player.getWorld().getName());
                        if (id2 != -1) {
                            if (split[3].equalsIgnoreCase("toggle") || split[3].equalsIgnoreCase("open") || split[3].equalsIgnoreCase("close")) {
                                int linkIndex = plugin.triggerhelper.findLink(mytrigger, split[2].toLowerCase(), player.getName(), "ONESTATE");
                                if (linkIndex == -1) {
                                    mytrigger.links.add(new Link(split[2].toLowerCase(), player.getName(), split[3].toLowerCase(), "ONESTATE"));
                                    dataWriter.saveDatabase();
                                    player.sendMessage("Added mytrigger '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                                else {
                                    if (split[3].equalsIgnoreCase("TOGGLE") || split[3].equalsIgnoreCase("T"))
                                        mytrigger.links.get(linkIndex).linkType = Types.TOGGLE;
                                    else if (split[3].equalsIgnoreCase("ON") || split[3].equalsIgnoreCase("OPEN"))
                                        mytrigger.links.get(linkIndex).linkType = Types.OPEN;
                                    else if (split[3].equalsIgnoreCase("OFF") || split[3].equalsIgnoreCase("CLOSE"))
                                        mytrigger.links.get(linkIndex).linkType = Types.CLOSE;
                                    else {
                                        player.sendMessage(ChatColor.RED + "Not a valid link type: " + ChatColor.WHITE + "OPEN, CLOSE, TOGGLE");
                                        return false;
                                    }
                                    mytrigger.links.get(linkIndex).link_name = split[2].toLowerCase();
                                    mytrigger.links.get(linkIndex).link_creator = player.getName();
                                    mytrigger.links.get(linkIndex).doorType = "ONESTATE";
                                    dataWriter.saveDatabase();
                                    player.sendMessage("Updated mytrigger '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                            }
                            else {
                                player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <triggername> <doorname> <type> <doorType>");
                                player.sendMessage(ChatColor.BLUE + "Ex: /dlink mytrigger triggername doorname toggle 1");
                                return true;
                            }
                        }
                        else {
                            player.sendMessage(ChatColor.RED + "The door - " + ChatColor.WHITE + split[2] + ChatColor.RED + " was not found");
                            return true;
                        }
                    }
                    else if (state == 2) {
                        int id2 = plugin.twostatedoorhelper.findDoor(split[2].toLowerCase(), player.getName(), player.getWorld().getName());
                        if (id2 != -1) {
                            if (split[3].equalsIgnoreCase("toggle") || split[3].equalsIgnoreCase("open") || split[3].equalsIgnoreCase("close")) {
                                int linkIndex = plugin.triggerhelper.findLink(mytrigger, split[2].toLowerCase(), player.getName(), "TWOSTATE");
                                if (linkIndex == -1) {
                                    mytrigger.links.add(new Link(split[2].toLowerCase(), player.getName(), split[3].toLowerCase(), "TWOSTATE"));
                                    dataWriter.saveDatabase();
                                    player.sendMessage("Added mytrigger '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - TwoState - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                                else {
                                    if (split[3].equalsIgnoreCase("TOGGLE") || split[3].equalsIgnoreCase("T"))
                                        mytrigger.links.get(linkIndex).linkType = Types.TOGGLE;
                                    else if (split[3].equalsIgnoreCase("ON") || split[3].equalsIgnoreCase("OPEN"))
                                        mytrigger.links.get(linkIndex).linkType = Types.OPEN;
                                    else if (split[3].equalsIgnoreCase("OFF") || split[3].equalsIgnoreCase("CLOSE"))
                                        mytrigger.links.get(linkIndex).linkType = Types.CLOSE;
                                    else {
                                        player.sendMessage(ChatColor.RED + "Not a valid link type: " + ChatColor.WHITE + "OPEN, CLOSE, TOGGLE");
                                        return false;
                                    }
                                    mytrigger.links.get(linkIndex).link_name = split[2].toLowerCase();
                                    mytrigger.links.get(linkIndex).link_creator = player.getName();
                                    mytrigger.links.get(linkIndex).doorType = "TWOSTATE";
                                    dataWriter.saveDatabase();
                                    player.sendMessage("Updated mytrigger '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                            }
                            else {
                                player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <triggername> <doorname> <type> <doorType>");
                                player.sendMessage(ChatColor.BLUE + "Ex: /dlink mytrigger triggername doorname toggle 1");
                                return true;
                            }
                        }
                        else {
                            player.sendMessage(ChatColor.RED + "The Two State door - " + ChatColor.WHITE + split[2] + ChatColor.RED + " was not found");
                            return true;
                        }
                    }
                    else if (state == 3) {
                        int id3 = plugin.hdoorhelper.findDoor(split[2].toLowerCase(), player.getName(), player.getWorld().getName());
                        if (id3 != -1) {
                            if (split[3].equalsIgnoreCase("toggle") || split[3].equalsIgnoreCase("open") || split[3].equalsIgnoreCase("close")) {
                                int linkIndex = plugin.triggerhelper.findLink(mytrigger, split[2].toLowerCase(), player.getName(), "HYBRIDSTATE");
                                if (linkIndex == -1) {
                                    mytrigger.links.add(new Link(split[2].toLowerCase(), player.getName(), split[3].toLowerCase(), "HYBRIDSTATE"));
                                    dataWriter.saveDatabase();
                                    player.sendMessage("Added mytrigger '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - HybridState - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                                else {
                                    if (split[3].equalsIgnoreCase("TOGGLE") || split[3].equalsIgnoreCase("T"))
                                        mytrigger.links.get(linkIndex).linkType = Types.TOGGLE;
                                    else if (split[3].equalsIgnoreCase("ON") || split[3].equalsIgnoreCase("OPEN"))
                                        mytrigger.links.get(linkIndex).linkType = Types.OPEN;
                                    else if (split[3].equalsIgnoreCase("OFF") || split[3].equalsIgnoreCase("CLOSE"))
                                        mytrigger.links.get(linkIndex).linkType = Types.CLOSE;
                                    else {
                                        player.sendMessage(ChatColor.RED + "Not a valid link type: " + ChatColor.WHITE + "OPEN, CLOSE, TOGGLE");
                                        return false;
                                    }
                                    mytrigger.links.get(linkIndex).link_name = split[2].toLowerCase();
                                    mytrigger.links.get(linkIndex).link_creator = player.getName();
                                    mytrigger.links.get(linkIndex).doorType = "HYBRIDSTATE";
                                    dataWriter.saveDatabase();
                                    player.sendMessage("Updated mytrigger '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                            }
                            else {
                                player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <triggername> <doorname> <type> <doorType>");
                                player.sendMessage(ChatColor.BLUE + "Ex: /dlink mytrigger triggername doorname toggle 1");
                                return true;
                            }
                        }
                        else {
                            player.sendMessage(ChatColor.RED + "The Hybrid State door - " + ChatColor.WHITE + split[2] + ChatColor.RED + " was not found");
                            return true;
                        }
                    }
                } catch (Exception e) {
                    player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <triggername> <doorname> <type> <doorType>");
                    player.sendMessage(ChatColor.BLUE + "Ex: /dlink mytrigger triggername doorname toggle 1");
                    return true;
                }
            }
            else {
                player.sendMessage(ChatColor.RED + "The mytrigger - " + ChatColor.WHITE + split[1] + ChatColor.RED + " was not found");
                return true;
            }
        }
        else if (settings.friendlyCommand.equals("singleState-MyTrigger")) {
            MyTrigger mytrigger = (MyTrigger) plugin.triggerhelper.findTrigger("MYTRIGGER", split[1].toLowerCase(), player.getName(), player.getWorld().getName());
            if (mytrigger != null) {
                int id2 = plugin.singlestatedoorhelper.findDoor(split[2].toLowerCase(), player.getName(), player.getWorld().getName());
                if (id2 != -1) {
                    if (split[3].equalsIgnoreCase("toggle") || split[3].equalsIgnoreCase("open") || split[3].equalsIgnoreCase("close")) {
                        int linkIndex = plugin.triggerhelper.findLink(mytrigger, split[2].toLowerCase(), player.getName(), "ONESTATE");
                        if (linkIndex == -1) {
                            mytrigger.links.add(new Link(split[2].toLowerCase(), player.getName(), split[3].toLowerCase(), "ONESTATE"));
                            dataWriter.saveDatabase();
                            player.sendMessage("Added mytrigger '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                            return true;
                        }
                        else {
                            if (split[3].equalsIgnoreCase("TOGGLE") || split[3].equalsIgnoreCase("T"))
                                mytrigger.links.get(linkIndex).linkType = Types.TOGGLE;
                            else if (split[3].equalsIgnoreCase("ON") || split[3].equalsIgnoreCase("OPEN"))
                                mytrigger.links.get(linkIndex).linkType = Types.OPEN;
                            else if (split[3].equalsIgnoreCase("OFF") || split[3].equalsIgnoreCase("CLOSE"))
                                mytrigger.links.get(linkIndex).linkType = Types.CLOSE;
                            else {
                                player.sendMessage(ChatColor.RED + "Not a valid link type: " + ChatColor.WHITE + "OPEN, CLOSE, TOGGLE");
                                return false;
                            }
                            mytrigger.links.get(linkIndex).link_name = split[2].toLowerCase();
                            mytrigger.links.get(linkIndex).link_creator = player.getName();
                            mytrigger.links.get(linkIndex).doorType = "ONESTATE";
                            dataWriter.saveDatabase();
                            player.sendMessage("Updated mytrigger '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                            return true;
                        }
                    }
                    else {
                        player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <triggername> <doorname> <type> <doorType>");
                        player.sendMessage(ChatColor.BLUE + "Ex: /dlink mytrigger triggername doorname toggle 1");
                        return true;
                    }
                }
                else {
                    player.sendMessage(ChatColor.RED + "The door - " + ChatColor.WHITE + split[2] + ChatColor.RED + " was not found");
                    return true;
                }
            }
            else {
                player.sendMessage(ChatColor.RED + "The mytrigger - " + ChatColor.WHITE + split[1] + ChatColor.RED + " was not found");
                return true;
            }
        }
        else if (settings.friendlyCommand.equals("stateSelected-redTrig")) {
            RedTrig redtrig = (RedTrig) plugin.triggerhelper.findTrigger("REDTRIG", split[1].toLowerCase(), player.getName(), player.getWorld().getName());
            if (redtrig != null) {
                try {
                    int state = Integer.parseInt(split[4]);
                    if (state == 1) {
                        int id2 = plugin.singlestatedoorhelper.findDoor(split[2].toLowerCase(), player.getName(), player.getWorld().getName());
                        if (id2 != -1) {
                            if (split[3].equalsIgnoreCase("toggle") || split[3].equalsIgnoreCase("open") || split[3].equalsIgnoreCase("close")) {
                                int linkIndex = plugin.triggerhelper.findLink(redtrig, split[2].toLowerCase(), player.getName(), "ONESTATE");
                                if (linkIndex == -1) {
                                    redtrig.links.add(new Link(split[2].toLowerCase(), player.getName(), split[3].toLowerCase(), "ONESTATE"));
                                    dataWriter.saveDatabase();
                                    player.sendMessage("Added redstone trigger '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                                else {
                                    if (split[3].equalsIgnoreCase("TOGGLE") || split[3].equalsIgnoreCase("T"))
                                        redtrig.links.get(linkIndex).linkType = Types.TOGGLE;
                                    else if (split[3].equalsIgnoreCase("ON") || split[3].equalsIgnoreCase("OPEN"))
                                        redtrig.links.get(linkIndex).linkType = Types.OPEN;
                                    else if (split[3].equalsIgnoreCase("OFF") || split[3].equalsIgnoreCase("CLOSE"))
                                        redtrig.links.get(linkIndex).linkType = Types.CLOSE;
                                    else {
                                        player.sendMessage(ChatColor.RED + "Not a valid link type: " + ChatColor.WHITE + "OPEN, CLOSE, TOGGLE");
                                        return false;
                                    }
                                    redtrig.links.get(linkIndex).link_name = split[2].toLowerCase();
                                    redtrig.links.get(linkIndex).link_creator = player.getName();
                                    redtrig.links.get(linkIndex).doorType = "ONESTATE";
                                    dataWriter.saveDatabase();
                                    player.sendMessage("Updated redstone trigger '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                            }
                            else {
                                player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <triggername> <doorname> <type> <doorType>");
                                player.sendMessage(ChatColor.BLUE + "Ex: /dlink redtrig triggername doorname toggle 1");
                                return true;
                            }
                        }
                        else {
                            player.sendMessage(ChatColor.RED + "The door - " + ChatColor.WHITE + split[2] + ChatColor.RED + " was not found");
                            return true;
                        }
                    }
                    else if (state == 2) {
                        int id2 = plugin.twostatedoorhelper.findDoor(split[2].toLowerCase(), player.getName(), player.getWorld().getName());
                        if (id2 != -1) {
                            if (split[3].equalsIgnoreCase("toggle") || split[3].equalsIgnoreCase("open") || split[3].equalsIgnoreCase("close")) {
                                int linkIndex = plugin.triggerhelper.findLink(redtrig, split[2].toLowerCase(), player.getName(), "TWOSTATE");
                                if (linkIndex == -1) {
                                    redtrig.links.add(new Link(split[2].toLowerCase(), player.getName(), split[3].toLowerCase(), "TWOSTATE"));
                                    dataWriter.saveDatabase();
                                    player.sendMessage("Added redstone trigger '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - TwoState - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                                else {
                                    if (split[3].equalsIgnoreCase("TOGGLE") || split[3].equalsIgnoreCase("T"))
                                        redtrig.links.get(linkIndex).linkType = Types.TOGGLE;
                                    else if (split[3].equalsIgnoreCase("ON") || split[3].equalsIgnoreCase("OPEN"))
                                        redtrig.links.get(linkIndex).linkType = Types.OPEN;
                                    else if (split[3].equalsIgnoreCase("OFF") || split[3].equalsIgnoreCase("CLOSE"))
                                        redtrig.links.get(linkIndex).linkType = Types.CLOSE;
                                    else {
                                        player.sendMessage(ChatColor.RED + "Not a valid link type: " + ChatColor.WHITE + "OPEN, CLOSE, TOGGLE");
                                        return false;
                                    }
                                    redtrig.links.get(linkIndex).link_name = split[2].toLowerCase();
                                    redtrig.links.get(linkIndex).link_creator = player.getName();
                                    redtrig.links.get(linkIndex).doorType = "TWOSTATE";
                                    dataWriter.saveDatabase();
                                    player.sendMessage("Updated redstone trigger '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                            }
                            else {
                                player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <triggername> <doorname> <type> <doorType>");
                                player.sendMessage(ChatColor.BLUE + "Ex: /dlink redtrig triggername doorname toggle 1");
                                return true;
                            }
                        }
                        else {
                            player.sendMessage(ChatColor.RED + "The Two State door - " + ChatColor.WHITE + split[2] + ChatColor.RED + " was not found");
                            return true;
                        }
                    }
                    else if (state == 3) {
                        int id3 = plugin.hdoorhelper.findDoor(split[2].toLowerCase(), player.getName(), player.getWorld().getName());
                        if (id3 != -1) {
                            if (split[3].equalsIgnoreCase("toggle") || split[3].equalsIgnoreCase("open") || split[3].equalsIgnoreCase("close")) {
                                int linkIndex = plugin.triggerhelper.findLink(redtrig, split[2].toLowerCase(), player.getName(), "HYBRIDSTATE");
                                if (linkIndex == -1) {
                                    redtrig.links.add(new Link(split[2].toLowerCase(), player.getName(), split[3].toLowerCase(), "HYBRIDSTATE"));
                                    dataWriter.saveDatabase();
                                    player.sendMessage("Added redstone trigger '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - HybridState - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                                else {
                                    if (split[3].equalsIgnoreCase("TOGGLE") || split[3].equalsIgnoreCase("T"))
                                        redtrig.links.get(linkIndex).linkType = Types.TOGGLE;
                                    else if (split[3].equalsIgnoreCase("ON") || split[3].equalsIgnoreCase("OPEN"))
                                        redtrig.links.get(linkIndex).linkType = Types.OPEN;
                                    else if (split[3].equalsIgnoreCase("OFF") || split[3].equalsIgnoreCase("CLOSE"))
                                        redtrig.links.get(linkIndex).linkType = Types.CLOSE;
                                    else {
                                        player.sendMessage(ChatColor.RED + "Not a valid link type: " + ChatColor.WHITE + "OPEN, CLOSE, TOGGLE");
                                        return false;
                                    }
                                    redtrig.links.get(linkIndex).link_name = split[2].toLowerCase();
                                    redtrig.links.get(linkIndex).link_creator = player.getName();
                                    redtrig.links.get(linkIndex).doorType = "HYBRIDSTATE";
                                    dataWriter.saveDatabase();
                                    player.sendMessage("Updated redstone trigger '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                            }
                            else {
                                player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <triggername> <doorname> <type> <doorType>");
                                player.sendMessage(ChatColor.BLUE + "Ex: /dlink redtrig triggername doorname toggle 1");
                                return true;
                            }
                        }
                        else {
                            player.sendMessage(ChatColor.RED + "The Hybrid State door - " + ChatColor.WHITE + split[2] + ChatColor.RED + " was not found");
                            return true;
                        }
                    }
                } catch (Exception e) {
                    player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <triggername> <doorname> <type> <doorType>");
                    player.sendMessage(ChatColor.BLUE + "Ex: /dlink redtrig triggername doorname toggle 1");
                    return true;
                }
            }
            else {
                player.sendMessage(ChatColor.RED + "The redstone trigger - " + ChatColor.WHITE + split[1] + ChatColor.RED + " was not found");
                return true;
            }
        }
        else if (settings.friendlyCommand.equals("singleState-redTrig")) {
            RedTrig redtrig = (RedTrig) plugin.redtrighelper.findTrigger("REDTRIG", split[1].toLowerCase(), player.getName(), player.getWorld().getName());
            if (redtrig != null) {
                int id2 = plugin.singlestatedoorhelper.findDoor(split[2].toLowerCase(), player.getName(), player.getWorld().getName());
                if (id2 != -1) {
                    if (split[3].equalsIgnoreCase("toggle") || split[3].equalsIgnoreCase("open") || split[3].equalsIgnoreCase("close")) {
                        int linkIndex = plugin.triggerhelper.findLink(redtrig, split[2].toLowerCase(), player.getName(), "ONESTATE");
                        if (linkIndex == -1) {
                            redtrig.links.add(new Link(split[2].toLowerCase(), player.getName(), split[3].toLowerCase(), "ONESTATE"));
                            dataWriter.saveDatabase();
                            player.sendMessage("Added redstone trigger '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                            return true;
                        }
                        else {
                            if (split[3].equalsIgnoreCase("TOGGLE") || split[3].equalsIgnoreCase("T"))
                                redtrig.links.get(linkIndex).linkType = Types.TOGGLE;
                            else if (split[3].equalsIgnoreCase("ON") || split[3].equalsIgnoreCase("OPEN"))
                                redtrig.links.get(linkIndex).linkType = Types.OPEN;
                            else if (split[3].equalsIgnoreCase("OFF") || split[3].equalsIgnoreCase("CLOSE"))
                                redtrig.links.get(linkIndex).linkType = Types.CLOSE;
                            else {
                                player.sendMessage(ChatColor.RED + "Not a valid link type: " + ChatColor.WHITE + "OPEN, CLOSE, TOGGLE");
                                return false;
                            }
                            redtrig.links.get(linkIndex).link_name = split[2].toLowerCase();
                            redtrig.links.get(linkIndex).link_creator = player.getName();
                            redtrig.links.get(linkIndex).doorType = "ONESTATE";
                            dataWriter.saveDatabase();
                            player.sendMessage("Updated redstone trigger '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                            return true;
                        }
                    }
                    else {
                        player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <triggername> <doorname> <type> <doorType>");
                        player.sendMessage(ChatColor.BLUE + "Ex: /dlink redtrig triggername doorname toggle 1");
                        return true;
                    }
                }
                else {
                    player.sendMessage(ChatColor.RED + "The door - " + ChatColor.WHITE + split[2] + ChatColor.RED + " was not found");
                    return true;
                }
            }
            else {
                player.sendMessage(ChatColor.RED + "The redstone trigger - " + ChatColor.WHITE + split[1] + ChatColor.RED + " was not found");
                return true;
            }
        }
        else if (settings.friendlyCommand.equals("stateSelected-zone")) {
            Zone zone = plugin.zonehelper.findZone("ZONE", split[1].toLowerCase(), player.getName(), player.getWorld().getName());
            if (zone != null) {
                try {
                    int state = Integer.parseInt(split[4]);
                    if (state == 1) {
                        int id2 = plugin.singlestatedoorhelper.findDoor(split[2].toLowerCase(), player.getName(), player.getWorld().getName());
                        if (id2 != -1) {
                            if (split[3].equalsIgnoreCase("toggle") || split[3].equalsIgnoreCase("open") || split[3].equalsIgnoreCase("close")) {
                                int linkIndex = plugin.zonehelper.findLink(zone, split[2].toLowerCase(), player.getName(), "ONESTATE");
                                if (linkIndex == -1) {
                                    zone.links.add(new Link(split[2].toLowerCase(), player.getName(), split[3].toLowerCase(), "ONESTATE"));
                                    dataWriter.saveDatabase();
                                    player.sendMessage("Added zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                                else {
                                    if (split[3].equalsIgnoreCase("TOGGLE") || split[3].equalsIgnoreCase("T"))
                                        zone.links.get(linkIndex).linkType = Types.TOGGLE;
                                    else if (split[3].equalsIgnoreCase("ON") || split[3].equalsIgnoreCase("OPEN"))
                                        zone.links.get(linkIndex).linkType = Types.OPEN;
                                    else if (split[3].equalsIgnoreCase("OFF") || split[3].equalsIgnoreCase("CLOSE"))
                                        zone.links.get(linkIndex).linkType = Types.CLOSE;
                                    else {
                                        player.sendMessage(ChatColor.RED + "Not a valid link type: " + ChatColor.WHITE + "OPEN, CLOSE, TOGGLE");
                                        return false;
                                    }
                                    zone.links.get(linkIndex).link_name = split[2].toLowerCase();
                                    zone.links.get(linkIndex).link_creator = player.getName();
                                    zone.links.get(linkIndex).doorType = "ONESTATE";
                                    dataWriter.saveDatabase();
                                    player.sendMessage("Updated zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                            }
                            else {
                                player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <triggername> <doorname> <type> <doorType>");
                                player.sendMessage(ChatColor.BLUE + "Ex: /dlink zone triggername doorname toggle 1");
                                return true;
                            }
                        }
                        else {
                            player.sendMessage(ChatColor.RED + "The door - " + ChatColor.WHITE + split[2] + ChatColor.RED + " was not found");
                            return true;
                        }
                    }
                    else if (state == 2) {
                        int id2 = plugin.twostatedoorhelper.findDoor(split[2].toLowerCase(), player.getName(), player.getWorld().getName());
                        if (id2 != -1) {
                            if (split[3].equalsIgnoreCase("toggle") || split[3].equalsIgnoreCase("open") || split[3].equalsIgnoreCase("close")) {
                                int linkIndex = plugin.zonehelper.findLink(zone, split[2].toLowerCase(), player.getName(), "TWOSTATE");
                                if (linkIndex == -1) {
                                    zone.links.add(new Link(split[2].toLowerCase(), player.getName(), split[3].toLowerCase(), "TWOSTATE"));
                                    dataWriter.saveDatabase();
                                    player.sendMessage("Added zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - TwoState - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                                else {
                                    if (split[3].equalsIgnoreCase("TOGGLE") || split[3].equalsIgnoreCase("T"))
                                        zone.links.get(linkIndex).linkType = Types.TOGGLE;
                                    else if (split[3].equalsIgnoreCase("ON") || split[3].equalsIgnoreCase("OPEN"))
                                        zone.links.get(linkIndex).linkType = Types.OPEN;
                                    else if (split[3].equalsIgnoreCase("OFF") || split[3].equalsIgnoreCase("CLOSE"))
                                        zone.links.get(linkIndex).linkType = Types.CLOSE;
                                    else {
                                        player.sendMessage(ChatColor.RED + "Not a valid link type: " + ChatColor.WHITE + "OPEN, CLOSE, TOGGLE");
                                        return false;
                                    }
                                    zone.links.get(linkIndex).link_name = split[2].toLowerCase();
                                    zone.links.get(linkIndex).link_creator = player.getName();
                                    zone.links.get(linkIndex).doorType = "TWOSTATE";
                                    dataWriter.saveDatabase();
                                    player.sendMessage("Updated zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                            }
                            else {
                                player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <triggername> <doorname> <type> <doorType>");
                                player.sendMessage(ChatColor.BLUE + "Ex: /dlink zone triggername doorname toggle 1");
                                return true;
                            }
                        }
                        else {
                            player.sendMessage(ChatColor.RED + "The door - " + ChatColor.WHITE + split[2] + ChatColor.RED + " was not found");
                            return true;
                        }
                    }
                    else if (state == 3) {
                        int id3 = plugin.hdoorhelper.findDoor(split[2].toLowerCase(), player.getName(), player.getWorld().getName());
                        if (id3 != -1) {
                            if (split[3].equalsIgnoreCase("toggle") || split[3].equalsIgnoreCase("open") || split[3].equalsIgnoreCase("close")) {
                                int linkIndex = plugin.zonehelper.findLink(zone, split[2].toLowerCase(), player.getName(), "HYBRIDSTATE");
                                if (linkIndex == -1) {
                                    zone.links.add(new Link(split[2].toLowerCase(), player.getName(), split[3].toLowerCase(), "HYBRIDSTATE"));
                                    dataWriter.saveDatabase();
                                    player.sendMessage("Added zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - HybridState - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                                else {
                                    if (split[3].equalsIgnoreCase("TOGGLE") || split[3].equalsIgnoreCase("T"))
                                        zone.links.get(linkIndex).linkType = Types.TOGGLE;
                                    else if (split[3].equalsIgnoreCase("ON") || split[3].equalsIgnoreCase("OPEN"))
                                        zone.links.get(linkIndex).linkType = Types.OPEN;
                                    else if (split[3].equalsIgnoreCase("OFF") || split[3].equalsIgnoreCase("CLOSE"))
                                        zone.links.get(linkIndex).linkType = Types.CLOSE;
                                    else {
                                        player.sendMessage(ChatColor.RED + "Not a valid link type: " + ChatColor.WHITE + "OPEN, CLOSE, TOGGLE");
                                        return false;
                                    }
                                    zone.links.get(linkIndex).link_name = split[2].toLowerCase();
                                    zone.links.get(linkIndex).link_creator = player.getName();
                                    zone.links.get(linkIndex).doorType = "HYBRIDSTATE";
                                    dataWriter.saveDatabase();
                                    player.sendMessage("Updated zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                            }
                            else {
                                player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <triggername> <doorname> <type> <doorType>");
                                player.sendMessage(ChatColor.BLUE + "Ex: /dlink zone triggername doorname toggle 1");
                                return true;
                            }
                        }
                        else {
                            player.sendMessage(ChatColor.RED + "The Hybrid door - " + ChatColor.WHITE + split[2] + ChatColor.RED + " was not found");
                            return true;
                        }
                    }
                } catch (Exception e) {
                    player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <triggername> <doorname> <type> <doorType>");
                    player.sendMessage(ChatColor.BLUE + "Ex: /dlink zone triggername doorname toggle 1");
                    return true;
                }
            }
            else {
                player.sendMessage(ChatColor.RED + "The zone - " + ChatColor.WHITE + split[1] + ChatColor.RED + " was not found");
                return true;
            }
        }
        else if (settings.friendlyCommand.equals("singleState-zone")) {
            Zone zone = plugin.zonehelper.findZone("ZONE", split[1].toLowerCase(), player.getName(), player.getWorld().getName());
            if (zone != null) {
                int id2 = plugin.singlestatedoorhelper.findDoor(split[2].toLowerCase(), player.getName(), player.getWorld().getName());
                if (id2 != -1) {
                    if (split[3].equalsIgnoreCase("toggle") || split[3].equalsIgnoreCase("open") || split[3].equalsIgnoreCase("close")) {
                        int linkIndex = plugin.zonehelper.findLink(zone, split[2].toLowerCase(), player.getName(), "ONESTATE");
                        if (linkIndex == -1) {
                            zone.links.add(new Link(split[2].toLowerCase(), player.getName(), split[3].toLowerCase(), "ONESTATE"));
                            dataWriter.saveDatabase();
                            player.sendMessage("Added zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                            return true;
                        }
                        else {
                            if (split[3].equalsIgnoreCase("TOGGLE") || split[3].equalsIgnoreCase("T"))
                                zone.links.get(linkIndex).linkType = Types.TOGGLE;
                            else if (split[3].equalsIgnoreCase("ON") || split[3].equalsIgnoreCase("OPEN"))
                                zone.links.get(linkIndex).linkType = Types.OPEN;
                            else if (split[3].equalsIgnoreCase("OFF") || split[3].equalsIgnoreCase("CLOSE"))
                                zone.links.get(linkIndex).linkType = Types.CLOSE;
                            else {
                                player.sendMessage(ChatColor.RED + "Not a valid link type: " + ChatColor.WHITE + "OPEN, CLOSE, TOGGLE");
                                return false;
                            }
                            zone.links.get(linkIndex).link_name = split[2].toLowerCase();
                            zone.links.get(linkIndex).link_creator = player.getName();
                            zone.links.get(linkIndex).doorType = "ONESTATE";
                            dataWriter.saveDatabase();
                            player.sendMessage("Updated zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                            return true;
                        }
                    }
                    else {
                        player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <triggername> <doorname> <type> <doorType>");
                        player.sendMessage(ChatColor.BLUE + "Ex: /dlink zone triggername doorname toggle 1");
                        return true;
                    }
                }
                else {
                    player.sendMessage(ChatColor.RED + "The door - " + ChatColor.WHITE + split[2] + ChatColor.RED + " was not found");
                    return true;
                }
            }
            else {
                player.sendMessage(ChatColor.RED + "The zone - " + ChatColor.WHITE + split[1] + ChatColor.RED + " was not found");
                return true;
            }
        }
        else if (settings.friendlyCommand.equals("stateSelected-myzone")) {
            Zone myzone = plugin.zonehelper.findZone("MYZONE", split[1].toLowerCase(), player.getName(), player.getWorld().getName());
            if (myzone != null) {
                try {
                    int state = Integer.parseInt(split[4]);
                    if (state == 1) {
                        int id2 = plugin.singlestatedoorhelper.findDoor(split[2].toLowerCase(), player.getName(), player.getWorld().getName());
                        if (id2 != -1) {
                            if (split[3].equalsIgnoreCase("toggle") || split[3].equalsIgnoreCase("open") || split[3].equalsIgnoreCase("close")) {
                                int linkIndex = plugin.zonehelper.findLink(myzone, split[2].toLowerCase(), player.getName(), "ONESTATE");
                                if (linkIndex == -1) {
                                    myzone.links.add(new Link(split[2].toLowerCase(), player.getName(), split[3].toLowerCase(), "ONESTATE"));
                                    dataWriter.saveDatabase();
                                    player.sendMessage("Added My zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                                else {
                                    if (split[3].equalsIgnoreCase("TOGGLE") || split[3].equalsIgnoreCase("T"))
                                        myzone.links.get(linkIndex).linkType = Types.TOGGLE;
                                    else if (split[3].equalsIgnoreCase("ON") || split[3].equalsIgnoreCase("OPEN"))
                                        myzone.links.get(linkIndex).linkType = Types.OPEN;
                                    else if (split[3].equalsIgnoreCase("OFF") || split[3].equalsIgnoreCase("CLOSE"))
                                        myzone.links.get(linkIndex).linkType = Types.CLOSE;
                                    else {
                                        player.sendMessage(ChatColor.RED + "Not a valid link type: " + ChatColor.WHITE + "OPEN, CLOSE, TOGGLE");
                                        return false;
                                    }
                                    myzone.links.get(linkIndex).link_name = split[2].toLowerCase();
                                    myzone.links.get(linkIndex).link_creator = player.getName();
                                    myzone.links.get(linkIndex).doorType = "ONESTATE";
                                    dataWriter.saveDatabase();
                                    player.sendMessage("Updated My zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                            }
                            else {
                                player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <triggername> <doorname> <type> <doorType>");
                                player.sendMessage(ChatColor.BLUE + "Ex: /dlink myzone triggername doorname toggle 1");
                                return true;
                            }
                        }
                        else {
                            player.sendMessage(ChatColor.RED + "The door - " + ChatColor.WHITE + split[2] + ChatColor.RED + " was not found");
                            return true;
                        }
                    }
                    else if (state == 2) {
                        int id2 = plugin.twostatedoorhelper.findDoor(split[2].toLowerCase(), player.getName(), player.getWorld().getName());
                        if (id2 != -1) {
                            if (split[3].equalsIgnoreCase("toggle") || split[3].equalsIgnoreCase("open") || split[3].equalsIgnoreCase("close")) {
                                int linkIndex = plugin.zonehelper.findLink(myzone, split[2].toLowerCase(), player.getName(), "TWOSTATE");
                                if (linkIndex == -1) {
                                    myzone.links.add(new Link(split[2].toLowerCase(), player.getName(), split[3].toLowerCase(), "TWOSTATE"));
                                    dataWriter.saveDatabase();
                                    player.sendMessage("Added My zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - TwoState - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                                else {
                                    if (split[3].equalsIgnoreCase("TOGGLE") || split[3].equalsIgnoreCase("T"))
                                        myzone.links.get(linkIndex).linkType = Types.TOGGLE;
                                    else if (split[3].equalsIgnoreCase("ON") || split[3].equalsIgnoreCase("OPEN"))
                                        myzone.links.get(linkIndex).linkType = Types.OPEN;
                                    else if (split[3].equalsIgnoreCase("OFF") || split[3].equalsIgnoreCase("CLOSE"))
                                        myzone.links.get(linkIndex).linkType = Types.CLOSE;
                                    else {
                                        player.sendMessage(ChatColor.RED + "Not a valid link type: " + ChatColor.WHITE + "OPEN, CLOSE, TOGGLE");
                                        return false;
                                    }
                                    myzone.links.get(linkIndex).link_name = split[2].toLowerCase();
                                    myzone.links.get(linkIndex).link_creator = player.getName();
                                    myzone.links.get(linkIndex).doorType = "TWOSTATE";
                                    dataWriter.saveDatabase();
                                    player.sendMessage("Updated My zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                            }
                            else {
                                player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <triggername> <doorname> <type> <doorType>");
                                player.sendMessage(ChatColor.BLUE + "Ex: /dlink myzone triggername doorname toggle 1");
                                return true;
                            }
                        }
                        else {
                            player.sendMessage(ChatColor.RED + "The door - " + ChatColor.WHITE + split[2] + ChatColor.RED + " was not found");
                            return true;
                        }
                    }
                    else if (state == 3) {
                        int id3 = plugin.hdoorhelper.findDoor(split[2].toLowerCase(), player.getName(), player.getWorld().getName());
                        if (id3 != -1) {
                            if (split[3].equalsIgnoreCase("toggle") || split[3].equalsIgnoreCase("open") || split[3].equalsIgnoreCase("close")) {
                                int linkIndex = plugin.zonehelper.findLink(myzone, split[2].toLowerCase(), player.getName(), "HYBRIDSTATE");
                                if (linkIndex == -1) {
                                    myzone.links.add(new Link(split[2].toLowerCase(), player.getName(), split[3].toLowerCase(), "HYBRIDSTATE"));
                                    dataWriter.saveDatabase();
                                    player.sendMessage("Added My zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - HybridState - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                                else {
                                    if (split[3].equalsIgnoreCase("TOGGLE") || split[3].equalsIgnoreCase("T"))
                                        myzone.links.get(linkIndex).linkType = Types.TOGGLE;
                                    else if (split[3].equalsIgnoreCase("ON") || split[3].equalsIgnoreCase("OPEN"))
                                        myzone.links.get(linkIndex).linkType = Types.OPEN;
                                    else if (split[3].equalsIgnoreCase("OFF") || split[3].equalsIgnoreCase("CLOSE"))
                                        myzone.links.get(linkIndex).linkType = Types.CLOSE;
                                    else {
                                        player.sendMessage(ChatColor.RED + "Not a valid link type: " + ChatColor.WHITE + "OPEN, CLOSE, TOGGLE");
                                        return false;
                                    }
                                    myzone.links.get(linkIndex).link_name = split[2].toLowerCase();
                                    myzone.links.get(linkIndex).link_creator = player.getName();
                                    myzone.links.get(linkIndex).doorType = "HYBRIDSTATE";
                                    dataWriter.saveDatabase();
                                    player.sendMessage("Updated My zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                            }
                            else {
                                player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <triggername> <doorname> <type> <doorType>");
                                player.sendMessage(ChatColor.BLUE + "Ex: /dlink myzone triggername doorname toggle 1");
                                return true;
                            }
                        }
                        else {
                            player.sendMessage(ChatColor.RED + "The Hybrid door - " + ChatColor.WHITE + split[2] + ChatColor.RED + " was not found");
                            return true;
                        }
                    }
                } catch (Exception e) {
                    player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <triggername> <doorname> <type> <doorType>");
                    return true;
                }
            }
            else {
                player.sendMessage(ChatColor.RED + "The My zone - " + ChatColor.WHITE + split[1] + ChatColor.RED + " was not found");
                return true;
            }
        }
        else if (settings.friendlyCommand.equals("singleState-myzone")) {
            Zone myzone = plugin.zonehelper.findZone("MYZONE", split[1].toLowerCase(), player.getName(), player.getWorld().getName());
            if (myzone != null) {
                int id2 = plugin.singlestatedoorhelper.findDoor(split[2].toLowerCase(), player.getName(), player.getWorld().getName());
                if (id2 != -1) {
                    if (split[3].equalsIgnoreCase("toggle") || split[3].equalsIgnoreCase("open") || split[3].equalsIgnoreCase("close")) {
                        int linkIndex = plugin.zonehelper.findLink(myzone, split[2].toLowerCase(), player.getName(), "ONESTATE");
                        if (linkIndex == -1) {
                            myzone.links.add(new Link(split[2].toLowerCase(), player.getName(), split[3].toLowerCase(), "ONESTATE"));
                            dataWriter.saveDatabase();
                            player.sendMessage("Added myzone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                            return true;
                        }
                        else {
                            if (split[3].equalsIgnoreCase("TOGGLE") || split[3].equalsIgnoreCase("T"))
                                myzone.links.get(linkIndex).linkType = Types.TOGGLE;
                            else if (split[3].equalsIgnoreCase("ON") || split[3].equalsIgnoreCase("OPEN"))
                                myzone.links.get(linkIndex).linkType = Types.OPEN;
                            else if (split[3].equalsIgnoreCase("OFF") || split[3].equalsIgnoreCase("CLOSE"))
                                myzone.links.get(linkIndex).linkType = Types.CLOSE;
                            else {
                                player.sendMessage(ChatColor.RED + "Not a valid link type: " + ChatColor.WHITE + "OPEN, CLOSE, TOGGLE");
                                return false;
                            }
                            myzone.links.get(linkIndex).link_name = split[2].toLowerCase();
                            myzone.links.get(linkIndex).link_creator = player.getName();
                            myzone.links.get(linkIndex).doorType = "ONESTATE";
                            dataWriter.saveDatabase();
                            player.sendMessage("Updated My zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                            return true;
                        }
                    }
                    else {
                        player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <triggername> <doorname> <type> <doorType>");
                        player.sendMessage(ChatColor.BLUE + "Ex: /dlink myzone triggername doorname toggle 1");
                        return true;
                    }
                }
                else {
                    player.sendMessage(ChatColor.RED + "The door - " + ChatColor.WHITE + split[2] + ChatColor.RED + " was not found");
                    return true;
                }
            }
            else {
                player.sendMessage(ChatColor.RED + "The myzone - " + ChatColor.WHITE + split[1] + ChatColor.RED + " was not found");
                return true;
            }
        }
        else if (settings.friendlyCommand.equals("stateSelected-azone")) {
            Zone azone = plugin.zonehelper.findZone("AZONE", split[1].toLowerCase(), player.getName(), player.getWorld().getName());
            if (azone != null) {
                try {
                    int state = Integer.parseInt(split[4]);
                    if (state == 1) {
                        int id2 = plugin.singlestatedoorhelper.findDoor(split[2].toLowerCase(), player.getName(), player.getWorld().getName());
                        if (id2 != -1) {
                            if (split[3].equalsIgnoreCase("toggle") || split[3].equalsIgnoreCase("open") || split[3].equalsIgnoreCase("close")) {
                                int linkIndex = plugin.zonehelper.findLink(azone, split[2].toLowerCase(), player.getName(), "ONESTATE");
                                if (linkIndex == -1) {
                                    azone.links.add(new Link(split[2].toLowerCase(), player.getName(), split[3].toLowerCase(), "ONESTATE"));
                                    dataWriter.saveDatabase();
                                    player.sendMessage("Added Aggressive Mob zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                                else {
                                    if (split[3].equalsIgnoreCase("TOGGLE") || split[3].equalsIgnoreCase("T"))
                                        azone.links.get(linkIndex).linkType = Types.TOGGLE;
                                    else if (split[3].equalsIgnoreCase("ON") || split[3].equalsIgnoreCase("OPEN"))
                                        azone.links.get(linkIndex).linkType = Types.OPEN;
                                    else if (split[3].equalsIgnoreCase("OFF") || split[3].equalsIgnoreCase("CLOSE"))
                                        azone.links.get(linkIndex).linkType = Types.CLOSE;
                                    else {
                                        player.sendMessage(ChatColor.RED + "Not a valid link type: " + ChatColor.WHITE + "OPEN, CLOSE, TOGGLE");
                                        return false;
                                    }
                                    azone.links.get(linkIndex).link_name = split[2].toLowerCase();
                                    azone.links.get(linkIndex).link_creator = player.getName();
                                    azone.links.get(linkIndex).doorType = "ONESTATE";
                                    dataWriter.saveDatabase();
                                    player.sendMessage("Updated Aggressive Mob zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                            }
                            else {
                                player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <triggername> <doorname> <type> <doorType>");
                                player.sendMessage(ChatColor.BLUE + "Ex: /dlink azone triggername doorname toggle 1");
                                return true;
                            }
                        }
                        else {
                            player.sendMessage(ChatColor.RED + "The door - " + ChatColor.WHITE + split[2] + ChatColor.RED + " was not found");
                        }
                    }
                    else if (state == 2) {
                        int id2 = plugin.twostatedoorhelper.findDoor(split[2].toLowerCase(), player.getName(), player.getWorld().getName());
                        if (id2 != -1) {
                            if (split[3].equalsIgnoreCase("toggle") || split[3].equalsIgnoreCase("open") || split[3].equalsIgnoreCase("close")) {
                                int linkIndex = plugin.zonehelper.findLink(azone, split[2].toLowerCase(), player.getName(), "TWOSTATE");
                                if (linkIndex == -1) {
                                    azone.links.add(new Link(split[2].toLowerCase(), player.getName(), split[3].toLowerCase(), "TWOSTATE"));
                                    dataWriter.saveDatabase();
                                    player.sendMessage("Added Aggressive Mob zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - TwoState - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                                else {
                                    if (split[3].equalsIgnoreCase("TOGGLE") || split[3].equalsIgnoreCase("T"))
                                        azone.links.get(linkIndex).linkType = Types.TOGGLE;
                                    else if (split[3].equalsIgnoreCase("ON") || split[3].equalsIgnoreCase("OPEN"))
                                        azone.links.get(linkIndex).linkType = Types.OPEN;
                                    else if (split[3].equalsIgnoreCase("OFF") || split[3].equalsIgnoreCase("CLOSE"))
                                        azone.links.get(linkIndex).linkType = Types.CLOSE;
                                    else {
                                        player.sendMessage(ChatColor.RED + "Not a valid link type: " + ChatColor.WHITE + "OPEN, CLOSE, TOGGLE");
                                        return false;
                                    }
                                    azone.links.get(linkIndex).link_name = split[2].toLowerCase();
                                    azone.links.get(linkIndex).link_creator = player.getName();
                                    azone.links.get(linkIndex).doorType = "TWOSTATE";
                                    dataWriter.saveDatabase();
                                    player.sendMessage("Updated Aggressive Mob zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                            }
                            else {
                                player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <triggername> <doorname> <type> <doorType>");
                                player.sendMessage(ChatColor.BLUE + "Ex: /dlink azone triggername doorname toggle 1");
                                return true;
                            }
                        }
                        else {
                            player.sendMessage(ChatColor.RED + "The door - " + ChatColor.WHITE + split[2] + ChatColor.RED + " was not found");
                            return true;
                        }
                    }
                    else if (state == 3) {
                        int id3 = plugin.hdoorhelper.findDoor(split[2].toLowerCase(), player.getName(), player.getWorld().getName());
                        if (id3 != -1) {
                            if (split[3].equalsIgnoreCase("toggle") || split[3].equalsIgnoreCase("open") || split[3].equalsIgnoreCase("close")) {
                                int linkIndex = plugin.zonehelper.findLink(azone, split[2].toLowerCase(), player.getName(), "HYBRIDSTATE");
                                if (linkIndex == -1) {
                                    azone.links.add(new Link(split[2].toLowerCase(), player.getName(), split[3].toLowerCase(), "HYBRIDSTATE"));
                                    dataWriter.saveDatabase();
                                    player.sendMessage("Added Aggressive Mob zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - HybridState - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                                else {
                                    if (split[3].equalsIgnoreCase("TOGGLE") || split[3].equalsIgnoreCase("T"))
                                        azone.links.get(linkIndex).linkType = Types.TOGGLE;
                                    else if (split[3].equalsIgnoreCase("ON") || split[3].equalsIgnoreCase("OPEN"))
                                        azone.links.get(linkIndex).linkType = Types.OPEN;
                                    else if (split[3].equalsIgnoreCase("OFF") || split[3].equalsIgnoreCase("CLOSE"))
                                        azone.links.get(linkIndex).linkType = Types.CLOSE;
                                    else {
                                        player.sendMessage(ChatColor.RED + "Not a valid link type: " + ChatColor.WHITE + "OPEN, CLOSE, TOGGLE");
                                        return false;
                                    }
                                    azone.links.get(linkIndex).link_name = split[2].toLowerCase();
                                    azone.links.get(linkIndex).link_creator = player.getName();
                                    azone.links.get(linkIndex).doorType = "HYBRIDSTATE";
                                    dataWriter.saveDatabase();
                                    player.sendMessage("Updated Aggressive Mob zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                            }
                            else {
                                player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <triggername> <doorname> <type> <doorType>");
                                player.sendMessage(ChatColor.BLUE + "Ex: /dlink azone triggername doorname toggle 1");
                                return true;
                            }
                        }
                        else {
                            player.sendMessage(ChatColor.RED + "The Hybrid door - " + ChatColor.WHITE + split[2] + ChatColor.RED + " was not found");
                            return true;
                        }
                    }
                } catch (Exception e) {
                    player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <triggername> <doorname> <type> <doorType>");
                    player.sendMessage(ChatColor.BLUE + "Ex: /dlink azone triggername doorname toggle 1");
                    return true;
                }
            }
            else {
                player.sendMessage(ChatColor.RED + "The Aggressive Mob zone - " + ChatColor.WHITE + split[1] + ChatColor.RED + " was not found");
                return true;
            }
        }
        else if (settings.friendlyCommand.equals("singleState-azone")) {
            Zone azone = plugin.zonehelper.findZone("AZONE", split[1].toLowerCase(), player.getName(), player.getWorld().getName());
            if (azone != null) {
                int id2 = plugin.singlestatedoorhelper.findDoor(split[2].toLowerCase(), player.getName(), player.getWorld().getName(), player);
                if (id2 != -1) {
                    if (split[3].equalsIgnoreCase("toggle") || split[3].equalsIgnoreCase("open") || split[3].equalsIgnoreCase("close")) {
                        int linkIndex = plugin.zonehelper.findLink(azone, split[2].toLowerCase(), player.getName(), "ONESTATE");
                        if (linkIndex == -1) {
                            azone.links.add(new Link(split[2].toLowerCase(), player.getName(), split[3].toLowerCase(), "ONESTATE"));
                            dataWriter.saveDatabase();
                            player.sendMessage("Added Aggressive Mob zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                            return true;
                        }
                        else {
                            if (split[3].equalsIgnoreCase("TOGGLE") || split[3].equalsIgnoreCase("T"))
                                azone.links.get(linkIndex).linkType = Types.TOGGLE;
                            else if (split[3].equalsIgnoreCase("ON") || split[3].equalsIgnoreCase("OPEN"))
                                azone.links.get(linkIndex).linkType = Types.OPEN;
                            else if (split[3].equalsIgnoreCase("OFF") || split[3].equalsIgnoreCase("CLOSE"))
                                azone.links.get(linkIndex).linkType = Types.CLOSE;
                            else {
                                player.sendMessage(ChatColor.RED + "Not a valid link type: " + ChatColor.WHITE + "OPEN, CLOSE, TOGGLE");
                                return false;
                            }
                            azone.links.get(linkIndex).link_name = split[2].toLowerCase();
                            azone.links.get(linkIndex).link_creator = player.getName();
                            azone.links.get(linkIndex).doorType = "ONESTATE";
                            dataWriter.saveDatabase();
                            player.sendMessage("Updated Aggressive Mob zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                            return true;
                        }
                    }
                    else {
                        player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <triggername> <doorname> <type> <doorType>");
                        player.sendMessage(ChatColor.BLUE + "Ex: /dlink azone triggername doorname toggle 1");
                        return true;
                    }
                }
                else {
                    player.sendMessage(ChatColor.RED + "The door - " + ChatColor.WHITE + split[2] + ChatColor.RED + " was not found");
                    return true;
                }
            }
            else {
                player.sendMessage(ChatColor.RED + "The Aggressive Mob zone - " + ChatColor.WHITE + split[1] + ChatColor.RED + " entered was not found");
                return true;
            }
        }
        else if (settings.friendlyCommand.equals("stateSelected-ezone")) {
            Zone ezone = plugin.zonehelper.findZone("EZONE", split[1].toLowerCase(), player.getName(), player.getWorld().getName());
            if (ezone != null) {
                try {
                    int state = Integer.parseInt(split[4]);
                    if (state == 1) {
                        int id2 = plugin.singlestatedoorhelper.findDoor(split[2].toLowerCase(), player.getName(), player.getWorld().getName());
                        if (id2 != -1) {
                            if (split[3].equalsIgnoreCase("toggle") || split[3].equalsIgnoreCase("open") || split[3].equalsIgnoreCase("close")) {
                                int linkIndex = plugin.zonehelper.findLink(ezone, split[2].toLowerCase(), player.getName(), "ONESTATE");
                                if (linkIndex == -1) {
                                    ezone.links.add(new Link(split[2].toLowerCase(), player.getName(), split[3].toLowerCase(), "ONESTATE"));
                                    dataWriter.saveDatabase();
                                    player.sendMessage("Added Living Entities zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                                else {
                                    if (split[3].equalsIgnoreCase("TOGGLE") || split[3].equalsIgnoreCase("T"))
                                        ezone.links.get(linkIndex).linkType = Types.TOGGLE;
                                    else if (split[3].equalsIgnoreCase("ON") || split[3].equalsIgnoreCase("OPEN"))
                                        ezone.links.get(linkIndex).linkType = Types.OPEN;
                                    else if (split[3].equalsIgnoreCase("OFF") || split[3].equalsIgnoreCase("CLOSE"))
                                        ezone.links.get(linkIndex).linkType = Types.CLOSE;
                                    else {
                                        player.sendMessage(ChatColor.RED + "Not a valid link type: " + ChatColor.WHITE + "OPEN, CLOSE, TOGGLE");
                                        return false;
                                    }
                                    ezone.links.get(linkIndex).link_name = split[2].toLowerCase();
                                    ezone.links.get(linkIndex).link_creator = player.getName();
                                    ezone.links.get(linkIndex).doorType = "ONESTATE";
                                    dataWriter.saveDatabase();
                                    player.sendMessage("Updated Living Entities zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                            }
                            else {
                                player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <triggername> <doorname> <type> <doorType>");
                                player.sendMessage(ChatColor.BLUE + "Ex: /dlink ezone triggername doorname toggle 1");
                                return true;
                            }
                        }
                        else {
                            player.sendMessage(ChatColor.RED + "The door - " + ChatColor.WHITE + split[2] + ChatColor.RED + " was not found");
                            return true;
                        }
                    }
                    else if (state == 2) {
                        int id2 = plugin.twostatedoorhelper.findDoor(split[2].toLowerCase(), player.getName(), player.getWorld().getName());
                        if (id2 != -1) {
                            if (split[3].equalsIgnoreCase("toggle") || split[3].equalsIgnoreCase("open") || split[3].equalsIgnoreCase("close")) {
                                int linkIndex = plugin.zonehelper.findLink(ezone, split[2].toLowerCase(), player.getName(), "TWOSTATE");
                                if (linkIndex == -1) {
                                    ezone.links.add(new Link(split[2].toLowerCase(), player.getName(), split[3].toLowerCase(), "TWOSTATE"));
                                    dataWriter.saveDatabase();
                                    player.sendMessage("Added Living Entities zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - TwoState - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                                else {
                                    if (split[3].equalsIgnoreCase("TOGGLE") || split[3].equalsIgnoreCase("T"))
                                        ezone.links.get(linkIndex).linkType = Types.TOGGLE;
                                    else if (split[3].equalsIgnoreCase("ON") || split[3].equalsIgnoreCase("OPEN"))
                                        ezone.links.get(linkIndex).linkType = Types.OPEN;
                                    else if (split[3].equalsIgnoreCase("OFF") || split[3].equalsIgnoreCase("CLOSE"))
                                        ezone.links.get(linkIndex).linkType = Types.CLOSE;
                                    else {
                                        player.sendMessage(ChatColor.RED + "Not a valid link type: " + ChatColor.WHITE + "OPEN, CLOSE, TOGGLE");
                                        return false;
                                    }
                                    ezone.links.get(linkIndex).link_name = split[2].toLowerCase();
                                    ezone.links.get(linkIndex).link_creator = player.getName();
                                    ezone.links.get(linkIndex).doorType = "TWOSTATE";
                                    dataWriter.saveDatabase();
                                    player.sendMessage("Updated Living Entities zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                            }
                            else {
                                player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <triggername> <doorname> <type> <doorType>");
                                player.sendMessage(ChatColor.BLUE + "Ex: /dlink ezone triggername doorname toggle 1");
                                return true;
                            }
                        }
                        else {
                            player.sendMessage(ChatColor.RED + "The door - " + ChatColor.WHITE + split[2] + ChatColor.RED + " was not found");
                            return true;
                        }
                    }
                    else if (state == 3) {
                        int id3 = plugin.hdoorhelper.findDoor(split[2].toLowerCase(), player.getName(), player.getWorld().getName());
                        if (id3 != -1) {
                            if (split[3].equalsIgnoreCase("toggle") || split[3].equalsIgnoreCase("open") || split[3].equalsIgnoreCase("close")) {
                                int linkIndex = plugin.zonehelper.findLink(ezone, split[2].toLowerCase(), player.getName(), "HYBRIDSTATE");
                                if (linkIndex == -1) {
                                    ezone.links.add(new Link(split[2].toLowerCase(), player.getName(), split[3].toLowerCase(), "HYBRIDSTATE"));
                                    dataWriter.saveDatabase();
                                    player.sendMessage("Added Living Entities zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - HybridState - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                                else {
                                    if (split[3].equalsIgnoreCase("TOGGLE") || split[3].equalsIgnoreCase("T"))
                                        ezone.links.get(linkIndex).linkType = Types.TOGGLE;
                                    else if (split[3].equalsIgnoreCase("ON") || split[3].equalsIgnoreCase("OPEN"))
                                        ezone.links.get(linkIndex).linkType = Types.OPEN;
                                    else if (split[3].equalsIgnoreCase("OFF") || split[3].equalsIgnoreCase("CLOSE"))
                                        ezone.links.get(linkIndex).linkType = Types.CLOSE;
                                    else {
                                        player.sendMessage(ChatColor.RED + "Not a valid link type: " + ChatColor.WHITE + "OPEN, CLOSE, TOGGLE");
                                        return false;
                                    }
                                    ezone.links.get(linkIndex).link_name = split[2].toLowerCase();
                                    ezone.links.get(linkIndex).link_creator = player.getName();
                                    ezone.links.get(linkIndex).doorType = "HYBRIDSTATE";
                                    dataWriter.saveDatabase();
                                    player.sendMessage("Updated Living Entities zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                            }
                            else {
                                player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <triggername> <doorname> <type> <doorType>");
                                player.sendMessage(ChatColor.BLUE + "Ex: /dlink ezone triggername doorname toggle 1");
                                return true;
                            }
                        }
                        else {
                            player.sendMessage(ChatColor.RED + "The Hybrid door - " + ChatColor.WHITE + split[2] + ChatColor.RED + " was not found");
                            return true;
                        }
                    }
                } catch (Exception e) {
                    player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <triggername> <doorname> <type> <doorType>");
                    player.sendMessage(ChatColor.BLUE + "Ex: /dlink ezone triggername doorname toggle 1");
                    return true;
                }
            }
            else {
                player.sendMessage(ChatColor.RED + "The Living Entities zone - " + ChatColor.WHITE + split[1] + ChatColor.RED + " was not found");
                return true;
            }
        }
        else if (settings.friendlyCommand.equals("singleState-ezone")) {
            Zone ezone = plugin.zonehelper.findZone("EZONE", split[1].toLowerCase(), player.getName(), player.getWorld().getName());
            if (ezone != null) {
                int id2 = plugin.singlestatedoorhelper.findDoor(split[2].toLowerCase(), player.getName(), player.getWorld().getName());
                if (id2 != -1) {
                    if (split[3].equalsIgnoreCase("toggle") || split[3].equalsIgnoreCase("open") || split[3].equalsIgnoreCase("close")) {
                        int linkIndex = plugin.zonehelper.findLink(ezone, split[2].toLowerCase(), player.getName(), "ONESTATE");
                        if (linkIndex == -1) {
                            ezone.links.add(new Link(split[2].toLowerCase(), player.getName(), split[3].toLowerCase(), "ONESTATE"));
                            dataWriter.saveDatabase();
                            player.sendMessage("Added Living Entities zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                            return true;
                        }
                        else {
                            if (split[3].equalsIgnoreCase("TOGGLE") || split[3].equalsIgnoreCase("T"))
                                ezone.links.get(linkIndex).linkType = Types.TOGGLE;
                            else if (split[3].equalsIgnoreCase("ON") || split[3].equalsIgnoreCase("OPEN"))
                                ezone.links.get(linkIndex).linkType = Types.OPEN;
                            else if (split[3].equalsIgnoreCase("OFF") || split[3].equalsIgnoreCase("CLOSE"))
                                ezone.links.get(linkIndex).linkType = Types.CLOSE;
                            else {
                                player.sendMessage(ChatColor.RED + "Not a valid link type: " + ChatColor.WHITE + "OPEN, CLOSE, TOGGLE");
                                return false;
                            }
                            ezone.links.get(linkIndex).link_name = split[2].toLowerCase();
                            ezone.links.get(linkIndex).link_creator = player.getName();
                            ezone.links.get(linkIndex).doorType = "ONESTATE";
                            dataWriter.saveDatabase();
                            player.sendMessage("Updated Living Entities zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                            return true;
                        }
                    }
                    else {
                        player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <triggername> <doorname> <type> <doorType>");
                        player.sendMessage(ChatColor.BLUE + "Ex: /dlink ezone triggername doorname toggle 1");
                        return true;
                    }
                }
                else {
                    player.sendMessage(ChatColor.RED + "The door - " + ChatColor.WHITE + split[2] + ChatColor.RED + " was not found");
                    return true;
                }
            }
            else {
                player.sendMessage(ChatColor.RED + "The Living Entities zone - " + ChatColor.WHITE + split[1] + ChatColor.RED + " entered was not found");
                return true;
            }
        }
        else if (settings.friendlyCommand.equals("stateSelected-mzone")) {
            Zone mzone = plugin.zonehelper.findZone("MZONE", split[1].toLowerCase(), player.getName(), player.getWorld().getName());
            if (mzone != null) {
                try {
                    int state = Integer.parseInt(split[4]);
                    if (state == 1) {
                        int id2 = plugin.singlestatedoorhelper.findDoor(split[2].toLowerCase(), player.getName(), player.getWorld().getName());
                        if (id2 != -1) {
                            if (split[3].equalsIgnoreCase("toggle") || split[3].equalsIgnoreCase("open") || split[3].equalsIgnoreCase("close")) {
                                int linkIndex = plugin.zonehelper.findLink(mzone, split[2].toLowerCase(), player.getName(), "ONESTATE");
                                if (linkIndex == -1) {
                                    mzone.links.add(new Link(split[2].toLowerCase(), player.getName(), split[3].toLowerCase(), "ONESTATE"));
                                    dataWriter.saveDatabase();
                                    player.sendMessage("Added All Mob zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                                else {
                                    if (split[3].equalsIgnoreCase("TOGGLE") || split[3].equalsIgnoreCase("T"))
                                        mzone.links.get(linkIndex).linkType = Types.TOGGLE;
                                    else if (split[3].equalsIgnoreCase("ON") || split[3].equalsIgnoreCase("OPEN"))
                                        mzone.links.get(linkIndex).linkType = Types.OPEN;
                                    else if (split[3].equalsIgnoreCase("OFF") || split[3].equalsIgnoreCase("CLOSE"))
                                        mzone.links.get(linkIndex).linkType = Types.CLOSE;
                                    else {
                                        player.sendMessage(ChatColor.RED + "Not a valid link type: " + ChatColor.WHITE + "OPEN, CLOSE, TOGGLE");
                                        return false;
                                    }
                                    mzone.links.get(linkIndex).link_name = split[2].toLowerCase();
                                    mzone.links.get(linkIndex).link_creator = player.getName();
                                    mzone.links.get(linkIndex).doorType = "ONESTATE";
                                    dataWriter.saveDatabase();
                                    player.sendMessage("Updated All Mob zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                            }
                            else {
                                player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <triggername> <doorname> <type> <doorType>");
                                player.sendMessage(ChatColor.BLUE + "Ex: /dlink mzone triggername doorname toggle 1");
                                return true;
                            }
                        }
                        else {
                            player.sendMessage(ChatColor.RED + "The door - " + ChatColor.WHITE + split[2] + ChatColor.RED + " was not found");
                            return true;
                        }
                    }
                    else if (state == 2) {
                        int id2 = plugin.twostatedoorhelper.findDoor(split[2].toLowerCase(), player.getName(), player.getWorld().getName());
                        if (id2 != -1) {
                            if (split[3].equalsIgnoreCase("toggle") || split[3].equalsIgnoreCase("open") || split[3].equalsIgnoreCase("close")) {
                                int linkIndex = plugin.zonehelper.findLink(mzone, split[2].toLowerCase(), player.getName(), "TWOSTATE");
                                if (linkIndex == -1) {
                                    mzone.links.add(new Link(split[2].toLowerCase(), player.getName(), split[3].toLowerCase(), "TWOSTATE"));
                                    dataWriter.saveDatabase();
                                    player.sendMessage("Added All Mob zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - TwoState - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                                else {
                                    if (split[3].equalsIgnoreCase("TOGGLE") || split[3].equalsIgnoreCase("T"))
                                        mzone.links.get(linkIndex).linkType = Types.TOGGLE;
                                    else if (split[3].equalsIgnoreCase("ON") || split[3].equalsIgnoreCase("OPEN"))
                                        mzone.links.get(linkIndex).linkType = Types.OPEN;
                                    else if (split[3].equalsIgnoreCase("OFF") || split[3].equalsIgnoreCase("CLOSE"))
                                        mzone.links.get(linkIndex).linkType = Types.CLOSE;
                                    else {
                                        player.sendMessage(ChatColor.RED + "Not a valid link type: " + ChatColor.WHITE + "OPEN, CLOSE, TOGGLE");
                                        return false;
                                    }
                                    mzone.links.get(linkIndex).link_name = split[2].toLowerCase();
                                    mzone.links.get(linkIndex).link_creator = player.getName();
                                    mzone.links.get(linkIndex).doorType = "TWOSTATE";
                                    dataWriter.saveDatabase();
                                    player.sendMessage("Updated All Mob zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                            }
                            else {
                                player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <triggername> <doorname> <type> <doorType>");
                                player.sendMessage(ChatColor.BLUE + "Ex: /dlink mzone triggername doorname toggle 1");
                                return true;
                            }
                        }
                        else {
                            player.sendMessage(ChatColor.RED + "The door - " + ChatColor.WHITE + split[2] + ChatColor.RED + " was not found");
                            return true;
                        }
                    }
                    else if (state == 3) {
                        int id3 = plugin.hdoorhelper.findDoor(split[2].toLowerCase(), player.getName(), player.getWorld().getName());
                        if (id3 != -1) {
                            if (split[3].equalsIgnoreCase("toggle") || split[3].equalsIgnoreCase("open") || split[3].equalsIgnoreCase("close")) {
                                int linkIndex = plugin.zonehelper.findLink(mzone, split[2].toLowerCase(), player.getName(), "HYBRIDSTATE");
                                if (linkIndex == -1) {
                                    mzone.links.add(new Link(split[2].toLowerCase(), player.getName(), split[3].toLowerCase(), "HYBRIDSTATE"));
                                    dataWriter.saveDatabase();
                                    player.sendMessage("Added All Mob zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - HybridState - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                                else {
                                    if (split[3].equalsIgnoreCase("TOGGLE") || split[3].equalsIgnoreCase("T"))
                                        mzone.links.get(linkIndex).linkType = Types.TOGGLE;
                                    else if (split[3].equalsIgnoreCase("ON") || split[3].equalsIgnoreCase("OPEN"))
                                        mzone.links.get(linkIndex).linkType = Types.OPEN;
                                    else if (split[3].equalsIgnoreCase("OFF") || split[3].equalsIgnoreCase("CLOSE"))
                                        mzone.links.get(linkIndex).linkType = Types.CLOSE;
                                    else {
                                        player.sendMessage(ChatColor.RED + "Not a valid link type: " + ChatColor.WHITE + "OPEN, CLOSE, TOGGLE");
                                        return false;
                                    }
                                    mzone.links.get(linkIndex).link_name = split[2].toLowerCase();
                                    mzone.links.get(linkIndex).link_creator = player.getName();
                                    mzone.links.get(linkIndex).doorType = "HYBRIDSTATE";
                                    dataWriter.saveDatabase();
                                    player.sendMessage("Updated All Mob zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                            }
                            else {
                                player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <triggername> <doorname> <type> <doorType>");
                                player.sendMessage(ChatColor.BLUE + "Ex: /dlink mzone triggername doorname toggle 1");
                                return true;
                            }
                        }
                        else {
                            player.sendMessage(ChatColor.RED + "The Hybrid door - " + ChatColor.WHITE + split[2] + ChatColor.RED + " was not found");
                            return true;
                        }
                    }
                } catch (Exception e) {
                    player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <triggername> <doorname> <type> <doorType>");
                    player.sendMessage(ChatColor.BLUE + "Ex: /dlink mzone triggername doorname toggle 1");
                    return true;
                }
            }
            else {
                player.sendMessage(ChatColor.RED + "The All Mob zone - " + ChatColor.WHITE + split[1] + ChatColor.RED + " was not found");
                return true;
            }
        }
        else if (settings.friendlyCommand.equals("singleState-mzone")) {
            Zone mzone = plugin.zonehelper.findZone("MZONE", split[1].toLowerCase(), player.getName(), player.getWorld().getName());
            if (mzone != null) {
                int id2 = plugin.singlestatedoorhelper.findDoor(split[2].toLowerCase(), player.getName(), player.getWorld().getName());
                if (id2 != -1) {
                    if (split[3].equalsIgnoreCase("toggle") || split[3].equalsIgnoreCase("open") || split[3].equalsIgnoreCase("close")) {
                        int linkIndex = plugin.zonehelper.findLink(mzone, split[2].toLowerCase(), player.getName(), "ONESTATE");
                        if (linkIndex == -1) {
                            mzone.links.add(new Link(split[2].toLowerCase(), player.getName(), split[3].toLowerCase(), "ONESTATE"));
                            dataWriter.saveDatabase();
                            player.sendMessage("Added All Mob zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                            return true;
                        }
                        else {
                            if (split[3].equalsIgnoreCase("TOGGLE") || split[3].equalsIgnoreCase("T"))
                                mzone.links.get(linkIndex).linkType = Types.TOGGLE;
                            else if (split[3].equalsIgnoreCase("ON") || split[3].equalsIgnoreCase("OPEN"))
                                mzone.links.get(linkIndex).linkType = Types.OPEN;
                            else if (split[3].equalsIgnoreCase("OFF") || split[3].equalsIgnoreCase("CLOSE"))
                                mzone.links.get(linkIndex).linkType = Types.CLOSE;
                            else {
                                player.sendMessage(ChatColor.RED + "Not a valid link type: " + ChatColor.WHITE + "OPEN, CLOSE, TOGGLE");
                                return false;
                            }
                            mzone.links.get(linkIndex).link_name = split[2].toLowerCase();
                            mzone.links.get(linkIndex).link_creator = player.getName();
                            mzone.links.get(linkIndex).doorType = "ONESTATE";
                            dataWriter.saveDatabase();
                            player.sendMessage("Updated All Mob zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                            return true;
                        }
                    }
                    else {
                        player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <triggername> <doorname> <type> <doorType>");
                        player.sendMessage(ChatColor.BLUE + "Ex: /dlink mzone triggername doorname toggle 1");
                        return true;
                    }
                }
                else {
                    player.sendMessage(ChatColor.RED + "The door - " + ChatColor.WHITE + split[2] + ChatColor.RED + " was not found");
                    return true;
                }
            }
            else {
                player.sendMessage(ChatColor.RED + "The All Mob zone - " + ChatColor.WHITE + split[1] + ChatColor.RED + " was not found");
                return true;
            }
        }
        else if (settings.friendlyCommand.equals("stateSelected-pzone")) {
            Zone pzone = plugin.zonehelper.findZone("PZONE", split[1].toLowerCase(), player.getName(), player.getWorld().getName());
            if (pzone != null) {
                try {
                    int state = Integer.parseInt(split[4]);
                    if (state == 1) {
                        int id2 = plugin.singlestatedoorhelper.findDoor(split[2].toLowerCase(), player.getName(), player.getWorld().getName());
                        if (id2 != -1) {
                            if (split[3].equalsIgnoreCase("toggle") || split[3].equalsIgnoreCase("open") || split[3].equalsIgnoreCase("close")) {
                                int linkIndex = plugin.zonehelper.findLink(pzone, split[2].toLowerCase(), player.getName(), "ONESTATE");
                                if (linkIndex == -1) {
                                    pzone.links.add(new Link(split[2].toLowerCase(), player.getName(), split[3].toLowerCase(), "ONESTATE"));
                                    dataWriter.saveDatabase();
                                    player.sendMessage("Added Passive Mob zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                                else {
                                    if (split[3].equalsIgnoreCase("TOGGLE") || split[3].equalsIgnoreCase("T"))
                                        pzone.links.get(linkIndex).linkType = Types.TOGGLE;
                                    else if (split[3].equalsIgnoreCase("ON") || split[3].equalsIgnoreCase("OPEN"))
                                        pzone.links.get(linkIndex).linkType = Types.OPEN;
                                    else if (split[3].equalsIgnoreCase("OFF") || split[3].equalsIgnoreCase("CLOSE"))
                                        pzone.links.get(linkIndex).linkType = Types.CLOSE;
                                    else {
                                        player.sendMessage(ChatColor.RED + "Not a valid link type: " + ChatColor.WHITE + "OPEN, CLOSE, TOGGLE");
                                        return false;
                                    }
                                    pzone.links.get(linkIndex).link_name = split[2].toLowerCase();
                                    pzone.links.get(linkIndex).link_creator = player.getName();
                                    pzone.links.get(linkIndex).doorType = "ONESTATE";
                                    dataWriter.saveDatabase();
                                    player.sendMessage("Updated Passive Mob zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                            }
                            else {
                                player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <triggername> <doorname> <type> <doorType>");
                                player.sendMessage(ChatColor.BLUE + "Ex: /dlink mzone triggername doorname toggle 1");
                                return true;
                            }
                        }
                        else {
                            player.sendMessage(ChatColor.RED + "The door - " + ChatColor.WHITE + split[2] + ChatColor.RED + " was not found");
                            return true;
                        }
                    }
                    else if (state == 2) {
                        int id2 = plugin.twostatedoorhelper.findDoor(split[2].toLowerCase(), player.getName(), player.getWorld().getName());
                        if (id2 != -1) {
                            if (split[3].equalsIgnoreCase("toggle") || split[3].equalsIgnoreCase("open") || split[3].equalsIgnoreCase("close")) {
                                int linkIndex = plugin.zonehelper.findLink(pzone, split[2].toLowerCase(), player.getName(), "TWOSTATE");
                                if (linkIndex == -1) {
                                    pzone.links.add(new Link(split[2].toLowerCase(), player.getName(), split[3].toLowerCase(), "TWOSTATE"));
                                    dataWriter.saveDatabase();
                                    player.sendMessage("Added Passive Mob zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - TwoState - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                                else {
                                    if (split[3].equalsIgnoreCase("TOGGLE") || split[3].equalsIgnoreCase("T"))
                                        pzone.links.get(linkIndex).linkType = Types.TOGGLE;
                                    else if (split[3].equalsIgnoreCase("ON") || split[3].equalsIgnoreCase("OPEN"))
                                        pzone.links.get(linkIndex).linkType = Types.OPEN;
                                    else if (split[3].equalsIgnoreCase("OFF") || split[3].equalsIgnoreCase("CLOSE"))
                                        pzone.links.get(linkIndex).linkType = Types.CLOSE;
                                    else {
                                        player.sendMessage(ChatColor.RED + "Not a valid link type: " + ChatColor.WHITE + "OPEN, CLOSE, TOGGLE");
                                        return false;
                                    }
                                    pzone.links.get(linkIndex).link_name = split[2].toLowerCase();
                                    pzone.links.get(linkIndex).link_creator = player.getName();
                                    pzone.links.get(linkIndex).doorType = "TWOSTATE";
                                    dataWriter.saveDatabase();
                                    player.sendMessage("Updated Passive Mob zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                            }
                            else {
                                player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <triggername> <doorname> <type> <doorType>");
                                player.sendMessage(ChatColor.BLUE + "Ex: /dlink mzone triggername doorname toggle 1");
                                return true;
                            }
                        }
                        else {
                            player.sendMessage(ChatColor.RED + "The door - " + ChatColor.WHITE + split[2] + ChatColor.RED + " was not found");
                            return true;
                        }
                    }
                    else if (state == 3) {
                        int id3 = plugin.hdoorhelper.findDoor(split[2].toLowerCase(), player.getName(), player.getWorld().getName());
                        if (id3 != -1) {
                            if (split[3].equalsIgnoreCase("toggle") || split[3].equalsIgnoreCase("open") || split[3].equalsIgnoreCase("close")) {
                                int linkIndex = plugin.zonehelper.findLink(pzone, split[2].toLowerCase(), player.getName(), "HYBRIDSTATE");
                                if (linkIndex == -1) {
                                    pzone.links.add(new Link(split[2].toLowerCase(), player.getName(), split[3].toLowerCase(), "HYBRIDSTATE"));
                                    dataWriter.saveDatabase();
                                    player.sendMessage("Added Passive Mob zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - HybridState - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                                else {
                                    if (split[3].equalsIgnoreCase("TOGGLE") || split[3].equalsIgnoreCase("T"))
                                        pzone.links.get(linkIndex).linkType = Types.TOGGLE;
                                    else if (split[3].equalsIgnoreCase("ON") || split[3].equalsIgnoreCase("OPEN"))
                                        pzone.links.get(linkIndex).linkType = Types.OPEN;
                                    else if (split[3].equalsIgnoreCase("OFF") || split[3].equalsIgnoreCase("CLOSE"))
                                        pzone.links.get(linkIndex).linkType = Types.CLOSE;
                                    else {
                                        player.sendMessage(ChatColor.RED + "Not a valid link type: " + ChatColor.WHITE + "OPEN, CLOSE, TOGGLE");
                                        return false;
                                    }
                                    pzone.links.get(linkIndex).link_name = split[2].toLowerCase();
                                    pzone.links.get(linkIndex).link_creator = player.getName();
                                    pzone.links.get(linkIndex).doorType = "HYBRIDSTATE";
                                    dataWriter.saveDatabase();
                                    player.sendMessage("Updated Passive Mob zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                            }
                            else {
                                player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <triggername> <doorname> <type> <doorType>");
                                player.sendMessage(ChatColor.BLUE + "Ex: /dlink mzone triggername doorname toggle 1");
                                return true;
                            }
                        }
                        else {
                            player.sendMessage(ChatColor.RED + "The Hybrid door - " + ChatColor.WHITE + split[2] + ChatColor.RED + " was not found");
                            return true;
                        }
                    }
                } catch (Exception e) {
                    player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <triggername> <doorname> <type> <doorType>");
                    player.sendMessage(ChatColor.BLUE + "Ex: /dlink mzone triggername doorname toggle 1");
                    return true;
                }
            }
            else {
                player.sendMessage(ChatColor.RED + "The Passive Mob zone - " + ChatColor.WHITE + split[1] + ChatColor.RED + " was not found");
                return true;
            }
        }
        else if (settings.friendlyCommand.equals("singleState-pzone")) {
            Zone pzone = plugin.zonehelper.findZone("PZONE", split[1].toLowerCase(), player.getName(), player.getWorld().getName());
            if (pzone != null) {
                int id2 = plugin.singlestatedoorhelper.findDoor(split[2].toLowerCase(), player.getName(), player.getWorld().getName());
                if (id2 != -1) {
                    if (split[3].equalsIgnoreCase("toggle") || split[3].equalsIgnoreCase("open") || split[3].equalsIgnoreCase("close")) {
                        int linkIndex = plugin.zonehelper.findLink(pzone, split[2].toLowerCase(), player.getName(), "ONESTATE");
                        if (linkIndex == -1) {
                            pzone.links.add(new Link(split[2].toLowerCase(), player.getName(), split[3].toLowerCase(), "ONESTATE"));
                            dataWriter.saveDatabase();
                            player.sendMessage("Added Passive Mob zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                            return true;
                        }
                        else {
                            if (split[3].equalsIgnoreCase("TOGGLE") || split[3].equalsIgnoreCase("T"))
                                pzone.links.get(linkIndex).linkType = Types.TOGGLE;
                            else if (split[3].equalsIgnoreCase("ON") || split[3].equalsIgnoreCase("OPEN"))
                                pzone.links.get(linkIndex).linkType = Types.OPEN;
                            else if (split[3].equalsIgnoreCase("OFF") || split[3].equalsIgnoreCase("CLOSE"))
                                pzone.links.get(linkIndex).linkType = Types.CLOSE;
                            else {
                                player.sendMessage(ChatColor.RED + "Not a valid link type: " + ChatColor.WHITE + "OPEN, CLOSE, TOGGLE");
                                return false;
                            }
                            pzone.links.get(linkIndex).link_name = split[2].toLowerCase();
                            pzone.links.get(linkIndex).link_creator = player.getName();
                            pzone.links.get(linkIndex).doorType = "ONESTATE";
                            dataWriter.saveDatabase();
                            player.sendMessage("Updated Passive Mob zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                            return true;
                        }
                    }
                    else {
                        player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <triggername> <doorname> <type> <doorType>");
                        player.sendMessage(ChatColor.BLUE + "Ex: /dlink mzone triggername doorname toggle 1");
                        return true;
                    }
                }
                else {
                    player.sendMessage(ChatColor.RED + "The door - " + ChatColor.WHITE + split[2] + ChatColor.RED + " was not found");
                    return true;
                }
            }
            else {
                player.sendMessage(ChatColor.RED + "The Passive Mob zone - " + ChatColor.WHITE + split[1] + ChatColor.RED + " entered was not found");
                return true;
            }
        }
        return false;
    }
}