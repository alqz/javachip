package momentum.buffer;

import clojure.lang.*;
import java.io.IOException;
import java.nio.channels.WritableByteChannel;

final class TransientBufferBackedBuffer extends TransientBuffer {

  final TransientBuffer buf;

  final int off;

  TransientBufferBackedBuffer(TransientBuffer b) {
    this(b, 0, b.count);
  }

  TransientBufferBackedBuffer(TransientBuffer b, int o, int l) {
    super(l, b.capacity - o);

    buf = b;
    off = o;
  }

  /*
   * ===== Buffer API ======
   */

  protected final byte _read(int idx) {
    return buf._read(off + idx);
  }

  protected TransientBuffer _write(int idx, byte b) {
    buf._write(off + idx, b);
    return this;
  }

  protected TransientBuffer _slice(int idx, int len) {
    TransientBuffer ret = buf._slice(off + idx, len);

    if (copyOnPersistent)
      ret.copyOnPersistent();

    return ret;
  }

  protected PersistentBuffer _persistent() {
    return new PersistentBufferBackedBuffer(buf.persistent());
  }

  protected int _transferTo(WritableByteChannel chan, int idx, int len) throws IOException {
    return buf._transferTo(chan, off + idx, len);
  }

}
