/*******************************************************************************
 * Copyright (c) 2012 EclipseSource and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    EclipseSource - initial API and implementation
 ******************************************************************************/
package com.eclipsesource.tabris.internal;


public class Constants {
  
  public static final String USER_AGENT = "User-Agent";
  public static final String ID_ANDROID = "com.eclipsesource.rap.mobile.client.android";
  public static final String ID_IOS = "com.eclipsesource.rap.mobile.client.ios";
  
  private Constants() {
    // prevent instantiation
  }
  
}
