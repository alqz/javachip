
package qse_sepm_ss12_07.service;

import java.util.ArrayList;
import java.util.List;
import junit.framework.Assert;
import org.junit.Test;
import static org.mockito.Mockito.*;
import qse_sepm_ss12_07.dao.DaoException;
import qse_sepm_ss12_07.dao.ITagDao;
import qse_sepm_ss12_07.dao.TagEntity;
import qse_sepm_ss12_07.domain.*;

/**
 * Unit tests for the taggingService
 */
public class TaggingServiceUnitTest 
{	
	@Test
	public void testGetTagsForProduct() throws DaoException, ServiceException
	{
		ITagDao tagDao = mock(ITagDao.class);
		TaggingService taggingService = new TaggingService();
		taggingService.setTagDao(tagDao);			
		Product product = new Product();
		
		List<TagEntity> tagEntitys = new ArrayList<TagEntity>();
		tagEntitys.add(new TagEntity());
		tagEntitys.add(new TagEntity());
		
		when(tagDao.getTags(product.getEntity())).thenReturn(tagEntitys);
		
		List<Tag> tags = taggingService.getTags(product);
		
		Assert.assertEquals(2, tags.size());
		for(Tag tag : tags)
		{
			Assert.assertTrue(tag instanceof ProductTag);
			Assert.assertTrue(tagEntitys.contains(tag.getEntity()));
		}
	}
	
	@Test
	public void testGetTagsForProductCategory() throws DaoException, ServiceException
	{
		ITagDao tagDao = mock(ITagDao.class);
		TaggingService taggingService = new TaggingService();
		taggingService.setTagDao(tagDao);			
		ProductCategory productCategory = new ProductCategory();
		productCategory.setName("testCategory");
		productCategory.setMinimumStockAmount(3.0);
		productCategory.getEntity().setUnitId(0);
		productCategory.setImageFileName("bla");
		productCategory.getEntity().setIsDeleted(false);
		
		List<TagEntity> tagEntitys = new ArrayList<TagEntity>();
		tagEntitys.add(new TagEntity());
		tagEntitys.add(new TagEntity());
		
		when(tagDao.getTags(productCategory.getEntity())).thenReturn(tagEntitys);
		
		List<Tag> tags = taggingService.getTags(productCategory);
		
		Assert.assertEquals(2, tags.size());
		for(Tag tag : tags)
		{
			Assert.assertTrue(tag instanceof ProductCategoryTag);
			Assert.assertTrue(tagEntitys.contains(tag.getEntity()));
		}
	}
	
	@Test
	public void testGetTagsForRecipe() throws DaoException, ServiceException
	{
		ITagDao tagDao = mock(ITagDao.class);
		TaggingService taggingService = new TaggingService();
		taggingService.setTagDao(tagDao);			
		Recipe recipe = new Recipe();
		
		List<TagEntity> tagEntitys = new ArrayList<TagEntity>();
		tagEntitys.add(new TagEntity());
		tagEntitys.add(new TagEntity());
		
		when(tagDao.getTags(recipe.getRecipeEntity())).thenReturn(tagEntitys);
		
		List<Tag> tags = taggingService.getTags(recipe);
		
		Assert.assertEquals(2, tags.size());
		for(Tag tag : tags)
		{
			Assert.assertTrue(tag instanceof RecipeTag);
			Assert.assertTrue(tagEntitys.contains(tag.getEntity()));
		}
	}
	
	@Test
	public void testSaveTagsForProduct() throws DaoException, ServiceException
	{
		ITagDao tagDao = mock(ITagDao.class);
		TaggingService taggingService = new TaggingService();
		taggingService.setTagDao(tagDao);
		
		Product product = new Product();
		
		ProductTag tag = new ProductTag();
		tag.setProduct(product);
		ProductTag tag2 = new ProductTag();
		tag2.setProduct(product);
		
		tag.getEntity().setId(1);
		
		taggingService.addTag(product, tag);
		taggingService.addTag(product, tag2);		
		
		verify(tagDao).update(tag.getEntity());
		verify(tagDao).create(tag2.getEntity());	
		verify(tagDao).link(tag.getEntity(), product.getEntity());
		verify(tagDao).link(tag2.getEntity(), product.getEntity());
	}
	
	@Test
	public void testSaveTagsForProductCategory() throws DaoException, ServiceException
	{
		ITagDao tagDao = mock(ITagDao.class);
		TaggingService taggingService = new TaggingService();
		taggingService.setTagDao(tagDao);
		
		ProductCategory productCategory = new ProductCategory();
		productCategory.setName("testCategory");
		productCategory.setMinimumStockAmount(3.0);
		productCategory.getEntity().setUnitId(0);
		productCategory.setImageFileName("bla");
		productCategory.getEntity().setIsDeleted(false);
		
		ProductCategoryTag tag = new ProductCategoryTag();
		tag.setProductCategory(productCategory);
		ProductCategoryTag tag2 = new ProductCategoryTag();
		tag2.setProductCategory(productCategory);
		
		tag.getEntity().setId(1);
						
		taggingService.addTag(productCategory, tag);
		taggingService.addTag(productCategory, tag2);
		
		verify(tagDao).update(tag.getEntity());
		verify(tagDao).create(tag2.getEntity());	
		verify(tagDao).link(tag.getEntity(), productCategory.getEntity());
		verify(tagDao).link(tag2.getEntity(), productCategory.getEntity());
	}
	
	@Test
	public void testSaveTagsForRecipe() throws DaoException, ServiceException
	{
		ITagDao tagDao = mock(ITagDao.class);
		TaggingService taggingService = new TaggingService();
		taggingService.setTagDao(tagDao);
		
		Recipe recipe = new Recipe();
		
		RecipeTag tag = new RecipeTag();
		tag.setRecipe(recipe);
		RecipeTag tag2 = new RecipeTag();
		tag2.setRecipe(recipe);
		
		tag.getEntity().setId(1);		
		
		taggingService.addTag(recipe, tag);
		taggingService.addTag(recipe, tag2);
		
		verify(tagDao).update(tag.getEntity());
		verify(tagDao).create(tag2.getEntity());	
		verify(tagDao).link(tag.getEntity(), recipe.getRecipeEntity());
		verify(tagDao).link(tag2.getEntity(), recipe.getRecipeEntity());
	}
}
