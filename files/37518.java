package com.alibaba.crm.app;

import org.junit.Before;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;

public class BaseInitAPP {

    public ApplicationContext ap = null;

    @Before
    public void setUp() {
        ClassPathResource res = new ClassPathResource("applicationContext.xml");
        
        ap = new ClassPathXmlApplicationContext("applicationContext.xml");
    }
}
