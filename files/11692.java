package com.remindthatmovie.util;

public class ResultNotIdentifyingException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = -870241781863709548L;

	public ResultNotIdentifyingException(){
		super("Result is not identifying");
	}
	
	public ResultNotIdentifyingException(String detailMessage){
		super(detailMessage);
	}
}
