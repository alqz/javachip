package qse_sepm_ss12_07.service;

import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import qse_sepm_ss12_07.dao.DaoException;
import qse_sepm_ss12_07.dao.IProductCategoryDao;
import qse_sepm_ss12_07.dao.ProductCategoryEntity;
import qse_sepm_ss12_07.domain.Product;
import qse_sepm_ss12_07.domain.ProductCategory;
import qse_sepm_ss12_07.domain.ProductListProductCategoryEntry;

public class ProductCategoryService implements IProductCategoryService
{
	private IProductCategoryDao productCategoryDao;
	private IProductService productService;
	private IUnitService unitService;
	private Logger logger = Logger.getLogger(ProductCategoryService.class);

	public void setProductCategoryDao(IProductCategoryDao productCategoryDao)
	{
		this.productCategoryDao = productCategoryDao;
	}

	public void setProductService(IProductService productService)
	{
		this.productService = productService;
	}
	
	public void setUnitService(IUnitService unitService)
	{
		this.unitService = unitService;
	}

	public void saveProductCategory(ProductCategory category) throws ServiceException
	{
		try 
		{
			if (category.getEntity().getId() != -1)
			{
				productCategoryDao.update(category.getEntity());
			}
			else
			{
				productCategoryDao.create(category.getEntity());
			}
		}
		catch(DaoException de)
		{
			logger.error("could not save product category", de);
			throw new ServiceException(de);
		}
	}

	public List<ProductCategory> getAllProductCategories() throws ServiceException
	{
		List<ProductCategoryEntity> productCategoryEntityList;
		
		try
		{
			productCategoryEntityList = productCategoryDao.findAll();
		}
		catch(DaoException de)
		{
			logger.error("could not get product categoroes", de);
			throw new ServiceException(de);
		}
		
		List<ProductCategory> productCategoryList = new ArrayList<ProductCategory>();
		
		for(ProductCategoryEntity productCategoryEntity : productCategoryEntityList)
		{
			ProductCategory newProductCategory = new ProductCategory(productCategoryEntity);
			newProductCategory.addRangeProducts(productService.getProductsByProductCategory(newProductCategory));
			newProductCategory.setUnit(unitService.getUnitById(newProductCategory.getEntity().getUnitId()));
			productCategoryList.add(newProductCategory);
		}
		
		return productCategoryList;
	}

	public ProductCategory getProductCategoryByProductListEntry(ProductListProductCategoryEntry entry) throws ServiceException
	{
		try
		{
			return new ProductCategory(productCategoryDao.find(entry.getEntity().getProductCategoryId()));
		}
		catch(DaoException exception)
		{
			logger.error("Could not load product category", exception);
			throw new ServiceException("Could not load product category", exception);
		}			
	}
	
	public void removeProductCategory(ProductCategory productCategory) throws ServiceException
	{
		try
		{
			for(Product product : productService.getProductsByProductCategory(productCategory))
			{
				productService.deleteProduct(product);
			}
			
			productCategoryDao.delete(productCategory.getEntity());
		}
		catch(DaoException exception)
		{
			logger.error("Could not delet product category", exception);
			throw new ServiceException("Could not load product category", exception);
		}			
	}
	
	public ProductCategory getProductCategoryById(int id) throws ServiceException
	{
		try
		{
			ProductCategory productCategory = new ProductCategory(productCategoryDao.find(id));
			productCategory.addRangeProducts(productService.getProductsByProductCategory(productCategory));
			productCategory.setUnit(unitService.getUnitById(productCategory.getEntity().getUnitId()));
			return productCategory;
		}
		catch (DaoException de)
		{
			logger.error("Could not load product category", de);
			throw new ServiceException("Could not load product category", de);
		}
	}

	public List<ProductCategory> getAllProductCategoriesByName(String name) throws ServiceException
	{List<ProductCategoryEntity> productCategoryEntityList;
		
		try
		{
			productCategoryEntityList = productCategoryDao.findAllByName(name);
		}
		catch(DaoException de)
		{
			logger.error("could not get product categoroes", de);
			throw new ServiceException(de);
		}
		
		List<ProductCategory> productCategoryList = new ArrayList<ProductCategory>();
		
		for(ProductCategoryEntity productCategoryEntity : productCategoryEntityList)
		{
			ProductCategory newProductCategory = new ProductCategory(productCategoryEntity);
			newProductCategory.addRangeProducts(productService.getProductsByProductCategory(newProductCategory));
			newProductCategory.setUnit(unitService.getUnitById(newProductCategory.getEntity().getUnitId()));
			productCategoryList.add(newProductCategory);
		}
		
		return productCategoryList;
	}
}
