package info.bytecraft.chat.listeners;

import info.bytecraft.chat.Channel;
import info.bytecraft.chat.Chat;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;

public class ChannelListener implements Listener {
	
	@EventHandler
	public void onChat(PlayerChatEvent event){
		event.getRecipients().clear();
		Channel channel = Chat.getChannel(event.getPlayer());
		if(channel == null){
			channel = new Channel();
			channel.setChannelName("global");
			channel.setPlayer(event.getPlayer());
			Chat.plugin.getDatabase().save(channel);
		}
		for(Player player: Channel.getPlayers(channel.getChannelName())){
			event.getRecipients().add(player);
		}
			if(channel.getChannelName().equalsIgnoreCase("global")){
				event.setFormat("<" + event.getPlayer().getDisplayName() + ChatColor.WHITE + "> " + event.getMessage());
			}else{
				event.setFormat(channel.getChannelName().toUpperCase() + " <" + event.getPlayer().getDisplayName() + ChatColor.WHITE + "> " + event.getMessage());
			}
	}
	
	@EventHandler
	public void onJoin(PlayerJoinEvent event){
		Channel channel = Chat.getChannel(event.getPlayer());
		if(channel == null){
			channel = new Channel();
			channel.setChannelName("global");
			channel.setPlayer(event.getPlayer());
			Chat.plugin.getDatabase().save(channel);
		}
	}
}
