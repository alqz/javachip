package qse_sepm_ss12_07.gui;

import java.awt.*;

import javax.swing.text.*;

public class IntegerDocument extends LimitedDocument {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * @param maxOffSet
	 */
	public IntegerDocument(int length) {
		super(length);
	}
	
	@Override
	public void insertString(int offset, String s, AttributeSet attributeSet)
			throws BadLocationException {
		try {
			Integer.parseInt(s);
		} catch (Exception ex) // only allow integer values
		{
			Toolkit.getDefaultToolkit().beep(); // macht ein DßT
			return;
		}
		super.insertString(offset, s, attributeSet);
	}
}
