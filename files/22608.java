/*
 * JBoss, Home of Professional Open Source.
 * Copyright 2009, Red Hat Middleware LLC, and individual contributors
 * as indicated by the @author tags. See the copyright.txt file in the
 * distribution for a full listing of individual contributors.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package org.jboss.managed.bean.mc.deployer;

import java.util.Collection;

import javax.naming.RefAddr;
import javax.naming.Reference;
import javax.naming.StringRefAddr;

import org.jboss.beans.metadata.api.annotations.Inject;
import org.jboss.beans.metadata.plugins.AbstractInjectionValueMetaData;
import org.jboss.beans.metadata.plugins.builder.BeanMetaDataBuilderFactory;
import org.jboss.beans.metadata.spi.BeanMetaData;
import org.jboss.beans.metadata.spi.builder.BeanMetaDataBuilder;
import org.jboss.deployers.spi.DeploymentException;
import org.jboss.deployers.spi.deployer.DeploymentStages;
import org.jboss.deployers.spi.deployer.helpers.AbstractDeployer;
import org.jboss.deployers.structure.spi.DeploymentUnit;
import org.jboss.managed.bean.mc.JNDIBinder;
import org.jboss.managed.bean.mc.util.ManagedBeanMCBeanUtil;
import org.jboss.managed.bean.metadata.ManagedBeanDeploymentMetaData;
import org.jboss.managed.bean.metadata.ManagedBeanMetaData;
import org.jboss.managed.bean.proxy.impl.ManagedBeanClassRefAddr;
import org.jboss.managed.bean.proxy.impl.ManagedBeanManagerRegistryRefAddr;
import org.jboss.managed.bean.proxy.impl.ManagedBeanProxyObjectFactory;
import org.jboss.managed.bean.proxy.impl.ManagedBeanProxyRefAddrType;
import org.jboss.managed.bean.spi.ManagedBeanManagerRegistry;
import org.jboss.reloaded.naming.deployers.javaee.JavaEEComponentInformer;

/**
 * ManagedBeanNamingDeployer
 *
 * @author Jaikiran Pai
 * @version $Revision: $
 */
public class ManagedBeanNamingDeployer extends AbstractDeployer
{
   /**
    * component informer
    */
   private JavaEEComponentInformer javaeeComponentInformer;
   
   private ManagedBeanManagerRegistry managedBeanManagerRegistry;

   public ManagedBeanNamingDeployer()
   {
      this.setStage(DeploymentStages.REAL);

      this.setInput(ManagedBeanDeploymentMetaData.class);
      
      this.setOutput(BeanMetaData.class);

   }

   @Inject
   public void setManagedBeanManagerRegistry(ManagedBeanManagerRegistry registry)
   {
      this.managedBeanManagerRegistry = registry;
   }
   
   @Inject
   public void setJavaEEComponentInformer(JavaEEComponentInformer componentInformer)
   {
      this.javaeeComponentInformer = componentInformer;
   }

   @Override
   public void deploy(DeploymentUnit unit) throws DeploymentException
   {
      ManagedBeanDeploymentMetaData managedBeanDeployment = unit.getAttachment(ManagedBeanDeploymentMetaData.class);

      Collection<ManagedBeanMetaData> managedBeans = managedBeanDeployment.getManagedBeans();
      if (managedBeans == null || managedBeans.isEmpty())
      {
         return;
      }

      for (ManagedBeanMetaData managedBean : managedBeans)
      {
         this.deploy(unit, managedBean);
      }

   }

   private void deploy(DeploymentUnit unit, ManagedBeanMetaData managedBean) throws DeploymentException
   {
      
      // create the ObjectFactory reference for JNDI
      String managedBeanManagerName = ManagedBeanMCBeanUtil.getManagedBeanManagerMCBeanName(this.javaeeComponentInformer, unit, managedBean);
      Reference reference = new Reference("Object Factory for managed bean", ManagedBeanProxyObjectFactory.class.getName(), null);
      // RefAddr for Managed bean manager name
      RefAddr managerNameRefAddr = new StringRefAddr(ManagedBeanProxyRefAddrType.MANAGER_NAME_REF_ADDR_TYPE, managedBeanManagerName);
      reference.add(managerNameRefAddr);
      // RefAddr for Managed bean class
      RefAddr managedBeanClassRefAddr = new ManagedBeanClassRefAddr(this.getManagedBeanClass(unit.getClassLoader(), managedBean));
      reference.add(managedBeanClassRefAddr);
      // RefAddr for Managed bean manager registry
      RefAddr managedBeanManagerRegistryRefAddr = new ManagedBeanManagerRegistryRefAddr(this.managedBeanManagerRegistry);
      reference.add(managedBeanManagerRegistryRefAddr);


      // Create and bind a JNDIBinder, which binds the ObjectFactory, in java:app namespace 
      String moduleName = this.javaeeComponentInformer.getModuleName(unit);
      // the jndi name
      String javaAppNamespaceJNDIName = moduleName + "/" + managedBean.getName();
      // create a JNDIBinder 
      JNDIBinder javaAppJNDIBinder = new JNDIBinder(javaAppNamespaceJNDIName, reference, true);
      String javaAppJNDIBinderName = ManagedBeanMCBeanUtil.getJNDIBinderMCBeanPrefix(unit) + ",jndiname=java:app/" + javaAppNamespaceJNDIName;
      BeanMetaDataBuilder javaAppJNDIBinderBuilder = BeanMetaDataBuilderFactory.createBuilder(javaAppJNDIBinderName, JNDIBinder.class.getName());
      javaAppJNDIBinderBuilder.setConstructorValue(javaAppJNDIBinder);
      AbstractInjectionValueMetaData javaeeModuleInjectMetaData = new AbstractInjectionValueMetaData(this.getJavaEEModuleMCBeanName(unit));
      javaAppJNDIBinderBuilder.addPropertyMetaData("javaEEModule", javaeeModuleInjectMetaData);
      // add the JNDIBinder to the Deployment unit
      unit.addAttachment(BeanMetaData.class + ":" + javaAppJNDIBinderName, javaAppJNDIBinderBuilder.getBeanMetaData(), BeanMetaData.class);
      
      
      // Create and bind a JNDIBinder, which binds the ObjectFactory, in java:module namespace
      String javaModuleNamespaceJNDIName = managedBean.getName();
      // create a JNDIBinder 
      JNDIBinder javaModuleJNDIBinder = new JNDIBinder(javaModuleNamespaceJNDIName, reference, false);
      String javaModuleJNDIBinderName = ManagedBeanMCBeanUtil.getJNDIBinderMCBeanPrefix(unit) + ",jndiname=java:module/" + javaModuleNamespaceJNDIName;
      BeanMetaDataBuilder javaModuleJNDIBinderBuilder = BeanMetaDataBuilderFactory.createBuilder(javaModuleJNDIBinderName, JNDIBinder.class.getName());
      javaModuleJNDIBinderBuilder.setConstructorValue(javaModuleJNDIBinder);
      javaModuleJNDIBinderBuilder.addPropertyMetaData("javaEEModule", javaeeModuleInjectMetaData);
      // add the JNDIBinder to the deployment unit
      unit.addAttachment(BeanMetaData.class + ":" + javaModuleJNDIBinderName, javaModuleJNDIBinderBuilder.getBeanMetaData(), BeanMetaData.class);
   
   }

   private String getJavaEEModuleMCBeanName(DeploymentUnit deploymentUnit)
   {
      String applicationName = this.javaeeComponentInformer.getApplicationName(deploymentUnit);
      String moduleName = this.javaeeComponentInformer.getModuleName(deploymentUnit);

      final StringBuilder builder = new StringBuilder("jboss.naming:");
      if (applicationName != null)
      {
         builder.append("application=").append(applicationName).append(",");
      }
      builder.append("module=").append(moduleName);
      return builder.toString();
   }

   private Class<?> getManagedBeanClass(ClassLoader cl, ManagedBeanMetaData managedBean) throws DeploymentException
   {
      String klass = managedBean.getManagedBeanClass();
      try
      {
         return cl.loadClass(klass);
      }
      catch (ClassNotFoundException cnfe)
      {
         throw new DeploymentException(cnfe);
      }
   }
   
}
