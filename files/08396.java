package ui;

import java.awt.BorderLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.WindowConstants;

import util.GameFont;
import util.ImageLoader;
import util.MusicPlayer;

public class MainFrame extends JFrame {

	private StartUpPanel startUpPanel;
	private GameProperPanel gameProperPanel;
	private JPanel contentPanel;
	private MainFrameEvent mainFrameEvent;

	public MainFrame() {
		setConfiguration();
		initComponents();
		showStartUpPanel();
	}

	private void setConfiguration() {
		super.setIconImage(Toolkit.getDefaultToolkit().getImage("resources/frame_icon.png"));
		setTitle("Four-Trap");
		setSize(800, 600);
		setResizable(false);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() {

			@Override
			public void windowClosing(WindowEvent e) {
				try {
					UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
					if (JOptionPane.showConfirmDialog(null, "Are you sure you want to exit?") == JOptionPane.OK_OPTION)
						System.exit(0);
				} catch (Exception ex){}
			}
		});
	}

	private void initComponents() {
		GameFont.initFont();

		contentPanel = new JPanel() {
			@Override
			public void paintComponent(Graphics g) {
				Graphics2D g2d = (Graphics2D) g;
				g2d.drawImage(ImageLoader.getImage("background"), 0, 0, null);
			}
		};
		contentPanel.setLayout(new BorderLayout());
		this.setContentPane(contentPanel);

		mainFrameEvent = new MainFrameEvent();
		mainFrameEvent.addListener(this);

		startUpPanel = new StartUpPanel();
		startUpPanel.setLayout(null);
		startUpPanel.addMainFrameEvent(mainFrameEvent);

		gameProperPanel = new GameProperPanel();
		
	}

	public void showStartUpPanel() {
		getContentPane().add(startUpPanel, BorderLayout.CENTER);
		contentPanel.updateUI();
	}

	public void hideStartUpPanel() {
		remove(startUpPanel);
	}

	public void showGameProperPanel() {
		getContentPane().add(gameProperPanel, BorderLayout.CENTER);
		contentPanel.updateUI();
	}

}
