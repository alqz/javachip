package _6_v100;

import javax.swing.*;
import javax.swing.plaf.*;
import javax.swing.plaf.basic.*;
import java.awt.*;


public class MyScrollPane extends JScrollPane
{
	public MyScrollPane( Container container )
	{
		super( container );
		fixUI();
	}
	
	public void fixUI()
	{
		MyScrollBar newHorizontalScrollbar 	= new MyScrollBar( JScrollBar.HORIZONTAL );
		MyScrollBar newVerticalScrollbar 	= new MyScrollBar( JScrollBar.VERTICAL );
		newHorizontalScrollbar				.setUI( new MyScrollBarUI() );
		newVerticalScrollbar				.setUI( new MyScrollBarUI() );
		setHorizontalScrollBar( newHorizontalScrollbar );
		setVerticalScrollBar( newVerticalScrollbar );
	}
	
	
	// custom scrollbar & scrollbar ui >>
		public class MyScrollBar extends JScrollBar
		{
			public MyScrollBar( int orientation )
			{
				super( orientation );
			}
			
			public void setUI( ScrollBarUI ui )
			{
				super.setUI( ui );
			}
		}
		
		public class MyScrollBarUI extends BasicScrollBarUI
		{
			@Override
			protected void paintThumb( Graphics g, JComponent c, Rectangle thumbBounds )
			{
				if( thumbBounds.isEmpty() || !scrollbar.isEnabled() )
				{
					return;
				}
				
				if( scrollbar.getOrientation() == JScrollBar.VERTICAL )
				{
					g.setColor( new Color(86, 149, 172) );
					g.fillRect( thumbBounds.x+thumbBounds.width/4, thumbBounds.y, thumbBounds.width/2, thumbBounds.height );
				}
				else if( scrollbar.getOrientation() == JScrollBar.HORIZONTAL )
				{
					g.setColor( new Color(86, 149, 172) );
					g.fillRect( thumbBounds.x, thumbBounds.y+thumbBounds.height/4, thumbBounds.width, thumbBounds.height/2 );
				}
			}
			
			@Override
			protected void paintTrack( Graphics g, JComponent c, Rectangle trackBounds )
			{
				if( scrollbar.getOrientation() == JScrollBar.VERTICAL )
				{
					g.setColor( new Color(185, 218, 223) );
					g.fillRect( trackBounds.x+trackBounds.width/4, trackBounds.y, trackBounds.width/2, trackBounds.height );
				}
				else if( scrollbar.getOrientation() == JScrollBar.HORIZONTAL )
				{
					g.setColor( new Color(185, 218, 223) );
					g.fillRect( trackBounds.x, trackBounds.y+trackBounds.height/4, trackBounds.width, trackBounds.height/2 );
				}
				
				if( this.trackHighlight == BasicScrollBarUI.DECREASE_HIGHLIGHT )
				{
					paintDecreaseHighlight( g );
				}
				else if( this.trackHighlight == BasicScrollBarUI.INCREASE_HIGHLIGHT )
				{
					paintIncreaseHighlight( g );
				}
			}
			
			
			@Override
			protected JButton createDecreaseButton( int orientation )
			{
				return createEmptyButton();
			}
			
			@Override
			protected JButton createIncreaseButton( int orientation )
			{
				return createEmptyButton();
			}
			
			private JButton createEmptyButton()
			{
				JButton jbutton = new JButton();
				jbutton.setPreferredSize( new Dimension( 0, 0 ) );
				jbutton.setMinimumSize( new Dimension( 0, 0 ) );
				jbutton.setMaximumSize( new Dimension( 0, 0 ) );
				return jbutton;
			}
		}
	// custom scrollbar & scrollbar ui <<
}