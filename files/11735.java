/**
 * Copyright 1999-2009 The Pegadi Team
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Title:        <p>
 * Description:  <p>
 * Copyright:    Copyright (c) <p>
 * Company:      <p>
 * @author
 * @version 1.0
 */
package org.pegadi.lister;

import org.pegadi.articlelist.ArticleList;
import org.pegadi.articlelist.GenericComparator;
import org.pegadi.articlelist.Grouper;
import org.pegadi.util.GridBagConstraints2;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Comparator;
import java.util.Locale;
import java.util.ResourceBundle;

public class GroupDialog extends JDialog {
    JLabel header = new JLabel();

    JPanel jPanel1 = new JPanel();

    JButton okButton = new JButton();

    JButton cancelButton = new JButton();

    Grouper grouper;

    GenericComparator genericComparator;

    JPanel jPanel2 = new JPanel();

    JComboBox groupCombo = new JComboBox();

    GridBagLayout gridBagLayout1 = new GridBagLayout();

    JLabel jLabel1 = new JLabel();

    Component component1;

    JLabel jLabel2 = new JLabel();

    JLabel jLabel3 = new JLabel();

    JLabel jLabel4 = new JLabel();

    JComboBox sortCombo1 = new JComboBox();

    JComboBox sortCombo2 = new JComboBox();

    JComboBox sortCombo3 = new JComboBox();

    Locale currentLocale;

    ResourceBundle messages;

    boolean wasCanceled = false;

    private final Logger log = LoggerFactory.getLogger(getClass());

    public GroupDialog(JFrame frame, Grouper currentGrouper) {
        super(frame, true);
        this.grouper = currentGrouper;
        currentLocale = Locale.getDefault();
        try {
            messages = ResourceBundle.getBundle(
                    "org.pegadi.lister.GroupDialogStrings", currentLocale);
        } catch (Exception e) {
            log.error("Error initialising strings", e);
        }
        try {
            jbInit();
        } catch (Exception e) {
            log.error("Error initialising GUI", e);
        }
        setTitle(messages.getString("header"));
        populateCombo();
        populateSortCombos();
    }

    private void populateCombo() {
        Grouper[] availGroupers = ArticleList.getAvailableGroupers();
        for (int i = 0; i < availGroupers.length; i++) {
            groupCombo.addItem(availGroupers[i]);
            if (availGroupers[i].equals(this.grouper))
                groupCombo.setSelectedIndex(i);
        }
    }

    private void populateSortCombos() {
        Comparator[] availComparators = ArticleList.getAvailableComparators();
        for (Comparator availComparator : availComparators) {
            sortCombo1.addItem(availComparator);
            sortCombo2.addItem(availComparator);
            sortCombo3.addItem(availComparator);
        }
    }

    public Grouper getSelectedGrouper() {
        return this.grouper;
    }

    private void setSelectedGrouper() {
        this.grouper = (Grouper) groupCombo.getSelectedItem();
    }

    public Comparator getSelectedComparator() {
        return this.genericComparator;
    }

    private void setSelectedComparators() {
        this.genericComparator = new GenericComparator((Comparator) sortCombo1
                .getSelectedItem(), (Comparator) sortCombo2.getSelectedItem(),
                (Comparator) sortCombo3.getSelectedItem());
    }

    private void jbInit() {
        component1 = Box.createHorizontalStrut(8);
        header.setText(messages.getString("header"));
        okButton.setText(messages.getString("ok_text"));
        okButton.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(ActionEvent e) {
                okButton_actionPerformed(e);
            }
        });
        cancelButton.setText(messages.getString("cancel_text"));
        cancelButton.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(ActionEvent e) {
                cancelButton_actionPerformed(e);
            }
        });
        jPanel2.setLayout(gridBagLayout1);
        jLabel1.setText(messages.getString("group_by"));
        jLabel2.setText(messages.getString("sort_key") + " 1:");
        jLabel3.setText(messages.getString("sort_key") + " 2:");
        jLabel4.setText(messages.getString("sort_key") + " 3:");
        this.getContentPane().add(header, BorderLayout.NORTH);
        this.getContentPane().add(jPanel1, BorderLayout.SOUTH);
        jPanel1.add(okButton, null);
        jPanel1.add(cancelButton, null);
        this.getContentPane().add(jPanel2, BorderLayout.CENTER);
        jPanel2.add(groupCombo, new GridBagConstraints2(1, 0, 1, 1, 1.0, 0.0,
                GridBagConstraints2.SOUTHWEST, GridBagConstraints2.NONE,
                new Insets(14, 0, 0, 0), 0, 0));
        jPanel2.add(jLabel1, new GridBagConstraints2(0, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints2.SOUTHWEST, GridBagConstraints2.NONE,
                new Insets(0, 0, 0, 0), 0, 0));
        jPanel2.add(component1, new GridBagConstraints2(0, 4, 1, 1, 0.0, 0.0,
                GridBagConstraints2.CENTER, GridBagConstraints2.NONE,
                new Insets(0, 0, 0, 0), 0, 0));
        jPanel2.add(jLabel2, new GridBagConstraints2(0, 1, 1, 1, 0.0, 0.0,
                GridBagConstraints2.CENTER, GridBagConstraints2.NONE,
                new Insets(27, 0, 0, 0), 0, 0));
        jPanel2.add(jLabel3, new GridBagConstraints2(0, 2, 1, 1, 0.0, 0.0,
                GridBagConstraints2.CENTER, GridBagConstraints2.NONE,
                new Insets(0, 0, 0, 0), 0, 0));
        jPanel2.add(jLabel4, new GridBagConstraints2(0, 3, 1, 1, 0.0, 0.0,
                GridBagConstraints2.CENTER, GridBagConstraints2.NONE,
                new Insets(0, 0, 0, 0), 0, 0));
        jPanel2.add(sortCombo2, new GridBagConstraints2(1, 2, 1, 1, 0.0, 0.0,
                GridBagConstraints2.WEST, GridBagConstraints2.NONE, new Insets(
                0, 0, 0, 0), 0, 0));
        jPanel2.add(sortCombo3, new GridBagConstraints2(1, 3, 1, 1, 0.0, 0.0,
                GridBagConstraints2.WEST, GridBagConstraints2.NONE, new Insets(
                0, 0, 0, 0), 0, 0));
        jPanel2.add(sortCombo1, new GridBagConstraints2(1, 1, 1, 1, 0.0, 0.0,
                GridBagConstraints2.SOUTHWEST, GridBagConstraints2.NONE,
                new Insets(0, 0, 0, 0), 0, 0));

        addKeyListener(new KeyAdapter() {
            public void keyPressed(KeyEvent e) {

                if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
                    wasCanceled = true;
                dispose();
            }
        });

    }

    void okButton_actionPerformed(ActionEvent e) {
        setSelectedGrouper();
        setSelectedComparators();
        dispose();
    }

    void cancelButton_actionPerformed(ActionEvent e) {
        wasCanceled = true;
        dispose();
    }

    boolean wasCanceled() {
        return wasCanceled;
    }
}
