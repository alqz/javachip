package oxen.commands.user;

import java.util.logging.Level;
import java.util.logging.Logger;
import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import oxen.game.components.actions.Action;
import oxen.game.components.actions.Use;
import oxen.game.components.dialogue.DescriptionLine;
import oxen.game.components.dialogue.Dialogue;
import oxen.game.components.items.InventoryIsFullException;
import oxen.game.components.items.Item;
import oxen.game.components.items.ItemIsTooHeavyException;
import oxen.game.components.items.LimitedInventory;
import oxen.game.components.room.Room;
import oxen.state.ActionDnxException;
import oxen.state.GameState;
import oxen.state.TravelLog;

/**
 *
 * @author nikola
 */
public class UseTest extends CommandTestCase {
    private Action           action;
    private UseCommand       command;
    private Dialogue         dialogue;
    private LimitedInventory inventory;
    private Item             item1;
    private Room             currentRoom;
    private TravelLog        trLog;

    @Before
    public void setUp() throws Exception {
        command   = new UseCommand(manager);
        state     = mock(GameState.class);
        trLog     = mock(TravelLog.class);
        currentRoom = new Room("this-is-a-room");
        inventory = new LimitedInventory();
        item1     = new Item("thetorch");
        item1.setName("TORCH");
        item1.setLimited(true);
        item1.setUsable(true);
        try {
            inventory.add(item1);
        }
        catch (InventoryIsFullException ex) {
            Logger.getLogger(UseTest.class.getName()).log(Level.SEVERE, null, ex);
            fail();
        }
        catch (ItemIsTooHeavyException ex) {
            Logger.getLogger(UseTest.class.getName()).log(Level.SEVERE, null, ex);
            fail();
        }
        action   = mock(Action.class);
        dialogue = new Dialogue();
        dialogue.add(new DescriptionLine("Torches are cool."));
        when(manager.getCurrentState()).thenReturn(state);
        when(manager.getLogManager()).thenReturn(logger);
        when(state.getPlayer()).thenReturn(player);
        when(player.getInventory()).thenReturn(inventory);
        when(action.getDialogue()).thenReturn(dialogue);
        when(state.resolveAction(Use.class, item1)).thenReturn(action);
        when(trLog.getCurrentRoom()).thenReturn(currentRoom);
        when(state.getPlayerTravelLog()).thenReturn(trLog);
    }

    /**
     * test if a use command accepts valid input
     */
    @Test
    public void testCanRespondTo() {
        assertTrue(command.canRespondTo("USE TORCH"));
        assertTrue(command.canRespondTo("use itemsd"));
        assertFalse(command.canRespondTo("se item"));
    }

    /**
     * Test if dialogue is generated when using an item.
     */
    @Test
    public void testRespondTo() {
        command.respondTo("USE TORCH");
        assertThat(collector.output, containsString("Torches are cool."));
    }

    /**
     * Test if using a limited item twice deletes it from the inventory.
     */
    @Test
    public void testLimitedUse() {
        assertTrue(inventory.containsKey("thetorch"));
        command.respondTo("USE TORCH");
        command.respondTo("USE TORCH");
        assertFalse(inventory.containsKey("thetorch"));
    }

    /**
     * Test use with item not in inventory.
     */
    @Test
    public void testRespondToFalse() {
        command.respondTo("USE SWORD");
        assertThat(collector.output, containsString("You cannot see that here."));
    }

    /**
     * Test with item that cannot be used.
     */
    @Test
    public void testUnusableItem() throws Exception {
        when(state.resolveAction(Use.class, item1)).thenThrow(new ActionDnxException());
        command.respondTo("USE TORCH");
        assertThat(collector.output, containsString("You cannot use that now."));
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
