package qw0rumbot.plugins;
import org.jibble.pircbot.User;
import qw0rumbot.Qw0rumBot;

/**
 * Plugin for de-opping people and voicing people.
 * @author SL5R0
 *
 */
public class OpNazi extends BotPlugin{
		
	/**
	 * Constructor requires reference to the bot.
	 * @param b
	 */
	public OpNazi(Qw0rumBot b){
		super(b);
		b.registerHandler("OnModeChange", this);
		b.registerHandler("OnJoin", this);
	}
	
	@Override
	public void onModeChange(){
		this.ensureOp();
		this.ensureVoice();
	}
	
	@Override
	public void onJoin(String channel, String sender, String login,
			String hostname){
		this.ensureOp();
		this.ensureVoice();
		if (!sender.equals(this.bot.getNick())){
			this.bot.deOp(channel, sender);
		}
	}
	
	/**
	 * Ensures that the bot is opped, if possible, and de-op everybody else.
	 */
	private void ensureOp(){
		for(String c : this.bot.getChannels()){
			this.bot.op(c, this.bot.getNick());
			for(User u : this.bot.getUsers(c)){
				if (u.getNick().compareTo(this.bot.getNick()) == 0){
					continue;
				}
				if (u.isOp()){
					this.bot.deOp(c, u.getNick());
				}
			}
		}
	}
	
	/**
	 * Ensures that everybody in the channel has voice.
	 */
	private void ensureVoice(){
		for(String c : this.bot.getChannels()){
			for(User u : this.bot.getUsers(c)){
				if (!u.hasVoice()){
					this.bot.voice(c, u.getNick());
				}
			}
		}
	}
}
