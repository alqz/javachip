package cg2.raytracer.shapes;

import cg2.raytracer.Hit;
import cg2.raytracer.Ray;
import cg2.raytracer.materials.PhongMaterial;
import cg2.vecmath.Vector;

/**
 * this class represents a triangle, calculations after "Moller and Trumbore"
 * formulas are from http://www.lighthouse3d.com/tutorials/maths/ray-triangle-intersection/
 */
public class Triangle extends Shape {
	
	/**
	 * the first point of the triangle
	 */
	protected Vector p1;
	
	/**
	 * the second point of the triangle
	 */
	protected Vector p2;
	
	/**
	 * the third point of the triangle
	 */
	protected Vector p3;
	
	/**
	 * constructor for a new triangle
	 * @param p1: the first point of the triangle
	 * @param p2: the second point of the triangle
	 * @param p3: the third point of the triangle
	 * @param color: the surface color of the triangle
	 */
	public Triangle(Vector p1, Vector p2, Vector p3, PhongMaterial material) {
		super(material);
		this.p1 = p1;
		this.p2 = p2;
		this.p3 = p3;
	}
	
	//-------------------------GETTER-------------------------
	
	public Vector getPoint1() {
		return p1;
	}
	
	public Vector getPoint2() {
		return p2;
	}
	
	public Vector getPoint3() {
		return p3;
	}
	
	//--------------------------------------------------------	

	@Override
	public Hit intersect(Ray ray) {
	
		//directions: p1 to p2/p3
		Vector e1 = p2.sub(p1);
		Vector e2 = p3.sub(p1);	
		
		Vector h = ray.getDirection().cross(e2);
		float a = e1.dot(h);
		
		//no intersection if (a == 0)
		if (a > -0.00001 && a < 0.00001) {
			return new Hit(this, ray, -1, null);
		}
		
		float f = 1/a;
		Vector s = ray.getOrigin().sub(p1);
		float u = f * s.dot(h);
		
		//no intersection if not (0 < u < 1)
		if (u < 0.0 || u > 1.0) {
			return new Hit(this, ray, -1, null);
		}
		
		Vector q = s.cross(e1);
		float v = f * ray.getDirection().dot(q);
		
		//no intersection if v negative or ((u + v) > 1)
		if (v < 0.0 || (u + v) > 1.0) {
			return new Hit(this, ray, -1, null);
		}
		
		//all restrictions satisfied: intersection
		float t = f * e2.dot(q);
		return new Hit(this, ray, t, (e1.cross(e2)).normalize());
	}

}
