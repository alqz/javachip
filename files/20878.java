
package webservice.jaxws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;



@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getTransportType", propOrder = { "transport_id" })
public class GetTransportType {
    @XmlElement(name = "transport_id")
    protected int transport_id;

	public int getTransport_id() {
		return transport_id;
	}

	public void setTransport_id(int transport_id) {
		this.transport_id = transport_id;
	}
}