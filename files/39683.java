package unity.controller;

import org.slim3.controller.router.RouterImpl;

public class AppRouter extends RouterImpl {

    public AppRouter() {
        addRouting("/user/{name}", "/user/?name={name}");
        addRouting("/game/ug{id}", "/game/?id={id}");
        addRouting("/api/game/{gameId}", "/api/gameApi?id={gameId}");
        addRouting("/game/upload/{newGame}", "/game/upload/?g={newGame}");
        addRouting("/game/upload/{newGame}/{guest}", "/game/upload/?g={newGame}&t={guest}");
        addRouting("/game/upload/{newGame}/{guest}{id}", "/game/upload/?g={newGame}&t={guest}&id={id}");
    }
}