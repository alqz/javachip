package dart.runner;

import dart.Consts;
import dart.Player;

public class GreenLoopUFO extends AttackerRunner {

	
	
	//Main constructor
	public GreenLoopUFO(int startx, int starty, Player owner) {
		super(startx, starty, owner);
		this._type=Consts.RunnerType.GREENLOOPUFO;
		
		//Set constants
		this.cost = 75;
		this.attackSpeed=1000;
		this.currentDamage=this.initialDamage=2;
		this.range=3;
	}

}
