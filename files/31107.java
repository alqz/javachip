/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package qse_sepm_ss12_07.dao;

/**
 *
 * @author vauvenal5
 */
public class CheckPriceEntity
{
	private boolean isShared;
	private int productCount;
	private int checkId;
	private int priceId;
	private int id;
	
	public CheckPriceEntity()
	{
		id = -1;
	}
	
	public int getId()
	{
		return this.id;
	}
	
	public void setId(int id)
	{
		this.id = id;
	}
	
	public int getPriceId()
	{
		return this.priceId;
	}
	
	public void setPriceId(int id)
	{
		this.priceId = id;
	}
	
	public int getProductCount()
	{
		return this.productCount;
	}
	
	public void setProductCount(int productCount)
	{
		this.productCount = productCount;
	}
	
	public int getCheckId()
	{
		return this.checkId;
	}
	
	public void setCheckId(int id)
	{
		this.checkId = id;
	}
	
	public boolean getIsShared()
	{
		return isShared;
	}
	
	public void setIsShared(boolean isShared)
	{
		this.isShared = isShared;
	}
	
	@Override
	public boolean equals(Object o)
	{
		if(o instanceof CheckPriceEntity)
		{
			CheckPriceEntity checkPriceEntity = (CheckPriceEntity)o;
			
			if(this.checkId == checkPriceEntity.getCheckId() &&
				this.id == checkPriceEntity.getId() &&
				this.priceId == checkPriceEntity.getPriceId())
			{
				return true;
			}
		}
		
		return false;
	}
}
