import wheels.users.*;
import java.awt.Color;

public class Patchg{

    private boolean _isAlive;
    private int _pxcor, _pycor, _liveNeighbors, _xdim, _ydim;
    private Patchg [][] _grid;
    private Neighborsg _neighbors;
    private Rectangle _box;

    public Patchg(int xcor,int ycor,Patchg[][] grid){
	_box = new Rectangle(Color.black);
	_isAlive = false;
	_pxcor = xcor;
	_pycor = ycor;
	_grid = grid;
	_grid[_pxcor][_pycor] = this;
	_box.setSize(700 / _grid.length, 500 / _grid[_pxcor].length);
	_xdim = 700 / _grid.length;
	_ydim = 500 / _grid[_pxcor].length;
	_neighbors = new Neighborsg();
    }

    public Patchg(int xcor, int ycor, Patchg[][] grid, boolean alive){
	_box = new Rectangle(Color.black);
	_isAlive = alive;
	_pxcor = xcor;
	_pycor = ycor;
	_grid = grid;
	_grid[_pxcor][_pycor] = this;
	_box.setSize(700 / _grid.length, 500 / _grid[_pxcor].length);
	_xdim = 700 / _grid.length;
	_ydim = 500 / _grid[_pxcor].length;
	_neighbors = new Neighborsg();
	change();
    }

    // adds its neighbors to the matrix in _neighbors
    public void accessNeighbors(){
	for(int i = -1; i < 2; i++){
	    for(int j = -1; j < 2; j++){
		if(!(i == 0 && j == 0) && _pxcor + i >= 0 && +_pxcor + i < _grid.length && _pycor + j >= 0 && _pycor + j < _grid[_pxcor].length)
		    _neighbors.add(_grid[_pxcor + i][_pycor + j]);
	    }	   
	}
    }

    public void nextGen(){
	if(_liveNeighbors == 3)
	    _isAlive = true;
	else if (_liveNeighbors < 2 || _liveNeighbors > 3)
	    _isAlive = false;
	change();
    }

    public void live(){
	_isAlive = true;
	change();
    }

    public int count(){
	_liveNeighbors = _neighbors.count();
	return _liveNeighbors;
    }

    public void change(){
	if(_isAlive)
	    _box.setColor(Color.white);
	else
	    _box.setColor(Color.black);
    }

    public boolean status(){
	return _isAlive;
    }

    public int getXdim(){
	return _xdim;
    }

    public int getYdim(){
	return _ydim;
    }

    public void move(int x, int y){
	_box.setLocation(x,y);
    }
}