package me.cosban.suckchat.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import me.cosban.suckchat.SuckChat;
import me.cosban.suckchat.parsers.Censor;

public class CensorCommand implements CommandExecutor {

	private Censor c;

	public CensorCommand(SuckChat instance) {
		c = SuckChat.getAPI().getCensor();
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (command.getName().equalsIgnoreCase("censor")) {
			if(args.length < 1){
				sender.sendMessage(ChatColor.DARK_RED + "You have provided insufficient parameters.");
				return false;
			}

			switch(args[0].toLowerCase()){
			case "enable":
				if(sender.hasPermission("chat.censor.toggle")){
					if(!c.isEnabled()){
						sender.sendMessage(ChatColor.YELLOW + "Censors are now enabled!");
						c.setEnabled(true);
					} else sender.sendMessage(ChatColor.DARK_RED + "Censors are already enabled!");
					return true;
				} return false;

			case "disable":
				if(sender.hasPermission("chat.censor.toggle")){
					if(!c.isEnabled()){
						sender.sendMessage(ChatColor.YELLOW + "Censors are now disabled!");
						c.setEnabled(false);
					} else sender.sendMessage(ChatColor.DARK_RED + "Censors are already disabled!");
					return true;
				} return false;	

				// /censor add cosban 0.2 fuck
			case "add":
				if(sender.hasPermission("chat.censor.edit")){
					if(args.length==4){
						c.addVulgarity(args[1], Double.valueOf(args[2]), args[3]);
						sender.sendMessage("Now replacing "+args[1]+ " with "+args[3]+" at a probability of "+args[2]);
						return true;
					} else if(args.length==3){
						c.addVulgarity(args[1], Double.valueOf(args[2]), "&8****");
						sender.sendMessage("Now censoring "+args[1]+" at a probability of "+args[2]);
						return true;
					} else if(args.length==2){
						c.addVulgarity(args[1], 1.0, "&8****");
						sender.sendMessage("Now censoring "+args[1]);
						return true;
					} else sender.sendMessage(ChatColor.DARK_RED + "You have provided insufficient parameters.");
				}
				return false;

			case "remove":
			case "rm":
				if(sender.hasPermission("chat.censor.edit")){
					if(args.length<2){
						sender.sendMessage(ChatColor.DARK_RED + "You have provided insufficient parameters.");
						return false;
					} else if (args.length>=2){
						for(int i = 1; i<args.length;i++){
							c.rmVulgarity(args[i]);
							sender.sendMessage("No longer censoring "+args[i]);
						} return true;
					} return false;
				}

			default:
				sender.sendMessage(ChatColor.DARK_RED + "You have provided insufficient parameters.");
				break;
			}
		} return false;
	}

}
