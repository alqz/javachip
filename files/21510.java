package org.blockfreie.lithium.console;

import java.util.Arrays;
import java.util.List;


/**
 * Run the Jython console with all the classes of the projects (dependencies and
 * builded)
 * 
 * @author <a href="mailto:major.seitan@gmail.com">Major Seitan</a>
 * @version $Id: b9db9b106c93c905559a3ccf01b6b0b246365c41 $ 
 * 
 * @goal console
 * @requiresDependencyResolution test
 * @inheritByDefault true
 * @requiresDirectInvocation true
 * @executionStrategy once-per-session
 */
public class ConsoleMojo extends AbstractJythonConsoleMojo {
	@Override
	public List<String> getArgument() {
		List<String> result = Arrays.asList(new String[] {});
		if(getScriptFile() !=null)
		{	result.add("-i");
			result.add(getScriptFile());
		}
		return result;
	}

}
