package unity.controller.api;

import java.io.PrintWriter;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;

import unity.service.RSSService;

public class RssController extends Controller {

    private RSSService rssService = new RSSService();

    @Override
    public Navigation run() throws Exception {
        String rss = rssService.getRSSString();

        response.setContentType("text/xml;charset=UTF-8");
        PrintWriter out = response.getWriter();
        out.println(rss);
        out.close();

        return null;
    }
}
