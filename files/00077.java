/**
 * Copyright 1999-2009 The Pegadi Team
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.pegadi.articlesearch;

import org.pegadi.sqlsearch.SearchTerm;

/**
 * This class imlements an article type search.
 *
 * @author Eirik Bjorsnos <bjorsnos@underdusken.no>
 * @version $Revision$, $Date$
 */
public class ArticleTypeTerm extends SearchTerm {

    /**
     * The ID of the article type to search by
     */
    private int articleTypeRef;

    /**
     * Constructor that takes the reference of the article type to match.
     *
     * @param articleTypeRef the reference to the article type
     */
    public ArticleTypeTerm(int articleTypeRef) {
        this.articleTypeRef = articleTypeRef;
    }

    /**
     * Generates the WHERE part of the SQL query (without including the string
     * "WHERE").
     *
     * @return the WHERE clause
     */
    public String whereClause() {
        return "Article.refArticleType = " + articleTypeRef;
    }

    /**
     * Returns the names of the tables involved in this search.
     *
     * @return A String[] containing the involved tables. The array may contain
     *         multiple instances of each table name.
     */
    public String[] getTables() {

        String[] ret = new String[1];
        ret[0] = "Article";
        return ret;
    }

}
