package unity.controller.api;

import org.junit.Test;
import org.slim3.datastore.Datastore;
import org.slim3.datastore.GlobalTransaction;
import org.slim3.tester.ControllerTestCase;

import unity.model.GameData;
import unity.model.User;
import unity.service.SaveDataService;

public class EvalControllerTest extends ControllerTestCase {
    SaveDataService sd = new SaveDataService();

    @Test
    public void run() throws Exception {
    }

    GameData createGame() {
        GameData g = new GameData();
        g.setKey(Datastore.allocateId(GameData.class));
        save(g);
        return g;
    }

    User createUser() {
        User u = new User();
        u.setKey(Datastore.allocateId(User.class));
        save(u);
        return u;
    }

    void save(Object o) {
        GlobalTransaction tx = Datastore.beginGlobalTransaction();
        Datastore.put(o);
        tx.commit();
    }
}
