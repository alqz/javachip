package gui;

import java.awt.FlowLayout;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.swing.JFileChooser;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;
import javax.swing.ImageIcon;

import entities.Ampl;

public class AmplGUI extends JFrame {
	private static final long serialVersionUID = 1L;

    private JMenuBar menu;
    private JMenu file, edit, view, send, help;
    private JMenuItem readme, about;

    private JPanel header, innerHead, buttonsHeaderPanel;
    private JPanel outputPanel, headOutputPanel, buttonsOutputPanel, cmdPanel;
    private JPanel sideBar;
    private JPanel modelPanel, headModelPanel, buttonsModelPanel;
    private JPanel dataPanel, headDataPanel, buttonsDataPanel;
    private JPanel statusBar;
	
    private JSplitPane body;
	
    private JLabel cinIcon;
	
    private JButton minosBtn, cplexBtn, resetBtn, displayBtn, expandBtn, solveBtn;
    private JTextField cmd;
	
    private JButton clearOutputBtn, saveOutputBtn, openModelBtn, clearModelBtn, saveModelBtn, openDataBtn, clearDataBtn, saveDataBtn;
	
    private JTextArea outputTextArea, modelTextArea, dataTextArea;
    private JScrollPane outputScrollingTextArea, modelScrollingTextArea, dataScrollingTextArea;
	
    private JFileChooser chooser;

    private Ampl ampl;
	
	public static void main(String[] args) {
		AmplGUI gui = new AmplGUI();
		gui.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		gui.setSize(800, 600);
		gui.setVisible(true);
		
		gui.ampl = gui.startAmpl("ampl");
	}
	
	public Ampl startAmpl(String arg) {
		if (ampl != null) ampl.close();
		ampl = null;
		
		cmd.setBackground(Color.WHITE);
		cmd.setText("");
		
		try {
			ampl = new Ampl(arg);
		} catch (Exception e) {
			cmd.setBackground(Color.decode("#FFFF00"));
			cmd.setText(e + " starting " + arg);
			System.err.println(e + " starting " + arg);
		}
		
		if (ampl != null) {
			ampl.send("option show_stats 1; option display_eps .000001; print $version;\n");
			outputTextArea.append(ampl.rcv());
		}
		
		return ampl;
	}
	
	public AmplGUI() {
		super("Ampl GUI - CIn/UFPE");
		
		chooser = new JFileChooser();
		
		menu = new JMenuBar();
		file = new JMenu("File");
		edit = new JMenu("Edit");
		view = new JMenu("View");
		send = new JMenu("Send");
		help = new JMenu("Help");
		readme = new JMenuItem("Readme");
		about = new JMenuItem("About");

		menu.add(readme);
		menu.add(about);
		
		help.add(readme);
		help.add(about);
		
		menu.add(file);
		menu.add(edit);
		menu.add(view);
		menu.add(send);
		menu.add(help);
		
		header = new JPanel(new BorderLayout());
		header.setBackground(Color.decode("#FFFFFF"));
		header.setPreferredSize(new Dimension((int) this.getSize().getWidth(), 64));
		
		cinIcon = new JLabel(new ImageIcon("logo_cin.jpg"));
		header.add(cinIcon, BorderLayout.EAST);
		
		Insets headButtonsInsets = new Insets(4, 4, 4, 4);		
		
		minosBtn = new JButton("MINOS");
		minosBtn.setMargin(headButtonsInsets);
		cplexBtn = new JButton("CPLEX");
		cplexBtn.setMargin(headButtonsInsets);
		resetBtn = new JButton("RESET");
		resetBtn.setMargin(headButtonsInsets);
		resetBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				ampl.send("reset;");
			}
		});
		
		displayBtn = new JButton("DISPLAY");
		displayBtn.setMargin(headButtonsInsets);
		displayBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				ampl.send("display " + cmd.getText() + ";");
				outputTextArea.append(ampl.rcv());
			}
		});
		
		expandBtn = new JButton("EXPAND");
		expandBtn.setMargin(headButtonsInsets);
		expandBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
			}
		});

		solveBtn = new JButton("SOLVE");
		solveBtn.setMargin(headButtonsInsets);
		solveBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				ampl.send("reset;");
				ampl.send(modelTextArea.getText());
				ampl.send(dataTextArea.getText());
				ampl.send("solve;");
				
				String strOut = "";
				
				while(true) {
					strOut = ampl.rcv();
					if (!strOut.isEmpty()) break;
				}
				
				outputTextArea.setText(strOut);
			}
		});
		
		buttonsHeaderPanel = new JPanel(new FlowLayout());
		buttonsHeaderPanel.setBackground(Color.decode("#FFFFFF"));
        buttonsHeaderPanel.add(minosBtn);
		buttonsHeaderPanel.add(cplexBtn);
		buttonsHeaderPanel.add(resetBtn);
		buttonsHeaderPanel.add(displayBtn);
		buttonsHeaderPanel.add(expandBtn);
		buttonsHeaderPanel.add(solveBtn);
		
		cmdPanel = new JPanel(new BorderLayout());
		cmdPanel.setBackground(Color.decode("#FFFFFF"));
		cmd = new JTextField(31);
		cmdPanel.add(new JLabel("  Ampl:"), BorderLayout.WEST);
		cmdPanel.add(cmd, BorderLayout.EAST);
		
		innerHead = new JPanel(new BorderLayout());
		innerHead.add(buttonsHeaderPanel, BorderLayout.NORTH);
		innerHead.add(cmdPanel, BorderLayout.CENTER);
		
		header.add(innerHead, BorderLayout.WEST);
		

		// OUTPUT 
		outputPanel = new JPanel(new BorderLayout());
		outputPanel.setBackground(Color.decode("#FFFFFF"));
		
		headOutputPanel = new JPanel(new BorderLayout());
		headOutputPanel.setBackground(Color.decode("#EEEEEE"));
		
		clearOutputBtn = new JButton("Clear");
		clearOutputBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				outputTextArea.setText("");
			}
		});
		
		saveOutputBtn = new JButton("Report");
		saveOutputBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null, "Salvando dados do output");
			}
		});
		
		buttonsOutputPanel = new JPanel();
		buttonsOutputPanel.setBackground(Color.decode("#EEEEEE"));
		buttonsOutputPanel.add(clearOutputBtn);
		buttonsOutputPanel.add(saveOutputBtn);

		headOutputPanel.add(new JLabel(" OUTPUT"), BorderLayout.WEST);
		headOutputPanel.add(buttonsOutputPanel, BorderLayout.EAST);
		
		outputTextArea = new JTextArea();
		outputScrollingTextArea = new JScrollPane(outputTextArea);
		outputScrollingTextArea.setBorder(null);
		
		outputPanel.add(headOutputPanel, BorderLayout.NORTH);
		outputPanel.add(outputScrollingTextArea, BorderLayout.CENTER);
		
		// SIDEBAR
		sideBar = new JPanel(new GridLayout(2, 1));

        // MODEL
		modelPanel = new JPanel(new BorderLayout());

        headModelPanel = new JPanel(new BorderLayout());
        headModelPanel.setBackground(Color.decode("#EEEEEE"));
        
        openModelBtn = new JButton("Open");
        openModelBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (chooser.showOpenDialog(menu.getParent()) == JFileChooser.APPROVE_OPTION) {
					try {
						FileInputStream fstream = new FileInputStream(chooser.getSelectedFile());
						DataInputStream in = new DataInputStream(fstream);
						BufferedReader br = new BufferedReader(new InputStreamReader(in));
						String strLine, model = "";

						while ((strLine = br.readLine()) != null) {
							model += strLine + "\n";
						}
						
						modelTextArea.setText(model);
						
					} catch (FileNotFoundException ex) {
						ex.printStackTrace();
					} catch (IOException ex) {
						ex.printStackTrace();
					}
				}
			}
		});
        
        clearModelBtn = new JButton("Clear");
        clearModelBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				modelTextArea.setText("");
			}
		});
        
        saveModelBtn = new JButton("Save");
        saveModelBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null, "Salvando modelo");
			}
		});
        
        buttonsModelPanel = new JPanel();
        buttonsModelPanel.setBackground(Color.decode("#EEEEEE"));
        buttonsModelPanel.add(openModelBtn);
        buttonsModelPanel.add(clearModelBtn);
        buttonsModelPanel.add(saveModelBtn);

        headModelPanel.add(new JLabel(" MODEL"), BorderLayout.WEST);
        headModelPanel.add(buttonsModelPanel, BorderLayout.EAST);
        modelPanel.add(headModelPanel, BorderLayout.NORTH);

        modelTextArea = new JTextArea();
        //modelTextArea.setBackground(Color.decode("#F7F7F7"));
        modelScrollingTextArea = new JScrollPane(modelTextArea);
        modelScrollingTextArea.setBorder(null);
        modelPanel.add(modelScrollingTextArea);

        // DATA 

		dataPanel = new JPanel(new BorderLayout());

		headDataPanel = new JPanel(new BorderLayout());
		headDataPanel.setBackground(Color.decode("#EEEEEE"));
		
		openDataBtn = new JButton("Open");
		openDataBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (chooser.showOpenDialog(menu.getParent()) == JFileChooser.APPROVE_OPTION) {
					try {
						FileInputStream fstream = new FileInputStream(chooser.getSelectedFile());
						DataInputStream in = new DataInputStream(fstream);
						BufferedReader br = new BufferedReader(new InputStreamReader(in));
						String strLine, data = "";

						while ((strLine = br.readLine()) != null) {
							data += strLine + "\n";
						}
						
						dataTextArea.setText(data);
						
					} catch (FileNotFoundException ex) {
						ex.printStackTrace();
					} catch (IOException ex) {
						ex.printStackTrace();
					}
				}
			}
		});
		
		clearDataBtn = new JButton("Clear");
		clearDataBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				dataTextArea.setText("");
			}
		});
		
		saveDataBtn = new JButton("Save");
		saveDataBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null, "Salvando dados");
			}
		});
		
		buttonsDataPanel = new JPanel();
        buttonsDataPanel.setBackground(Color.decode("#EEEEEE"));
        buttonsDataPanel.add(openDataBtn);
        buttonsDataPanel.add(clearDataBtn);
        buttonsDataPanel.add(saveDataBtn);
		
		headDataPanel.add(new JLabel(" DATA"), BorderLayout.WEST);
		headDataPanel.add(buttonsDataPanel, BorderLayout.EAST);
		dataPanel.add(headDataPanel, BorderLayout.NORTH);

        dataTextArea = new JTextArea();
        //dataTextArea.setBackground(Color.decode("#F7F7F7"));
		dataScrollingTextArea = new JScrollPane(dataTextArea);
		dataScrollingTextArea.setBorder(null);
		dataPanel.add(dataScrollingTextArea);
		
		sideBar.add(modelPanel);
		sideBar.add(dataPanel);

		body = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, outputPanel, sideBar);
		body.setDividerLocation(400);
		
		// STATUS
		statusBar = new JPanel(new BorderLayout());
		statusBar.add(new JLabel("Status"), BorderLayout.WEST);
		
        this.setJMenuBar(menu);		
		this.getContentPane().add(header, BorderLayout.NORTH);
		this.getContentPane().add(statusBar, BorderLayout.SOUTH);
		this.getContentPane().add(body);
	}
}
