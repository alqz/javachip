package org.rsbot.bot.event.impl;

import org.rsbot.bot.Context;
import org.rsbot.bot.accessors.Client;
import org.rsbot.script.internal.event.PaintListener;
import org.rsbot.script.methods.Calculations;
import org.rsbot.script.methods.Game;
import org.rsbot.script.wrappers.Player;

import java.awt.*;

public class DrawPlayers implements PaintListener {
	public void onRepaint(final Graphics render) {
		if (!Game.isLoggedIn()) {
			return;
		}
		final Client client = Context.get().client;
		final org.rsbot.bot.accessors.RSPlayer[] players = client.getRSPlayerArray();
		if (players == null) {
			return;
		}
		final FontMetrics metrics = render.getFontMetrics();
		for (final org.rsbot.bot.accessors.RSPlayer element : players) {
			if (element == null) {
				continue;
			}
			final Player player = new Player(element);
			final Point location = player.getLocation().toScreen(player.getHeight() / 2);
			if (!Calculations.isPointOnScreen(location)) {
				continue;
			}
			render.setColor(Color.RED);
			render.fillRect((int) location.getX() - 1, (int) location.getY() - 1, 2, 2);
			String s = player.getName() + " (" + player.getCombatLevel() + ")";
			render.setColor(player.isInCombat() ? (player.isDead() ? Color.GRAY : Color.RED) : player.isMoving() ? Color.GREEN : Color.WHITE);
			render.drawString(s, location.x - metrics.stringWidth(s) / 2, location.y - metrics.getHeight() / 2);
			final String msg = player.getMessage();
			boolean raised = false;
			if (player.getAnimation() != -1 || player.getGraphic() > 0 || player.getNPCID() != -1) {
				if (player.getNPCID() != -1) {
					s = "(NPC: " + player.getNPCID() + " | L: " + player.getLevel() + " | A: " + player.getAnimation() + " | G: " + player.getGraphic() + ")";
				} else {
					s = "(A: " + player.getAnimation() + " (" + player.getAnimationFrame() + ") | G: " + player.getGraphic() + ")";
				}
				render.drawString(s, location.x - metrics.stringWidth(s) / 2, location.y - metrics.getHeight() * 3 / 2);
				raised = true;
			}
			if (msg != null) {
				render.setColor(Color.ORANGE);
				render.drawString(msg, location.x - metrics.stringWidth(msg) / 2, location.y - metrics.getHeight() * (raised ? 5 : 3) / 2);
			}
		}
	}
}