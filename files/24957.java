package com.snc.automation.autobot.command;

import com.snc.automation.autobot.AutoBot;
import com.snc.automation.autobot.Command;

public final class StrlenCommand extends Command {
	public static final CommandMeta META = new CommandMeta(
		"strlen",
		"Calculates the length of a string of characters.",
		"<string>",
		StrlenCommand.class
	);
	
	public StrlenCommand(AutoBot bot, String channel, String sender, String login, String hostname, String message) {
		super(bot, channel, sender, login, hostname, message);
	}

	@Override
	public void run() {
    	String str = getArgument(mMessage);
    	if (str == null) {
    		mBot.sendMessage(mChannel, mSender + ": strlen('') = 0" );
    		return;
    	}

		mBot.sendMessage(mChannel, mSender + ": strlen('" + str + "') = " + str.length() );
	}
}
