/*
 */
package qse_sepm_ss12_07.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;

/**
 *
 */
public class JDBCUserDao implements IUserDao
{

	private JdbcTemplate jdbcTemplate;
	private SimpleJdbcInsert createUser;

	public void setDataSource(DataSource dataSource)
	{
		createUser = new SimpleJdbcInsert(dataSource).withSchemaName("yasl").withTableName("user").usingGeneratedKeyColumns("id");
		jdbcTemplate = new JdbcTemplate(dataSource);
	}

	public UserEntity create(UserEntity user) throws DaoException
	{
		try {
			SqlParameterSource parameters = new BeanPropertySqlParameterSource(user);
			Number id = createUser.executeAndReturnKey(parameters);
			user.setId(id.intValue());
			return user;			
		}
		catch(Exception e)
		{
			throw new DaoException("create fail!", e);
		}
		
	}

	public void update(UserEntity user) throws DaoException
	{
		try {
			jdbcTemplate.update("UPDATE YASL.USER SET name=?, password=?, supplierId=?, monthlyBudget=? WHERE id=?",
				new Object[]
				{
					user.getName(), user.getPassword(), user.getSupplierId(), user.getMonthlyBudget(), user.getId()
				});		
		}
		catch(Exception e)
		{
			throw new DaoException("update fail!", e);
		}
		
	}

	public UserEntity find(int id) throws DaoException
	{
		try {
			return jdbcTemplate.queryForObject("SELECT * FROM YASL.USER WHERE id=?", new BeanPropertyRowMapper<UserEntity>(UserEntity.class), id);
		}
		catch(Exception e)
		{
			throw new DaoException("find fail!", e);
		}
		
	}

	public List<UserEntity> findAll() throws DaoException
	{
		try {
			return jdbcTemplate.query("SELECT * FROM YASL.USER", new BeanPropertyRowMapper<UserEntity>(UserEntity.class));		
		}
		catch(Exception e)
		{
			throw new DaoException("findAll fail!", e);
		}
		
	}

	public List<UserEntity> findUserByName(String user) throws DaoException
	{
		try {
			return jdbcTemplate.query("SELECT * FROM YASL.USER WHERE name LIKE ?", new BeanPropertyRowMapper<UserEntity>(UserEntity.class), "%" + user + "%");		
		}
		catch(Exception e)
		{
			throw new DaoException("findUserByName fail!", e);
		}
		
	}

	public UserEntity findUserByNameAndPassword(String userName, String password) throws DaoException
	{
		try {
			return jdbcTemplate.queryForObject("SELECT * FROM YASL.USER WHERE name=? AND password=?",
					new BeanPropertyRowMapper<UserEntity>(UserEntity.class), userName, password);		
		}
		catch(Exception e)
		{
			throw new DaoException("findUserByNameAndPassword fail!", e);
		}
	}

	public void delete(UserEntity user) throws DaoException
	{
		try {
			jdbcTemplate.update("DELETE FROM YASL.USER WHERE id=?", user.getId());
			user.setId(-1);		
		}
		catch(Exception e)
		{
			throw new DaoException("delete fail!", e);
		}
		
	}
}
