/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package qse_sepm_ss12_07.service;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author vauvenal5
 */
public class HashingService implements IHashingService
{

	public String hashPassword(String password) throws HashingServiceException
	{
		try
		{
			MessageDigest md = MessageDigest.getInstance("SHA-512");

			md.update(password.getBytes("UTF-8"));
			byte[] digest = md.digest();
			
			BigInteger bigInt = new BigInteger(1, digest);
            String output = bigInt.toString(16);
			
			return output;
		} 
		catch (NoSuchAlgorithmException ex)
		{
			throw new HashingServiceException(ex);
		} 
		catch (UnsupportedEncodingException ex)
		{
			throw new HashingServiceException(ex);
		}
	}
	
}
