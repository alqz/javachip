package qse_sepm_ss12_07.controller;

import java.awt.Dimension;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.swing.Icon;
import javax.swing.event.EventListenerList;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.MutableTreeNode;
import javax.swing.tree.TreeNode;
import org.apache.log4j.Logger;
import qse_sepm_ss12_07.domain.IContainsImage;
import qse_sepm_ss12_07.domain.Product;
import qse_sepm_ss12_07.domain.ProductCategory;
import qse_sepm_ss12_07.gui.ITreeNode;
import qse_sepm_ss12_07.gui.ITreeView;
import qse_sepm_ss12_07.service.IImageService;
import qse_sepm_ss12_07.service.IProductCategoryService;
import qse_sepm_ss12_07.service.ServiceException;

public class TreeController implements ITreeController
{
	private static final Logger logger = Logger.getLogger(TreeController.class);
	private EventListenerList eventListener = new EventListenerList();
	private ITreeView treeView;
	private IProductCategoryService productCategoryService;
	private IImageService imageService;
	private YaslTreeNode rootNode;
	
	public TreeController()
	{
		rootNode = new YaslTreeNode();
	}
	
	public void setTreeView(ITreeView treeView)
	{
		this.treeView = treeView;
		treeView.setController(this);
	}

	public void setImageService(IImageService imageService)
	{
		this.imageService = imageService;
	}

	public void setProductCategoryService(IProductCategoryService productCategoryService)
	{
		this.productCategoryService = productCategoryService;
	}
	
	public ITreeView getView()
	{
		return treeView;
	}
		
	@PostConstruct
	public void initView()
	{
		refreshTree();
	}
	
	public void refreshTree()
	{
		try
		{						
			List<ProductCategory> productCategories = productCategoryService.getAllProductCategories();						
			rootNode.removeAllChildren();		
			treeView.setRootNode(rootNode);
			
			for (ProductCategory category : productCategories)
			{
				YaslTreeNode node = new YaslTreeNode();
				node.setDataObject(category);		
				
				treeView.addTreeNode(rootNode, node);
				
				for(Product product : category.getAllProducts())
				{
					YaslTreeNode child = new YaslTreeNode();
					child.setDataObject(product);					
					node.add(child);
				}				
			}
			
			
			treeView.refresh();
		}
		catch (ServiceException exception)
		{
			logger.error("could not refresh list", exception);
		}		
	}
	
	public void productCategoryAdded(ProductCategory productCategory)
	{
		YaslTreeNode node = new YaslTreeNode();
		node.setDataObject(productCategory);		
		treeView.addTreeNode(rootNode, node);
	}

	public void productCategoryRemoved(ProductCategory productCategory)
	{
		YaslTreeNode node = (YaslTreeNode) rootNode.find(productCategory);
		treeView.removeTreeNode(node);		
	}

	public void productCategoryChanged(ProductCategory productCategory)
	{
		YaslTreeNode node = (YaslTreeNode) rootNode.find(productCategory);
		treeView.treeNodeChanged(node);
	}
	
	public void setSelectedCategoryNode(ProductCategory productCategory)
	{
		YaslTreeNode node = (YaslTreeNode) rootNode.find(productCategory);
		treeView.setSelectedNode(node);
	}

	public void productAdded(Product product)
	{
		YaslTreeNode parent = (YaslTreeNode) rootNode.find(product.getProductCategory());
		YaslTreeNode node = new YaslTreeNode();
		node.setDataObject(product);		
		treeView.addTreeNode(parent, node);
	}

	public void productRemoved(Product product)
	{
		YaslTreeNode parent = (YaslTreeNode) rootNode.find(product.getProductCategory());
		if (parent != null)
		{
			YaslTreeNode child = (YaslTreeNode) parent.find(product);
			treeView.removeTreeNode(child);
		}
	}

	public void productChanged(Product product)
	{
		YaslTreeNode parent = (YaslTreeNode) rootNode.find(product.getProductCategory());
		if (parent != null)
		{
			YaslTreeNode child = (YaslTreeNode) parent.find(product);
			treeView.treeNodeChanged(child);
		}
	}
	
	public void addTreeSelectionListener(ITreeSelectionListener listener)
	{
		eventListener.add(ITreeSelectionListener.class, listener);
	}
	
	public void removeTreeSelectionListener(ITreeSelectionListener listener)
	{
		eventListener.remove(ITreeSelectionListener.class, listener);
	}
	
	public void productCategorySelectionChanged(ProductCategory productCategory)
	{
		for(ITreeSelectionListener listener : eventListener.getListeners(ITreeSelectionListener.class))
		{
			listener.productCategorySelectionChanged(productCategory);
		}
	}

	public void producSelectionChanged(Product product)
	{	
		for(ITreeSelectionListener listener : eventListener.getListeners(ITreeSelectionListener.class))
		{
			listener.productSelectionChanged(product);
		}
	}
		
	class YaslTreeNode extends DefaultMutableTreeNode implements ITreeNode
	{
		private IContainsImage dataObject;				
		
		public Object getDataObject()
		{
			return dataObject;
		}

		@Override
		public TreeNode[] getPath()
		{
			return super.getPath();
		}
		
		
		@Override
		public Object getUserObject()
		{
			return getDataObject();
		}
		
		public void setDataObject(IContainsImage dataObject)
		{
			this.dataObject = dataObject;
		}

		public String getText()
		{
			if (dataObject instanceof Product)
			{
				return ((Product) dataObject).getName();
			}
			
			if (dataObject instanceof ProductCategory)
			{
				return ((ProductCategory) dataObject).getName();
			}
			
			return "";
		}

		public Icon getIcon()
		{
			try
			{
				if (dataObject != null)
				{
					return imageService.getSizedImageIcon(dataObject, new Dimension(15, 15));
				}
			}
			catch(ServiceException exception)
			{
				logger.warn("could not get icon", exception);
			}
			
			return null;
		}
				
		public ITreeNode find(ProductCategory productCategory)
		{
			for(Object child : children)
			{
				if (((YaslTreeNode)child).getDataObject().equals(productCategory))
				{
					return (ITreeNode) child;
				}
			}
			
			return null;
		}
		
		public ITreeNode find(Product product)
		{
			for(Object child : children)
			{
				if (((YaslTreeNode)child).getDataObject().equals(product))
				{
					return (ITreeNode) child;
				}
			}
			
			return null;
		}
		
		@Override
		public boolean equals(Object object)
		{
			if (super.equals(object))
			{
				return true;
			}
			
			if (object instanceof YaslTreeNode)
			{
				YaslTreeNode other = (YaslTreeNode) object;
				if (other.dataObject != null) 
				{
					return other.dataObject.equals(dataObject);
				}
			}
			
			return super.equals(object);
		}
	}
}
