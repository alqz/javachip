package de.baralc.springspielereien.springintegration;

import de.baralc.springspielereien.springintegration.model.Report;
import de.baralc.springspielereien.springintegration.service.ReportService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Test;

@ContextConfiguration("spring-integration.xml")
public class WorkflowTest extends AbstractTestNGSpringContextTests {

  Logger LOG = Logger.getLogger(WorkflowTest.class);

  @Autowired
  ReportService reportService;

  @Autowired
  Gateway gateway;

  @Test
  public void starteWorkflow() {

    Report report = gateway.generiereReport("peter");

    LOG.info(report);
  }

  /*@Test
  public void serviceTest() {
    Report report = reportService.generiereReportFuerBenutzer("peter");

    LOG.info(report);
  }*/
}
