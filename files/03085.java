package com.karimson.jasmine.driver;

import org.junit.Test;

import static org.junit.Assert.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class SpecResultsTest {
    
    @Test
    public void shouldParseStatusString() {
        String validString = "10 specs, 1 failures in 0.088s";
        SpecResults results = new SpecResults(validString);

        assertThat(results.getPassedSpecs(), is(10));
        assertThat(results.getFailedSpecs(), is(1));
        assertThat(results.getSecondsTaken(), is(0.088f));
        assertTrue(results.containsFailedSpecs());

        validString = "10 specs, 0 failures in 0.088s";
        results = new SpecResults(validString);

        assertThat(results.getPassedSpecs(), is(10));
        assertThat(results.getFailedSpecs(), is(0));
        assertThat(results.getSecondsTaken(), is(0.088f));
        assertFalse(results.containsFailedSpecs());
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldRaiseExceptionWhenStatusStringCannotBeParsed() {
        String invalidString = "foobar barfoo";
        SpecResults results = new SpecResults(invalidString);
    }

}
