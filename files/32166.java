package theTUIProcessor;
/**
 * This class is part of the "World of Zuul" application. 
 * "World of Zuul" is a very simple, text based adventure game.  
 *
 * This class manages the "take" command to pick up items.
 *
 * @author  Ian T. Nabney
 * @version 01-02-2005
 */

public class RunCommand extends Command
{
	private static int numWords = 1; // Command has one extra word
	private static String syntax = "take <item>";

	/**
	 * Create a command object. 
	 */
	public RunCommand()
	{
		super(numWords);
	}

	/**
	 * Return the number of words allowed in this command.
	 */
	public int numAllowedWords() {
		return numWords;
	}

	/**
	 * Return the correct syntax for this command
	 */
	public String getSyntax() {
		return syntax;
	}

	/**
	 * Make the command do its action.  
	 * Pick up an item in the current room.
	 * @return true if game is over, false otherwise.
	 */
	public boolean act() {
/*
		String itemName = getWord(0);  // Get the word
		Room currentRoom = player.getCurrentRoom();
		Item item = currentRoom.getItem(itemName);
		if (item != null) {
			if (player.canCarry(item.getWeight())) {
				player.pickUpItem(item);
				currentRoom.removeItem(item);
			}
			else {
				System.out.println("It's too heavy.");
			}
		}
		else {
			System.out.println("That item is not in this room.");
		}
*/
		return false;  // continue the game
	}


}

