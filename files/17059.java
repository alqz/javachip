/**
 * Copyright 1999-2009 The Pegadi Team
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.pegadi.sources.search;

import org.pegadi.sqlsearch.SearchTerm;

/**
 * This class represents a search based on the notes of a source.
 *
 * @version $Revision$, $Date$
 * @author Eirik Bjorsnos <bjorsnos@underdusken.no>
 */
public class NotesTerm extends SearchTerm implements java.io.Serializable {

    private String pattern;

    /**
     * Constructor that takes the note-string to search for
     */
    public NotesTerm(String pattern) {
        this.pattern=pattern;
    }

    public String whereClause() {
        return "Source_person.notes like '%" +pattern +"%'";
    }

    public String[] getTables() {
        String[] tables = new String[1];

        tables[0] = "Source_person";

        return tables;
    }
}

