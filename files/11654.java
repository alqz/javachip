/**
 * Copyright 1999-2009 The Pegadi Team
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.pegadi.server.log;

import org.pegadi.model.Article;
import org.pegadi.server.LogServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class LogServerImpl extends NamedParameterJdbcDaoSupport implements LogServer {

    Logger log = LoggerFactory.getLogger(getClass());

    public void logSaveArticle(Article article) {

        if (article == null) { //huh - no article?
            log.error("Can't logsave a non-existing article.");
        } else {
            String sql = "INSERT INTO ArticleSaveLog (refArticle, name, refJournalist, refPhotographer, refPublication, description, " +
                    "refStatus, refArticleType, wantedCharacters, wantedPages, refDepartment, characterCount, savetime) " +
                    "VALUES(:article,:name,:journalist,:photographer,:publication,:description,:status,:type,:wantedchars,:wantedpages,:section,:charcount,:savetime)";

            Map<String, Object> params = new HashMap<String, Object>();
            params.put("article", article.getId());

            if (article.getName() != null)
                params.put("name", article.getName());
            else
                params.put("name", article.getName());

            if (article.getJournalist() != null)
                params.put("journalist", article.getJournalist().getUsername());
            else
                params.put("journalist", null);

            if (article.getPhotographer() != null)
                params.put("photographer", article.getPhotographer().getUsername());
            else
                params.put("photographer", null);

            if (article.getPublication() != null)
                params.put("publication", article.getPublication().getId());
            else
                params.put("publication", 0);
            if (article.getDescription() != null)
                params.put("description", article.getDescription());
            else
                params.put("description", "");
            if (article.getArticleStatus() != null)
                params.put("status", article.getArticleStatus().getId());
            else
                params.put("status", 1);

            if (article.getArticleType() != null)
                params.put("type", article.getArticleType().getId());
            else
                params.put("type", 0);

            params.put("wantedchars", article.getWantedNumberOfCharacters());
            params.put("wantedpages", article.getWantedNumberOfPages());
            if (article.getSection() != null)
                params.put("section", article.getSection().getId());
            else
                params.put("section", 0);
            params.put("charcount", article.getCurrentNumberOfCharacters());

            params.put("savetime", new Date());
            try {
                getNamedParameterJdbcTemplate().update(sql, params);
                log.info("Saved log for saving article {}", article.getId());
            } catch (DataAccessException e) {
                log.error("SQL-error logging article save", e);
            }
        }
    }

    public void logLogin(String sessionKey, String ipAddress, String username) {
        log.debug("Logging login {} {} {}", new String[]{sessionKey, ipAddress, username});
        try {
            getJdbcTemplate().update("INSERT INTO SessionLog (sessionKey, userID, ipAddress, loginTime) VALUES(?,?,?,?)",
                    sessionKey, username, ipAddress, new Date());
            log.debug("Logged login {} {} {}", new String[]{sessionKey, ipAddress, username});
        } catch (DataAccessException e) {
            log.error("SQL-error logging login", e);
        }
    }

    public void logLogout(String sessionKey) {
        try{
            getJdbcTemplate().update("UPDATE SessionLog SET logoutTime=? WHERE sessionKey=?", new Date(), sessionKey);
            log.info("Logged user logging out");
        } catch (DataAccessException e) {
            log.error("SQL-error logging logout", e);
        }
    }
}
