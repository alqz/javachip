package oxen.commands.user;

import javax.swing.JOptionPane;
import oxen.commands.Command;
import oxen.game.Game;
import oxen.game.components.dialogue.Dialogue;
import oxen.state.GameState;

/**
 * The START command; starts a new game.
 * @author matt
 */
public class StartCommand extends Command {
    /**
     * One version of the constructor. Takes a game manager.
     */
    String playerName;
    public StartCommand(Game manager) {
        super(manager);
    }
    /**
     * This constructor is used for TESTING ONLY.
     * @param manager
     * @param pane 
     */
    public StartCommand(Game manager, String playerName){
        super(manager);
        this.playerName = playerName;
    }

    /**
     * Returns the help description for this command.
     * @return the help String.
     */
    @Override
    public String getHelpDescription() {
        return "START - Starts a new game";
    }

    /**
     * Asks whether this command can respond to the current input form the user.
     * @param input from the user.
     * @return a boolean indicating whether this command can respond to the input.
     */
    @Override
    public boolean canRespondTo(String input) {
        return input.equalsIgnoreCase("START");
    }

    /**
     * Executes the functionality for this command; starting the game in this case.
     * @param input from the user.
     */
    @Override
    public void respondTo(String input) {
        //Ask the player what they want their name to be. . .
        if (playerName == null) {
            playerName = JOptionPane.showInputDialog(null, "What is your name?", "Your name?", JOptionPane.DEFAULT_OPTION);
        }
        //If the player is being all stubborn and wants to remain nameless . . . GAME DENIED.
        if (playerName == null) {
            getLogger().logOutput("Game start cancelled, you must set a player name.");
        } else if (playerName.equals("")) {         //GAME DENIED.
            getLogger().logOutput("Your name cannot be blank!");
        } else if (playerName.length() > 25) {      //If the name the user selects is greater than 25 characters.. GAME DENIED.
            getLogger().logOutput("Your name is limited to 25 characters!");
        } else {
            //Set the player name in stone.
            GameState newState = getManager().getStateManager().loadDefaultState(getManager());
            if (newState != null) {
            getState().getPlayer().setName(playerName);
            getState().getIntialDescription().dictate(getManager());

            // Dictate the new room's entry dialogue.
            Dialogue entryDialogue = getCurrentRoom().getEntryDialogue();

            if (entryDialogue != null) {
                entryDialogue.dictate(getManager());
            }

            getManager().setupCommandsForState();
            getLogger().logOutput("");
        } else {
                getLogger().logOutput("Game file is invalid.");
            }
        }
        playerName = null;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
