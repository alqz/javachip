/**
 * Copyright 1999-2009 The Pegadi Team
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*
 * @(#)GraphTreeModel.java      1.0.5 06/01/03
 *
 * Copyright (C) 2002 Gaudenz Alder
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */

package org.pegadi.storysketch;

import org.jgraph.event.GraphModelEvent;
import org.jgraph.event.GraphModelListener;
import org.jgraph.graph.GraphModel;
import org.pegadi.storysketch.cells.StorySketchCell;

import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeNode;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

public class GraphTreeModel extends DefaultTreeModel implements GraphModelListener {

    public GraphTreeModel(GraphModel model) {
        super(new CellTypesTreeNode(model));
    }

    public void graphChanged(GraphModelEvent e) {
        reload();
    }

    public static class CellTypesTreeNode implements TreeNode {

        protected GraphModel model;

        String[] classes = null;

        public CellTypesTreeNode(GraphModel model) {
            this.model = model;
        }

        public Enumeration children() {
            Vector l = new Vector();
            String[] types = getTypes();
            l.addAll(Arrays.asList(types));
            return l.elements();
        }

        public String[] getTypes() {
            Hashtable types = new Hashtable();

            for (int i = 0; i < model.getRootCount(); i++) {
                Object r = model.getRootAt(i);
                if(r instanceof StorySketchCell) {
                    String key = ((StorySketchCell)r).getClassDescription();

                    // Add to types if it doesn't already exist
                    if(!types.containsKey(key)) {
                        Vector type = new Vector();
                        type.add(r);
                        types.put(key,type);
                    }
                    else {
                        Vector type = (Vector) types.get(key);
                        type.add(r);
                    }
                }
            }

            return (String[]) types.keySet().toArray(new String[types.keySet().size()]);
        }

        public boolean getAllowsChildren() {
            return true;
        }

        public TreeNode getChildAt(int childIndex) {
            return new SingleTypeTreeNode(model,classes[childIndex]);
        }

        public int getChildCount() {
            classes = getTypes();
            return classes.length;
        }

        public int getIndex(TreeNode node) {
            return model.getIndexOfRoot(node);
        }

        public TreeNode getParent() {
            return null;
        }

        public boolean isLeaf() {
            return false;
        }

        public String toString() {
            return model.toString();
        }

    }

    public static class SingleTypeTreeNode implements TreeNode {

        protected GraphModel model;

        String description;

        public SingleTypeTreeNode(GraphModel model, String description) {
            this.model = model;
            this.description = description;
        }

        public Enumeration children() {
            return (new Vector()).elements();
        }

        public boolean getAllowsChildren() {
            return true;
        }

        public TreeNode[] getChildren() {

            Vector nodes = new Vector();

            for(int i = 0; i < model.getRootCount(); i++) {
                TreeNode n = (TreeNode) model.getRootAt(i);
                if(n instanceof StorySketchCell) {
                    if(((StorySketchCell) n).getClassDescription().equals(description)) {
                        nodes.add(n);
                    }
                }
            }

            return (TreeNode[]) nodes.toArray(new TreeNode[nodes.size()]);
        }

        public TreeNode getChildAt(int childIndex) {
            return getChildren()[childIndex];
        }

        public int getChildCount() {
            return getChildren().length;
        }

        public int getIndex(TreeNode node) {
            return -1;
        }

        public TreeNode getParent() {
            return null;
        }

        public boolean isLeaf() {
            return false;
        }

        public String toString() {
            return description;
        }
    }
}
