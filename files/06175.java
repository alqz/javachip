package org.zeroturnaround.ui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.io.IOException;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.zeroturnaround.Akt2;

public class ConversionTab extends JPanel {
  private JTextField sourceFileTextField;

  public ConversionTab() {
    initPane();
  }

  private void initPane() {
    // 1st row
    GridBagLayout gbl = new GridBagLayout();
    GridBagConstraints gc = new GridBagConstraints();
    setLayout(gbl);

    JLabel label = new JLabel("Source");
    gc.fill = GridBagConstraints.HORIZONTAL;
    gc.weightx = 1.0;
    gc.gridx = 0;
    gc.gridy = 0;
    gc.insets = new Insets(0, 20, 0, 0);
    add(label, gc);

    String tmpPath = System.getProperty("user.home") + System.getProperty("file.separator") + "tmp";
    sourceFileTextField = new JTextField(tmpPath + System.getProperty("file.separator") + "private.pem");
    sourceFileTextField.setEnabled(false);
    gc.insets = new Insets(0, 0, 0, 20);
    gc.gridx = 1;
    gc.gridy = 0;
    add(sourceFileTextField, gc);

    // 2nd row
    label = new JLabel("Type");
    gc.insets = new Insets(0, 20, 0, 0);
    gc.gridx = 0;
    gc.gridy = 1;
    add(label, gc);

    JTextField tmpTextField = new JTextField("PEM x509 Certificate");
    tmpTextField.setEnabled(false);
    gc.insets = new Insets(0, 0, 0, 20);
    gc.gridx = 1;
    gc.gridy = 1;
    add(tmpTextField, gc);

    // 3rd row

    label = new JLabel("Convert");
    gc.insets = new Insets(0, 20, 0, 0);
    gc.gridx = 0;
    gc.gridy = 2;
    add(label, gc);

    String[] petStrings = { "Choose", "PEM" };
    JComboBox petList = new JComboBox(petStrings);
    gc.insets = new Insets(0, 0, 0, 20);
    gc.gridx = 1;
    gc.gridy = 2;
    add(petList, gc);
  }

  public void updateToModel() {
    if (Akt2.conversionModel.getInputFile() != null) {
      sourceFileTextField.setText(Akt2.conversionModel.getInputFile().getName());
      try {
        sourceFileTextField.setToolTipText(Akt2.conversionModel.getInputFile().getCanonicalPath());
      }
      catch (IOException e) {
        e.printStackTrace();
      }
    }
    else {
      sourceFileTextField.setText("");
    }
  }
}
