package entities.security;

import java.util.Set;

import javax.persistence.*;


@Entity
@Table(name="user")
@NamedQuery(name="username",
query="SELECT u from User u WHERE u.username=:username")


public class User {

	@Id
	@Column(name="userid",
				unique=true, nullable=false)
	@JoinColumn(name="agent_id")
	private int userid;
	
	@Column(name="username",
			unique=true, nullable=false, length=64)
	private String username;
	
	@Column(name="password", 
			nullable=false, length=64)
	private String password;
	
	@ManyToMany
	@JoinTable(name="usergroup",
	joinColumns={@JoinColumn(name="userid", nullable=false)},
	inverseJoinColumns = {@JoinColumn(name="groupid", nullable=false)})
	private Set<Group> userGroup;

	public int getUserid() {
		return userid;
	}

	public void setUserid(int userId) {
		this.userid = userId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Set<Group> getUserGroup() {
		return userGroup;
	}

	public void setUserGroup(Set<Group> userGroup) {
		this.userGroup = userGroup;
	}
	
	
}
