package com.sk89q.regionbook.bukkit.listeners;

import com.sk89q.regionbook.bukkit.RegionBookPlugin;
import org.bukkit.event.world.WorldListener;
import org.bukkit.event.world.WorldLoadEvent;
import org.bukkit.event.world.WorldUnloadEvent;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class RegionBookWorldListener extends WorldListener {
    private final RegionBookPlugin plugin;
    
    public RegionBookWorldListener(RegionBookPlugin plugin) {
        this.plugin = plugin;
    }
    
    public void registerEvents() {
        plugin.registerEvent("WORLD_LOAD", this);
        plugin.registerEvent("WORLD_UNLOAD", this);
    }
    
    @Override
    public void onWorldLoad(WorldLoadEvent event) {
        try {
            plugin.getGlobalRegionManager().load(event.getWorld());
        } catch (Exception e) {
            Logger.getLogger("Minecraft.RegionBook").log(Level.SEVERE,"Unable to load regions for world" + event.getWorld().getName(), e);
        }
    }
    
    @Override
    public void onWorldUnload(WorldUnloadEvent event) {
        plugin.getGlobalRegionManager().unload(event.getWorld());
    }
}
