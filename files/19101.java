/*
 * Copyright 2011 Objectos, Fábrica de Software LTDA.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package br.com.objectos.comuns.relational;

import static br.com.objectos.comuns.base.Dates.newLocalDate;
import static com.google.common.collect.Lists.newArrayList;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import java.util.List;

import org.joda.time.LocalDate;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import br.com.objectos.comuns.relational.BancoDeDados;
import br.com.objectos.comuns.relational.Registro;
import br.com.objectos.comuns.relational.Registros;
import br.com.objectos.comuns.relational.RegistrosGuice;

import com.google.common.collect.ImmutableList;

/**
 * @author marcio.endo@objectos.com.br (Marcio Endo)
 */
@Test
public class TesteDeRegistros {

  private Registros registros;

  private Asserts asserts;

  @BeforeClass
  public void prepararRegistros() {
    BancoDeDados bancoDeDados = new BancoDeDadosFalso();

    registros = new RegistrosGuice(bancoDeDados);
  }

  @BeforeMethod
  public void reiniciar() {
    asserts = new Asserts();
  }

  public void adicionarCasoInicial() {
    List<Registro> existentes = ImmutableList.of();
    Registro novoRegisto = novoRegistro(2010, 2011, "true");

    registros.adicionar(existentes, novoRegisto);

    assertEquals(asserts.atualizados.size(), 1);
    assertEquals(asserts.excluidos.size(), 0);

    assertEquals(asserts.atualizados.get(0), novoRegisto);
  }

  public void adicionarVerifiqueRemocaoDeDatasRepetidas() {
    Registro r0 = novoRegistro(2010, 1999, "true");
    Registro r1 = novoRegistro(2011, 1999, "false");
    Registro r2 = novoRegistro(2012, 1999, "true");
    List<Registro> existentes = ImmutableList.of(r0, r1, r2);

    Registro rn = novoRegistro(2011, 2000, "false");

    List<Registro> resultado;
    resultado = registros.adicionar(existentes, rn);

    assertEquals(asserts.atualizados.size(), 3);
    assertEquals(resultado.size(), 3);

    assertTrue(resultado.contains(r0));
    assertTrue(resultado.contains(rn));
    assertTrue(resultado.contains(r2));

    assertEquals(asserts.excluidos.size(), 1);
    assertTrue(asserts.excluidos.contains(r1));
  }

  public void adicioanrVerifiqueRemocaoDeRepetidas() {
    Registro r0 = novoRegistro(2010, 1999, "true");
    Registro r1 = novoRegistro(2011, 1999, "false");
    Registro r2 = novoRegistro(2013, 1999, "true");
    List<Registro> existentes = ImmutableList.of(r0, r1, r2);

    Registro rn = novoRegistro(2012, 2000, "false");

    List<Registro> resultado;
    resultado = registros.adicionar(existentes, rn);

    assertEquals(asserts.atualizados.size(), 3);
    assertEquals(resultado.size(), 3);

    assertTrue(resultado.contains(r0));
    assertTrue(resultado.contains(rn));
    assertTrue(resultado.contains(r2));

    assertEquals(asserts.excluidos.size(), 1);
    assertTrue(asserts.excluidos.contains(r1));
  }

  public void removerCasoTrivialDeUmUnicoRegistro() {
    Registro registro = novoRegistro(2010, 2011, "true");

    List<Registro> existentes = ImmutableList.of(registro);

    registros.remover(existentes, registro);

    assertEquals(asserts.atualizados.size(), 0);
    assertEquals(asserts.excluidos.size(), 1);

    assertEquals(asserts.excluidos.get(0), registro);
  }

  public void remocaoDoRegistroFinalDeveAtualizarDataFinalDoPenultimo() {
    Registro r0 = novoRegistro(2010, 1999, "true");
    Registro r1 = novoRegistro(2011, 1999, "false");
    Registro r2 = novoRegistro(2013, 1999, "true");
    List<Registro> existentes = ImmutableList.of(r0, r1, r2);

    List<Registro> resultado;
    resultado = registros.remover(existentes, r2);

    assertEquals(asserts.atualizados.size(), 2);
    assertEquals(resultado.size(), 2);

    assertTrue(resultado.contains(r0));
    assertTrue(resultado.contains(r1));

    assertEquals(asserts.excluidos.size(), 1);
    assertTrue(asserts.excluidos.contains(r2));
  }

  private Registro novoRegistro(int ano, int anoCriacao, String propriedade) {
    LocalDate dataInicial = newLocalDate(ano, 12, 1);
    LocalDate dataDeCriacao = newLocalDate(anoCriacao, 12, 1);

    return new ConstrutorDeRegistroQualquerFalso().dataInicial(dataInicial)
        .dataDeCriacao(dataDeCriacao).propriedade(propriedade).novaInstancia();
  }

  private class Asserts {
    List<Registro> atualizados = newArrayList();
    List<Registro> excluidos = newArrayList();
  }

  private class BancoDeDadosFalso extends BancoDeDadosVazio {
    @Override
    public <E> E atualizar(E entidade) {
      asserts.atualizados.add((Registro) entidade);
      return entidade;
    }

    @Override
    public void remover(Object entidade) {
      asserts.excluidos.add((Registro) entidade);
    }
  }

}