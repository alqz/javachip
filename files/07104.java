/*
 */
package qse_sepm_ss12_07.controller;

import javax.swing.JPanel;
import org.apache.log4j.Logger;
import qse_sepm_ss12_07.gui.IErrorView;
import qse_sepm_ss12_07.gui.IMainView;
import qse_sepm_ss12_07.service.IBeanService;

/**
 *
 */
public class AppController
{

	private Logger logger = Logger.getLogger(AppController.class);
	private IMainView view;
	private IErrorView errorView;
	private IBeanService beanService;

	public void setMainView(IMainView view)
	{
		this.view = view;
		view.setController(this);
	}

	public void setBeanService(IBeanService service)
	{
		this.beanService = service;
	}

	public void setErrorView(IErrorView view)
	{
		this.errorView = view;
	}

	public void run()
	{
		logger.info("Showing main view");
		setLoginView();		
		view.showView();
	}

	public void setFormsAndViews()
	{
		MainTabViewController mainTabViewcontroller = beanService.getBean(MainTabViewController.class);
		view.setContainedView(mainTabViewcontroller.getView());

		StockController stockController = beanService.getBean(StockController.class);
		mainTabViewcontroller.registerController(stockController);
		
		//ShoppingListGeneratorController shoppingListGeneratorController = beanService.getBean(ShoppingListGeneratorController.class);
		//mainTabViewcontroller.registerController(shoppingListGeneratorController);
		
		//RecipeProposalController recipeProposalController = beanService.getBean(RecipeProposalController.class);
		//mainTabViewcontroller.registerController(recipeProposalController);
				
		ProductCategoryController productCategoryController = beanService.getBean(ProductCategoryController.class);
		mainTabViewcontroller.registerController(productCategoryController);

		SupplierController supplierController = beanService.getBean(SupplierController.class);
		mainTabViewcontroller.registerController(supplierController);

		UnitController unitController = beanService.getBean(UnitController.class);
		mainTabViewcontroller.registerController(unitController);

		RecipeController recipeController = beanService.getBean(RecipeController.class);
		mainTabViewcontroller.registerController(recipeController);
		
		ShoppingListController shoppingListController = beanService.getBean(ShoppingListController.class);		
		mainTabViewcontroller.registerController(shoppingListController);
		
		HouseholdStatisticsController householdStatisticsController = beanService.getBean(HouseholdStatisticsController.class);
		mainTabViewcontroller.registerController(householdStatisticsController);
		
		UserStatisticsController userStatisticsController = beanService.getBean(UserStatisticsController.class);
		mainTabViewcontroller.registerController(userStatisticsController);
		
		CheckController checkController = beanService.getBean(CheckController.class);
		mainTabViewcontroller.registerController(checkController);
		
		UserPreferencesController userPreferencesController = beanService.getBean(UserPreferencesController.class);
		mainTabViewcontroller.registerController(userPreferencesController);
		
		
	}

	public void setLoginView()
	{
		LoginController mainLoginController = beanService.getBean(LoginController.class);
		view.setContainedView(mainLoginController.getView());
	}
}
