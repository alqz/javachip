package view;
import javax.media.opengl.GL2;
import javax.media.opengl.glu.GLU;


public class CampusViewCamera {
	
	public float e_x;
	public float e_y;
	public float e_z;
	
	public float c_x;
	public float c_y;
	public float c_z;
	
	public void set_eye(float x, float y, float z) {
		e_x = x;
		e_y = y;
		e_z = z;
	}
	
	public void set_center(float x, float y, float z) {
		c_x = x;
		c_y = y;
		c_z = z;
	}
	
	public void gl_draw_camera(GL2 gl, GLU glu) {
    	CLib.gl_setCamera(gl,glu,
    					  e_x, e_y, e_z,
    					  c_x, c_y, c_z,
    					  	0,	 0,   1);
	}
	
	public String toString() {
		return String.format("E(%1.1f,%1.1f,%1.1f) C(%1.1f,%1.1f,%1.1f)", e_x, e_y, e_z, c_x, c_y, c_z);
	}

}
