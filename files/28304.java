package org.rsbot.script.wrappers;

import org.rsbot.bot.Context;
import org.rsbot.bot.accessors.Client;
import org.rsbot.bot.accessors.RSProjectile;

import java.awt.*;
import java.lang.ref.SoftReference;

public class Projectile implements Locatable, Targetable {
	protected SoftReference<org.rsbot.bot.accessors.RSProjectile> accessor;

	public Projectile(org.rsbot.bot.accessors.RSProjectile paramProjectile) {
		this.accessor = new SoftReference<RSProjectile>(paramProjectile);
	}

	public boolean isValid() {
		return this.accessor.get() != null;
	}

	public Tile getLocation() {
		org.rsbot.bot.accessors.RSProjectile localProjectile = this.accessor.get();
		if (localProjectile == null) {
			return new Tile(-1, -1);
		}
		final Client client = Context.get().client;
		int i = client.getBaseX() + ((int) localProjectile.getLocalX() >> 9);
		int j = client.getBaseY() + ((int) localProjectile.getLocalY() >> 9);
		return new Tile(i, j, localProjectile.getPlane());
	}

	public Tile getSpawn() {
		org.rsbot.bot.accessors.RSProjectile localProjectile = this.accessor.get();
		if (localProjectile == null) {
			return new Tile(-1, -1);
		}
		final Client client = Context.get().client;
		int i = client.getBaseX() + (localProjectile.getX() >> 9);
		int j = client.getBaseY() + (localProjectile.getY() >> 9);
		return new Tile(i, j, localProjectile.getPlane());
	}

	public double getHeight() {
		return this.accessor.get().getLocalZ();
	}

	public GameModel getModel() {
		org.rsbot.bot.accessors.RSProjectile localProjectile = this.accessor.get();
		if (localProjectile != null) {
			org.rsbot.bot.accessors.Model localModel = localProjectile.getModel();
			if (localModel != null) {
				return new ProjectileModel(localModel, localProjectile);
			}
		}
		return null;
	}

	public Point getPoint() {
		return getLocation().toScreen((int) (getHeight() / 2.0D));
	}

	public boolean contains(final Point p) {
		return getLocation().contains(p);
	}

	public void draw(final Graphics render) {
		final Tile location = getLocation();
		final int height = (int) Math.round(getHeight() / 2);
		final Point p = location.toScreen(height);
		render.fillRect((int) p.getX() - 5, (int) p.getY() - 5, 10, 10);
	}
}
