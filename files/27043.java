//********************************************************************************************
// ARconfig.java
// Adam McManigal
// This class contains constants used often in the metaio SDK.
//********************************************************************************************

package com.ParadigmLearningSystems;

public abstract class ARconfig 
{
	public static final String signature = "iHudGbiaREZcEUBFoX9yBX5+2Ianwi1bbAo8LlONwLU=";

	public abstract class Camera
	{
		public static final long resolutionX = 640;  	
		public static final long resolutionY = 480;
		//-----------------------------------------
		// 0: normal camera
		// 1: front facing camera
		 //----------------------------------------
		public static final int deviceId = 0;
	}
}
