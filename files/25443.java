import java.util.ArrayList;

public abstract class VendingMachine {
	protected int
		dollars, quarters, dimes, nickels, currentPaid,
		initQuarters, initDimes, initNickels,
		totalDollars, totalQuarters, totalDimes, totalNickels,
		changeDollars = 0, changeQuarters = 0, changeDimes = 0, changeNickels = 0;
	// First itemSelector will correspond to first inventory item
	protected ArrayList<String>
		itemSelectors;
	//use Inventory class instead of ArrayList<Product>, maybe?
	protected Inventory
		inventory;
	protected ArrayList<Products>
		products = new ArrayList<Products>();
	protected ArrayList<Transaction>
		transactions = new ArrayList<Transaction>();
	protected String
		name, model;
	
	//use Inventory class instead of ArrayList<Product>, maybe?
	public VendingMachine() { 
		this.name = "N/A: Unnamed";
		this.inventory = new Inventory(); 
		this.initQuarters = 0;
		this.totalQuarters = this.initQuarters;
		this.initDimes = 0;
		this.totalDimes = this.initDimes;
		this.initNickels = 0;
		this.totalNickels = this.initNickels;
	}
	public VendingMachine(ArrayList<String> itemSelectors, Inventory inventory,
							int quarters, int dimes, int nickels) { 
		this.initQuarters = quarters;
		this.totalQuarters = this.initQuarters;
		this.initDimes = dimes;
		this.totalDimes = this.initDimes;
		this.initNickels = nickels;
		this.totalNickels = this.initNickels;
		this.itemSelectors = itemSelectors;
		this.inventory = inventory;
	}
	
	//public void setInventory(ArrayList<Products> inventory) { this.inventory = inventory; }
	public void setItemSelectors(ArrayList<String> itemSelectors) { this.itemSelectors = itemSelectors; }
	public String getModel() { return this.model; }
	public String getName() { return this.name; }
	public void setName(String name) { this.name = name; }
	
	public int getQuarters() { return this.quarters; }
	public int getDimes() { return this.dimes; }
	public int getNickels() { return this.nickels; }
	public ArrayList<String> getItemSelectors() { return this.itemSelectors; }
	public Inventory getInventory() { return this.inventory; }
	
	public abstract void takeMoney();
	public abstract void pickItem(String selector);
	public abstract void report();
	/*
	public void selectItem(String itemSelector) {
		if (this.itemSelectors.contains(itemSelector)) {
			System.out.format("You entered an amount of %d cents\n" +
								"The cost of this item is %d cents\n" +
								"Thank you and please take your item.\n", 
								this.currentPaid, this.inventory.get(this.itemSelectors.indexOf(itemSelector)).getPrice());
																// index of itemSelector should be same as index of product we want to get
		}
		
	}
	*/
	public void printProducts() {
		System.out.println("Available Products: ");
		for (int i = 0; i < this.getInventory().getProducts().size(); i++) {
			System.out.format("%s %d %s\n", this.getItemSelectors().get(i),
								this.getInventory().getProducts().get(i).getPrice(),
								this.getInventory().getProducts().get(i).getDescription());
		}
	}
	public String toString() {
		return String.format("This vending machine has: %d products.\n", inventory.inventoryAmount());
	}
}
