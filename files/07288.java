package _2_v220.views;

import _2_v220.controllers.*;
import java.awt.*;
import javax.swing.*;


public class MenuView extends MenuView_Extender
{
	public Menu_Controller 		controller;
	public JFrame				frame;
	
	public Test1Controller 		panel1;
	public Test2Controller 		panel2;
	public JButton				a, b, c, d;
	
	
	public MenuView( Menu_Controller controller, JFrame frame )
	{
		super( frame, 300, 160 );
		
		JLabel version = new JLabel( "v2.20" );
		version.setForeground( Color.WHITE );
		version.setBounds( 6, 0, 70, 25 );
		add( version );
		
		a = new JButton( "Down" );
		a.addActionListener( controller );
		a.setBounds( 25, 40, 120, 25 );
		add( a );
		
		b = new JButton( "Up" );
		b.addActionListener( controller );
		b.setBounds( 155, 40, 120, 25 );
		add( b );
		
		c = new JButton( "Go Mad" );
		c.addActionListener( controller );
		c.setBounds( 25, 70, 250, 25 );
		add( c );
		
		d = new JButton( "Change" );
		d.addActionListener( controller );
		d.setBounds( 25, 100, 250, 25 );
		add( d );
		
		panel1 = new Test1Controller( frame );
		panel2 = new Test2Controller( frame );
		add( panel1.getView() );
	}
	
	
	public void paintComponent( Graphics g )
	{
		super.paintComponent( g );
		
		g.setColor( Color.BLUE );
		g.fillRect( 0, 0, width, height );
	}
}
