package qse_sepm_ss12_07.gui;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.ResourceBundle;
import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.plaf.basic.BasicTreeUI;
import javax.swing.table.AbstractTableModel;
import net.miginfocom.swing.MigLayout;
import org.apache.log4j.Logger;
import qse_sepm_ss12_07.controller.ProductListContentController;
import qse_sepm_ss12_07.domain.ProductList;
import qse_sepm_ss12_07.domain.ProductListEntry;

/**
 * implementation of IProductListContentView
 */
public class ProductListContentView extends JPanel implements IProductListContentView
{

	private static Logger logger = Logger.getLogger(ProductListContentView.class);
	private ResourceBundle labels = ResourceBundle.getBundle("yasl_strings");
	private ProductListContentController controller;
	private JPanel treePanel;
	private JTable ingredientsTable;
	private JButton addProductButton;
	private JButton removeProductButton;
	private ProductListContentActionListener actionListener;
	private ProductListContentTableModel tableModel;
	private ProductList productList;
	private RecipeProductEntrySelectionListener selectionListener;
	
	public ProductListContentView()
	{
		super(new MigLayout("fill"));
		treePanel = new JPanel(new MigLayout("fill"));

		actionListener = new ProductListContentActionListener();
		selectionListener = new RecipeProductEntrySelectionListener();

		addProductButton = new JButton();
		addProductButton.setIcon(createImageIcon("/images/add.png", ""));
		addProductButton.addActionListener(actionListener);

		removeProductButton = new JButton();
		removeProductButton.setIcon(createImageIcon("/images/delete.png", ""));
		removeProductButton.addActionListener(actionListener);

		tableModel = new ProductListContentTableModel();
		ingredientsTable = new JTable(tableModel);
		ingredientsTable.setFillsViewportHeight(true);
		ingredientsTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		ingredientsTable.getSelectionModel().addListSelectionListener(selectionListener);
		
		add(treePanel, "growy");
		add(addProductButton, "split 2, flowy");
		add(removeProductButton);
		add(new JScrollPane(ingredientsTable), "span, grow, wrap");
	}

	/**
	 * Returns an ImageIcon, or null if the path was invalid.
	 */
	private ImageIcon createImageIcon(String path,
			String description)
	{
		java.net.URL imgURL = getClass().getResource(path);
		if (imgURL != null)
		{
			return new ImageIcon(imgURL, description);
		}
		else
		{
			logger.warn("could not load icon " + path);
			return null;
		}
	}

	public void setController(ProductListContentController controller)
	{
		this.controller = controller;
	}

	public void setTreeView(ITreeView treeView)
	{
		JScrollPane treeScrollPane = new JScrollPane((JComponent) treeView);
		treeScrollPane.setMinimumSize(new Dimension (100,100));
		treePanel.removeAll();
		treePanel.add(treeScrollPane, "growy");
	}

	public void setProductList(ProductList productList)
	{
		if (productList != null)
		{
			this.productList = productList;
		}
	}

	public void productListContentChanged()
	{
		tableModel.fireTableDataChanged();
	}
	
	class ProductListContentActionListener implements ActionListener
	{

		public void actionPerformed(ActionEvent e)
		{
			if (e.getSource() == addProductButton)
			{
				controller.addProductEntry();
			}

			if (e.getSource() == removeProductButton)
			{
				controller.removeProductEntry();
			}
		}
	}

	class ProductListContentTableModel extends AbstractTableModel
	{

		public int getRowCount()
		{
			if (productList == null || productList.getProductListEntrys() == null)
			{
				return 0;
			}
			return productList.getProductListEntrys().size();
		}

		public int getColumnCount()
		{
			return 2;
		}

		@Override
		public boolean isCellEditable(int rowIndex, int columnIndex)
		{
			return (rowIndex < getRowCount() && rowIndex >= 0 && columnIndex == 1);
		}

		
		
		@Override
		public void setValueAt(Object aValue, int rowIndex, int columnIndex)
		{
			if (rowIndex < getRowCount() && rowIndex >= 0)
			{
				if (columnIndex == 1)
				{
					// change amount
					controller.changeAmount((Double)aValue);
				}
			}
		}

		@Override
		public Class getColumnClass(int column)
		{
			switch (column)
			{
				case 0:
					return String.class;
				case 1:
					return Double.class;
				default:
					return null;
			}
		}

		@Override
		public String getColumnName(int column)
		{
			switch (column)
			{
				case 0:
					return labels.getString("recipe_edit_column_product");
				case 1:
					return labels.getString("recipe_edit_column_amount");
				default:
					return "";
			}
		}

		public Object getValueAt(int row, int column)
		{
			if (row > getRowCount())
			{
				return null;
			}
			ProductListEntry entry = productList.getProductListEntrys().get(row);
			switch (column)
			{
				case 0:
					return entry.getName();
				case 1:
					return entry.getAmount();
				default:
					return null;
			}
		}
	}

	class RecipeProductEntrySelectionListener implements ListSelectionListener
	{

		public void valueChanged(ListSelectionEvent e)
		{
			if(!e.getValueIsAdjusting() && ingredientsTable.isEditing())
			{
				controller.startEditing();
				ingredientsTable.getSelectionModel().setValueIsAdjusting(true);
				ingredientsTable.setRowSelectionInterval(ingredientsTable.getEditingRow(), ingredientsTable.getEditingRow());
				ingredientsTable.getSelectionModel().setValueIsAdjusting(false);
			}
			
			if (!e.getValueIsAdjusting())
			{
				controller.finishEditing();
				int selectedRow = ingredientsTable.getSelectedRow();
				List<ProductListEntry> entries = productList.getProductListEntrys();
				if (selectedRow >= 0 && selectedRow < entries.size())
				{
					controller.productListEntryChanged(entries.get(selectedRow));
				}
				else
				{
					controller.productListEntryChanged(null);
				}
			}
		}
	}
}
