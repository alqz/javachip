package db.server;

import java.io.Serializable;

public class category implements Serializable {

    private String name = null;     //name of the category ie: test, quiz, etc..
    
    private String id = null;
    private String courseID = null;
    private String schemeID = null;
    
    private double maxScore = 0;        //set the max score of the category ???
    private double weight = 0;         //weight of the category, as a precentage.
   
    private int strategy;
    private int drops;

    private char command = 'R';

    //constructor
    category() {
    }

    public String getSchemeID() {
        return schemeID;
    }

    public void setSchemeID(String schemeID) {
        this.schemeID = schemeID;
    }
    

    //sets the name of the category
    public void setName(String name) {
        this.name = name;
    }

    //returns the name of the category as a string
    public String getName() {
        return name;
    }

    //set the weight in precent
    public void setWeight(double weight) {
        this.weight = weight;
    }

    //returns the weight precentage of the category as an int
    public double getWeight() {
        return weight;
    }

    //returns the category id
    public String getID() {
        return id;
    }

    //sets the category id
    public void setID(String input) {
        this.id = input;
    }

    //sets the command code
    public boolean setCommand(char command) {
        command = Character.toUpperCase(command);

        //check that the command is valid
        if (command == 'R' || command == 'W' || command == 'D') {
            this.command = command;
            return true;
        } else {
            return false;
        }
    }

    //returns the command as a char
    public char getCommand() {
        return command;
    }
    
    //set the max score of the assignment
    public double getMaxScore(){
        return maxScore;
    }
    
    public void setMaxScore(double maxScore){
        this.maxScore = maxScore;
    }
    
    public void setStrategy(int strat){
        this.strategy = strat;
    }
    
    public int getStrategy(){
        return strategy;
    }
    
    public void setCourseID(String course){
        this.courseID = course;
    }
       
    public String getCourseID(){
        return courseID;
    }
    
    public void setDrops(int drops){
        this.drops = drops;
    }
    
    public int getDrops(){
        return drops;
    }
}
