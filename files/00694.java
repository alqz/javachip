package info.bytecraft.zones;

import info.bytecraft.zones.commands.LotCommand;
import info.bytecraft.zones.commands.TownCommand;
import info.bytecraft.zones.commands.ZoneCommand;
import info.bytecraft.zones.exception.ZoneNotFoundException;
import info.bytecraft.zones.listeners.LotBuildListener;
import info.bytecraft.zones.listeners.Selector;
import info.bytecraft.zones.listeners.ZoneMob;
import info.bytecraft.zones.listeners.ZoneMovement;
import info.bytecraft.zones.listeners.ZonePlayerListener;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.PersistenceException;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class Zones extends JavaPlugin{

	public static Zones plugin;

	@Override
	public void onEnable(){
		this.setUpZoneDatabase();
		this.setUpZonePlayersDatabase();
		this.setUpLotsDatabase();
		this.setUpLotPlayersDatabase();
		plugin = this;
		this.registerCommands();
		this.registerEvents();
	}

	private void setUpZoneDatabase(){
		try{
			this.getDatabase().find(Zone.class).findRowCount();
		}catch(PersistenceException ex){
			this.installDDL();
		}
	}

	private void setUpZonePlayersDatabase(){
		try{
			this.getDatabase().find(ZonePlayer.class).findRowCount();
		}catch(PersistenceException ex){
			this.installDDL();
		}
	}

	private void setUpLotsDatabase(){
		try{
			this.getDatabase().find(Lot.class).findRowCount();
		}catch(PersistenceException ex){
			this.installDDL();
		}
	}

	private void setUpLotPlayersDatabase(){
		try{
			this.getDatabase().find(LotPlayer.class).findRowCount();
		}catch(PersistenceException ex){
			this.installDDL();
		}
	}

	private void registerCommands(){
		this.getCommand("zone").setExecutor(new ZoneCommand());
		this.getCommand("town").setExecutor(new TownCommand());
		this.getCommand("lot").setExecutor(new LotCommand());
	}


	private void registerEvents(){
		PluginManager pm = this.getServer().getPluginManager();
		pm.registerEvents(new Selector(), this);
		pm.registerEvents(new ZoneMovement(), this);
		pm.registerEvents(new ZoneMob(), this);
		pm.registerEvents(new ZonePlayerListener(), this);
		pm.registerEvents(new LotBuildListener(), this);
	}

	@Override
	public List<Class<?>> getDatabaseClasses(){
		List<Class<?>> list = new ArrayList<Class<?>>();
		list.add(Zone.class);
		list.add(ZonePlayer.class);
		list.add(Lot.class);
		list.add(LotPlayer.class);
		return list;
	}

	/**
	 * Gets all the zones.
	 * @return a list of all zones.
	 */
	public static List<Zone> getZones(){
		return plugin.getDatabase().find(Zone.class).where().findList();
	}

	/**
	 * Gets a zone by its name.
	 * @param name - The name of the zone to get.
	 * @return The zone, if it exists
	 * @throws ZoneNotFoundException if no zone exists with the name.
	 */
	public static Zone getZone(String name){
		Zone zone = plugin.getDatabase().find(Zone.class).where().ieq("name", name).findUnique();
		return zone;
	}

	/**
	 * Gets a list of ZonePlayer for a player
	 * @param player - The player.
	 * @return A list of ZonePlayer(s) for the player.
	 */
	public static List<ZonePlayer> getZonePlayer(Player player){
		return plugin.getDatabase().find(ZonePlayer.class).where().ieq("playerName", player.getName()).findList();
	}

	public static List<ZonePlayer> getZonePlayers(Zone zone){
		return plugin.getDatabase().find(ZonePlayer.class).where().ieq("zoneName", zone.getName()).findList();
	}

	public static ZonePlayer getZonePlayer(Zone zone, Player player){
		return plugin.getDatabase().find(ZonePlayer.class).where().ieq("zoneName", zone.getName()).ieq("playerName", player.getName()).findUnique();
	}

	/**
	 * Gets a zone based on a players location.
	 * @param player - The player whose location to check.
	 * @return The zone at the player's location.
	 * @throws ZoneNotFoundException if the zone does not exist.
	 */
	public static Zone getZone(Player player){
		for(Zone zone: getZones()){
			if(zone.contains(player.getLocation())){
				return zone;
			}
		}
		return null;
	}

	/**
	 * Gets a zone at a location.
	 * @param loc - A point to check, checks to see if the point is inside a zone.
	 * @return The zone, if it exists.
	 * @throws ZoneNotFoundException If the zone does not exist.
	 */
	public static Zone getZone(Location loc){
		for(Zone zone: getZones()){
			if(zone.contains(loc)){
				return zone;
			}
		}
		return null;
	}

	/**
	 * Gets a lot in a zone for the name.
	 * @param zone - The zone to check for the lot.
	 * @param name - The name of the lot you want.
	 * @return - The lot if it exists.
	 */
	public static Lot getLot(Zone zone, String name){
		return plugin.getDatabase().find(Lot.class).where().ieq("zoneName", zone.getName()).ieq("name", name).findUnique();
	}

}