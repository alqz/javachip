/**
 * Copyright 1999-2009 The Pegadi Team
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.pegadi.swing;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.PlainDocument;
import java.awt.*;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

public class FloatField extends JTextField {
    private Toolkit toolkit;
    private NumberFormat floatFormatter;


    public FloatField() {
        super();
        toolkit = Toolkit.getDefaultToolkit();
        floatFormatter = NumberFormat.getNumberInstance(Locale.getDefault());
        floatFormatter.setParseIntegerOnly(false);
    }

    public FloatField(int value, int columns) {
        super(columns);
        toolkit = Toolkit.getDefaultToolkit();
        floatFormatter = NumberFormat.getNumberInstance(Locale.getDefault());
        floatFormatter.setParseIntegerOnly(false);
        setValue(value);
    }

    public int getValue() {
        int retVal = 0;
        try {
            retVal = floatFormatter.parse(getText()).intValue();
        } catch (ParseException e) {
            // This should never happen because insertString allows
            // only properly formatted data to get in the field.
            toolkit.beep();
            //Log.m(this,"float","tjoHO!!");
        }
        return retVal;
    }


    public void setValue(int value) {
        setText(floatFormatter.format(value));
    }

    protected Document createDefaultModel() {
        return new FloatNumberDocument();
    }

    protected class FloatNumberDocument extends PlainDocument {
        private final Logger log = LoggerFactory.getLogger(getClass());

        public void insertString(int offs,
                                 String str,
                                 AttributeSet a)
                throws BadLocationException {
            char[] source = str.toCharArray();
            char[] result = new char[source.length];
            int j = 0;

            for (int i = 0; i < result.length; i++) {

              //
              // FIX ME! Her slippes igjennom både punktum eller komma
              //
                if (Character.isDigit(source[i]) || (source[i] == '.') || (source[i] == ','))
                    result[j++] = source[i];
                else {
                    toolkit.beep();
                    log.debug("String: {}", source[i]);
                }
            }
            super.insertString(offs, new String(result, 0, j), a);
        }
    }
}
