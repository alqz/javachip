package ui;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;

import util.ImageLoader;
import util.MusicPlayer;

public class CustomButton extends JButton implements MouseListener, ActionListener {
	private String filename;
	private String origFile;

	public CustomButton(String filename) {
		this.filename = filename;
		origFile = filename;
		setContentAreaFilled(false);
		setBorderPainted(false);
		addMouseListener(this);
		addActionListener(this);
	}

	@Override
	public void paintComponent(Graphics g) {
		g.drawImage(ImageLoader.getImage(filename), 0, 0, null);
	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
		filename = origFile;
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		filename = "h_" + filename;
		repaint();
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		filename = origFile;
		repaint();
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		filename = origFile;
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		filename = origFile;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		MusicPlayer.playSFX(MusicPlayer.BUTTON);
	}

}
