import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import reversestack.core.BoardTest;
import reversestack.minmax.BoardStateTest;
import reversestack.minmax.EvaluatorTest;
import reversestack.minmax.GameOverCheckerTest;
import reversestack.minmax.MinMaxTest;

@RunWith(Suite.class)
@SuiteClasses({ BoardStateTest.class, BoardTest.class, EvaluatorTest.class, GameOverCheckerTest.class, MinMaxTest.class })
public class AllTests {

}
