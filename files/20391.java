
import java.awt.Graphics;
import java.awt.Shape;
import java.awt.geom.Line2D;

/**
 * Class extending TwoEndShape which defines a basic line class that runs from a starting point to and ending point.
 */
public class LineShape extends TwoEndShape {

    /**
     * < Constructor >
     * Constructor for class LineShape
     */
    public LineShape() {
        type = 0;
    }

    /**
     * Method to draw a simple line from the starting to ending point.
     *
     * @param g Graphics used to draw line
     * @param x0 x-coordinate of starting point
     * @param y0 y-coordinate of starting point
     * @param x1 x-coordinate of ending point
     * @param y1 y-coordinate of ending point
     * @return The drawn line shape
     * @see tools.shapes.TwoEndShape#draw(java.awt.Graphics, int, int, int, int)
     */
    public Shape draw(Graphics g, int x0, int y0, int x1, int y1) {
        Shape shape = new Line2D.Double(x0, y0, x1, y1);
        g.drawLine(x0, y0, x1, y1);
        return shape;
    }

    /**
     * Method to draw the outline of a line from the starting to ending point.
     * @param g Graphics used to draw line
     * @param x0 x-coordinate of starting point
     * @param y0 y-coordinate of starting point
     * @param x1 x-coordinate of ending point
     * @param y1 y-coordinate of ending point
     * @see tools.shapes.TwoEndShape#drawOutline(java.awt.Graphics, int, int, int, int)
     */
    public void drawOutline(Graphics g, int x0, int y0, int x1, int y1) {
        g.drawLine(x0, y0, x1, y1);
    }

    /**
     * Get method to return the freehand shape that is drawn
     * @param x0 x-coordinate of starting point
     * @param y0 y-coordinate of starting point
     * @param x1 x-coordinate of ending point
     * @param y1 y-coordinate of ending point
     * @return The drawn freehand shape
     */
    // CHANGE
    public Shape getShape(int x0, int y0, int x1, int y1) {
        return new Line2D.Double(x0, y0, x1, y1);
    }
}// end public class LineShape extends TwoEndShape
