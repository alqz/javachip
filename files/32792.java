package info.bytecraft.zones.listeners;

import info.bytecraft.zones.Zone;
import info.bytecraft.zones.Zones;

import java.util.EnumSet;
import java.util.Set;

import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;

public class ZoneMob implements Listener {
	private final Set<EntityType> types = EnumSet.of(EntityType.CREEPER, EntityType.BLAZE, EntityType.ZOMBIE, EntityType.SPIDER
			, EntityType.SLIME, EntityType.SKELETON, EntityType.SILVERFISH, EntityType.GHAST, EntityType.ENDERMAN);
	
	@EventHandler
	public void onMove(CreatureSpawnEvent event){
		if(Zones.getZones().isEmpty()){
			return;
		}
		for(Zone zone: Zones.getZones()){
			if(zone.contains(event.getLocation())){
				if(types.contains(event.getEntityType())){
					if(!zone.isHostiles()){
						event.setCancelled(true);
					}
				}
			}
		}
	}
}
