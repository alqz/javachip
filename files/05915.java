package entities;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Customer {

	@Id
	@GeneratedValue
	private int Id;
	
	@Column(nullable=false, length=2)
	private String InsuredState;
	
	@Column(nullable=false, length=25)
	private String FirstName;
	
	@Column(nullable=false, length=40)
	private String LastName;
	
	@Column(nullable=false, length=1)
	private String Gender;
	
	@Column(nullable=false, length=100)
	private String Email;
	
	@Column(nullable=false, length=20)
	private String Marital_Status;
	
	@Column(nullable=false)
	private boolean IsActive;
	
	@Column(nullable=false)
	private boolean IsContracted;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable=false, updatable=false)
	private Date First_Contact_Date;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable=false)
	private Date Birthday;
	
	@Column(nullable=false)
	private boolean IsSmoker;
	
	@ManyToOne(optional=false, cascade = CascadeType.PERSIST)
	private Address address;
	
	@ManyToOne(optional=false, cascade = CascadeType.PERSIST)
	private Phone phone;
	
	@OneToOne
	@JoinColumn(name="sale_id")
	private Sale sale;
	
	@OneToOne
	@JoinColumn(name="policy_id")
	private Insurance_Policy policy;
	
	@OneToMany(mappedBy="customer", cascade = CascadeType.REMOVE)
	private List<Note> notes;
	
	@ManyToOne(optional=false)
	private Agent agent;
	
	public Customer(){}

	public int getId() {
		return Id;
	}

	public void setId(int id) {
		Id = id;
	}

	public String getInsuredState() {
		return InsuredState;
	}

	public void setInsuredState(String insuredState) {
		InsuredState = insuredState;
	}

	public String getFirstName() {
		return FirstName;
	}

	public void setFirstName(String firstName) {
		FirstName = firstName;
	}

	public String getLastName() {
		return LastName;
	}

	public void setLastName(String lastName) {
		LastName = lastName;
	}
	
	public String getFullName()
	{
		return LastName + ", " + FirstName;
	}
	
	public String getProperFullName()
	{
		return FirstName + " " + LastName;
	}

	public String getGender() {
		return Gender;
	}

	public void setGender(String gender) {
		Gender = gender;
	}

	public String getEmail() {
		return Email;
	}

	public void setEmail(String email) {
		Email = email;
	}

	public String getMarital_Status() {
		return Marital_Status;
	}

	public void setMarital_Status(String marital_Status) {
		Marital_Status = marital_Status;
	}

	public boolean isIsActive() {
		return IsActive;
	}

	public void setIsActive(boolean isActive) {
		IsActive = isActive;
	}

	public boolean isIsContracted() {
		return IsContracted;
	}

	public void setIsContracted(boolean isContracted) {
		IsContracted = isContracted;
	}

	public Date getFirst_Contact_Date() {
		return First_Contact_Date;
	}

	public void setFirst_Contact_Date(Date first_Contact_Date) {
		First_Contact_Date = first_Contact_Date;
	}

	public Date getBirthday() {
		return Birthday;
	}

	public void setBirthday(Date birthday) {
		Birthday = birthday;
	}

	public boolean isIsSmoker() {
		return IsSmoker;
	}

	public void setIsSmoker(boolean isSmoker) {
		IsSmoker = isSmoker;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public Phone getPhone() {
		return phone;
	}

	public void setPhone(Phone phone) {
		this.phone = phone;
	}

	public Sale getSale() {
		return sale;
	}

	public void setSale(Sale sale) {
		this.sale = sale;
	}

	public Insurance_Policy getPolicy() {
		return policy;
	}

	public void setPolicy(Insurance_Policy policy) {
		this.policy = policy;
	}

	public List<Note> getNotes() {
		return notes;
	}

	public void setNotes(List<Note> notes) {
		this.notes = notes;
	}

	public Agent getAgent() {
		return agent;
	}

	public void setAgent(Agent agent) {
		this.agent = agent;
	}
	
	
}
