package oxen.commands.user;

import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;
import oxen.commands.Command;
import oxen.game.Game;

/**
 * The SAVE command; saves the game.
 *
 * @author nin500
 */
public class SaveCommand extends Command {

    private JFileChooser locationChooser;

    /**
     * Constructor; takes a game manager.
     *
     * @param manager
     */
    public SaveCommand(Game manager) {
        super(manager);
        locationChooser = new JFileChooser();
    }

    /**
     * Returns the help description for this command.
     *
     * @return the help String.
     */
    @Override
    public String getHelpDescription() {
        return "SAVE - saves the game.";
    }

    /**
     * Asks whether this command can respond to the users input.
     *
     * @param input form the user.
     * @return a boolean indicating whether this command can respond to the
     * users request.
     */
    @Override
    public boolean canRespondTo(String input) {
        return input.equalsIgnoreCase("SAVE");
    }

    /**
     * Executes the functionality of this command.
     *
     * @param input form the user.
     */
    @Override
    public void respondTo(String input) {
        try {
            //Save the game to a directory specified by the user.          
            String file;
            int outcome = locationChooser.showSaveDialog(locationChooser);
            if (outcome == JFileChooser.APPROVE_OPTION) {
                file = locationChooser.getSelectedFile().getAbsolutePath();
                if (!file.endsWith(".sav") && !file.endsWith(".SAV")) {
                    file = file + ".sav"; // Force the format to be .sav
                } else {
                }
            } else if (outcome == JFileChooser.CANCEL_OPTION) {
                getLogger().logOutput("Save cancelled!");
                getLogger().logOutput("");
                return;
            } else {
                getLogger().logOutput("Invalid Save!");
                getLogger().logOutput("");
                return;
            }
            
            FileOutputStream stream = new FileOutputStream(file);
            ObjectOutputStream output = new ObjectOutputStream(stream);

            output.writeObject(getState());
            output.close();
            //indicate successful save.
            getLogger().logOutput("Game successfully saved at: " + file);
        }
        catch (FileNotFoundException ex) {
            Logger.getLogger(SaveCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
        catch (IOException ex) {
            Logger.getLogger(SaveCommand.class.getName()).log(Level.SEVERE, null, ex);
        }

        getLogger().logOutput("");
    }
    
    /**
     * Method for testing purposes.
     * @param chooser 
     */
    public void setChooser(JFileChooser chooser) {
        locationChooser = chooser;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
