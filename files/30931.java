package intranet.infra.dao;

import lombok.extern.java.Log;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;

/**
 * 
 * @author Gabriel Cardelli
 * @since Feb 21, 2012 - 12:24:19 PM
 *
 */
@Repository
@Log
public class JpaDao extends HibernateDaoSupport implements DAO {

	@Autowired
	public JpaDao( SessionFactory sessionFactory ) {
		setSessionFactory( sessionFactory );
	}

	public Session getHibernateSession() {
		
		return getSession();
	}

}
