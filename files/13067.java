package mas.worldview;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.LinkedList;
import javax.swing.JPanel;
import mas.FileHandler;

/**
 *
 * @author Christoph
 */
public class FileListEntry extends JPanel {
    
    private boolean selected, mouseIsOver;
    private String entryName;
    private final int textSpacing;
    private LinkedList<ThumbnailPanel> thumbnailPanels;
    private FileHandler fh;
    
    public FileListEntry(String entryName) {
        super();
        this.setOpaque(false);
        this.textSpacing = 4;
        this.entryName   = entryName.substring(0, entryName.length()-4);
        this.selected    = false;
        this.mouseIsOver = false;
        this.initActionListener();
        FontMetrics fm = super.getFontMetrics(super.getFont());
        this.fh = new FileHandler();
        this.thumbnailPanels = new LinkedList<ThumbnailPanel>();
    }

    //PRIVATE METHODS//
    
    private void initActionListener() {
        // we need on click, on enter and on exit
        final FileListEntry itself = this;
        this.addMouseListener(new MouseListener() {

            @Override
            public void mouseClicked(MouseEvent e) {}

            @Override
            public void mousePressed(MouseEvent e) {
                Rectangle bounds = itself.getBounds();
                bounds.setLocation(0, 0);
                if(bounds.contains(e.getX(), e.getY())) {
                    if(getParent() instanceof FileListPanel) {
                        ((FileListPanel)getParent()).deselectAll();
                        ((FileListPanel)getParent()).setSelectedEntry(itself);
                    }
                    selected = true;

                    Object parent = itself.getParent();
                    if(parent instanceof FileListPanel) {
                        ((FileListPanel) parent).setSelectedEntry(itself);
                    }

                    BufferedImage thumbnailPicture = null;
                    try {
                        thumbnailPicture = fh.readImage(entryName, GUIUtil.MAP_FOLDER);
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }

                    for(ThumbnailPanel thumbnail : thumbnailPanels) {
                        thumbnail.setThumbnail(thumbnailPicture);
                    }
                    repaint();
                }
            }

            @Override
            public void mouseReleased(MouseEvent e) {}

            @Override
            public void mouseEntered(MouseEvent e) {
                mouseIsOver = true;
                repaint();
            }

            @Override
            public void mouseExited(MouseEvent e) {
                mouseIsOver = false;
                repaint();
            }
        });
    }
    
    //PUBLIC METHODS//
    
    public void addThumbnailPanel(ThumbnailPanel panel) {
        this.thumbnailPanels.add(panel);
    }
    
    public void removeThumbnailPanel(ThumbnailPanel panel) {
        this.thumbnailPanels.remove(panel);
    }
    
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g;
        FontMetrics fm = super.getFontMetrics(super.getFont());
        Color tempColor = g2d.getColor();
        Composite tempComposite = g2d.getComposite();
        
        g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER,0.5f)); // draw transparent background
        if(selected || mouseIsOver) {
            g2d.setColor(new Color(75, 200, 255));
            g2d.fillRect(0, 0, this.getWidth(), this.getHeight());
            g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER,1.0f)); // draw transparent background
            g2d.setColor(new Color(0, 0, 255));
            g2d.drawRect(0, 0, this.getWidth(), this.getHeight());
        } else {
            g2d.setColor(new Color(0, 0, 255));
            g2d.fillRect(0, 0, this.getWidth(), this.getHeight());
            g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER,1.0f)); // draw transparent background
            g2d.drawRect(0, 0, this.getWidth(), this.getHeight());
        }
        
        g2d.setColor(Color.WHITE);
        if(entryName != null) {
            g2d.drawString(entryName, textSpacing, (this.getHeight()+fm.getHeight()-textSpacing)/2);
        }
        g2d.setColor(tempColor);
        g2d.setComposite(tempComposite);
    }
    
    //ACCESSOR METHODS//
    
    public String getText() {
        return entryName;
    }
    
    public void setText(String entryName) {
        this.entryName = entryName;
    }
    
    public boolean isSelected() {
        return selected;
    }
    
    public void deselect() {
        this.selected = false;
        paintComponent(getGraphics());
    }
    
    public void select() {
        this.selected = true;
    }
}
