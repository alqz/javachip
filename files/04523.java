package at.moma.gui.login;

import java.io.IOException;
import java.sql.SQLException;

import org.apache.log4j.Logger;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;

import at.moma.gui.GuiUtil;
import at.moma.gui.messages.GenericMessage;
import at.moma.persistence.DbAccessException;
import at.moma.service.LoginService;
import at.moma.service.SettingsUtil;
import at.moma.service.User;

/**
 * Creates a new user account.
 * 
 * @author stefan
 *
 */
public class CreateAccountListener implements SelectionListener {

	private static Logger log = Logger.getLogger(CreateAccountListener.class);
	
	private User user;
	private CreateNewUserAccount callback;
	private LoginService lService;
	private String firstName;
	private String lastName;
	private GenericMessage message;
	
	//constructor	
	public CreateAccountListener(CreateNewUserAccount callback) {
		super();
		this.callback = callback;
		lService = LoginService.instance();
	}

	@Override
	public void widgetSelected(SelectionEvent e) {
		log.debug("selection event");
		firstName = callback.getFirstName();
		lastName = callback.getLastName();
		
		/*
		 * Set propertys of user and set it as user in SettingsUtil.
		 * Execute the createAccount() method privided by user and
		 * checking for errors.
		 */
		if (lService.checkInputString(firstName) && lService.checkInputString(lastName)) {
			user = new User(firstName, lastName);
			
			if (callback.isAutologin()) {
				//firstName and lastName are valid and autoligin is true.
				if (createAccount()) {
					try {
						SettingsUtil.setAutoLogin(true);
					} catch (IOException e1) {
						log.fatal("couldn't write config file");
						e1.printStackTrace();
						System.exit(2);
					}
					try {
						SettingsUtil.setEstablishedUser(true);
					} catch (IOException e1) {
						e1.printStackTrace();
						System.exit(2);
					}
					//TODO: weiterleitung zu Hauptfenster
					GuiUtil.switchSubWindow(callback.getShell(), callback, LoginComposite.class.getName());
				}
			} else {
				if (lService.checkPassword(callback.getPassword(), callback.getRetypedPassword()) && lService.checkInputString(callback.getUsername())) {
					log.debug("username and password valid");
					user.setPassword(callback.getPassword());
					user.setUsername(callback.getUsername());
					//firstName and lastName are valid and autoligin is false.
					//pw and username are valid, too.
					if(createAccount()){
						try {
							SettingsUtil.setAutoLogin(false);
						} catch (IOException e1) {
							log.fatal("couldn't write config file");
							e1.printStackTrace();
							System.exit(2);
						}
						try {
							SettingsUtil.setEstablishedUser(true);
						} catch (IOException e1) {
							e1.printStackTrace();
							System.exit(2);
						}
						//TODO: Weiterleitung zu Hauptfenster
						GuiUtil.switchSubWindow(callback.getShell(), callback, LoginComposite.class.getName());
					}					
				} else {
					log.debug("username and/or password invalid");
					message = new GenericMessage(callback.getShell(), "warning", "warning");
					message.setMessage("message_invalid_pw_username");
					message.open();
				}
			}
		}else{
			log.info("input invalid");
			message = new GenericMessage(callback.getShell(), "warning", "warning");
			message.setMessage("message_invalid_input");
			message.open();
		}
	}

	@Override
	public void widgetDefaultSelected(SelectionEvent e) {
		
	}

	/*
	 * uses the createAccount() method from user and
	 * handles errors.
	 */
	private boolean createAccount(){
		try {
			user.createAccount();
			SettingsUtil.setUser(user);
			return true;
		} catch (DbAccessException | SQLException e1) {
			log.error("couldn't create account");
			message = new GenericMessage(callback.getShell(), "error", "error");
			message.setMessage("message_create_account_failure");
			message.open();
			return false;
		} catch (ClassNotFoundException e2) {
			log.fatal("class not found");
			message = new GenericMessage(callback.getShell(), "error", "error");
			message.setMessage("message_programming_failure");
			message.open();
			e2.printStackTrace();
			System.exit(3);
			return false;
		}				
	}
}
