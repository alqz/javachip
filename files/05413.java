/**
 * Copyright 1999-2009 The Pegadi Team
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.pegadi.model;

import java.io.Serializable;


/**
 * A class for Section.  We think that the journalists and photographers of a
 * business is organized into smaller Section. We use this class for grouping articles, for clarity in lists.  also, there is normally on editor per
 * section.
 *
 * @author <a href="mailto:jb@underdusken.no">Jørgen Binningsbø</a>
 */
public class Section implements Serializable, Comparable {

    /**
     * The id of this description
     */
    private Integer id;

    /**
     * The name of this description
     */
    private String name;

    /**
     * A description of this department
     */
    private String description;

    /**
     * Constructor taking an ID as an argument
     *
     * @param id the id of the department
     */
    public Section(Integer id) {
        this.id = id;
    }

    /**
     * Creates a new, empty Section
     */
    public Section() {
    }


    /**
     * Constructor taking an ID, a name and a description as an argument
     *
     * @param id          the id of the department
     * @param name        the name of the department
     * @param description the description of the department
     */
    public Section(int id, String name, String description) {
        this.id = id;
        this.name = name;
        this.description = description;
    }


    /**
     * Compars for equality
     *
     * @param obj the Object to compare with
     * @return <code>true</code> if the objects are equal
     */
    public boolean equals(Object obj) {
        if (obj instanceof Section) {
            Section object = (Section) obj;
            return this.getId() == object.getId();
        } else {
            return false;
        }
    }

    /**
     * Comapares this department to another department. Used in sorting
     *
     * @param o1 the object to compare to
     * @return positive if after/larger
     */
    public int compareTo(Object o1) {
        Section section = (Section) o1;
        return this.getName().compareTo(section.getName());
    }


    /**
     * Returns a string representation of this department by calling getName()
     *
     * @return the name
     */
    public String toString() {
        return this.getName();
    }


    /**
     * Get the value of ID.
     *
     * @return value of ID.
     */
    public int getId() {
        return id;
    }

    /**
     * Set the value of ID.
     *
     * @param id Value to assign to ID.
     */
    public void setId(int id) {
        this.id = id;
    }


    /**
     * Get the value of name.
     *
     * @return value of name.
     */
    public String getName() {
        return name;
    }

    /**
     * Set the value of name.
     *
     * @param name Value to assign to name.
     */
    public void setName(String name) {
        this.name = name;
    }


    /**
     * Get the value of description.
     *
     * @return value of description.
     */
    public String getDescription() {
        return description;
    }

    /**
     * Set the value of description.
     *
     * @param description Value to assign to description.
     */
    public void setDescription(String description) {
        this.description = description;
    }


    /**
     * Clone this object. Will perform a deep clone.
     *
     * @return The clone.
     */
    public Object clone() {
        Section c = new Section(id);
        c.name = name == null ? null : name;
        c.description = description == null ? null : description;

        return c;
    }
}
