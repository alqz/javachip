package at.moma.service;

import java.sql.SQLException;

import org.apache.log4j.Logger;

import at.moma.persistence.DbAccessException;
import at.moma.persistence.DbAccessFactory;

public class User extends Person {

	private static Logger log = Logger.getLogger(User.class);
	
	private String username = "default";
	private String password = "";
	
	//constructor
	public User(String firstName, String lastName) {
		super(firstName, lastName);
	}
	
	public User(){
		super("unknown", "unknown");
	}
	
	//getter and setter
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public void createAccount() throws ClassNotFoundException, SQLException, DbAccessException{
		log.info("create account");
		try {
			DbAccessFactory.getNewDbAccess(SettingsUtil.getDb(), username, password).getConnection();
		} catch (ClassNotFoundException | SQLException | DbAccessException e) {
			DbAccessFactory.destroyDbAccess(SettingsUtil.getDb());
			throw e;
		}
	}
	
	public void login() throws ClassNotFoundException, SQLException, DbAccessException{
		log.info("login");
		try {
			DbAccessFactory.getExistingDbAccess(SettingsUtil.getDb(), username, password).getConnection();
		} catch (ClassNotFoundException | SQLException | DbAccessException e) {
			DbAccessFactory.destroyDbAccess(SettingsUtil.getDb());
			throw e;
		}
	}
}
