package ucdavis.bqleclipse.internal.commands;

import java.util.Map;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.Command;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.commands.ICommandService;
import org.eclipse.ui.commands.IElementUpdater;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.menus.UIElement;

import ucdavis.bqleclipse.BQLPlugin;
import ucdavis.bqleclipse.internal.Messages;
import ucdavis.bqleclipse.internal.ui.BQLPreferencePage;
import ucdavis.bqleclipse.models.ITraceElement;

public class ToggleLevelHandler extends AbstractHandler implements IElementUpdater {
	public static final String TOGGLE_LEVEL_COMMAND = 
			"ucdavis.bqleclipse.commands.toggleLevel"; //$NON-NLS-1$
	
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		IWorkbench wb = BQLPlugin.getDefault().getWorkbench();
		ICommandService service = (ICommandService)wb.getService(ICommandService.class);
		Command command = service.getCommand(ChangeLevelHandler.CHANGE_LEVEL_COMMAND);
		
		if (BQLPreferencePage.getTraceLevel() == null) {
			if (BQLPlugin.canConnectToServer()) {
				BQLPreferencePage.setTraceLevel(ITraceElement.TraceLevel.METHOD_LEVEL);

				HandlerUtil.updateRadioState(command, "method");
			} else {
				BQLPlugin.error(Messages.BQLJob_cantconnecttoservererror_message);
			}
		} else {
			BQLPreferencePage.setTraceLevel(null);
			HandlerUtil.updateRadioState(command, "disabled");
		}
		
		service.refreshElements(TOGGLE_LEVEL_COMMAND, null);
		
		return null;
	}

	@Override
	public void updateElement(UIElement element, Map parameters) {
		ITraceElement.TraceLevel level = BQLPreferencePage.getTraceLevel();
		String state;
		if (level == null) {
			state = "disabled";
		} else if (level == ITraceElement.TraceLevel.METHOD_LEVEL) {
			state = "method";
		} else if (level == ITraceElement.TraceLevel.INSTRUCTION_LEVEL) {
			state = "instruction";
		} else {
			return;
		}
		
		String path = String.format("icons/bql-level-%s.png", state);
		ImageDescriptor image = BQLPlugin.getImageDescriptor(path);
		element.setIcon(image);
	}
}
