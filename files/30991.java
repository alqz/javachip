/**
 * Copyright 1999-2009 The Pegadi Team
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.pegadi.photosearch;

import org.pegadi.sqlsearch.SearchTerm;

/**
 * This searchterm finds all the publishingphotos which belongs to a given archivephoto
 *
 * @author Ingar Saltvik <saltvik@underdusken.no>
 */
public class PublishingPhotoByArchivePhotoTerm extends SearchTerm {


    /**
     * The id of the archivephoto for which this searchterm finds all publishingphotos
     */
    private int archivePhotoId;

    /**
     * Default constructor
     *
     * @param archivePhotoId The id of the archivephoto for which this searchterm
     *                       finds all publishingphotos
     */
    public PublishingPhotoByArchivePhotoTerm(int archivePhotoId) {
        this.archivePhotoId = archivePhotoId;
    }

    /**
     * Generates the WHERE part of the SQL query (without including the string
     * "WHERE").
     *
     * @return the WHERE clause
     */
    public String whereClause() {
        return "archivephoto.id = publishingphoto.refarchivephoto AND archivephoto.id = " + archivePhotoId;
    }

    /**
     * Returns the names of the tables involved in this search.
     *
     * @return A String[] containing the involved tables. The array may contain
     *         multiple instances of each table name.
     */
    public String[] getTables() {
        String[] tables = new String[2];
        tables[0] = "archivephoto";
        tables[1] = "publishingphoto";
        return tables;
    }
}
