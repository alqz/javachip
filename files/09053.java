package net.milkycraft;

import java.io.File;

import net.milkbowl.vault.permission.Permission;
import net.milkycraft.Commands.EasyExecutor;
import net.milkycraft.Commands.RankExecutor;
import net.milkycraft.Config.Settings;
import net.milkycraft.Listeners.GenericListeners;
import net.milkycraft.Metrics.Metrics;
import net.milkycraft.io.HistoryHandler;
import net.milkycraft.io.LoggerHandler;
import net.milkycraft.io.PendingHandler;

import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * The Class EasyRank.
 */
public class EasyRank extends JavaPlugin {

	public static Permission perms = null;
	public static EasyRank main = null;
	public static String maindirectory;

	@Override
	public void onEnable() {
		main = this;
		long time = System.currentTimeMillis();
		maindirectory = this.getDataFolder() + File.separator;
		setUpPermissions();
		Settings config = new Settings(this);
		config.load();
		fileIO();
		getServer().getPluginManager().registerEvents(new GenericListeners(),this);
		setUpExecutors();
		try {
			new Metrics(this).start();
		} catch (Exception e) {
			writeWarn("The task of starting metrics generated an exception: ");
			e.printStackTrace();
		} finally {
			writeInfo("Finished enabling, took " + (System.currentTimeMillis() - time) + "ms");
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.bukkit.plugin.java.JavaPlugin#onDisable()
	 */
	@Override
	public void onDisable() {
		writeInfo("[EasyRank] Successfully disabled!");
	}

	public void fileIO() {
		(new LoggerHandler()).load();
		(new PendingHandler()).load();
		(new HistoryHandler()).load();
	}

	/**
	 * Sets up the executors.
	 */
	private void setUpExecutors() {
		getCommand("rank").setExecutor(new RankExecutor());
		getCommand("easyrank").setExecutor(new EasyExecutor());
	}

	/**
	 * Sets up the permissions.
	 * 
	 * @return true, if successful
	 */
	private boolean setUpPermissions() {
		RegisteredServiceProvider<Permission> rsp = getServer()
				.getServicesManager().getRegistration(Permission.class);
		perms = rsp.getProvider();
		return perms != null;
	}

	/**
	 * Write info to console .
	 * 
	 * @param info
	 *            the info
	 */
	public static void writeInfo(String info) {
		java.util.logging.Logger.getLogger("Minecraft").info("[EasyRank] " + info);
	}

	/**
	 * Write warning to console .
	 * 
	 * @param warn
	 *            the warn
	 */
	public static void writeWarn(String warn) {
		java.util.logging.Logger.getLogger("Minecraft").warning("[EasyRank] " +warn);
	}

	/**
	 * Gets the main class.
	 * 
	 * @return the main
	 */
	public static final EasyRank getMain() {
		return main;
	}
}
