package datamodeler.awt.components;

import java.awt.Checkbox;

import datamodeler.models.Database;

/**
 * A checkbox that represents a single database
 * 
 * @author Kyle Sletten
 */
@SuppressWarnings("serial")
public class DatabaseCheckbox extends Checkbox {
	private Database database;

	public DatabaseCheckbox(Database database) {
		this.setDatabase(database);
	}

	public Database getDatabase() {
		return this.database;
	}

	public void setDatabase(Database database) {
		this.database = database;

		super.setLabel(database == null ? "" : database.getName());
	}
}
