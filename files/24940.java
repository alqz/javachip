/**
 * Copyright 1999-2009 The Pegadi Team
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.pegadi.articlelist;

//resys-imports

import org.pegadi.model.Article;

import java.util.Comparator;

/**
 * Compares two articles by their status.
 * (and that only, does not fail back to ID should the names match).
 *
 * @author <a href="mailto:jb@underdusken.no">Jørgen Binningsbø</a>
 * @version $Id$
 * @see Comparator
 */
public class ArticleByStatusComparator implements Comparator {

    /**
     * The name of the comparator
     */
    protected String name;


    /**
     * Creates a new comparator with the given name.
     */
    public ArticleByStatusComparator(String name) {
        this.name = name;
    }


    /**
     * Creates an new comparator with the default name.
     */
    public ArticleByStatusComparator() {
        this.name = "Status";
    }

    public int compare(Object o1, Object o2) {
        Article art1 = (Article) o1;
        Article art2 = (Article) o2;
        return art1.getArticleStatus().getId() - art2.getArticleStatus().getId();
    }

    public String toString() {
        return name;
    }
}
