package pokeherface.network;

import java.util.List;

import pokeherface.poker.Card;

public class Message
{
	private String action;
	private String[] args;
	
	public Message(String message)
	{
		String[] parts = message.split(" ");
		
		if (parts.length == 0)
		{
			// throw new IOException("Invalid message");
		}
			
		action = parts[0];
		args = new String[parts.length - 1];
		
		if (parts.length > 1)
		{
			System.arraycopy(parts, 1, args, 0, parts.length - 1);
		}
	}
	
	/**
	 * Generates a message string with one string argument.
	 */
	public static String generate(String action, String arg)
	{
		return action + " " + arg;
	}
	
	/**
	 * Generates a message string with one integer argument.
	 */
	public static String generate(String action, int arg)
	{
		return generate(action, new String[] {Integer.toString(arg)});
	}
	
	/**
	 * Generates a message string with a integer array argument.
	 */
	public static String generate(String action, int[] intArgs)
	{
		String[] args = new String[intArgs.length];
		
		for (int i = 0; i < args.length; i++)
		{
			args[i] = Integer.toString(intArgs[i]);
		}
		
		return generate(action, args);
	}
	
	/**
	 * Generates a message string with a card array argument.
	 */
	public static String generate(String action, Card[] cardArgs)
	{
		String[] args = new String[cardArgs.length];
		
		for (int i = 0; i < cardArgs.length; i++)
		{
			args[i] = cardArgs[i].toString();
		}
		
		return generate(action, args);
	}
	
	/**
	 * Generates a message string with a list argument
	 */
	public static String generate(String action, List<Integer> list)
	{
		String args[] = new String[list.size()];
		int i = 0;
		
		for (Integer integer : list)
		{
			args[i] = integer.toString();
			i++;
		}
		
		return generate(action, args);
	}
	
	/**
	 * Generates a message string with two integer arguments.
	 */
	public static String generate(String action, int arg1, int arg2)
	{
		return generate(action, new String[] {Integer.toString(arg1), Integer.toString(arg2)});
	}
	
	/**
	 * Generates a message string with one integer argument and one string argument.
	 */
	public static String generate(String action, int arg1, String arg2)
	{
		return generate(action, new String[] {Integer.toString(arg1),arg2});
	}
	
	/**
	 * Generates a message string with one integer argument and a card array argument.
	 */
	public static String generate(String action, int arg1, Card[] cardArgs)
	{
		String[] args = new String[cardArgs.length + 1];
		args[0] = Integer.toString(arg1);
		
		for (int i = 0; i < cardArgs.length; i++)
		{
			args[i + 1] = cardArgs[i].toString();
		}
		
		return generate(action, args);
	}
	
	/**
	 * Generates a message string with a string array argument.
	 */
	public static String generate(String action, String[] args)
	{
		StringBuilder sb = new StringBuilder();
		sb.append(action);
		
		for (String arg : args)
		{
			sb.append(' ');
			sb.append(arg);
		}
		
		return sb.toString();
	}
	
	public String getAction()
	{
		return action;
	}
	
	public String[] getArgs()
	{
		return args;
	}
	
	public String toString()
	{
		return generate(action, args);
	}
	
	public static String[] cardsToStringArray(Card[] cards)
	{
		String[] strCards = new String[cards.length];
		
		for (int i = 0; i < cards.length; i++)
		{
			strCards[i] = cards[i].toString();
		}
		
		return strCards;
	}
}
