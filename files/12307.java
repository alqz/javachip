/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mas.worldmodel.entity.statesystem.guardStates;

import java.awt.Point;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import mas.worldcontroller.SimulationController;
import mas.worldcontroller.WindowController;
import mas.worldmodel.Utilities;
import mas.worldmodel.entity.Entity;
import mas.worldmodel.entity.statesystem.actionsystem.Action;
import mas.worldmodel.entity.statesystem.actionsystem.MoveToAction;
import mas.worldmodel.entity.statesystem.guardStates.graphSystem.Edge;
import mas.worldmodel.entity.statesystem.guardStates.graphSystem.Graph;
import mas.worldmodel.entity.statesystem.guardStates.graphSystem.Vertex;
import mas.worldmodel.objects.LineWorldObject;
import mas.worldmodel.objects.RectWorldObject;
import mas.worldmodel.objects.SentryTower;
import mas.worldmodel.objects.SentryTowerLine;
import mas.worldview.sframesystem.rts.viewport.drawcommands.PatrolDrawCommand;
import org.apache.commons.math3.distribution.UniformRealDistribution;

/**
 *
 * @author christopher_hh
 */
public class PatrollingChrisState extends PatrollingState {

    public LinkedList<Entity> entList;
    
    
    
    public PatrollingChrisState(Entity e) {
        super(e);
        
    }
    
    


    
    
    
    @Override
    public Action getNextAction() {
        Action action = null;
        
        action = new MoveToAction(e, e.getLocationInCM(), 0);
        
        
        
        
        
        
        
        /* ----- This is for the Sentry Towers -> Doesn't work yet ----- */
//        Point.Double pt = getAllSentryTowerCoordinates().getFirst();
//        action = new MoveToAction(e, pt, Entity.MAX_SPEED);
        
        
        
        /* ----- This is for multi entities -> Doesn't work yet ----- */
//        if (entList != null) {
//            LinkedList<Double> distances = new LinkedList<Double>();
//            Entity shortest = null;
//            double distance = Double.POSITIVE_INFINITY;
//            for (Entity ent : entList) {
//                distances.add(new Double(Utilities.getLineLength(e.getXInCM(), e.getYInCM(), ent.getXInCM(), ent.getYInCM())));
//                if (distances.getLast() < distance) {
//                    distance = distances.getLast();
//                    shortest = ent;
//                }
//            }
//            double angle = shortest.getAngle();
//            action = new MoveToAction(e, Entity.MAX_SPEED, angle+180, 100);
//        }
//        else {
//            action = new MoveToAction(e, new Point.Double(50, 50), Entity.MAX_SPEED);
//        }
        
        
        return action;
    }

    @Override
    public void communicate() {
        entList = e.getCommunicationList();
    }
    
    private LinkedList<Point.Double> getAllSentryTowerCoordinates() {
        LinkedList<LineWorldObject> lineList = e.getMemory().getAllObjects();
        LinkedList<LineWorldObject> sentryList = new LinkedList<LineWorldObject>();

        for (LineWorldObject line : lineList) {
            if (line instanceof SentryTowerLine) {
                sentryList.add((SentryTowerLine)line);
            }
        }
        LinkedList<Point.Double> pts = new LinkedList<Point.Double>();
        pts.add(assembleSentry(sentryList));
        return pts;
    }
    
    private Point.Double assembleSentry(LinkedList<LineWorldObject> list) {
        boolean xFound = false;
        boolean yFound = false; 
       
        double x = 0.0;
        double y = 0.0;
        Point.Double pt = null;
        for (LineWorldObject line : list) {
            if (line.getXInCM() - line.getX2InCM() == 0 && yFound == false) {
                yFound = true;
                y = line.getYInCM() + (Math.abs(line.getYInCM() - line.getY2InCM())/2);
            } else if (line.getYInCM() - line.getY2InCM() == 0 && xFound == false) {
                xFound = true;
                x = line.getXInCM() + (Math.abs(line.getXInCM() - line.getX2InCM())/2);
            }
        }
        Line2D.Double li = new Line2D.Double(e.getXInCM(), e.getYInCM(), x, y);
        double[] intersection = new double[2];
        if (Utilities.getIntersectionCoordinates(e.getXInCM(), e.getYInCM(), x, y, li, e.getRadius(), intersection)) {
            li.setLine(li.getX1(), li.getY1(), intersection[0], intersection[1]);
            Utilities.getNormalizedDirectionVector(li, intersection);
            pt = new Point.Double(li.getX2()-intersection[0]*e.getRadius(), li.getY2()-intersection[1]*e.getRadius());
                
        }
        
        return pt;
    }
    
    
}
