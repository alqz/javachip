package com.sk89q.regionbook.commands;

import com.sk89q.minecraft.util.commands.CommandException;
import com.sk89q.regionbook.bukkit.RegionBookPlugin;
import java.io.IOException;

public class RegionBaseCommand {
    protected final RegionBookPlugin plugin;

    public RegionBaseCommand(RegionBookPlugin plugin) {
        this.plugin = plugin;
    }

    public void saveRegions(String world) throws CommandException {
        try {
            plugin.getGlobalRegionManager().save(world);
        } catch (IOException e) {
            throw new CommandException("Failed to write regions file: "
                    + e.getMessage());
        }
    }
}
