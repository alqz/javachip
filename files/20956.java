package oxen.logging;

import java.util.ArrayList;
import oxen.game.Game;

/**
 *
 * @author matt
 */
@SuppressWarnings("serial")
public class LogManager extends ArrayList<Logger> {

    /**
     * Buffer that logs all text produced so far.
     */
    private StringBuffer logAll;
    private Game manager;

    /**
     * Add a manager to the logManager
     * @param manager The manager to add.
     */
    public void addManager(Game manager) {
        this.manager = manager;
    }

    /**
     * Constructor for LogManager
     */
    public LogManager() {
        logAll = new StringBuffer();
        logAll.append("Necropolis: Amilios Lost Treasure\nType an instruction to begin\n");
    }

    /**
     * Log the input string to the logger
     * @param input Received input to log.
     */
    public void logInput(String input) {
        log("> " + input.trim());
        logAll.append("> ").append(input.trim()).append("\n");
    }

    /**
     * Log the output string to the logger
     * @param output The string to log.
     */
    public void logOutput(String output) {
        log(format(output));
        logAll.append(output).append("\n");
    }
    

    /**
     * Log a message to the logger
     * @param message The message to log.
     */
    private void log(String message) {
        for (Logger logger : this) {
            logger.log(( message ));
        }
    }

    /**
     * Format a string input by adding the player name where necessary.
     * @param input The string that needs to be formatted.
     * @return The input string formatted for output.
     */
    private String format(String input) {
        String out = input;
        if (manager.getCurrentState() != null) {
            out = input.replace("${player.name}", manager.getCurrentState().getPlayer().getName());
        }
        return out;
    }

    /**
     *
     * @return all logs.
     */
    public String getAllLogs() {
        return logAll.toString();
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
