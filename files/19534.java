/*******************************************************************************
 * Copyright (c) 2010 Composent, Inc. and others. All rights reserved. This
 * program and the accompanying materials are made available under the terms of
 * the Eclipse Public License v1.0 which accompanies this distribution, and is
 * available at http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors: Composent, Inc. - initial API and implementation
 ******************************************************************************/
package org.eclipse.ecf.provider.wave.google.identity;

import org.eclipse.ecf.core.identity.ID;
import org.eclipse.ecf.core.identity.IDCreateException;
import org.eclipse.ecf.core.identity.Namespace;

public class WaveletNamespace extends Namespace {

	public static final String NAME = "ecf.namespace.googlewave.wavelet";
	public static final String SCHEME = "wavelet";

	private static final long serialVersionUID = 2615028840514406159L;

	public WaveletNamespace() {
		super(NAME, SCHEME);
	}
	
	public WaveletNamespace(String name, String desc) {
		super(name, desc);
	}

	public ID createInstance(Object[] objects) throws IDCreateException {
		try {
			if (objects == null || objects.length != 3 || objects[0] == null) {
				throw new IllegalArgumentException("Invalid WaveId creation arguments");
			}

			if (!(objects[0] instanceof String)) {
				throw new IllegalArgumentException("Invalid WaveId creation arguments");
			}

			String waveletDomain = (String) objects[0];
			String waveId = (String) objects[1];
			String waveletId = (String) objects[2];
			
			return new WaveletID(this, waveletDomain, waveId, waveletId);
		} catch (Exception e) {
			throw new IDCreateException("WaveletID creation failed", e);
		}
	}

	public String getScheme() {
		return SCHEME;
	}

}
