/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mas.worldmodel.entity;

import java.util.ArrayList;
import java.util.LinkedList;

/**
 *
 * @author christopher_hh
 */
public class WayPointList {

    private LinkedList<WayPoint> wayPointList;
    
    public WayPointList() {
        wayPointList        = new LinkedList<WayPoint>();
    }
    
    /**
     * Returns the next WayPoint in the List and REMOVES it from the list.
     * @return the next WayPoint
     */
    public WayPoint getFirstWayPoint() {
        WayPoint wp = wayPointList.getFirst();
        wayPointList.remove(wp);
        return wp;
    }
    
    public void addWayPoint(WayPoint wp) {
        wayPointList.add(wp);
    }
    
    public void addWayPoint(double x, double y) {
        addWayPoint(new WayPoint(x, y));
    }
    
    public LinkedList<WayPoint> getWayPoints() {
        return wayPointList;
    }
    
    public static void sortByDistance(WayPointList list) {
        //TODO needs implementation
    }
}
