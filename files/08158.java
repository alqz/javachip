package com.simpleorpg.common;

import java.io.Serializable;

public class Enemy implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private String name;
	private String ref;
	private String id;
	private int x = 0, y = 0;
	
	public Enemy() {
		
	}
	
	public Enemy(Enemy enemy) {
		setName(enemy.getName());
		setRef(enemy.getRef());
		setId(enemy.getId());
		setX(enemy.getX());
		setY(enemy.getY());
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getRef() {
		return ref;
	}
	public void setRef(String ref) {
		this.ref = ref;
	}

}
