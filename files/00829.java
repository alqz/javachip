package quoting.servlets;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import quoting.entities.*;
import quoting.managers.*;

/**
 * Servlet implementation class QuoteServlet
 */
@WebServlet("/Quote")
public class QuoteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	
	@EJB
	CompanyManager companyManager;
	
	@EJB
	CompanyZipGroupManager companyZipGroupMananger;
	
	@EJB
	PlanManager planManager;
	
	@EJB
	QuoteManager quoteMangager;
	
	@EJB
	ZipGroupingManager zipGroupingManager;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int zipCode = Integer.parseInt(request.getParameter("zipCode"));
		int age = Integer.parseInt(request.getParameter("age"));
		char gender = request.getParameter("gender").toCharArray()[0];
		boolean isSmoker;
		char smoker = request.getParameter("smoker").toCharArray()[0];
		isSmoker = (smoker == 'T')?true:false;
			
		List<QuoteViewMode> quotes = new ArrayList<>();
		
		List<Integer> zipGroupIds = zipGroupingManager.getGroupsByZip(zipCode);
		List<Insurance_Company> companies = companyManager.all();
		for(Insurance_Company company : companies){
			List<Plan> plans = planManager.allForCompany(company);
			for(Plan plan : plans){
				CompanyZipGroup companyZipGroup = null;
				for(Integer id : zipGroupIds){
					CompanyZipGroup companyZipGrouptemp = companyZipGroupMananger.getIdByZipGroupId(id, company);
					if(!companyZipGrouptemp.equals(null)){
						companyZipGroup = companyZipGrouptemp;
					}
				}
				
				Quote entity = quoteMangager.getQuoteByInput(age, gender, plan, isSmoker, companyZipGroup);
				QuoteViewMode quote = new QuoteViewMode(company.getName(), plan.getPlan(), entity.getPrice());
				
				quotes.add(quote);
			}
		}
		
		request.setAttribute("quotes", quotes);
		
		request.getRequestDispatcher("quoteDisplay.jsp").forward(request, response);
	}

}
