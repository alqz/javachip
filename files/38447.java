/**
 * 
 */
package com.oauth.wrap.model;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

/**
 * @author Alex
 * 
 */
public class AccessTokenTest {

    AccessToken accessToken;
    RefreshToken refreshToken;

    @Before
    public void setup() {
        accessToken = new AccessToken();
        refreshToken = new RefreshToken();
    }

    @Test
    public void shouldReturnValidAccessToken() {
        accessToken.setAccessToken("accessToken");
        accessToken.setAccessTokenExpiration("accessTokenExpiration");
        assertEquals("accessToken", accessToken.getAccessToken());
        assertEquals("accessTokenExpiration", accessToken.getAccessTokenExpiration());
        assertEquals(true, accessToken.isValid());
    }

    @Test
    public void shouldReturnValidAccessTokenIncludingRefreshToken() {
        accessToken.setAccessToken("accessToken");
        accessToken.setAccessTokenExpiration("accessTokenExpiration");
        accessToken.setRefreshToken(refreshToken);
        refreshToken.setRefreshToken("refreshToken");

        assertEquals("accessToken", accessToken.getAccessToken());
        assertEquals("accessTokenExpiration", accessToken.getAccessTokenExpiration());
        assertEquals(refreshToken, accessToken.getRefreshToken());
        assertEquals(true, accessToken.isValid());
        assertEquals(true, accessToken.isValidIncludingRefreshToken());
    }

    @Test
    public void shouldReturnInvalidAccessToken() {
        assertEquals(false, accessToken.isValid());
        assertEquals(false, accessToken.isValidIncludingRefreshToken());
    }

    @Test
    public void shouldReturnInvalidAccessTokenWithErrorReason() {
        accessToken.setErrorReason("errorReason");
        assertEquals("errorReason", accessToken.getErrorReason());
        assertEquals(false, accessToken.isValid());
    }
}
