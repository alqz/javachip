/**
 * Author: Shane M. Israel
 * Date: April 27, 2012
 * Version: 2.2
 */

import java.awt.AWTException;
import java.awt.BorderLayout;
import java.awt.Desktop;
import java.awt.Rectangle;
import java.awt.SystemTray;
import java.awt.TrayIcon;
import java.awt.TrayIcon.MessageType;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.swing.ImageIcon;
import javax.swing.JColorChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import com.melloware.jintellitype.HotkeyListener;
import com.melloware.jintellitype.JIntellitype;
import com.melloware.jintellitype.JIntellitypeConstants;

public class Tray extends JFrame implements ActionListener
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -3673374650195882019L;
	OverlayPanel overlayPanel;
	Preferences preferences;
	Upload upload;
	Upload textUpload;
	int x1 = 0, y1 = 0, x2 = 0, y2 = 0;
	int screenW, screenH;
	Tray tray;
	JXTrayIcon trayIcon;
	String os = System.getProperty("os.name");
	JColorChooser colorChooser;

	public Tray()
	{
		new DataDirectory();
		tray = this;
		this.setAlwaysOnTop(true);

		if (!SystemTray.isSupported())
		{
			System.out.println("SystemTray is not supported");
			return;
		}
		try
		{
			if (os.indexOf("Win") >= 0)
				JIntellitype.getInstance().addHotKeyListener(
						new HotkeyListener()
						{

							@Override
							public void onHotKey(int identifier)
							{
								if (identifier == 1) // CTRL + SHIFT + 1
								{
									overlayPanel.setUploadSnippet();
								} else if (identifier == 2) // CTRL + SHIFT + 2
								{
									overlayPanel.setUploadScreenshot();
								} else if (identifier == 3) // CTRL + SHIFT + 3
								{
									overlayPanel.setSaveSnippet();
								} else if (identifier == 4) // CTRL + SHIFT + 4
								{
									overlayPanel.setSaveScreenshot();
								} else if (identifier == 5) // ALT + SHIFT + 1
								{
									textUpload = new Upload(false, tray);
									/*
									 * Upload text to pastebin
									 */
								}

							}
						});
			else
				JOptionPane.showMessageDialog(null,
						"Hotkeys are only available on Windows OS",
						"Hotkeys Disabled", JOptionPane.WARNING_MESSAGE);
		} catch (Exception e)
		{
			e.printStackTrace();
		}

		initializeTray();
		setUndecorated(true);
		if (os.indexOf("Win") >= 0)
			registerHotkeys();
		initializeOverlayPanel();
	}

	private void initializeTray()
	{
		String icon;

		if (os.indexOf("Win") >= 0)
			icon = "trayIcon.png";
		else
			icon = "trayIconMac.png";

		ImageIcon ii = new ImageIcon(this.getClass().getResource(
				"/images/" + icon));
		final JPopupMenu popup = new JPopupMenu();

		trayIcon = new JXTrayIcon(ii.getImage());
		trayIcon.addActionListener(this);
		trayIcon.setActionCommand("tray");

		final SystemTray tray = SystemTray.getSystemTray();

		JMenu uploadMenu = new JMenu("Upload");
		uploadMenu.setIcon(new ImageIcon(this.getClass().getResource(
				"/images/icons/image_upload.png")));
		JMenu saveMenu = new JMenu("Save");
		saveMenu.setIcon(new ImageIcon(this.getClass().getResource(
				"/images/icons/image_save.png")));
		JMenuItem prefMenu = new JMenuItem("Preferences");
		prefMenu.setIcon(new ImageIcon(this.getClass().getResource(
				"/images/icons/pref.png")));
		prefMenu.addActionListener(this);
		prefMenu.setActionCommand("preferences");
		JMenuItem uScreenshot = new JMenuItem("Screenshot [Ctrl+Shift+2]");
		uScreenshot.setIcon(new ImageIcon(this.getClass().getResource(
				"/images/icons/screenshot.png")));
		uScreenshot.addActionListener(this);
		uScreenshot.setActionCommand("uScreen");
		JMenuItem uSnippet = new JMenuItem("Snippet [Ctrl+Shift+1]");
		uSnippet.setIcon(new ImageIcon(this.getClass().getResource(
				"/images/icons/snippet.png")));
		uSnippet.addActionListener(this);
		uSnippet.setActionCommand("uSnippet");
		JMenuItem uClipboard = new JMenuItem("Clipboard [Alt+Shift+1]");
		uClipboard.setIcon(new ImageIcon(this.getClass().getResource(
				"/images/icons/text.png")));
		uClipboard.addActionListener(this);
		uClipboard.setActionCommand("uClipboard");
		JMenuItem sScreenshot = new JMenuItem("Screenshot [Ctrl+Shift+4]");
		sScreenshot.setIcon(new ImageIcon(this.getClass().getResource(
				"/images/icons/screenshot.png")));
		sScreenshot.addActionListener(this);
		sScreenshot.setActionCommand("sScreen");
		JMenuItem sSnippet = new JMenuItem("Snippet [Ctrl+Shift+3]");
		sSnippet.setIcon(new ImageIcon(this.getClass().getResource(
				"/images/icons/snippet.png")));
		sSnippet.addActionListener(this);
		sSnippet.setActionCommand("sSnippet");
		JMenuItem multiUploadItem = new JMenuItem("Multi Image Uploader");
		multiUploadItem.setIcon(new ImageIcon(this.getClass().getResource(
				"/images/icons/multi-image.png")));
		multiUploadItem.addActionListener(this);
		multiUploadItem.setActionCommand("multi_upload");
		JMenuItem exitItem = new JMenuItem("Quit");
		exitItem.setIcon(new ImageIcon(this.getClass().getResource(
				"/images/icons/exit.png")));
		exitItem.addActionListener(this);
		exitItem.setActionCommand("exit");
		JMenuItem aboutItem = new JMenuItem("About");
		aboutItem.setIcon(new ImageIcon(this.getClass().getResource(
				"/images/icons/about_icon.png")));
		aboutItem.addActionListener(this);
		aboutItem.setActionCommand("about");
		uploadMenu.add(uClipboard);
		uploadMenu.addSeparator();
		uploadMenu.add(uSnippet);
		uploadMenu.add(uScreenshot);
		uploadMenu.addSeparator();
		uploadMenu.add(multiUploadItem);
		saveMenu.add(sSnippet);
		saveMenu.addSeparator();
		saveMenu.add(sScreenshot);
		popup.add(aboutItem);
		// popup.addSeparator();
		popup.add(prefMenu);
		popup.addSeparator();
		popup.add(uploadMenu);
		popup.add(saveMenu);
		popup.addSeparator();
		popup.add(exitItem);

		popup.setLightWeightPopupEnabled(true);
		trayIcon.setJPopupMenu(popup);

		try
		{
			tray.add(trayIcon);
			trayIcon.displayMessage("Snipping Tool++",
					"Right click for more options!", MessageType.INFO);
		} catch (AWTException e)
		{
			System.out.println("TrayIcon could not be added.");
		}

	}

	private void registerHotkeys()
	{
		// TODO Auto-generated method stub
		JIntellitype keyhook = JIntellitype.getInstance();
		keyhook.registerHotKey(1, JIntellitypeConstants.MOD_CONTROL
				+ JIntellitypeConstants.MOD_SHIFT, '1');
		keyhook.registerHotKey(2, JIntellitypeConstants.MOD_CONTROL
				+ JIntellitypeConstants.MOD_SHIFT, '2');
		keyhook.registerHotKey(3, JIntellitypeConstants.MOD_CONTROL
				+ JIntellitypeConstants.MOD_SHIFT, '3');
		keyhook.registerHotKey(4, JIntellitypeConstants.MOD_CONTROL
				+ JIntellitypeConstants.MOD_SHIFT, '4');
		keyhook.registerHotKey(5, JIntellitypeConstants.MOD_ALT
				+ JIntellitypeConstants.MOD_SHIFT, '1');

	}

	public void initializeOverlayPanel()
	{
		if (overlayPanel != null)
			this.remove(overlayPanel);

		Rectangle bounds = new ScreenBounds().getBounds();
		setBounds(bounds); // sets the frame bounds

		overlayPanel = new OverlayPanel(bounds.width, bounds.height, this);
		add(overlayPanel, BorderLayout.CENTER);
	}

	public static void main(String[] args) throws IOException
	{
		try
		{
			// Set System L&F
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (UnsupportedLookAndFeelException e)
		{
			// handle exception
		} catch (ClassNotFoundException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InstantiationException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		new Tray();
	}

	@Override
	public void actionPerformed(ActionEvent e)
	{
		// TODO Auto-generated method stub
		Object command = e.getActionCommand();
		if (command.equals("uSnippet"))
		{
			overlayPanel.setUploadSnippet();
		} else if (command.equals("uScreen"))
		{
			overlayPanel.setUploadScreenshot();
		} else if (command.equals("sSnippet"))
		{
			overlayPanel.setSaveSnippet();
		} else if (command.equals("sScreen"))
		{
			overlayPanel.setSaveScreenshot();
		} else if (command.equals("uClipboard"))
		{
			textUpload = new Upload(false, tray);
		} else if (command.equals("about"))
		{
			new AboutSplash();
		} else if (command.equals("preferences"))
		{
			preferences = new Preferences();
		} else if (command.equals("multi_upload"))
		{
			new MultiUploader();
		} else if (command.equals("tray"))
		{
			try
			{
				Desktop.getDesktop().open(new File(DataDirectory.CAPTURE_PATH));
			} catch (IOException e1)
			{
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		} else if (command.equals("exit"))
		{
			System.exit(0);
		}
	}

}
