package org.pegadi.server.user;

import no.dusken.common.model.Person;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * User: jan-preben
 * Date: Sep 10, 2010
 */
public class UserServerImpl extends DatabaseUserServer {

    private String emailHost;

    public void setEmailHost(String emailHost) {
        this.emailHost = emailHost;
    }

    @Override
    protected String getUserByIdQuery() {
        return "select * from userinfo where userid=?";
    }

    @Override
    protected String getUserByUsernameQuery() {
        return "select userid from userinfo where username=?";
    }

    @Override
    protected String loginQuery() {
        return "select userid from userinfo where username=?";
    }

    @Override
    protected String getUsersByRoleQuery() {
        return "SELECT userinfo.* FROM userinfo, role, userrole WHERE " +
                    " role.id=? AND userinfo.active=? " +
                    " AND userrole.fk_userid = userinfo.userid " +
                    " AND userrole.fk_roleid = role.id " +
                    "  ORDER BY userinfo.firstname";
    }

    @Override
    protected String getAllUsersQuery(boolean inactive) {
        String query = "select * from userinfo";
        return inactive ? query : query + " Where active=1";
    }

    @Override
    protected String isActiveQuery() {
        return "Select active From userinfo Where userid=?";
    }

    @Override
    protected String hasRoleQuery() {
        return "SELECT fk_userid FROM userrole WHERE (fk_roleid = ? AND fk_userid=?)";
    }

    @Override
    protected ParameterizedRowMapper<Person> getRowMapper() {
        return new ParameterizedRowMapper<Person>() {
            public Person mapRow(ResultSet rs, int rowNr) throws SQLException {
                return new Person(rs.getLong("userid"),
                    rs.getString("firstname"),
                    rs.getString("lastname"),
                    rs.getString("username"),
                    rs.getString("username") + "@" + emailHost);
            }
        };
    }

}
