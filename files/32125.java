package org.scrumbusters.viitegen.service;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.springframework.stereotype.Service;

/**
 *
 *
 * @author scrumbusters
 */
@Service
public class ACMServiceImpl implements ACMService {

    protected String luoAcmLatausosoiteArtikkelista(String url) {
        String id = "";
        String parentId = "";
        
        if (!url.startsWith("http://dl.acm.org/citation.cfm?id=")) {
            return "";
        }
        String kokoIdParametri = erotaIdParametri(url);
        if (kokoIdParametri == null) {
            return "";
        }
        
        id = erotaIdOsa(kokoIdParametri);
        if (hasParentId(kokoIdParametri)) {
            parentId = erotaParentId(kokoIdParametri);
        }
        return "http://dl.acm.org/downformats.cfm?id=" + id + "&parent_id=" + 
                parentId + "&expformat=bibtex";
        
    }

    @Override
    public String haeAcmViite(String url) {      
        url = luoAcmLatausosoiteArtikkelista(url);
        Scanner lukija;
        InputStream inputStream = null;
        String viite = "";
        if (url.isEmpty()) {
            return viite;
        }
        try {
            URL osoite = new URL(url);
            inputStream = osoite.openStream();
            lukija = new Scanner(inputStream);
            while (lukija.hasNext()) {
                viite += lukija.nextLine();
                viite += "\n";
            }
        } catch (Exception e) {
        } finally {
            try {
                inputStream.close();
            } catch (IOException ioe) {
            }
        }
        return viite;
    }
    
    private boolean hasParentId(String idOsa) {
        String[] ids = idOsa.split("\\.");
        if (ids.length > 1) {
            return true;
        }
        return false;
    }

    private String erotaParentId(String idOsa) {
        return idOsa.split("\\.")[0];
    }

    private String erotaIdOsa(String idOsa) {
        String[] ids = idOsa.split("\\.");
        if (ids.length > 1) {
            return ids[1];
        }
        return ids[0];
    }
    
    private String erotaIdParametri(String url) {
        String idMuoto = "id=[\\d\\.]+&";
        Matcher idMatcher = Pattern.compile(idMuoto).matcher(url);
        
        if (idMatcher.find()) {
            String kokoIdOsa = idMatcher.group();
            kokoIdOsa = kokoIdOsa.substring(3, kokoIdOsa.length() - 1);
            return kokoIdOsa;
            
        }
        return null;
    }
}
