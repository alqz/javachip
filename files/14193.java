package oxen.game.components.riddles;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author nikola
 */
public class RiddleTest {
    private Riddle riddle;

    @Before
    public void setUp() {
        riddle = new Riddle("5", "10 miles");
    }

    @Test
    public void testSolve() {
        assertTrue(riddle.solve("10 miles"));
        assertFalse(riddle.solve("miles 10"));
        assertTrue(riddle.isSolved());
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
