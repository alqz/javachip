package org.scrumbusters.viitegen.controller;

import java.net.URISyntaxException;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.scrumbusters.viitegen.domain.Viite;
import org.scrumbusters.viitegen.service.BibTeXGeneraattoriImpl;
import org.scrumbusters.viitegen.service.ViiteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Tämä kontrolleri kuuntelee osoitetta /generoi ja ohjaa generoi.jsp-sivulle.
 * 
 * @author scrumbusters
 */

@Controller
public class GeneroiController {
    
    @Autowired
    protected BibTeXGeneraattoriImpl generaattori;
    
    @Autowired
    protected ViiteService viiteService;
    
    @RequestMapping("generoi")
    public String kysyNimi() {
        return "generoi";
    }
    
    @RequestMapping(value = "generoi", method = RequestMethod.POST)
    public ResponseEntity<String> tiedosto(@RequestParam String nimi) throws URISyntaxException { 
        HttpHeaders responseHeaders = new HttpHeaders();
        String validRegexp = "[\\wåäöÅÄÖ]+"; //aakkoset, alaviiva, numerot. pituus > 0

        if (!nimi.matches(validRegexp)) {
            return redirectionTo("generoi");
        }

        List<Viite> viitteet = viiteService.annaViitteet();
        String bibtex = generaattori.generoiBibtex(viitteet);

        responseHeaders.setContentType(MediaType.parseMediaType("application/x-bibtex"));
        responseHeaders.set("Content-Disposition", "attachment; filename=" + nimi + ".bib");
        return new ResponseEntity<String>(bibtex, responseHeaders, HttpStatus.CREATED);
    }
    
    @RequestMapping(value = "generoi_hausta", method = RequestMethod.POST)
    public ResponseEntity<String> tiedostoHausta(Model model, @RequestParam String nimi,
                HttpServletRequest req) throws URISyntaxException { 
        HttpHeaders responseHeaders = new HttpHeaders();
        String validRegexp = "[\\wåäöÅÄÖ]+"; //aakkoset, alaviiva, numerot. pituus > 0

        if (!nimi.matches(validRegexp)) {
            return redirectionTo("haku");
        }

        List<Viite> viitteet = viiteService.haeNimella(req.getParameter("hakusana"));
        String bibtex = generaattori.generoiBibtex(viitteet);

        responseHeaders.setContentType(MediaType.parseMediaType("application/x-bibtex"));
        responseHeaders.set("Content-Disposition", "attachment; filename=" + nimi + ".bib");
        return new ResponseEntity<String>(bibtex, responseHeaders, HttpStatus.CREATED);
    }
    
    private ResponseEntity<String> redirectionTo(String uri) throws URISyntaxException {
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("Content-Type", "text/html");
        responseHeaders.set("Location", uri);
        return new ResponseEntity<String>(null, responseHeaders, HttpStatus.MOVED_PERMANENTLY);
    }
}
