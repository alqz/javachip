/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package qse_sepm_ss12_07.service;

import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import org.apache.log4j.Logger;
import qse_sepm_ss12_07.domain.*;

/**
 *
 * @author sbr
 */
/**
 * Calculator for shopping lists. usage: 1. add all needed stocks for categories
 * and products. 2. get your calculated productlist
 *
 * Calculator will calculate automatically the difference between existing stock
 * and your needed amount. So the shopping list contains only product and
 * productcategory entries which are necessary.
 */
public class ShoppingListGenerator
{
	private static final Logger log = Logger.getLogger(ShoppingListGenerator.class);
	private ResourceBundle labels = ResourceBundle.getBundle("yasl_strings");
	private GeneratorNeededStockList neededCategoryEntries;
	private GeneratorNeededStockList neededProductEntries;
	private IProductService productService;
	private IProductCategoryService categoryService;

	public void setProductService(IProductService productService)
	{
		this.productService = productService;
	}

	public void setCategoryService(IProductCategoryService categoryService)
	{
		this.categoryService = categoryService;
	}

	public ShoppingListGenerator()
	{
		reset();
	}

	public void addNeededStockForCategory(ProductCategory category, double neededAmount)
	{
		log.trace("addNeededStockForCategory: category=" + category.getName() + " neededAmount="+neededAmount);
		GeneratorStockEntry entry = neededCategoryEntries.getEntry(category.getEntity().getId());
		entry.addAmount(neededAmount);
	}

	public void addNeededStockForProduct(Product product, double neededAmount)
	{
		log.trace("addNeededStockForProduct: product=" + product.getName() + " neededAmount="+neededAmount);
		GeneratorStockEntry entry = neededProductEntries.getEntry(product.getEntity().getId());
		entry.addAmount(neededAmount);
	}

	public ShoppingList calculateShoppingList(String listName) throws ServiceException
	{
		GeneratorAvailableStockList availableStockEntries = new GeneratorAvailableStockList();
		ShoppingList returnList = new ShoppingList();
		;
		returnList.setName(labels.getString("generator_name_generated_shoppinglist") + " - " + listName);

		// ----------------calculate available stock entries ------------------
		List<Product> calculatedProductsForAvailability = new ArrayList<Product>();
		// add all products for categories
		for (GeneratorStockEntry categoryEntry : neededCategoryEntries)
		{
			calculatedProductsForAvailability.addAll(getProductsForCategory(categoryEntry.getId()));
		}

		// add products
		for (GeneratorStockEntry productEntry : neededProductEntries)
		{
			calculatedProductsForAvailability.add(productService.getProductByID(productEntry.getId()));
		}

		// generate available stock from necessaryproducts
		availableStockEntries.initializeWithProducts(calculatedProductsForAvailability);
		
		// generate product entries
		List<ProductListProductEntry> productEntries = calculateProductEntries(availableStockEntries);		
		
		// generate category entries
		List<ProductListProductCategoryEntry> productCategoryEntries = calculateProductCategoryEntries(availableStockEntries);		
		
		returnList.addProductListEntries(productEntries);
		returnList.addProductListEntries(productCategoryEntries);
		
		return returnList;
	}
	
	private List<ProductListProductEntry> calculateProductEntries(GeneratorAvailableStockList availableStock) throws ServiceException
	{
		try
		{
			List<ProductListProductEntry> returnList = new ArrayList<ProductListProductEntry>();
			for(GeneratorStockEntry generatorProductEntry: neededProductEntries)
			{
				// get generator entry for available stock
				GeneratorAvailableStockEntry availableStockEntry = availableStock.getEntryByProductId(generatorProductEntry.getId());
				
				// create new Product entry (ProductList)
				ProductListProductEntry newEntry = new ProductListProductEntry();
				
				// get Product from available
				newEntry.setProduct(availableStockEntry.getProduct());
				
				// use amount from available and get the difference as amount for new entry
				newEntry.setAmount(availableStockEntry.useAmount(generatorProductEntry.getAmount()));
				
				// add ProductListProductEntry when amount > 0
				if(newEntry.getAmount() > 0)
				{
					returnList.add(newEntry);
				}
			}
			
			return returnList;
		}
		catch (Exception e)
		{
			log.error("calculateProductEntries failed",e);
			throw new ServiceException("calculateProductEntries failed",e);
		}
	}
	
	
	private List<ProductListProductCategoryEntry> calculateProductCategoryEntries(GeneratorAvailableStockList availableStock) throws ServiceException
	{
		try
		{
			List<ProductListProductCategoryEntry> returnList = new ArrayList<ProductListProductCategoryEntry>();
			for(GeneratorStockEntry generatorCategoryEntry: neededCategoryEntries)
			{
				// create new Product entry (ProductList)
				ProductListProductCategoryEntry newEntry = new ProductListProductCategoryEntry();
				
				// get ProductCategory from Service
				newEntry.setProductCategory(categoryService.getProductCategoryById(generatorCategoryEntry.getId()));
				
				// use amount from availableStock and get the difference as amount for new entry
				newEntry.setAmount(availableStock.useAmountForCategory(generatorCategoryEntry));	
				
				// add ProductListProductEntry when amount > 0
				if(newEntry.getAmount() > 0)
				{
					returnList.add(newEntry);
				}
			}
			
			return returnList;
		}
		catch (Exception e)
		{
			log.error("calculateProductCategoryEntries failed",e);
			throw new ServiceException("calculateProductCategoryEntries failed",e);
		}
	}
	
	
	
	public void reset()
	{
		neededCategoryEntries = new GeneratorNeededStockList();
		neededProductEntries = new GeneratorNeededStockList();
	}

	private List<Product> getProductsForCategory(int categoryId) throws ServiceException
	{
		List<Product> products = productService.getProductsByProductCategory(categoryService.getProductCategoryById(categoryId));
		return products;
	}

	private Product getProduct(int productid) throws ServiceException
	{
		return productService.getProductByID(productid);
	}

	// ------------------- GENERATOR classes --------------------------------------------
	public class GeneratorNeededStockList extends ArrayList<GeneratorStockEntry>
	{
		public GeneratorStockEntry getEntry(int id)
		{
			for (GeneratorStockEntry entry : this)
			{
				if (entry.getId() == id)
				{
					return entry;
				}
			}

			// no entry found
			GeneratorStockEntry newEntry = new GeneratorStockEntry(id);
			this.add(newEntry);
			return newEntry;
		}
	}

	public class GeneratorAvailableStockList extends ArrayList<GeneratorAvailableStockEntry>
	{
		
		public double useAmountForCategory(GeneratorStockEntry categoryEntry) throws ServiceException
		{
			double returnAmount = categoryEntry.getAmount();
			
			log.trace("useAmountForCategory id=" + categoryEntry.getId() + " amount=" + categoryEntry.getAmount() );
			
			for(GeneratorAvailableStockEntry entry:this)
			{
				if(returnAmount > 0)
				{
					log.trace("useAmountForCategory entryid=" + entry.getCategoryId() + " amount=" + entry.getAvailableAmount() );
					
					if(entry.getCategoryId() == categoryEntry.getId())
					{
						returnAmount = entry.useAmount(returnAmount);
						log.trace("useAmountForCategory returnamount="+returnAmount );
					}
				}
				
				// check for earlier exit of loop
				if(returnAmount == 0)
				{
					return 0;
				}
			}
			
			log.trace("useAmountForCategory returnvalue=" + returnAmount );
					
			return returnAmount;
		}
		
		public GeneratorAvailableStockEntry getEntryByProductId(int id)
		{
			for(GeneratorAvailableStockEntry entry: this)
			{
				if(entry.getProductId()==id)
				{
					return entry;
				}
			}
			return null;
		}
		
		public void initializeWithProducts(List<Product> products)
		{
			for (Product product : products)
			{
				GeneratorAvailableStockEntry newEntry = new GeneratorAvailableStockEntry(product);
				if(!contains(newEntry))
				{
					add(newEntry);
				}
			}
		}
	}

	public class GeneratorStockEntry
	{

		protected int id;
		private double amount;

		public int getId()
		{
			return id;
		}

		public double getAmount()
		{
			return amount;
		}

		public GeneratorStockEntry(int id)
		{
			this.id = id;
			this.amount = 0;
		}

		public void addAmount(double amount)
		{
			this.amount += amount;
		}

		@Override
		public boolean equals(Object obj)
		{
			if (obj == null)
			{
				return false;
			}
			final GeneratorStockEntry other = (GeneratorStockEntry) obj;
			if (this.id != other.id)
			{
				return false;
			}
			return true;
		}

		@Override
		public int hashCode()
		{
			int hash = 5;
			hash = 17 * hash + this.id;
			return hash;
		}
	}

	protected class GeneratorAvailableStockEntry 
	{
		private Product product;
		private double availableAmount;

		public int getCategoryId() throws ServiceException
		{
			if(product.getProductCategory() == null)
			{
				product.setProductCategory(categoryService.getProductCategoryById(product.getEntity().getProductCategoryId()));
			}
			
			return product.getProductCategory().getEntity().getId();
		}
		
		public int getProductId()
		{
			return product.getEntity().getId();
		}
		
		public Product getProduct()
		{
			return product;
		}
		
		public double getAvailableAmount()
		{
			return availableAmount;
		}
		
		public double useAmount(double amount)
		{
			double returnValue;
			log.trace("useAmount: before "+toString()+" amount="+amount);
			
			if(amount <= availableAmount)
			{
				this.availableAmount = availableAmount - amount;
				returnValue = 0;
			}
			else
			{	
				returnValue = amount-availableAmount;
				this.availableAmount = 0;
			}
			
			log.trace("useAmount: after "+toString()+"returnValue=" + returnValue);
				
			return returnValue;
		}
		
		/**
		 * Constructor
		 * @param product 
		 */
		public GeneratorAvailableStockEntry(Product product)
		{
			this.product = product;
			this.availableAmount = product.getStockAmount();
			log.trace("construct GeneratorAvailableStockEntry: product=" + product + " available=" + this.availableAmount );
			
		}

		@Override
		public boolean equals(Object obj)
		{
			if (obj == null)
			{
				return false;
			}
			if (getClass() != obj.getClass())
			{
				return false;
			}
			final GeneratorAvailableStockEntry other = (GeneratorAvailableStockEntry) obj;
			if (this.getProductId() != other.getProductId())
			{
				return false;
			}
			return true;
		}

		@Override
		public int hashCode()
		{
			int hash = 7;
			hash = 23 * hash + (this.getProductId());
			return hash;
		}

		@Override
		public String toString()
		{
			return "GeneratorAvailableStockEntry{" + "product=" + product + ", availableAmount=" + availableAmount + '}';
		}
		
		
	}
}
