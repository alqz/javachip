/*
 */
package qse_sepm_ss12_07.domain;

import java.util.ArrayList;
import java.util.List;
import qse_sepm_ss12_07.dao.ProductListEntity;
import qse_sepm_ss12_07.dao.RecipeEntity;

/**
 * 
 */
public class Recipe extends ProductList implements Cloneable, IRateable
{
	// be aware that this recipeEntity has nothing to do with, ProductList.recipeEntity
	private RecipeEntity recipeEntity;
    private List<Rating> ratings = new ArrayList<Rating>();

	public Recipe(ProductListEntity productListEntity, RecipeEntity entity)
	{
		super(productListEntity);
		this.recipeEntity = entity;
	}

	public Recipe()
	{
		this(new ProductListEntity(), new RecipeEntity());
		setName("Recipe");
	}

	public RecipeEntity getRecipeEntity()
	{
		return recipeEntity;
	}

	public void setRecipeEntity(RecipeEntity entity)
	{
		this.recipeEntity = entity;
	}

	public String getDescription()
	{
		return recipeEntity.getDescription();
	}

	public void setDescription(String description)
	{
		recipeEntity.setDescription(description);
	}

	@Override
	public boolean equals(Object obj)
	{
		if (obj == null)
		{
			return false;
		}
		if (!(obj instanceof Recipe))
		{
			return false;
		}
		
		final Recipe other = (Recipe) obj;
		
		if (this.recipeEntity != other.recipeEntity && (this.recipeEntity == null || !this.recipeEntity.equals(other.recipeEntity)))
		{
			return false;
		}
		return true;
	}

	@Override
	public int hashCode()
	{
		int hash = 5;
		hash = 97 * hash + (this.recipeEntity != null ? this.recipeEntity.hashCode() : 0);
		return hash;
	}

	public boolean isNew()
	{
		return (this.recipeEntity.getId() == -1);
	}

	public Object clone()
	{
		Recipe cloned = (Recipe) super.clone();
		return cloned;
	}

    public List<Rating> getRatings() {
        return ratings;
    }

    public void setRatings(List<Rating> ratings) {
        this.ratings = ratings;
    }

    public void addRating(Rating rating) {
        this.ratings.add(rating);
    }

    public void deleteRating(Rating rating) {
        this.ratings.remove(rating);
    }
}
