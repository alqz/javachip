package application;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.ini4j.Ini;
import org.ini4j.InvalidFileFormatException;

/**
 * Programmkonfiguration; liest Konfiguration aus config.ini, wenn die Datei nicht gefunden werden
 * kann, wird eine config.ini mit Default-Werten erstellt.
 */
public class Configuration {
	/**
	 * Serial Port Baudrate
	 */
	private int baud;
	/**
	 * Auflösung der Y-Achse des Video-Streams (px), wird zu Berechnung der
	 * X-Achse benutzt. Unterstützt nur Seitenverhältnis 4:3 
	 */
	private int yres;
	/**
	 * Kamera ID (i.d.R 0 (oder 1 bei mehreren WebCams))
	 */
	private int deviceid;
	/**
	 * Maluspunkte pro Decay-Zeiteinheit in Prozentpunkten
	 */
	private int decay;
	/**
	 * Verfallszeit (ms)
	 */
	private int decayTime;
	/**
	 * Höchstwert zur Ansteuerung des Pulsgenerators (in eine Richtung)
	 */
	private int maxCamPos;
	/**
	 * Maximaler Winkel der Kameradrehung (°, X-Achse)
	 */
	private int maxCamAngleX;
	/**
	 * Maximaler Winkel der Kameradrehung (°, Y-Achse)
	 */
	private int maxCamAngleY;
	/**
	 * Größe des Fensters in dem die Kamera Körper noch nicht verfolgt (°)
	 */
	private int moveThresholdX, moveThresholdY;
	/**
	 * Schwellwert über dem Körper verfolgt werden in Prozentpunkten
	 */
	private int bodyThreshold;
	/**
	 * Ausführlichkeit der Suite-Konsole (1..6)
	 */
	private int verbosity;
	/**
	 * Anzahl der gepufferten Bewegungen zur Berechnung der Position wenn die
	 * Oberkörpererkennung fehlschlägt
	 */
	private int motionQueueLength;
	/**
	 * Zeit nach der letzten Oberkörpererkennung nach der die Position durch
	 * Bewegungserkennung ermittelt wird (ms)
	 */
	private int bodyLostTime;
	/**
	 * Serial Port ID (z.B. "COM3" (Win) oder "/dev/ttyUSB0" (Linux))
	 */
	private String port;
	/**
	 * Soll die Kamera verwendet werden?
	 */
	private	boolean camOn;
	/**
	 * maximal zulässige Geschwindigkeit von Körpern (m/s); bei Überschreitung
	 * als zusätzlicher Körper erkannt
	 */
	private	double maxVelocity;
	/**
	 * minimale Höhe von Körpern relativ zur Kamera (m)
	 */
	private	double minHeight;
	/**
	 * Blickwinkel der Kamera (°, X&Y-Achse)
	 */
	private	double camFOVX, camFOVY;
	/**
	 * Bonuspunkte bei Oberkörpererkennung in Prozentpunkten
	 */
	private	double detectBonus;
	/**
	 * Bonuspunkte bei Bewegungserkennung in Prozentpunkten
	 */
	private	double motionBonus;
	/**
	 * maximale Anzahl von gleichzeitigen Bewegungen (Kamerabewegungen werden aussortiert)
	 */
	private int maxMotionObjects;
	/**
	 * config.ini Datei
	 */
	private transient final	File inifile = new File("config.ini");
	/**
	 * Parameterobjekt
	 */
	private transient Ini ini;

	/**
	 * Konstruktor; lädt ini-Datei und weist Parameter zu
	 */
	public Configuration() {
		this.loadIniFile();
		
		this.verbosity			= Integer.parseInt(this.ini.get("program", "verbosity"));
		
		this.baud 				= Integer.parseInt(this.ini.get("serial", "baud"));
		this.port				= this.ini.get("serial", "port");
		
		this.yres 				= Integer.parseInt(this.ini.get("cam", "yres"));
		this.camOn 				= Boolean.parseBoolean(this.ini.get("cam", "on"));
		this.deviceid			= Integer.parseInt(this.ini.get("cam", "deviceid"));
		this.maxCamPos			= Integer.parseInt(this.ini.get("cam", "maxcampos"));
		this.maxCamAngleX		= Integer.parseInt(this.ini.get("cam", "maxcamanglex"));
		this.maxCamAngleY		= Integer.parseInt(this.ini.get("cam", "maxcamangley"));
		this.camFOVX			= Double.parseDouble(this.ini.get("cam", "fovx"));
		this.camFOVY			= Double.parseDouble(this.ini.get("cam", "fovy"));
		
		this.detectBonus		= Double.parseDouble(this.ini.get("algorithm", "detectbonus"));
		this.motionBonus		= Double.parseDouble(this.ini.get("algorithm", "motionbonus"));
		this.decay				= Integer.parseInt(this.ini.get("algorithm", "decay"));
		this.decayTime			= Integer.parseInt(this.ini.get("algorithm", "decaytime"));
		this.maxVelocity		= Double.parseDouble(this.ini.get("algorithm", "maxvelocity"));
		this.minHeight			= Double.parseDouble(this.ini.get("algorithm", "minheight"));
		this.moveThresholdX		= Integer.parseInt(this.ini.get("algorithm", "movethresholdx"));
		this.moveThresholdY		= Integer.parseInt(this.ini.get("algorithm", "movethresholdy"));
		this.bodyThreshold		= Integer.parseInt(this.ini.get("algorithm", "bodythreshold"));
		this.motionQueueLength	= Integer.parseInt(this.ini.get("algorithm", "motionqueuelength"));
		this.bodyLostTime		= Integer.parseInt(this.ini.get("algorithm", "bodylosttime"));
		this.maxMotionObjects	= Integer.parseInt(this.ini.get("algorithm", "maxmotionobjects"));
	}
	
	/**
	 * Lädt ini-Datei; wenn die Datei nicht existiert wird eine ini mit
	 * Default-Werten erstellt
	 */
	private void loadIniFile() {
		if (this.inifile.exists()) {
			try {
				this.ini = new Ini(this.inifile);
			} catch (InvalidFileFormatException e) {
				System.out.println("Config.ini Fehler");
			} catch (IOException e) {
				System.out.println("Config.ini Fehler");
			}
		} else {
			try {
				this.createIniFile();
			} catch (IOException e) {
				throw new NoConfigFileException(e);
			}
			
			try {
				this.ini = new Ini(this.inifile);
			} catch (InvalidFileFormatException e) {
				throw new NoConfigFileException(e);
			} catch (IOException e) {
				throw new NoConfigFileException(e);
			}
		}			
	}
	
	/**
	 * Erstellt ini-Datei mit Default-Werten
	 * @throws IOException
	 */
	private void createIniFile() throws IOException {
		this.inifile.createNewFile();
		
		final 	ClassLoader 	classLoader 	= Thread.currentThread().getContextClassLoader();
		final 	InputStream 	configStream 	= classLoader.getResourceAsStream("resources/config/config.ini");
		
		final 	OutputStream 	newConfig 		= new FileOutputStream(this.inifile);
		final 	byte[] 			buf 			= new byte[1024];
		int 	len;
		
		while ((len = configStream.read(buf)) > 0) {
			newConfig.write(buf, 0, len);
		}
		configStream.close();
		newConfig.close();
	}
	
	public int getBaud() {
		return this.baud;
	}
	public String getPort() {
		return this.port;
	}
	public int getYres() {
		return this.yres;
	}
	public int getDeviceId() {
		return this.deviceid;
	}
	public boolean getCamOn() {
		return this.camOn;
	}	
	public double getDetectBonus() {
		return this.detectBonus;
	}
	public double getMotionBonus() {
		return this.motionBonus;
	}
	public int getDecay() {
		return this.decay;
	}
	public int getDecayTime() {
		return this.decayTime;
	}
	public int getMaxCamPos() {
		return this.maxCamPos;
	}
	public int getMaxCamAngleX() {
		return this.maxCamAngleX;
	}
	public int getMaxCamAngleY() {
		return this.maxCamAngleY;
	}
	public double getMaxVelocity() {
		return this.maxVelocity;
	}
	public double getMinHeight() {
		return this.minHeight;
	}
	public int getMoveThresholdX() {
		return this.moveThresholdX;
	}
	public int getMoveThresholdY() {
		return this.moveThresholdY;
	}
	public int getBodyThreshold() {
		return this.bodyThreshold;
	}
	public double getCamFOVX() {
		return this.camFOVX;
	}
	public double getCamFOVY() {
		return this.camFOVY;
	}
	public int getVerbosity() {
		return this.verbosity;
	}
	public int getMotionQueueLength() {
		return this.motionQueueLength;
	}
	public int getBodyLostTime() {
		return this.bodyLostTime;
	}
	public int getMaxMotionObjects() {
		return this.maxMotionObjects;
	}
}
