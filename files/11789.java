package intranet.interfaces.params.impl;

import intranet.domain.entities.Aluno;
import intranet.domain.entities.Hora;
import intranet.domain.entities.HoraComplementar;
import intranet.infra.repository.impl.HoraRepositorioImpl;
import intranet.infra.repository.impl.PessoaRepositorioImpl;
import intranet.interfaces.params.HoraComplementarParams;
import intranet.utils.DateUtil;
import intranet.utils.IntranetException;
import lombok.Data;
import lombok.extern.java.Log;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 
 * @author Gabriel Cardelli
 * @since Feb 21, 2012 - 12:44:11 AM
 *
 */
@Component(value="horaComplementarNovoParams")
@Data 
@Log
public class HoraComplementarNovoParams implements HoraComplementarParams {
	
	private String atividadeIdKey;
	private String atividadeId;
	private String numeroHoras;
	private String nomeEvento;
	private Aluno aluno;
	
	@Autowired private PessoaRepositorioImpl pessoaDAO;
	@Autowired private HoraRepositorioImpl horaDAO;

	public String getAtividadeId() {
		return atividadeId;
	}

	public void setAtividadeId(String atividadeId) {
		this.atividadeId = atividadeId;
	}

	public String getNumeroHoras() {
		return numeroHoras;
	}

	public void setNumeroHoras(String numeroHoras) {
		this.numeroHoras = numeroHoras;
	}

	public Hora getHora() throws IntranetException {
		log.info( atividadeIdKey );
		log.info( "Atividade Id: " + atividadeId );
		HoraComplementar horaComplementar = new HoraComplementar();
		horaComplementar.setAluno((Aluno) pessoaDAO.buscarPeloId(Long.parseLong(aluno.getId().toString())));
		horaComplementar.setAtividade(horaDAO.getAtividadeById( Integer.parseInt( atividadeIdKey) ));
		horaComplementar.setTitulo(nomeEvento);
		horaComplementar.setMinutos(DateUtil.getMinutesByHourMinutes(numeroHoras));
		return horaComplementar;
	}

	public Aluno getAluno() {
		return aluno;
	}

	public void setAluno(Aluno aluno) {
		this.aluno = aluno;
	}

	public String getNomeEvento() {
		return nomeEvento;
	}

	public void setNomeEvento(String nomeEvento) {
		this.nomeEvento = nomeEvento;
	}

	
	
}
