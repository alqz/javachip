package olympic.screens;

import com.googlecode.javacv.cpp.opencv_core.CvRect;
import olympic.util.GlobalConfiguration;

/**
 * Copyright (C) 2012 Christopher Copeland
 * <p/>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
public class ScreenIcon {

    private long touched;
    private CvRect region;
    private boolean active;

    /**
     * create a new screen icon with the given region
     *
     * @param region the region on the screen to inhabit
     */
    public ScreenIcon(CvRect region) {
        this.region = region;
        this.touched = 0;
        this.active = false;
    }

    /**
     * retrieve whether the icon is currently being touched
     *
     * @return whether the icon is touched
     */
    public final boolean isTouched() {
        return (this.touched > 0);
    }

    /**
     * retrieve whether the icon is currently flagged as being active
     *
     * @return whether the icon is active
     */
    public final boolean isActive() {
        return this.active;
    }

    /**
     * retrieve whether the icon has hovered for the required duration of time
     *
     * @return whether the icon can be activated
     */
    public final boolean isTouchedForDuration() {
        if (this.touched == 0 || this.active)
            return false;
        else
            return (getTouchTime() >= GlobalConfiguration.config().icons_hover_time);
    }

    /**
     * retrieve whether the passed rectangle falls within the bounds of the current icon
     *
     * @param other the other rectangle to compare
     * @return true if within bounds, false otherwise
     */
    public final boolean isInBounds(CvRect other) {
        return (other.x() >= this.region.x() &&
                other.y() >= this.region.y() &&
                (other.x() + other.width()) <= (this.region.x() + this.region.width()) &&
                (other.y() + other.height()) <= (this.region.y() + this.region.height()));
    }

    /**
     * retrieve how long the icon has been touched for
     *
     * @return the span of time in milliseconds
     */
    public final long getTouchTime() {
        if (this.touched == 0)
            return 0;
        else
            return (System.currentTimeMillis() - this.touched);
    }

    /**
     * retrieve the region associated with this particular screen icon
     *
     * @return the region
     */
    public final CvRect getRegion() {
        return this.region;
    }

    /**
     * flag the current screen icon as currently being touched
     *
     * @param touched whether the icon is being touched
     */
    public void touch(boolean touched) {
        this.touched = (touched ? System.currentTimeMillis() : 0);
    }

    /**
     * flag the current screen icon as being active
     *
     * @param active whether the icon is now active
     */
    public void active(boolean active) {
        this.active = active;
    }
}
