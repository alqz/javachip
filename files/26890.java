package oxen.worldparser;

import java.util.HashMap;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

/**
 *
 * @author James
 */
final class ElementMap extends HashMap<String, ParsedElement> {
    /**
     * Adds a ROOM, ITEM, NPC, ACTION or RIDDLE to this map.
     * @param type The type of the object being added, must be one of those listed above.
     * @param atts The SAX attributes of the object being added.
     * @return Returns the ID of the object being added.
     * @throws SAXException Thrown if an object with the same ID is already defined in this map.
     * Also thrown if an object is provided with no ID attribute.
     */
    public final String addElement(ElementType type, Attributes atts) throws SAXException {
        if (!( type == ElementType.ROOM || type == ElementType.ITEM || type == ElementType.NPC || type == ElementType.ACTION || type == ElementType.RIDDLE )) {
            throw new SAXException("Incorrect object type was supplied to addElement, should be either ROOM, ITEM, NPC, ACTION or RIDDLE.");
        } else {
            if (!( atts.getValue("id") == null )) {
                if (!this.containsKey(atts.getValue("id"))) {
                    super.put(atts.getValue("id"), new ParsedElement(atts, type));
                    return atts.getValue("id");
                } else {
                    throw new SAXException(type.getTagText() + " with id=" + atts.getValue("id") + " defined multiple times.");
                }
            } else {
                throw new SAXException("Encountered " + type.getTagText() + " with missing id.");
            }
        }
    }
    /**
     * DO NOT USE. EVER.
     * @param key THIS WILL THROW A RUNTIME EXCEPTION IF YOU USE IT.  
     * @param element THIS WILL THROW A RUNTIME EXCEPTION IF YOU USE IT.
     * @return Returns NOTHING. THROWS A RUNTIME EXCEPTION.
     * @deprecated addElement replaces the functionality of this method. Except for returning the ID of the added
     * object rather than the object itself.
     */
    @Deprecated
    @Override
    public final ParsedElement put(String key, ParsedElement element){
        throw new UnsupportedOperationException("You should be calling addElement().");
    }
}
