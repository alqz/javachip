package olympic.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.log4j.Logger;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.io.Reader;

/**
 * Copyright (C) 2012 Oliver
 * <p/>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
public class GlobalConfiguration {

    private static final Gson gson = new GsonBuilder().create();
    private static final GlobalConfiguration instance;

    public int screen_count;
    public int width;
    public int height;
    public boolean force;
    public int icons_height;
    public int icons_hover_time;
    public int scale;
    public int resize;

    static {
        Reader reader = null;
        try {
            reader = new InputStreamReader(new FileInputStream("./config/global.properties"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        instance = gson.fromJson(reader, GlobalConfiguration.class);
        Logger.getLogger(ScreenConfiguration.class).info("Loaded global configuration.");
        ScreenConfiguration.load();
    }

    /**
     * the global configuration instance
     *
     * @return the representation for the global instance properties file
     */
    public synchronized static GlobalConfiguration config() {
        return instance;
    }


}
