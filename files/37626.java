package org.blockfreie.lithium.common;

import java.io.File;

import org.junit.Assert;

public class CommonTestUtil {
	public static File getTempDirectory()
	{	try{
		File result = File.createTempFile("jython.",".tmp");
		result.delete();
		Assert.assertTrue(result.mkdir());
		return result;
		}catch(Exception e)
		{ throw new RuntimeException(e.getMessage(), e); }
	}
}
