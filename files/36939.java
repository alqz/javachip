package monitorStatus3GModem;

import javax.swing.*;
import java.awt.*;
import java.io.*;

/**
 * Created with IntelliJ IDEA.
 * User: Goncharov
 * Date: 22.06.12
 * Time: 10:13
 */
class OutputPanel extends JPanel {
	static JTextArea logArea;
	PipedInputStream pin;
	BufferedReader iis;
	PrintStream ps;

	public OutputPanel() {
		setLayout(new FlowLayout());

		logArea = new JTextArea("Log info: \n");
        logArea.setLineWrap(true);
        logArea.setEditable(false);

        JScrollPane logPane = new JScrollPane(logArea);
        logPane.setAutoscrolls(true);
        logPane.setPreferredSize(new Dimension(300, 130));
        add(logPane);

        /*

        pin = new PipedInputStream();
        PipedOutputStream pout;
        try {
            pout = new PipedOutputStream(this.pin);
            iis = new BufferedReader(new InputStreamReader(pin));
            ps = new PrintStream(pout, true);
            System.setOut(ps);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null,
                    "*** Input or Output error ***\n" + e, "Error",
                    JOptionPane.ERROR_MESSAGE);
            e.printStackTrace();
        }

        //Construct and start a Thread to copy data from "is" to "os".
        new Thread() {
            public void run() {
                try {
                    String line;
                    while ((line = iis.readLine()) != null) {
                        logArea.append(line);
                        logArea.append("\n");
                    }
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(null,
                            "*** Input or Output error ***\n" + ex, "Error",
                            JOptionPane.ERROR_MESSAGE);
                }
            }
        }.start();

        */
    }

    public static void log(String line) {
        if (line != null) {
            System.out.println(line);
            synchronized (logArea) {
                if (logArea != null) {
                    logArea.append(line);
                    logArea.append("\n");
                }
            }
        }
    }
}
