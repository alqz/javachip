package org.mule.module.peoplematter.transformers;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.mule.api.MuleMessage;
import org.mule.api.transformer.TransformerException;
import org.mule.transformer.AbstractMessageTransformer;


public class MissingLookupValuesTransformer extends AbstractMessageTransformer {

	private static final String LOOKUP_VARIABLE_IDENTIFIER = "lookupvalue";
	private static final String MISSING_LOOKUP_VARIABLE_IDENTIFIER = "missingLookupValues";
	private static final String MISSING_LOOKUP_VARIABLE_MESSAGE_IDENTIFIER = "missingLookupValuesMSG";

	@Override
	public Object transformMessage(MuleMessage message, String outputEncoding)
			throws TransformerException {
		
		List<String> lookupVariableNames = (List<String>) CollectionUtils.select(message.getInvocationPropertyNames(), new IsLookupVariablePredicate((String)message.getInvocationProperty(MISSING_LOOKUP_VARIABLE_IDENTIFIER)));
		
		String missingMessage = "";
		for (String lookupVariableName: lookupVariableNames) {
			String value = message.getInvocationProperty(lookupVariableName);
			missingMessage += lookupVariableName.substring(LOOKUP_VARIABLE_IDENTIFIER.length()) + ": " + value + "\n";
		}
	
		message.setInvocationProperty(MISSING_LOOKUP_VARIABLE_MESSAGE_IDENTIFIER, missingMessage);
		
		return message;
	}
	
	private class IsLookupVariablePredicate implements Predicate {
		
		String[] validVariableNames;
		
		
		public IsLookupVariablePredicate (String lookupVariableNames){
			this.validVariableNames = lookupVariableNames.split(",");	
		}

		public boolean evaluate(Object variableName) {
			
			String variableNameStr = (String) variableName;
			for (String validVariableName: validVariableNames) {
				if  (variableNameStr.toLowerCase().equals(validVariableName.toLowerCase()))
					return true;
			}
			
			return false;
		}
		
	}
	

}
