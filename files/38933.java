package tk.blackwolf12333.grieflog.callback;

import org.bukkit.ChatColor;
import tk.blackwolf12333.grieflog.PlayerSession;
import tk.blackwolf12333.grieflog.data.BaseData;

public class TpToCallback extends BaseCallback {

	PlayerSession player;
	
	public TpToCallback(PlayerSession player) {
		this.player = player;
	}
	
	@Override
	public void start() {
		player.setSearching(false);
		player.setSearchResult(result);
		teleport();
	}
	
	private void teleport() {
		String lastLine = null;
		if(result.size() == 0) {
			player.print(ChatColor.DARK_RED + "Nothing was found, sorry.");
			return;
		} else {
			lastLine = result.get(result.size() - 1).toString();
		}
		
		BaseData data = BaseData.loadFromString(lastLine);
		data.tpto(player);
	}
}
