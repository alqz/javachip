package project;

import java.sql.*;
import java.util.Scanner;

public class Privileges {
	
	static Scanner input = new Scanner(System.in);
	
	public static void viewUserInfo(Connection connection) {
		try {
			int userID = Authenticate.authenticateUser(connection);
			Statement state = connection.createStatement();
			ResultSet rset = state.executeQuery("SELECT privilege FROM SiteMember WHERE user_id = " + userID);
			rset.next();
			int level = rset.getInt("privilege");
			if(level <= 1) {
				//view everything
				rset = state.executeQuery("SELECT *  FROM SiteMember");
				while(rset.next()) {
					System.out.println("ID: " + rset.getInt("user_id") + "\tUsername: " + rset.getString("email") + 
							"\tPassword: " + rset.getString("password") + "\tPhone: " + rset.getString("phone") +
							"\tAddress: " + rset.getString("address_1") + "State: " + rset.getString("state") + 
							"\tZip: " + rset.getInt("zip") + "Privilege Level: " + rset.getInt("privilege"));
				}
			}
			else {
				//view only yours
				rset = state.executeQuery("SELECT email, phone, address_1, address_2, state, zip  FROM SiteMember WHERE user_id = " + userID);
				while(rset.next()) {
					System.out.println("Username: " + rset.getString("email") + "\tPhone: " + rset.getString("phone") +
							"\tAddress: " + rset.getString("address_1") + "State: " + rset.getString("state") + 
							"\tZip: " + rset.getInt("zip"));
				}
			}
		} catch(Exception sqle) {
			System.out.println("Exception: " + sqle);
		}
	}
	
	public static void editUserInfo(Connection connection) {
		try {
			int userID = Authenticate.authenticateUser(connection);
			Statement state = connection.createStatement();
			ResultSet rset = state.executeQuery("SELECT privilege FROM SiteMember WHERE user_id = " + userID);
			rset.next();
			int level = rset.getInt("privilege");
			if(level == 0) {
				//edit all fields
			}
			else {
				//edit only yours
				System.out.print("What would you like your new phone number to be?\n\tInput: ");
				String phone = input.next();
				state.executeUpdate("UPDATE SiteMember SET phone = " + phone + " WHERE user_id = " + userID);
				System.out.print("Your information is now: \n");
				rset = state.executeQuery("SELECT email, phone, address_1, address_2, state, zip  FROM SiteMember WHERE user_id = " + userID);
				while(rset.next()) {
					System.out.println("Username: " + rset.getString("email") + "\tPhone: " + rset.getString("phone") +
							"\tAddress: " + rset.getString("address_1") + "State: " + rset.getString("state") + 
							"\tZip: " + rset.getInt("zip"));
				}
			}
		} catch(Exception sqle) {
			System.out.println("Exception: " + sqle);
		}
	}
	
	public static void editMovieSchedule(Connection connection) {
		try {
			int userID = Authenticate.authenticateUser(connection);
			Statement state = connection.createStatement();
			ResultSet rset = state.executeQuery("SELECT privilege FROM SiteMember WHERE user_id = " + userID);
			rset.next();
			int level = rset.getInt("privilege");
			if(level <= 2) {
				System.out.println("What is the movie ID?\n\tInput: ");
				int movieID = input.nextInt();
				System.out.println("What is the theatre ID?\n\tInput: ");
				int theatreID = input.nextInt();
				System.out.println("What is the date (Ex. 05-MAY-2012)?\n\tInput: ");
				String date = input.next();
				System.out.println("What is the time (as int, ending in 1 for PM, 0 for AM)?\n\tInput: ");
				int time = input.nextInt();
				System.out.println("What is the screen number?\n\tInput: ");
				int screenNum = input.nextInt();
				System.out.println("What is the capacity?\n\tInput: ");
				int capacity = input.nextInt();
				rset = state.executeQuery("SELECT MAX(slot_id) FROM Schedule");
				int slotID = 0;
				if(rset.next())
					slotID = rset.getInt("MAX(slot_id)") + 1;
				state.executeUpdate("INSERT INTO Schedule VALUES(" + slotID + ", " + theatreID + ", " + 
					movieID + ", '" + date + "', " + time + ", " + screenNum + ", " + capacity + ", 0)");
				System.out.println("Line Inserted...");
			}
			else {
				System.out.println("Sorry, insufficient privilege level...");
			}
		} catch(Exception sqle) {
			System.out.println("Exception: " + sqle);
		}
	}
	
	public static void editMovie(Connection connection) {
		try {
			int userID = Authenticate.authenticateUser(connection);
			Statement state = connection.createStatement();
			ResultSet rset = state.executeQuery("SELECT privilege FROM SiteMember WHERE user_id = " + userID);
			rset.next();
			int level = rset.getInt("privilege");
			if(level <= 2) {
				rset = state.executeQuery("SELECT MAX(movie_id) FROM Schedule");
				int movieID = 0;
				if(rset.next())
					movieID = rset.getInt("MAX(movie_id)") + 1;
				input.nextLine();
				System.out.println("What is the movie name?\n\tInput: ");
				String name = input.nextLine();
				System.out.println("What is the genre?\n\tInput: ");
				String genre = input.next();
				input.nextLine();
				System.out.println("What is the description?\n\tInput: ");
				String description = input.nextLine();
				System.out.println("What is the length (in seconds)?\n\tInput: ");
				int length = input.nextInt();
				state.executeUpdate("INSERT INTO Movie VALUES(" + movieID + ", '" + name + "', '" + 
					genre + "', '" + description + "', " + length + ")");
				System.out.println("Line Inserted...");
			}
			else {
				System.out.println("Sorry, insufficient privilege level...");
			}
		} catch(Exception sqle) {
			System.out.println("Exception: " + sqle);
		}
	}
	
	public static void upgradePrivileges(Connection connection) {
		try {
			int userID = Authenticate.authenticateUser(connection);
			Statement state = connection.createStatement();
			ResultSet rset = state.executeQuery("SELECT privilege FROM SiteMember WHERE user_id = " + userID);
			rset.next();
			int level = rset.getInt("privilege");
			if(level <= 1) {
				System.out.println("What ID is the person you are granting rights to?");
				System.out.print("Input: ");
				int ID = input.nextInt();
				System.out.println("What level of privileges should they recieve?");
				System.out.print("Input: ");
				int newLevel = input.nextInt();
				if(newLevel < 1 || newLevel > 3) {
					System.out.println("Sorry, that level is not allowed...");
					return;
				}
				else if(level == 1 && newLevel < 2) {
					System.out.println("Sorry, your permissions do not allow you to grant such privileges");
					return;
				}
				else {
					state.executeUpdate("UPDATE SiteMember SET privilege = " + newLevel + "WHERE user_id = " + ID);
				}
			}
			else {
				System.out.println("Sorry, insufficient privilege level...");
			}
		} catch(Exception sqle) {
			System.out.println("Exception: " + sqle);
		}
	}
	
	public static void editEverything(Connection connection) {
		try {
			int userID = Authenticate.authenticateUser(connection);
			Statement state = connection.createStatement();
			ResultSet rset = state.executeQuery("SELECT privilege FROM SiteMember WHERE user_id = " + userID);
			rset.next();
			int level = rset.getInt("privilege");
			if(level == 0) {
				input.nextLine();
				System.out.println("You are the admin, king of the database... Use your power wisely...");
				System.out.print("Input: ");
				String sql = input.nextLine();
				state.executeUpdate(sql);
			}
			else {
				System.out.println("Sorry, insufficient privilege level...");
			}
		} catch(Exception sqle) {
			System.out.println("Exception: " + sqle);
		}
	}

}
