package unity.controller.game.upload;

import java.util.UUID;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.datastore.Datastore;
import org.slim3.datastore.GlobalTransaction;

import unity.model.GameData;
import unity.model.User;
import unity.model.api.SaveLoadId;
import unity.service.GameDataService;
import unity.service.SaveDataService;
import unity.service.SearchService;
import unity.service.UserService;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.repackaged.com.google.common.util.Base64;

public class IndexController extends Controller {
    private GameDataService gs = new GameDataService();
    private SaveDataService sd = new SaveDataService();
    private UserService us = new UserService();
    private SearchService ss = new SearchService();

    @Override
    public Navigation run() throws Exception {

        String secretKey = "＊＊＊＊＊＊＊＊＊＊＊＊";
        String policy_document =
            "{\"expiration\":\"2117-12-01T12:00:00.000Z\",\"conditions\":[{\"acl\":\"public-read\" },{\"bucket\":\"unitygames\"},[\"starts-with\",\"$key\",\"\"],[\"content-length-range\",0,20000000]]}";
        String policy =
            Base64.encode(policy_document.getBytes("UTF-8")).replaceAll(
                "\n",
                "");
        Mac hmac = Mac.getInstance("HmacSHA1");
        hmac.init(new SecretKeySpec(secretKey.getBytes("UTF-8"), "HmacSHA1"));
        String signature =
            Base64.encode(hmac.doFinal(policy.getBytes("UTF-8"))).replaceAll(
                "\n",
                "");

        requestScope("policy", policy);
        requestScope("signature", signature);

        String loginType = "guest";

        // 補完ワード
        requestScope("words", ss.suggestionWords());
        Boolean isLogin = (Boolean) sessionScope("isLogin");
        requestScope("isLogin", isLogin);
        requestScope("user", sessionScope("user"));
        if (isLogin != null && isLogin) {
            loginType = "user";
            User user = sessionScope("user");
            requestScope("userId", user.getKey().getId());
        }

        requestScope("loginType", loginType);
        String game = asString("g");
        if (!game.equals("newGame")) {
            long id = 0;
            if (!asString("t").isEmpty() || asString("t") != null) {
                id =
                    Long.valueOf(asString("t").replace("ug", "").replace(
                        "guest",
                        ""));

            }

            String k = requestScope("key");
            String pass = requestScope("Pass");

            GameData g = null;
            if (k != null) {
                Key key = KeyFactory.stringToKey(k);
                g = Datastore.get(GameData.class, key);
            }
            if (id != 0) {
                User user = (User) sessionScope("user");
                g = gs.load(id);
                if (g.getUserKey() != null) {
                    Long uId = us.getUser(g.getUserKey()).getKey().getId();
                    if (!uId.equals(user.getKey().getId()))
                        return redirect(request.getHeader("REFERER"));
                }
            }
            // Passが違ったら変更画面へいかせない
            if (k != null) {
                if (!g.getPass().equals(pass) && id == g.getKey().getId()) {
                    return redirect(request.getHeader("REFERER"));
                }
            }
            requestScope("g", g);
            // 固定タグ。複数のを","で１つに
            if (g.getFixTags() != null && !g.getFixTags().isEmpty()) {
                requestScope("tag", gs.connectFixTags(g));
            }

            String[] size = g.getGameScreenSize().split(",");
            requestScope("width", size[0]);
            requestScope("height", size[1]);

            SaveLoadId saveLoadId =
                Datastore.query(SaveLoadId.class, g.getKey()).asSingle();

            if (saveLoadId.getLoadId().equals("null")) {
                System.out.println("null");
                saveLoadId(saveLoadId);
            }
            // save,laod id
            requestScope("sl", sd.getSaveLoadId(g.getKey()));

            return forward("change.jsp");
        } else {
            // create save,load id
            requestScope("saveId", UUID.randomUUID().toString());
            requestScope("loadId", UUID.randomUUID().toString());
            return forward("index.jsp");
        }

    }

    public void saveLoadId(SaveLoadId sl) {
        sl.setSaveId(UUID.randomUUID().toString());
        sl.setLoadId(UUID.randomUUID().toString());
        GlobalTransaction tx = Datastore.beginGlobalTransaction();
        Datastore.put(sl);
        tx.commit();
        System.out.println("created");
    }
}
