package plugin.arcwolf.blockdoor.Zones;

// Per player specific detection zone

import plugin.arcwolf.blockdoor.Link;

public class MyZone extends Zone {

    public MyZone(String in_Type, String in_name, String in_creator, String in_world) {
        super(in_Type, in_name, in_creator, in_world);
    }

    public MyZone(String in_string) {
        super(in_string);
    }

    public MyZone(Zone otherZone) {
        super(otherZone);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("MYZONE:");
        builder.append(zone_creator);
        builder.append(":");
        builder.append(zone_name);
        builder.append(":");

        builder.append(Integer.toString(zone_start_x));
        builder.append(":");
        builder.append(Integer.toString(zone_start_y));
        builder.append(":");
        builder.append(Integer.toString(zone_start_z));
        builder.append(":");

        builder.append(Integer.toString(zone_end_x));
        builder.append(":");
        builder.append(Integer.toString(zone_end_y));
        builder.append(":");
        builder.append(Integer.toString(zone_end_z));
        builder.append(":");

        builder.append(Boolean.toString(coordsSet));
        builder.append(":");

        builder.append(Integer.toString(occupants));
        builder.append(":");

        builder.append(zone_world);
        builder.append(":");

        for (Link l : links) {
            builder.append(l.link_name);
            builder.append(" ");
            builder.append(l.link_creator);
            builder.append(" ");
            builder.append(l.linkType.name());
            builder.append(" ");
            builder.append(l.doorType);
            builder.append("|");
        }
        return builder.toString();
    }
}