//
// PlaytomicTestActivity.java
// Playtomic
//
// This file is part of the official Playtomic API for Android games.
// Playtomic is a real time analytics platform for casual games
// and services that go in casual games. If you haven't used it
// before check it out:
// http://playtomic.com/
//
// Created by ben at the above domain on 10/19/11.
// Copyright 2011 Playtomic LLC. All rights reserved.
//
// Documentation is available at:
// http://playtomic.com/api/android
//
// PLEASE NOTE:
// You may modify this SDK if you wish but be kind to our servers. Be
// careful about modifying the analytics stuff as it may give you
// borked reports.
//
// If you make any awesome improvements feel free to let us know!
//
// -------------------------------------------------------------------------
// THIS SOFTWARE IS PROVIDED BY PLAYTOMIC, LLC "AS IS" AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
// LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

package com.playtomic.android.test;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Random;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import com.playtomic.android.api.Playtomic;
import com.playtomic.android.api.PlaytomicData;
import com.playtomic.android.api.PlaytomicExceptionHandler;
import com.playtomic.android.api.PlaytomicGameVars;
import com.playtomic.android.api.PlaytomicGeoIP;
import com.playtomic.android.api.PlaytomicLeaderboards;
import com.playtomic.android.api.PlaytomicLevel;
import com.playtomic.android.api.PlaytomicPlayerLevels;
import com.playtomic.android.api.PlaytomicPrivateLeaderboard;
import com.playtomic.android.api.PlaytomicRequestListener;
import com.playtomic.android.api.PlaytomicResponse;
import com.playtomic.android.api.PlaytomicScore;
import com.playtomic.android.test.R;

public class PlaytomicTestActivity extends Activity {
	
	@Override
	protected void onPause() {
		Playtomic.Log().freeze();
		super.onPause();
	}

	@Override
	protected void onResume() {
		Playtomic.Log().unfreeze();
		super.onResume();
	}

	private String mLevelId = "";
	private String mBitly = "";
	
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		// set the uncaught exception handler
		//
		Thread.setDefaultUncaughtExceptionHandler(new PlaytomicExceptionHandler());
		
		setContentView(R.layout.main);

		EditText log = (EditText) findViewById(R.id.txLog);
		InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(log.getWindowToken(), 0);		
		
		Button button;

		button = (Button) findViewById(R.id.cmdLogPlay);
		button.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				logPlay();
			}
		});

		button = (Button) findViewById(R.id.cmdLogCustomMetric);
		button.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				logCustomMetric();
			}
		});

		button = (Button) findViewById(R.id.cmdLogCustomMetricNg);
		button.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				logCustomMetricNg();
			}
		});

		button = (Button) findViewById(R.id.cmdLogCustomMetricU);
		button.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				logCustomMetricU();
			}
		});

		button = (Button) findViewById(R.id.cmdLogLevelCounterMetric);
		button.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				logLevelCounterMetric();
			}
		});

		button = (Button) findViewById(R.id.cmdLogLevelCounterMetricU);
		button.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				logLevelCounterMetricU();
			}
		});

		button = (Button) findViewById(R.id.cmdLogLevelAverageMetric);
		button.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				logLevelAverageMetric();
			}
		});

		button = (Button) findViewById(R.id.cmdLogLevelAverageMetricU);
		button.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				logLevelAverageMetricU();
			}
		});

		button = (Button) findViewById(R.id.cmdLogLevelRangedMetric);
		button.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				logLevelRangedMetric();
			}
		});

		button = (Button) findViewById(R.id.cmdLogLevelRangedMetricU);
		button.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				logLevelRangedMetricU();
			}
		});

		button = (Button) findViewById(R.id.cmdLoadGameVars);
		button.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				loadGameVars();
			}
		});

		button = (Button) findViewById(R.id.cmdGeoIP);
		button.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				geoip();
			}
		});

		button = (Button) findViewById(R.id.cmdLeaderboardList);
		button.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				leaderBoardList();
			}
		});

		button = (Button) findViewById(R.id.cmdLeaderboardSave);
		button.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				leaderboardSave();
			}
		});

		button = (Button) findViewById(R.id.cmdLeaderboardSaveAndList);
		button.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				leaderboardSaveAndList();
			}
		});
		
		button = (Button) findViewById(R.id.cmdPrivateLeaderboardSave);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                privateLeaderboardSave();
            }
        });
        
        button = (Button) findViewById(R.id.cmdPrivateLeaderboardLoad);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                privateLeaderboardLoad();
            }
        });

		button = (Button) findViewById(R.id.cmdLevelList);
		button.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				levelList();
			}
		});

		button = (Button) findViewById(R.id.cmdLevelLoad);
		button.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				levelLoad();
			}
		});

		button = (Button) findViewById(R.id.cmdLevelRate);
		button.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				levelRate();
			}
		});

		button = (Button) findViewById(R.id.cmdLevelSave);
		button.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				levelSave();
			}
		});

		button = (Button) findViewById(R.id.cmdLoadCounters);
		button.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				loadCounters();
			}
		});

		button = (Button) findViewById(R.id.cmdLoadCustomData);
		button.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				loadCustomData();
			}
		});

		button = (Button) findViewById(R.id.cmdLoadPlays);
		button.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				loadPlays();
			}
		});

		button = (Button) findViewById(R.id.cmdLoadPlaytime);
		button.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				loadPlaytime();
			}
		});

		button = (Button) findViewById(R.id.cmdLoadViews);
		button.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				loadViews();
			}
		});

		button = (Button) findViewById(R.id.cmdFreeze);
		button.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				freeze();
			}
		});

		button = (Button) findViewById(R.id.cmdUnfreeze);
		button.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				unfreeze();
			}
		});
		
		button = (Button) findViewById(R.id.cmdException);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                throwException();
            }
        });
        
		Playtomic.Log().setRequestListener(new PlaytomicRequestListener<String>() {
        	public void onRequestFinished(PlaytomicResponse<String> playtomicResponse) {
        		    requestLogFinished(playtomicResponse.getMap());
			}
		});
	}

	private void logPlay() {
		L.log("\nLog Play", this);
		Playtomic.Log().play();
		Playtomic.Log().forceSend();
	}

	private void logCustomMetric() {
		L.log("\nLog Custom Metric", this);
	    Playtomic.Log().customMetric("custom", "group", false);
	    Playtomic.Log().forceSend();
	}

	private void logCustomMetricNg() {
		L.log("\nLog Custom Metric Ng", this);
	    Playtomic.Log().customMetric("ungroupedcustom", null, false);
	    Playtomic.Log().forceSend();
	}

	private void logCustomMetricU() {
		L.log("\nLog Custom Metric U", this);
	    Playtomic.Log().customMetric("uniquecustom", "group", true);
	    Playtomic.Log().forceSend();
	}

	private void logLevelAverageMetric() {
		L.log("\nLog Average Metric", this);
	    Playtomic.Log().levelAverageMetric("average", "name", 100, false);
	    Playtomic.Log().levelAverageMetric("average", 1, 100, false);
	    Playtomic.Log().forceSend(); 
	}

	private void logLevelAverageMetricU() {
		L.log("\nLog Average Metric U", this);
	    Playtomic.Log().levelAverageMetric("average", "name", 100, true);
	    Playtomic.Log().levelAverageMetric("average", 1, 100, true);
	    Playtomic.Log().forceSend();
	}

	private void logLevelCounterMetric() {
		L.log("\nLog Counter Metric", this);
	    Playtomic.Log().levelCounterMetric("counter", "name", false);
	    Playtomic.Log().levelCounterMetric("counter", 1, false);
	    Playtomic.Log().forceSend(); 
	}

	private void logLevelCounterMetricU() {
		L.log("\nLog Counter Metric U", this);
	    Playtomic.Log().levelCounterMetric("counter", "name", true);
	    Playtomic.Log().levelCounterMetric("counter", 1, true);
	    Playtomic.Log().forceSend();
	}

	private void logLevelRangedMetric() {
		L.log("\nLog Ranged Metric", this);
	    Playtomic.Log().levelRangedMetric("ranged", "name", 10, false);
	    Playtomic.Log().levelRangedMetric("ranged", 1, 20, false);
	    Playtomic.Log().forceSend(); 
	}

	private void logLevelRangedMetricU() {
		L.log("\nLog Ranged Metric U", this);
	    Playtomic.Log().levelRangedMetric("ranged", "name", 30, true);
	    Playtomic.Log().levelRangedMetric("ranged", 1, 40, true);
	    Playtomic.Log().forceSend();
	}

	private void requestLogFinished(LinkedHashMap<String, String> data) {
		L.log("Log {", this);
		for (Map.Entry<String, String> entry : data.entrySet()) {
			L.log("Msg: Name=\"" + entry.getKey() + "\" Value=\"" + entry.getValue() + "\"", this);
		}
		L.log("}", this);
	}
		
	private void loadGameVars() {
		L.log("\nLoad Game Vars", this);

		PlaytomicGameVars vars = new PlaytomicGameVars();
        vars.setRequestListener(new PlaytomicRequestListener<String>() {
        	public void onRequestFinished(PlaytomicResponse<String> playtomicResponse) {
				if (playtomicResponse.getSuccess()) {
					requestVarsFinished(playtomicResponse.getMap());
				}			
				else {
					requestVarsFailed(playtomicResponse.getErrorCode(), playtomicResponse.getErrorMessage());
				}
			}
		});
        vars.loadSingle("Var1");
	    vars.load();
	}
	
	private void requestVarsFinished(LinkedHashMap<String, String> data) {
		L.log("Vars {", this);
		for (Map.Entry<String, String> entry : data.entrySet()) {
			L.log("Var: Name=\"" + entry.getKey() + "\" Value=\"" + entry.getValue() + "\"", this);
		}
		L.log("}", this);
	}
	
	private void requestVarsFailed(int errorCode, String message) {
		L.log("GameVars failed to load because of errorcode #" + errorCode + " - Message:" + message, this);
	}

	private void geoip() {
		L.log("\nGeoIP", this);
		PlaytomicGeoIP geoIP = new PlaytomicGeoIP();
        geoIP.setRequestListener(new PlaytomicRequestListener<String>() {
        	public void onRequestFinished(PlaytomicResponse<String> playtomicResponse) {
				if (playtomicResponse.getSuccess()) {
					requestGeoIPFinished(playtomicResponse.getMap());
				}			
				else {
					requestGeoIPFailed(playtomicResponse.getErrorCode(), playtomicResponse.getErrorMessage());
				}
			}
		});
	    geoIP.load();
	}

	private void requestGeoIPFinished(LinkedHashMap<String, String> data) {
		L.log("GeoIP {", this);
		for (Map.Entry<String, String> entry : data.entrySet()) {
			L.log("GeoIP: Name=\"" + entry.getKey() + "\" Value=\"" + entry.getValue() + "\"", this);
		}
		L.log("}", this);
	}
	
	private void requestGeoIPFailed(int errorCode, String message) {
		L.log("GeoIP failed to load because of errorcode #" + errorCode + " - Message:" + message, this);
	}
	
	private void leaderboardSave() {
		L.log("\nLeaderboard Save", this);
		PlaytomicLeaderboards leaderboards = new PlaytomicLeaderboards();
		leaderboards.setRequestListener(new PlaytomicRequestListener<PlaytomicScore>() {
        	public void onRequestFinished(PlaytomicResponse<PlaytomicScore> playtomicResponse) {
				if (playtomicResponse.getSuccess()) {
					requestLeaderBoardSaveFinished();
				}			
				else {
					requestLeaderBoardSaveFailed(playtomicResponse.getErrorCode(), playtomicResponse.getErrorMessage());
				}
			}
		});
		PlaytomicScore score = new PlaytomicScore("Ben", 2000000);
		LinkedHashMap<String, String> customData =  score.getCustomData();
		customData.put("cd1", "value1");
		customData.put("cd2", "value2");
		customData.put("cd3", "value3");
		leaderboards.save("High scores", score, true, true);
	}

	private void requestLeaderBoardSaveFinished() {
		L.log("Save score success", this);
	}
	
	private void requestLeaderBoardSaveFailed(int errorCode, String message) {
		L.log("Leaderboard save failed to save because of errorcode #" + errorCode + " - Message:" + message, this);
	}

	private void leaderBoardList() {
		L.log("\nLeaderboard List", this);
		PlaytomicLeaderboards leaderboards = new PlaytomicLeaderboards();
		leaderboards.setRequestListener(new PlaytomicRequestListener<PlaytomicScore>() {
        	public void onRequestFinished(PlaytomicResponse<PlaytomicScore> playtomicResponse) {
				if (playtomicResponse.getSuccess()) {
					requestLeaderBoardListFinished(playtomicResponse.getData());
				}			
				else {
					requestLeaderBoardListFailed(playtomicResponse.getErrorCode(), playtomicResponse.getErrorMessage());
				}
			}
		});
		leaderboards.list("High scores", true, "alltime", 1, 20, null);
	}

	private void requestLeaderBoardListFinished(ArrayList<PlaytomicScore> data) {
		L.log("Leaderboard {", this);
		Iterator<PlaytomicScore> itr = data.iterator();
		while (itr.hasNext()) {
			PlaytomicScore score = itr.next();
			L.log("----------------------------------\nScore:\nName=\"" + score.getName() + "\"", this);
			L.log("Points=\"" + score.getPoints() + "\"", this);
			L.log("Date=\"" + score.getDate() + "\"", this);
			L.log("Relative Date=\"" + score.getRelativeDate() + "\"", this);
			L.log("Rank=\"" + score.getRank() + "\"", this);
			L.log("Custom Data {", this);
			for (Map.Entry<String, String> entry : score.getCustomData().entrySet()) {
				L.log("Var: Name=\"" + entry.getKey() + "\" Value=\"" + entry.getValue() + "\"", this);
			}
			L.log("}", this);
		}
		L.log("}", this);
	}

	private void requestLeaderBoardListFailed(int errorCode, String message) {
		L.log("Leaderboard list failed to list because of errorcode #" + errorCode + " - Message:" + message, this);
	}

	private void leaderboardSaveAndList() {
		L.log("\nLeaderboard Save and List", this);
		PlaytomicLeaderboards leaderboards = new PlaytomicLeaderboards();
		leaderboards.setRequestListener(new PlaytomicRequestListener<PlaytomicScore>() {
        	public void onRequestFinished(PlaytomicResponse<PlaytomicScore> playtomicResponse) {
				if (playtomicResponse.getSuccess()) {
					requestLeaderBoardListFinished(playtomicResponse.getData());
				}			
				else {
					requestLeaderBoardSaveFailed(playtomicResponse.getErrorCode(), playtomicResponse.getErrorMessage());
				}
			}
		});
		PlaytomicScore score = new PlaytomicScore("Ben", 2000000);
		LinkedHashMap<String, String> customData =  score.getCustomData();
		customData.put("cd1", "value1");
		customData.put("cd2", "value2");
		customData.put("cd3", "value3");
		leaderboards.saveAndList("High scores", score, true, true, "alltime", 20, null, true, null);
	}

    private void privateLeaderboardSave() {
        L.log("\nPrivate Leaderboard Create", this);
        PlaytomicLeaderboards leaderboards = new PlaytomicLeaderboards();
        leaderboards.setRequestListenerPrivateLeaderboard(new PlaytomicRequestListener<PlaytomicPrivateLeaderboard>() {
            public void onRequestFinished(PlaytomicResponse<PlaytomicPrivateLeaderboard> playtomicResponse) {
                if (playtomicResponse.getSuccess()) {
                    requestPrivateLeaderBoardSaveFinished(playtomicResponse.getObject());
                }           
                else {
                    requestPrivateLeaderBoardSaveFailed(playtomicResponse.getErrorCode(), playtomicResponse.getErrorMessage());
                }
            }
        });
        leaderboards.createPrivateLeaderboard("Android Test", true);
    }
    
    private void requestPrivateLeaderBoardSaveFinished(PlaytomicPrivateLeaderboard leaderboard) {
        mBitly = leaderboard.getBitly();
        L.log("Private Leaderboard {", this);
        L.log("name = " + leaderboard.getName(), this);
        L.log("tableId = " + leaderboard.getTableId(), this);
        L.log("bitly = " + leaderboard.getBitly(), this);
        L.log("permalink = " + leaderboard.getPermalink(), this);
        L.log("highest = " + (leaderboard.getHighest() ? "yes" : "no"), this);
        L.log("realName = " + leaderboard.getRealName(), this);
        L.log("}", this);
    }
    
    private void requestPrivateLeaderBoardSaveFailed(int errorCode, String message) {
        L.log("Leaderboard create failed because of errorcode #" + errorCode + " - Message:" + message, this);
    }
    
    private void privateLeaderboardLoad() {
        if (mBitly.length() == 0) {
            L.log("You need to save a private leaderboar before loaded it.", this);
        }
        else {
            L.log("\nPrivate Leaderboard Load", this);
            PlaytomicLeaderboards leaderboards = new PlaytomicLeaderboards();
            leaderboards.setRequestListenerPrivateLeaderboard(new PlaytomicRequestListener<PlaytomicPrivateLeaderboard>() {
                public void onRequestFinished(PlaytomicResponse<PlaytomicPrivateLeaderboard> playtomicResponse) {
                    if (playtomicResponse.getSuccess()) {
                        requestPrivateLeaderBoardLoadFinished(playtomicResponse.getObject());
                    }           
                    else {
                        requestPrivateLeaderBoardLoadFailed(playtomicResponse.getErrorCode(), playtomicResponse.getErrorMessage());
                    }
                }
            });
            leaderboards.loadPrivateLeaderboard(mBitly);
        }
    }

    private void requestPrivateLeaderBoardLoadFinished(PlaytomicPrivateLeaderboard leaderboard) {
        L.log("Private Leaderboard {", this);
        L.log("name = " + leaderboard.getName(), this);
        L.log("tableId = " + leaderboard.getTableId(), this);
        L.log("bitly = " + leaderboard.getBitly(), this);
        L.log("permalink = " + leaderboard.getPermalink(), this);
        L.log("highest = " + (leaderboard.getHighest() ? "yes" : "no"), this);
        L.log("realName = " + leaderboard.getRealName(), this);
        L.log("}", this);
    }
    
    private void requestPrivateLeaderBoardLoadFailed(int errorCode, String message) {
        L.log("Leaderboard load failed because of errorcode #" + errorCode + " - Message:" + message, this);
    }
    
	private void levelSave() {
		L.log("\nLevel Save", this);
		Random generator = new Random();
		int r = generator.nextInt();
		PlaytomicLevel level = new PlaytomicLevel("level name: " + Math.abs(r % 100), "ben4", "0", 
				"r=-152&i0=13,440,140&i1=24,440,140&i2=25,440,140&i3=37,440,140,ie,37,450,150");
		PlaytomicPlayerLevels playerLevels = new PlaytomicPlayerLevels();
		playerLevels.setRequestListener(new PlaytomicRequestListener<PlaytomicLevel>() {
        	public void onRequestFinished(PlaytomicResponse<PlaytomicLevel> playtomicResponse) {
				if (playtomicResponse.getSuccess()) {
					requestPlayerLevelsSaveFinished(playtomicResponse.getData());
				}			
				else {
					requestPlayerLevelsSaveFailed(playtomicResponse.getErrorCode(), playtomicResponse.getErrorMessage());
				}
			}
		});
		playerLevels.save(level);
	}

	private void requestPlayerLevelsSaveFinished(ArrayList<PlaytomicLevel> data) {
		L.log("PlayerLevels Save", this);
		Iterator<PlaytomicLevel> itr = data.iterator();
		while (itr.hasNext()) {
			PlaytomicLevel level = itr.next();
			mLevelId = level.getLevelId();
			L.log("Level:\nLevel Id=\"" + mLevelId + "\"", this);
		}
	}

	private void requestPlayerLevelsSaveFailed(int errorCode, String message) {
		L.log("PlayerLevels Save failed to save because of errorcode #" + errorCode + " - Message:" + message, this);
	}

	private void levelList() {
		L.log("\nLevel List", this);
		PlaytomicPlayerLevels playerLevels = new PlaytomicPlayerLevels();
		playerLevels.setRequestListener(new PlaytomicRequestListener<PlaytomicLevel>() {
        	public void onRequestFinished(PlaytomicResponse<PlaytomicLevel> playtomicResponse) {
				if (playtomicResponse.getSuccess()) {
					requestPlayerLevelsListFinished(playtomicResponse.getData());
				}			
				else {
					requestPlayerLevelsListFailed(playtomicResponse.getErrorCode(), playtomicResponse.getErrorMessage());
				}
			}
		});
		playerLevels.list("popular", 1, 10, false, false, null);
	}

	private void requestPlayerLevelsListFinished(ArrayList<PlaytomicLevel> data) {
		L.log("PlayerLevels {", this);
		showLevels(data);
	}
	
	private void requestPlayerLevelsListFailed(int errorCode, String message) {
		L.log("PlayerLevels List failed to list because of errorcode #" + errorCode + " - Message:" + message, this);
	}

	private void levelLoad() {
		L.log("\nLevel Load", this);
		if (mLevelId.length() == 0) {
			L.log("Before load a level you must save one. Use the 'Level Save' button.", this);
		}
		else {
			PlaytomicPlayerLevels playerLevels = new PlaytomicPlayerLevels();
			playerLevels.setRequestListener(new PlaytomicRequestListener<PlaytomicLevel>() {
	        	public void onRequestFinished(PlaytomicResponse<PlaytomicLevel> playtomicResponse) {
					if (playtomicResponse.getSuccess()) {
						requestPlayerLevelsLoadFinished(playtomicResponse.getData());
					}			
					else {
						requestPlayerLevelsLoadFailed(playtomicResponse.getErrorCode(), playtomicResponse.getErrorMessage());
					}
				}
			});
			playerLevels.load(mLevelId);
		}
	}

	private void requestPlayerLevelsLoadFinished(ArrayList<PlaytomicLevel> data) {
		L.log("PlayerLevels {", this);
		showLevels(data);
	}
	
	private void requestPlayerLevelsLoadFailed(int errorCode, String message) {
		L.log("PlayerLevels Load failed to load because of errorcode #" + errorCode + " - Message:" + message, this);
	}
	
	private void levelRate() {
		L.log("\nLevel Rate", this);
		if (mLevelId.length() == 0) {
			L.log("Before rate a level you must save one. Use the 'Level Save' button.", this);
		}
		else {
			PlaytomicPlayerLevels playerLevels = new PlaytomicPlayerLevels(); 
			playerLevels.startLevel(mLevelId);
			playerLevels.startLevel(mLevelId);
			playerLevels.startLevel(mLevelId);
			playerLevels.retryLevel(mLevelId);
			playerLevels.winLevel(mLevelId);
			playerLevels.quitLevel(mLevelId);
			playerLevels.flagLevel(mLevelId);
			Playtomic.Log().forceSend();
			
			playerLevels.setRequestListener(new PlaytomicRequestListener<PlaytomicLevel>() {
	        	public void onRequestFinished(PlaytomicResponse<PlaytomicLevel> playtomicResponse) {
					if (playtomicResponse.getSuccess()) {
						requestPlayerLevelsRateFinished();
					}			
					else {
						requestPlayerLevelsRateFailed(playtomicResponse.getErrorCode(), playtomicResponse.getErrorMessage());
					}
				}
			});
			playerLevels.rate(mLevelId, 8);
		}
	}

	private void requestPlayerLevelsRateFinished() {
		L.log("Rate level success", this);
	}

	private void requestPlayerLevelsRateFailed(int errorCode, String message) {
		L.log("PlayerLevels Rate failed to rate because of errorcode #" + errorCode + " - Message:" + message, this);
	}
	
	private void showLevels(ArrayList<PlaytomicLevel> data) {		
		Iterator<PlaytomicLevel> itr = data.iterator();
		while (itr.hasNext()) {
			PlaytomicLevel level = itr.next();
			L.log("----------------------------------\nLevel:\nName=\"" + level.getName() + "\"", this);
			L.log("Level Id=\"" + level.getLevelId() + "\"", this);
			L.log("Player Name=\"" + level.getPlayerName() + "\"", this);
			L.log("Player Source=\"" + level.getPlayerSource() + "\"", this);
			L.log("Date=\"" + level.getDate() + "\"", this);
			L.log("Relative Date=\"" + level.getRelativeDate() + "\"", this);
			L.log("Data=\"" + level.getData() + "\"", this);
			L.log("Votes=\"" + level.getVotes() + "\"", this);
			L.log("Plays=\"" + level.getPlays() + "\"", this);
			L.log("Rating=\"" + level.getRating() + "\"", this);
			L.log("Score=\"" + level.getScore() + "\"", this);
			L.log("Custom Data {", this);
			for (Map.Entry<String, String> entry : level.getCustomData().entrySet()) {
				L.log("Var: Name=\"" + entry.getKey() + "\" Value=\"" + entry.getValue() + "\"", this);
			}
			L.log("}", this);
		}
		L.log("}", this);
	}

	private void loadCounters() {
		L.log("\nLoad Counters", this);
		PlaytomicData counters = new PlaytomicData();
		counters.setRequestListener(new PlaytomicRequestListener<String>() {
        	public void onRequestFinished(PlaytomicResponse<String> playtomicResponse) {
				if (playtomicResponse.getSuccess()) {
					requestCountersFinished(playtomicResponse.getMap());
				}			
				else {
					requestCountersFailed(playtomicResponse.getErrorCode(), playtomicResponse.getErrorMessage());
				}
			}
		});
		counters.levelCounterMetric("hi", "level 1");

		L.log("\nLoad Average", this);
		PlaytomicData average = new PlaytomicData();
		average.setRequestListener(new PlaytomicRequestListener<String>() {
        	public void onRequestFinished(PlaytomicResponse<String> playtomicResponse) {
				if (playtomicResponse.getSuccess()) {
					requestAverageFinished(playtomicResponse.getMap());
				}			
				else {
					requestAverageFailed(playtomicResponse.getErrorCode(), playtomicResponse.getErrorMessage());
				}
			}
		});
		average.levelCounterMetric("hi", "level 1", 1, 2010);

		L.log("\nLoad Range", this);
		PlaytomicData range = new PlaytomicData();
		range.setRequestListener(new PlaytomicRequestListener<String>() {
        	public void onRequestFinished(PlaytomicResponse<String> playtomicResponse) {
				if (playtomicResponse.getSuccess()) {
					requestRangeFinished(playtomicResponse.getMap());
				}			
				else {
					requestRangeFailed(playtomicResponse.getErrorCode(), playtomicResponse.getErrorMessage());
				}
			}
		});
		range.levelCounterMetric("hi", "level 1", 1, 1, 2010);
		
	}

	private void requestCountersFinished(LinkedHashMap<String, String> data) {
		L.log("\nCounters {", this);
		for (Map.Entry<String, String> entry : data.entrySet()) {
			L.log("Counters: Name=\"" + entry.getKey() + "\" Value=\"" + entry.getValue() + "\"", this);
		}
		L.log("}", this);
	}
	
	private void requestCountersFailed(int errorCode, String message) {
		L.log("\nCounters data failed to load because of errorcode #" + errorCode + " - Message:" + message, this);
	}

	private void requestAverageFinished(LinkedHashMap<String, String> data) {
		L.log("\nAverage {", this);
		for (Map.Entry<String, String> entry : data.entrySet()) {
			L.log("Average: Name=\"" + entry.getKey() + "\" Value=\"" + entry.getValue() + "\"", this);
		}
		L.log("}", this);
	}
	
	private void requestAverageFailed(int errorCode, String message) {
		L.log("\nAverage data failed to load because of errorcode #" + errorCode + " - Message:" + message, this);
	}
	
	private void requestRangeFinished(LinkedHashMap<String, String> data) {
		L.log("\nRange {", this);
		for (Map.Entry<String, String> entry : data.entrySet()) {
			L.log("Range: Name=\"" + entry.getKey() + "\" Value=\"" + entry.getValue() + "\"", this);
		}
		L.log("}", this);
	}
	
	private void requestRangeFailed(int errorCode, String message) {
		L.log("\nRange data failed to load because of errorcode #" + errorCode + " - Message:" + message, this);
	}

	private void loadCustomData() {
		L.log("\nLoad Cutom Data", this);
		PlaytomicData customData = new PlaytomicData();
		customData.setRequestListener(new PlaytomicRequestListener<String>() {
        	public void onRequestFinished(PlaytomicResponse<String> playtomicResponse) {
				if (playtomicResponse.getSuccess()) {
					requestCustomDataFinished(playtomicResponse.getMap());
				}			
				else {
					requestCustomDataFailed(playtomicResponse.getErrorCode(), playtomicResponse.getErrorMessage());
				}
			}
		});
		customData.customMetric("hi");
	}

	private void requestCustomDataFinished(LinkedHashMap<String, String> data) {
		L.log("Custom data {", this);
		for (Map.Entry<String, String> entry : data.entrySet()) {
			L.log("Custom data: Name=\"" + entry.getKey() + "\" Value=\"" + entry.getValue() + "\"", this);
		}
		L.log("}", this);
	}
	
	private void requestCustomDataFailed(int errorCode, String message) {
		L.log("Custom data failed to load because of errorcode #" + errorCode + " - Message:" + message, this);
	}

	private void loadPlays() {
		L.log("\nLoad Plays", this);
		PlaytomicData plays = new PlaytomicData();
		plays.setRequestListener(new PlaytomicRequestListener<String>() {
        	public void onRequestFinished(PlaytomicResponse<String> playtomicResponse) {
				if (playtomicResponse.getSuccess()) {
					requestPlaysFinished(playtomicResponse.getMap());
				}			
				else {
					requestPlaysFailed(playtomicResponse.getErrorCode(), playtomicResponse.getErrorMessage());
				}
			}
		});
		plays.plays();
	}

	private void requestPlaysFinished(LinkedHashMap<String, String> data) {
		L.log("Plays {", this);
		for (Map.Entry<String, String> entry : data.entrySet()) {
			L.log("Plays: Name=\"" + entry.getKey() + "\" Value=\"" + entry.getValue() + "\"", this);
		}
		L.log("}", this);
	}
	
	private void requestPlaysFailed(int errorCode, String message) {
		L.log("Plays failed to load because of errorcode #" + errorCode + " - Message:" + message, this);
	}

	private void loadPlaytime() {
		L.log("\nLoad Playtime", this);
		PlaytomicData playTime = new PlaytomicData();
		playTime.setRequestListener(new PlaytomicRequestListener<String>() {
        	public void onRequestFinished(PlaytomicResponse<String> playtomicResponse) {
				if (playtomicResponse.getSuccess()) {
					requestPlaytimeFinished(playtomicResponse.getMap());
				}			
				else {
					requestPlaytimeFailed(playtomicResponse.getErrorCode(), playtomicResponse.getErrorMessage());
				}
			}
		});
		playTime.playtime();
	}

	private void requestPlaytimeFinished(LinkedHashMap<String, String> data) {
		L.log("Playtime {", this);
		for (Map.Entry<String, String> entry : data.entrySet()) {
			L.log("Playtime: Name=\"" + entry.getKey() + "\" Value=\"" + entry.getValue() + "\"", this);
		}
		L.log("}", this);
	}
	
	private void requestPlaytimeFailed(int errorCode, String message) {
		L.log("Playtime failed to load because of errorcode #" + errorCode + " - Message:" + message, this);
	}

	private void loadViews() {
		L.log("\nLoad Views", this);
		PlaytomicData views = new PlaytomicData();
		views.setRequestListener(new PlaytomicRequestListener<String>() {
        	public void onRequestFinished(PlaytomicResponse<String> playtomicResponse) {
				if (playtomicResponse.getSuccess()) {
					requestViewsFinished(playtomicResponse.getMap());
				}			
				else {
					requestViewsFailed(playtomicResponse.getErrorCode(), playtomicResponse.getErrorMessage());
				}
			}
		});
		views.views();
	}

	private void requestViewsFinished(LinkedHashMap<String, String> data) {
		L.log("Views {", this);
		for (Map.Entry<String, String> entry : data.entrySet()) {
			L.log("View: Name=\"" + entry.getKey() + "\" Value=\"" + entry.getValue() + "\"", this);
		}
		L.log("}", this);
	}
	
	private void requestViewsFailed(int errorCode, String message) {
		L.log("Views failed to load because of errorcode #" + errorCode + " - Message:" + message, this);
	}

	private void freeze() {
		L.log("\nFreeze", this);
		Playtomic.Log().freeze();
	}

	private void unfreeze() {
		L.log("\nUnfreeze", this);
		Playtomic.Log().unfreeze();
	}
	
	private void throwException ()
	{
	    int i = 1 / 0;
	}
}