package _3_.objects;

import java.io.File;


public class DatabaseFileLocker
{
	private static object_FileLocker databaseFileLock;
	
	
	public static void setFile( File file )
	{
		if( databaseFileLock != null )
			if( databaseFileLock.isLocked() )
				databaseFileLock.unlock();
		
		databaseFileLock = new object_FileLocker( file );
	}
	
	
	public static void lock()
	{
		if( databaseFileLock != null )
		{
			databaseFileLock.lock();
		}
	}
	
	public static void unlock()
	{
		if( databaseFileLock != null )
		{
			databaseFileLock.unlock();
		}
	}
	
	
	public static boolean isLocked()
	{
		if( databaseFileLock != null )
			return databaseFileLock.isLocked();
		else
			return false;
	}
	
	public static boolean isFileSet()
	{
		return ( databaseFileLock != null );
	}
}
