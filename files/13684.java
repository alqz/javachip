package com.osmosis.robot.metadata;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Task Object
 *
 * @author Thiago Medeiros dos Santos
 *
 */
public class Task implements Serializable {

	private static final long serialVersionUID = -8094823308285281270L;

    /**
     * Period that this action can be executed
     */
	private String cron;

    /**
     * Action metadata
     */
    private Map<String, TaskAction> actions = new HashMap<String, TaskAction>();

    public String getCron() {
        return cron;
    }

    public void setCron(String cron) {
        this.cron = cron;
    }

    public Map<String, TaskAction> getActions() {
		return actions;
	}

	public void setActions(Map<String, TaskAction> actions) {
		this.actions = actions;
	}

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
