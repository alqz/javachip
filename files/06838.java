package qse_sepm_ss12_07.gui;

import java.util.HashMap;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import net.miginfocom.swing.MigLayout;

/**
 * implementation of the shopping list view
 */
public class ShoppingListView extends JPanel implements IShoppingListView
{
	private JPanel productListPanel;
	private JPanel detailViewPanel;
	private JPanel generatePanel;
	
	private IView detailView;
	
	private YaslTabPane tabPaneDetails;
	private HashMap<String, JPanel> tabs = new HashMap<String, JPanel>();
	
	
	public ShoppingListView()
	{
		super(new MigLayout("fill"));
	}
	
	public void start()
	{
		tabPaneDetails = new YaslTabPane();
		productListPanel = new JPanel(new MigLayout("fill"));
		detailViewPanel = new JPanel(new MigLayout("fill"));
		detailViewPanel.add(tabPaneDetails, "left, grow");
		generatePanel = new JPanel(new MigLayout("fill"));	
	}
	
	public void setProductListView(IProductListView productListView)
	{
		productListPanel.removeAll();
		productListPanel.add((JPanel) productListView, "grow");
	}

	public void showGenerator()
	{
		removeAll();
		add(generatePanel, "grow");
		updateUI();
	}

	public void showShoppingLists()
	{
		removeAll();
		add(productListPanel, "left, grow, width 20%");
		add(detailViewPanel,"grow, width 80%");
		updateUI();
	}

	public void setGeneratorView(IView view)
	{
		generatePanel.removeAll();
		generatePanel.add((JPanel)view,"grow");
	}
	
	public void setDetailView(IView view, String tabName)
	{
		if(!tabs.containsKey(tabName))
		{
			addNewDetailView(tabName);
		}
		JPanel panel = this.tabs.get(tabName);
		panel.removeAll();
		panel.add((JComponent) view, "grow");
		panel.updateUI();
	}

	public void addNewDetailView(String name)
	{
		tabs.put(name, new JPanel(new MigLayout("fill")));
		tabPaneDetails.addTab(name, tabs.get(name));

		if (tabPaneDetails.getTabCount() == 1)
		{
			tabPaneDetails.setSelectedIndex(0);
		}
	}

}
