package com.skywatcher.SimpleControlApp;

import java.io.IOException;

import com.skywatcher.api.AXISID;
import com.skywatcher.api.AstroMisc;
import com.skywatcher.api.Mount;
import com.skywatcher.api.MountControlException;
import com.skywatcher.api.Mount_Skywatcher;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class SimpleControlActivity extends Activity {
	// Layout Views
	private LinearLayout controlButtonsLayout; // 遙控用鈕最上層的layout
	private ImageButton button_up, button_down, button_left, button_right;
	private Button button_home, button_shot, button_high_speed,
			button_low_speed, button_set_home, button_setting;
	private TextView view_status; // Status Bar
	private TextView title_right; // 在畫面右上角(Title的右方) 顯示連線狀態

	// 腳架控制
	private static Mount mountControl;
	private static BluetoothManager mManager;

	// Name of the connected device
	private String mConnectedDeviceName = null;

	private double home_axis1, home_axis2; // 目前初始位置的座標
	private double speed; // 移動速度

	private HandlerThread handlerThread; // 快門使用的thread
	private int shotSecond; // 按下快門後剩下的秒數

	// Debugging
	private static final String TAG = "SimpleControlActivity";
	private static final boolean D = true;

	// Setting speed
	private final double HIGH_SPEED = AstroMisc.DegToRad(15);
	private final double LOW_SPEED = AstroMisc.DegToRad(1);

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// 更改Title為可顯示連線狀態的layout
		setCustomTitle();

		findViews();
		setListeners();

		setHome(0.0, 0.0);
		speed = HIGH_SPEED;

		// 初始化 MountControl 類別,並指定Bluetooth Event的Handler
		mManager = new BluetoothManager(this, mHandler);
		// mountControl = new Mount_Skywatcher();
		mountControl = new Mount_Skywatcher();
	}

	@Override
	protected void onResume() {

		// 若已連線則所有元件設為enable，反之則設 disable
		if (mManager.getState() == BluetoothManager.STATE_CONNECTED) {
			setAllEnabled(controlButtonsLayout, true);
			setStatusBar(true);
		} else {
			setAllEnabled(controlButtonsLayout, false);
			setStatusBar(false);
		}
		super.onResume();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();

		if (mManager != null)
			mManager.stop();
	}

	// 設定自改Title以顯示藍芽狀態
	private void setCustomTitle() {
		// Set up the window layout
		requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		setContentView(R.layout.main);
		getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE,
				R.layout.custom_title);

		// Set up the custom title
		((TextView) findViewById(R.id.title_left_text))
				.setText(R.string.app_name);
		title_right = (TextView) findViewById(R.id.title_right_text);
		title_right.setText(R.string.title_not_connected);
	}

	/**
	 * 連結畫面元件與程式內的控制
	 */
	private void findViews() {
		controlButtonsLayout = (LinearLayout) findViewById(R.id.simple_control_LinearLayout);

		button_home = (Button) findViewById(R.id.button_home);
		button_up = (ImageButton) findViewById(R.id.button_up);
		button_down = (ImageButton) findViewById(R.id.button_down);
		button_left = (ImageButton) findViewById(R.id.button_left);
		button_right = (ImageButton) findViewById(R.id.button_right);

		button_shot = (Button) findViewById(R.id.button_shot);
		button_high_speed = (Button) findViewById(R.id.button_high_speed);
		button_low_speed = (Button) findViewById(R.id.button_low_speed);
		button_set_home = (Button) findViewById(R.id.button_set_home);

		button_setting = (Button) findViewById(R.id.button_setting);

		view_status = (TextView) findViewById(R.id.view_status);
	}

	// 設定 Button 的 Action
	private void setListeners() {
		button_home.setOnClickListener(clickListener);

		button_up.setOnTouchListener(controlListener);
		button_down.setOnTouchListener(controlListener);
		button_left.setOnTouchListener(controlListener);
		button_right.setOnTouchListener(controlListener);

		button_shot.setOnClickListener(clickListener);
		button_high_speed.setOnClickListener(clickListener);
		button_low_speed.setOnClickListener(clickListener);
		button_set_home.setOnClickListener(clickListener);

		button_setting.setOnClickListener(settingListener);
	}

	private OnClickListener clickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			try {
				switch (v.getId()) {
				case R.id.button_home:
					mountControl.MCAxisSlewTo(AXISID.AXIS1, home_axis1);
					mountControl.MCAxisSlewTo(AXISID.AXIS2, home_axis2);
					break;

				case R.id.button_shot:
					v.setEnabled(false);
					mountControl.MCSetSwitch(true);

					// 從設定介面所設的值中，調出來使用 (Trigger Delay)
					SharedPreferences settings = PreferenceManager
							.getDefaultSharedPreferences(SimpleControlActivity.this);
					String triggerDelay = settings.getString(
							getString(R.string.key_trigger_delay), "3");
					shotSecond = Integer.parseInt(triggerDelay);

					// 使用thread來倒數秒數
					handlerThread = new HandlerThread("shot");
					handlerThread.start();
					new Handler(handlerThread.getLooper()).post(new Runnable() {
						@Override
						public void run() {
							atFrontOfQueue();
						}
					});
					break;

				case R.id.button_high_speed:
					speed = HIGH_SPEED;
					break;

				case R.id.button_low_speed:
					speed = LOW_SPEED;
					break;

				case R.id.button_set_home:
					setHome(mountControl.MCGetAxisPosition(AXISID.AXIS1),
							mountControl.MCGetAxisPosition(AXISID.AXIS2));
					Toast.makeText(
							SimpleControlActivity.this,
							"Home is (" + AstroMisc.RadToStr(home_axis1) + ", "
									+ AstroMisc.RadToStr(home_axis2) + ") now.",
							Toast.LENGTH_LONG).show();
					break;
				}
				setStatusBar(true);

			} catch (MountControlException e) {
				Toast.makeText(SimpleControlActivity.this, e.ErrMessage,
						Toast.LENGTH_SHORT).show();
				e.printStackTrace();
			}
		}
	};
	private int PreviousAction = -1;
	private AXISID PreviousAxis = null;

	private OnTouchListener controlListener = new OnTouchListener() {
		public boolean onTouch(View v, MotionEvent motionEvent) {
			try {
				AXISID Axis = AXISID.AXIS1;
				int direction = 1;
				switch (v.getId()) {
				case R.id.button_up:
					Axis = AXISID.AXIS2;
					direction = 1;
					break;
				case R.id.button_down:
					Axis = AXISID.AXIS2;
					direction = -1;
					break;
				case R.id.button_left:
					Axis = AXISID.AXIS1;
					direction = -1;
					break;
				case R.id.button_right:
					Axis = AXISID.AXIS1;
					direction = 1;
					break;
				}

				if (PreviousAxis == Axis
						&& PreviousAction == motionEvent.getAction())
					return false;

				// Should use message to avoid halting UI
				if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
					Log.i(TAG, "AxisSlew:" + Axis + "," + speed);
					mountControl.MCAxisSlew(Axis, direction * speed);
				} else if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
					Log.i(TAG, "AxisStop:" + Axis + "," + speed);
					mountControl.MCAxisStop(Axis);
				}

				PreviousAction = motionEvent.getAction();
				PreviousAxis = Axis;
				setStatusBar(true);
			} catch (MountControlException e) {
				if (e.ErrMessage != null) {
					Toast.makeText(SimpleControlActivity.this, e.ErrMessage,
							Toast.LENGTH_SHORT).show();
				} else {
					Toast.makeText(SimpleControlActivity.this,
							"Some thing wrong", Toast.LENGTH_SHORT).show();
				}
				Log.e(TAG, "Failed", e);
			}

			return false;
		}
	};

	private OnClickListener settingListener = new OnClickListener() {

		@Override
		public void onClick(View arg0) {
			// Move to the setting interface
			Intent settingIntent = new Intent(SimpleControlActivity.this,
					SettingActivity.class);
			startActivity(settingIntent);
		}
	};

	/**
	 * Set all descendant enable or disable.
	 * 
	 * @param vg
	 *            Like LinearLayout...
	 * @param enabled
	 *            enable or disable.
	 */
	public static void setAllEnabled(ViewGroup vg, boolean enabled) {
		for (int i = 0; i < vg.getChildCount(); i++) {
			if (vg.getChildAt(i) instanceof ViewGroup)
				setAllEnabled((ViewGroup) vg.getChildAt(i), enabled);
			else
				vg.getChildAt(i).setEnabled(enabled);
		}
	}

	// 顯示目前的角度讀數, 或未連線時顯示未連線
	private void setStatusBar(boolean connected) {
		if (!connected) {
			view_status.setText(R.string.title_not_connected);
			return;
		}

		try {
			view_status.setText("Axis1: "
					+ AstroMisc.RadToStr(mountControl
							.MCGetAxisPosition(AXISID.AXIS1))
					+ ", Axis2: "
					+ AstroMisc.RadToStr(mountControl
							.MCGetAxisPosition(AXISID.AXIS2)));
		} catch (MountControlException e) {
			Toast.makeText(SimpleControlActivity.this, e.ErrMessage,
					Toast.LENGTH_SHORT).show();
			e.printStackTrace();
		}
	}

	private void setHome(double axis1, double axis2) {
		home_axis1 = axis1;
		home_axis2 = axis2;
	}

	private void atFrontOfQueue() {
		new Handler(Looper.getMainLooper()).postAtFrontOfQueue(new Runnable() {
			@Override
			public void run() {
				if (shotSecond <= 0) {
					try {
						mountControl.MCSetSwitch(false);
						handlerThread.getLooper().quit();
						handlerThread.join();
						button_shot.setText(R.string.button_shot);
						button_shot.setEnabled(true);
					} catch (MountControlException e) {
						e.printStackTrace();
						mManager.connectionLost();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					return;
				}
				button_shot.setText(getString(R.string.button_shot_wait)
						+ shotSecond + getString(R.string.button_shot_second));
				--shotSecond;
				delayed();
			}
		});
	}

	private void delayed() {
		new Handler(handlerThread.getLooper()).postDelayed(new Runnable() {
			@Override
			public void run() {
				atFrontOfQueue();
			}
		}, 1000);
	}

	public static BluetoothManager getBluetoothManager() {
		return mManager;
	}

	public static Mount getMountControl() {
		return mountControl;
	}

	// Reconnect Dialog
	private static AlertDialog reconnectDialog;
	private static ProgressDialog connectDialog;

	// The Handler that gets information back from the BluetoothChatService
	private final Handler mHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case BluetoothManager.MESSAGE_RECONNECTED:
				doReconnected();
				break;
			case BluetoothManager.MESSAGE_CONNECTED:
				doConnected();
				break;
			case BluetoothManager.MESSAGE_CONNECTION_LOST:
				doConnectLost();
				PopupReconnectDialog(SimpleControlActivity.this);
				// mManager.requestReconnect();
				break;
			case BluetoothManager.MESSAGE_CONNECTION_FAILED:
				doConnectionFailed();
				break;
			case BluetoothManager.MESSAGE_CONNECTION_STOP:
				doConnectionStop();
				break;
			// case BluetoothManager.MESSAGE_REQUEST_RECONNECT:
			// PopupReconnectDialog(PanoramaAppActivity.this);
			// break;
			// case BluetoothManager.MESSAGE_REQUEST_CONNECT:
			// String address = (String) msg.obj;
			// PopupConnectDialog(PanoramaAppActivity.this, address);
			// break;
			case BluetoothManager.MESSAGE_STATE_CHANGE:
				if (D)
					Log.i(TAG, "MESSAGE_STATE_CHANGE: " + msg.arg1);
				doStateChange(msg.arg1);
				break;
			case BluetoothManager.MESSAGE_DEVICE_NAME:
				// save the connected device's name
				mConnectedDeviceName = msg.getData().getString(
						BluetoothManager.DEVICE_NAME);
				Toast.makeText(getApplicationContext(),
						"Connected to " + mConnectedDeviceName,
						Toast.LENGTH_SHORT).show();
				break;
			case BluetoothManager.MESSAGE_TOAST:
				Toast.makeText(getApplicationContext(),
						msg.getData().getString(BluetoothManager.TOAST),
						Toast.LENGTH_SHORT).show();
				break;
			}
		}

		private void doConnectionStop() {
			if (reconnectDialog != null)
				reconnectDialog.dismiss();

			if (connectDialog != null)
				connectDialog.dismiss();
		}

		private void doConnectionFailed() {
			if (reconnectDialog != null)
				reconnectDialog.dismiss();

			if (connectDialog != null)
				connectDialog.dismiss();
		}

		private void doReconnected() {
			try {
				mountControl.Connect_Bluetooth(mManager.getSocket());
			} catch (IOException e) {
				e.printStackTrace();
				Log.e(TAG, "Bluetooth Connect Failed", e);
			}

			// 啟用所有的按鈕
			SimpleControlActivity.setAllEnabled(controlButtonsLayout, true);

			if (reconnectDialog != null)
				reconnectDialog.dismiss();

			if (connectDialog != null)
				connectDialog.dismiss();
		}

		private void doStateChange(int state) {
			switch (state) {
			case BluetoothManager.STATE_CONNECTED:
				title_right.setText(R.string.title_connected_to);
				title_right.append(mConnectedDeviceName);
				break;
			case BluetoothManager.STATE_CONNECTING:
				title_right.setText(R.string.title_connecting);
				break;
			// case BluetoothManager.STATE_LISTEN:
			// title_right.setText(R.string.title_not_connected);
			// break;
			case BluetoothManager.STATE_NONE:
			case BluetoothManager.STATE_CONNECTION_LOST:
				title_right.setText(R.string.title_not_connected);
				break;
			}
		}

		private void doConnectLost() {
			// 停用所有的按鈕
			SimpleControlActivity.setAllEnabled(controlButtonsLayout, false);

			// Cancel Dialog
			if (reconnectDialog != null)
				reconnectDialog.dismiss();

			if (connectDialog != null)
				connectDialog.dismiss();
		}

		private void doConnected() {
			try {
				mountControl.Connect_Bluetooth(mManager.getSocket());
			} catch (IOException e) {
				e.printStackTrace();
				Log.e(TAG, "Bluetooth Connect Failed", e);
			}

			// 啟動腳架連線
			try {
				mountControl.MCOpenTelescopeConnection();
			} catch (MountControlException e) {
				e.printStackTrace();
				Log.e(TAG, "Open Telescope Connect Failed", e);
			}

			// Cancel Dialog
			if (reconnectDialog != null)
				reconnectDialog.dismiss();

			if (connectDialog != null)
				connectDialog.dismiss();

			// 啟用所有的按鈕
			SimpleControlActivity.setAllEnabled(controlButtonsLayout, true);
		}
	};

	// Popup a reconnection dialog
	public static void PopupReconnectDialog(final Context context) {
		if (reconnectDialog != null)
			reconnectDialog.dismiss();

		reconnectDialog = new AlertDialog.Builder(context)
				.setCancelable(true)
				.setTitle("Reconnect to Device")
				.setNegativeButton("Cancel", null)
				.setPositiveButton(R.string.dialog_title,
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {

								connectDialog = ProgressDialog.show(context,
										"", "Connecting. Please Wait", true,
										true,
										new DialogInterface.OnCancelListener() {
											@Override
											public void onCancel(
													DialogInterface dialog) {

												// Stop Bluetooth
												// connection
												mManager.stop();
											}
										});
								mManager.reconnect();
							}
						}).create();
		reconnectDialog.show();
	}

	// Popup a connection dialog
	public static void PopupConnectDialog(Context context, String Address) {
		if (connectDialog != null)
			connectDialog.dismiss();

		connectDialog = ProgressDialog.show(context, "",
				"Connecting. Please Wait", true, false);
		mManager.connect(Address);
	}
}