package net.skcomms.dtc.client.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import net.skcomms.dtc.client.PersistenceManager;

public class SearchHistoryDao {

  public static final String CLASS_NAME = "SearchHistoryDao";

  private static final int MAX_HISTORY_SIZE = 50;

  public static String combineKey(SearchHistory searchHistory) {
    return SearchHistoryDao.combineKeyPrefix("SearchHistory", searchHistory.getPath())
        + searchHistory.getSearchTime().getTime();
  }

  public static String combineKey(String path) {
    return SearchHistoryDao.combineKeyPrefix("FieldStates", path);
  }

  public static String combineKeyPrefix(String type, String path) {
    return SearchHistoryDao.CLASS_NAME + "." + type + "." + path + ".";
  }

  private List<SearchHistory> deserializeAllSearchHistories(String path) {
    List<SearchHistory> searchHistories = new ArrayList<SearchHistory>();
    for (String key : PersistenceManager.getInstance().getItemKeys()) {
      if (key.startsWith(SearchHistoryDao.combineKeyPrefix("SearchHistory", path))) {
        SearchHistory history = SearchHistory.deserialize(PersistenceManager.getInstance()
            .getItem(key));
        searchHistories.add(history);
      }
    }
    return searchHistories;
  }

  public String getFieldStates(String path) {
    return PersistenceManager.getInstance().getItem(SearchHistoryDao.combineKey(path));
  }

  public List<SearchHistory> getSearchHistroies(String path) {
    List<SearchHistory> searchHistories = this.deserializeAllSearchHistories(path);
    this.sortByTimeDesc(searchHistories);
    this.removeOldHistories(searchHistories);
    return searchHistories;
  }

  public void persistFieldStates(String path, String fieldStates) {
    PersistenceManager.getInstance().setItem(SearchHistoryDao.combineKey(path), fieldStates);
  }

  public void persistSearchHistory(SearchHistory searchHistory) {
    PersistenceManager.getInstance().setItem(
        SearchHistoryDao.combineKey(searchHistory), searchHistory.serialize());
  }

  private void removeOldHistories(List<SearchHistory> searchHistories) {
    for (int i = SearchHistoryDao.MAX_HISTORY_SIZE; i < searchHistories.size(); i++) {
      PersistenceManager.getInstance().removeItem(
          SearchHistoryDao.combineKey(searchHistories.get(i)));
    }
    if (searchHistories.size() > SearchHistoryDao.MAX_HISTORY_SIZE) {
      searchHistories.subList(SearchHistoryDao.MAX_HISTORY_SIZE, searchHistories.size()).clear();
    }
  }

  private void sortByTimeDesc(List<SearchHistory> searchHistories) {
    Collections.sort(searchHistories, new Comparator<SearchHistory>() {

      @Override
      public int compare(SearchHistory o1, SearchHistory o2) {
        return (int) (-(o1.getSearchTime().getTime() - o2.getSearchTime().getTime()));
      }
    });
  }

}
