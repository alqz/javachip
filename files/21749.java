package com.feelthebeats.charter;

/**
 * JSNexen
 * 5/8/12
 * 8:29 PM
 */
public abstract class TimedEvent implements Comparable<TimedEvent> {

    private double beat;
    private double value;

    public double getBeat() {
        return beat;
    }

    public void setBeat(double beat) {
        this.beat = beat;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    @Override
    public int compareTo(TimedEvent o) {
        return (int) (this.beat - o.beat);
    }
}
