/**
 * Copyright 1999-2009 The Pegadi Team
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.pegadi.sqlsearch;

import junit.framework.TestCase;

public class ComparisonTermTest extends TestCase {

    ComparisonTerm term;

    public void setUp() {
        term = new ComparisonTerm() {
            public String whereClause() {
                return "something";
            }

            public String[] getTables() {
                return new String[0];
            }
        };


    }

    public void testGetOperator() {

        term.setComparison(ComparisonTerm.EQ);
        assertEquals("=", term.getOperator());

        term.setComparison(ComparisonTerm.GE);
        assertEquals(">=", term.getOperator());

        term.setComparison(ComparisonTerm.LE);
        assertEquals("<=", term.getOperator());

        term.setComparison(ComparisonTerm.GT);
        assertEquals(">", term.getOperator());

        term.setComparison(ComparisonTerm.LT);
        assertEquals("<", term.getOperator());

        term.setComparison(ComparisonTerm.NE);
        assertEquals("!=", term.getOperator());

    }

    public void testIllegalOperator() {
        term.setComparison(Integer.MAX_VALUE);
        assertNull(term.getOperator());
    }

    public void testGetComparison() {
        term.setComparison(11);
        assertEquals(11, term.getComparison());
    }


}