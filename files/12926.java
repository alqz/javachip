package calendar;


import java.util.Calendar;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Callum Eves
 */
public class WorkEvent extends Event {
    private int m_guests;
    
    public static final int NO_FIELDS = 8;
    public static final int IDX_GUESTS = 7;

    /**
    * This gets the guests attending the work event
    * @return guests
    */ 
    public int GetGuests() {
        return m_guests;
    }

    /**
    * This sets the guests attending the work event
    * @param guests
    */ 
    public void SetGuests(int guests) {
        m_guests = guests;
    }

    /**
    * Super inherits from the Events class
     * @throws Exception 
    */ 
    public WorkEvent(int id, String title, Calendar start_date, 
    		Calendar end_date, String description, int repetition, int guests) 
    				throws Exception {
        super(id, title, start_date, end_date, description, repetition);
        m_guests = guests;
        m_type = Event.WORK_EVENT;
    }
    
    
    public WorkEvent() {
		super();
		m_guests = 0;
		m_type = Event.WORK_EVENT;
	}

    /**
    * Sends to the CSV file
    */ 
    @Override
    public String ToCSV() {
        String csv = super.ToCSV() + ","+m_guests;
        return csv;
    }
    
    /**
    * test to create a new work event
    */ 
    public static void main (String[] args){
    WorkEvent test = new WorkEvent();
    test.SetGuests(15);
    System.out.println(test.GetGuests());
}
}
