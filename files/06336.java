package com.remindthatmovie.webservice.themoviedb;

import java.util.List;

import com.moviejukebox.themoviedb.TheMovieDb;
import com.moviejukebox.themoviedb.model.MovieDB;
import com.remindthatmovie.webservice.IWebserviceCall;
import com.remindthatmovie.webservice.IWebserviceCallResult;

public class TheMovieDBCall implements IWebserviceCall {

	public static final String DEFAULT_LANGUAGE = "en-US";

	private String apiKey;
	private String movieTitle;
	private String language;
	
	public TheMovieDBCall(String apiKey, String movieTitle, String language){
		this.apiKey=apiKey;
		this.movieTitle=movieTitle;
		this.language=language;
	}
	
	@Override
	public TheMovieDBResult execute() {
		TheMovieDb tmdb = new TheMovieDb(this.apiKey);
		TheMovieDBResult result;
		List<MovieDB> dbList = tmdb.moviedbSearch(this.movieTitle, this.language);
		result = new TheMovieDBResult(dbList);
		return result;
	}

	public String getApiKey() {
		return apiKey;
	}

	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}

	public String getMovieTitle() {
		return movieTitle;
	}

	public void setMovieTitle(String movieTitle) {
		this.movieTitle = movieTitle;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}
}
