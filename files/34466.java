/**
 * Copyright 1999-2009 The Pegadi Team
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.pegadi.artis;

// UI imports
import org.w3c.dom.*;

import javax.swing.text.*;

/**
 * This class overrides {@link javax.swing.text.DefaultStyledDocument} to
 * provide verification when inserting new elements.
 *
 * @author Håvard Wigtil <havardw at pvv.org>
 * @version $Revision$, $Date$
 */

class TextDocument extends org.pegadi.artis.AbstractTextDocument {

    public TextDocument(Node root, StyleContext styles,
            DocumentType docType) {
        super(root, styles, docType);    }
 

    /**
     * Insert content into the document. <code>\n</code> is treated as a new element
     * marker, and this method will check to see if new elements are allowed.
     * When they are not allowed, the <code>\n</code> will be ignored.
     *
     * @param offset  The offset into the document to insert at.
     * @param str  The string to insert.
     * @param a  The attributes to apply to the text.
     * @throws BadLocationException  If the offset is not valid.
     */
    public void insertString(int offset, String str, AttributeSet a) throws BadLocationException {
        checkEaster(str);
        if(autoCorrection && str.length() == 1){
            if(getLength()==0){
                spaceCaseState = SPACE_CASE_SPACE;  //uppercase for the first letter in document

            }
            if(offset!=lastOffset+1 && getLength()!=0){
                spaceCaseState = SPACE_CASE_INIT;        //moving the cursor or pressing backspace will result in loss off  uppercase-state
            }
            if(str.equals(".")||str.equals("?")||str.equals("!")){
                spaceCaseState = SPACE_CASE_DOT;
                lastOffset = offset;
            }
            else if(str.equals(" ") && (spaceCaseState == SPACE_CASE_DOT || spaceCaseState == SPACE_CASE_SPACE )){
                spaceCaseState = SPACE_CASE_SPACE;
                lastOffset = offset;
            }
            else if( str.equals("\n") && (spaceCaseState == SPACE_CASE_DOT || spaceCaseState == SPACE_CASE_SPACE )){
                spaceCaseState = SPACE_CASE_SPACE;
                lastOffset = offset;
            }
            else if(spaceCaseState == SPACE_CASE_SPACE){
                str = str.toUpperCase();
                spaceCaseState = SPACE_CASE_INIT;
            }
            else{
                spaceCaseState = SPACE_CASE_INIT;
            }
        }

        String prev;
        if (offset>=1){
            prev = getText(offset-1,1); // The character before the one we're writing
        } else {
            prev = "\n";
        }
        if (str.equals("\"")){
            if (prev.equals(" ") || prev.equals("\n")){ // " after space or at start of line --> «
                str = "«";
            } else { // Other " --> «
                str = "»";
            }
        } else if (str.equals("-")){
            if (prev.equals("\n")){ // - at start of a line --> – (Aye, that line is actually longer. It's the one we use.)
                str = "–";
            }
        }

        super.insertString(offset, str, a);
    }

}
