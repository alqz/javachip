import java.util.*;
import java.math.*;
import java.io.*;
import cs1.Keyboard;
public class Fenrir extends Boss {
  public Fenrir() {
    level = 5;
    maxhp = 1000;
    hp = 1000; //amount of Health
    power = 180; //Physical Attack
    defense = 250; //Physical Defense
    magic = 20; //Magical Attack
    resistance = 150; //Magical Defense
    speed = 100; //Speed
    accuracy = 74; //Accuracy %
    name = "Fenrir";
  }
}