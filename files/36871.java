/**
 * This work is licensed under the Creative Commons Attribution-NonCommercial
 * 3.0 Unported License. To view a copy of this license, visit 
 * http://creativecommons.org/licenses/by-nc/3.0/ or send a letter to 
 * Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 
 * 94041, USA.
 */

package org.netslug.homesweethome;

import java.util.*;

import org.bukkit.Location;
import org.bukkit.entity.Player;

/**
 * Scheduler subclass for HomeSweetHome
 * 
 * @author netslug
 */
public class HomeSweetHomeScheduler {
	
	public static HomeSweetHome plugin;
	

	protected Map<Player, Boolean> playerDelay = new HashMap<Player, Boolean>();
	protected ArrayList<Player> playerHome = new ArrayList<Player>();
	
	public Long commandDelay;
	public Long homeDelay;
	public Integer sec;
	
	/**
	 * Create an instance of main class HomeSweetHome.
	 * 
	 * @param instance Instance of HomeSweetHome.
	 */
	public HomeSweetHomeScheduler(HomeSweetHome instance) {
		plugin = instance;
	}
	
	/**
	 * Converts seconds to ticks.
	 * 
	 * @param seconds Seconds (in Integer) to convert to ticks (Long).
	 * @return Ticks.
	 */
	public Long intToTicks(Integer seconds) {
		sec = seconds;
		Long ticks = (sec.longValue() * 20);
		return ticks;
	}
	
	/**
	 * Checks if specified player is currently in a scheduled delay.
	 * 
	 * @param player Player to be checked.
	 * @param type 'home' for /home delay, 'command' for /home set delay.
	 * @return True if player is currently being delayed.
	 */
	public boolean isPlayerDelayed(Player player, String type) {
		if (type.equals("home")) {
			if (playerHome.contains(player)) return true;
		} else if (type.equals("command")) {
			if (playerDelay.containsKey(player)) {
				if (playerDelay.get(player)) return true;
			} else {
				playerDelay.put(player, false);
			}
		}
		return false;
	}
	
	/**
	 * Adds a delay to the specified player.
	 * 
	 * @param player Player to be delayed.
	 * @param type 'home' for /home delay, 'command' for /home set delay.
	 */
	public void addPlayerDelay(final Player player, String type, final Location loc) {
		if (type.equals("home")) {
			playerHome.add(player);
			if (homeDelay > 0) {
				plugin.sendMessage(player, plugin.messageList.get("msg-seconds"));
			}
			plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
				public void run() {
					player.teleport(loc);
					playerHome.remove(player);
				}
			}, homeDelay);
		} else if (type.equals("command")) {
			playerDelay.put(player, true);
			plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
				public void run() {
					playerDelay.put(player, false);
				}
			}, commandDelay);
		}
	}
}
