package datamodeler.formats;

import java.io.IOException;
import java.io.Writer;

import datamodeler.models.Column;
import datamodeler.models.Database;
import datamodeler.models.Table;

/**
 * A simple XML format
 * 
 * @author Kyle Sletten
 */
public class XMLFormat implements Format<Database> {
	public void write(Iterable<Database> databases, Writer writer)
			throws IOException {
		XMLWriter xmlWriter = new XMLWriter(writer);
		this.write(databases, xmlWriter);
	}

	private void write(Iterable<Database> databases, XMLWriter writer)
			throws IOException {
		writer.open("databases");
		for (Database database : databases) {
			this.write(database, writer);
		}
		writer.close();
	}

	public void write(Database database, Writer writer) throws IOException {
		XMLWriter xmlWriter = new XMLWriter(writer);
		this.write(database, xmlWriter);
	}

	private void write(Database database, XMLWriter writer) throws IOException {
		if (database.getTables() != null && !database.getTables().isEmpty()) {
			writer.open("database", new Attribute("name", database.getName()));
			for (Table table : database.getTables()) {
				this.write(table, writer);
			}
			writer.close();
		} else {
			writer.tag("database", new Attribute("name", database.getName()));
		}
	}

	public void write(Table table, Writer writer) throws IOException {
		XMLWriter xmlWriter = new XMLWriter(writer);
		this.write(table, xmlWriter);
	}

	private void write(Table table, XMLWriter writer) throws IOException {
		if (table.getColumns() != null && !table.getColumns().isEmpty()) {
			writer.open("table", new Attribute("name", table.getName()));
			for (Column column : table.getColumns()) {
				this.write(column, writer);
			}
			writer.close();
		} else {
			writer.tag("table", new Attribute("name", table.getName()));
		}
	}

	public void write(Column column, Writer writer) throws IOException {
		XMLWriter xmlWriter = new XMLWriter(writer);
		this.write(column, xmlWriter);
	}

	private void write(Column column, XMLWriter writer) throws IOException {
		writer.tag("column", new Attribute("name", column.getName()),
		new Attribute("type", column.getColumnType()),
		new Attribute("key", column.getKeyType().toString()),
		new Attribute("nullable", String.valueOf(column.isNullable())),
		new Attribute("default", column.getDefaultValue()),
		new Attribute("notes", column.getNotes()));
	}
}
