package test.pathfinding;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.tiled.TiledMap;
import org.newdawn.slick.util.pathfinding.PathFindingContext;
import org.newdawn.slick.util.pathfinding.TileBasedMap;


/**
 * The data map from our example game. This holds the state and context of each tile
 * on the map. It also implements the interface required by the path finder. It's implementation
 * of the path finder related methods add specific handling for the types of units
 * and terrain in the example game.
 * 
 * @author Kevin Glass
 */
public class GameMap extends TiledMap implements TileBasedMap {
	
	/** Using in Debug **/
	private static final Color FADE_RED = new Color(255, 0, 0, 100);
	
	private boolean[][] blocked;
	private boolean[][] water;
	
	/** Indicate trees terrain at a given location */
	public static final int TREES = 1;
	/** Indicate grass terrain at a given location */
	public static final int GRASS = 2;
	/** Indicate water terrain at a given location */
	public static final int WATER = 3;
	/** Indicate a plane is at a given location */
	public static final int PLANE = 1;
	/** Indicate a boat is at a given location */
	public static final int BOAT = 2;
	/** Indicate a tank is at a given location */
	public static final int TANK = 3;
	
	/** The unit in each tile of the map */
	private int[][] units;
	/** Indicator if a given tile has been visited during the search */
	private boolean[][] visited;
	
	/**
	 * Create a new test map with some default configuration
	 * @throws SlickException 
	 */
	public GameMap(String ref, String tilePath) throws SlickException {
		super(ref, tilePath);
		
		this.blocked = new boolean[width][height];
		this.water = new boolean[width][height];
		this.visited = new boolean[width][height];
		this.units = new int[width][height];
		
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				
				// Collisions / Wall
				int tileID = this.getTileId(x, y, 0);
				String value = this.getTileProperty(tileID, "wall", "false");
				blocked[x][y] = value.equals("true");

				// Water
				value = this.getTileProperty(tileID, "water", "false");
				water[x][y] = value.equals("true");
				
			}
		}
		
		units[2][2] = TANK;
		units[7][15] = BOAT;
		units[14][20] = PLANE;
	}
	
	/**
	 * Clear the array marking which tiles have been visted by the path 
	 * finder.
	 */
	public void clearVisited() {
		for (int x=0;x<getWidthInTiles();x++) {
			for (int y=0;y<getHeightInTiles();y++) {
				visited[x][y] = false;
			}
		}
	}
	
	/**
	 * @see TileBasedMap#visited(int, int)
	 */
	public boolean visited(int x, int y) {
		return visited[x][y];
	}
	
	/**
	 * Get the terrain at a given location
	 * 
	 * @param x The x coordinate of the terrain tile to retrieve
	 * @param y The y coordinate of the terrain tile to retrieve
	 * @return The terrain tile at the given location
	 */
	public int getTerrain(int x, int y) {
		return this.getTileId(x, y, 0);
	}
	
	/**
	 * Get the unit at a given location
	 * 
	 * @param x The x coordinate of the tile to check for a unit
	 * @param y The y coordinate of the tile to check for a unit
	 * @return The ID of the unit at the given location or 0 if there is no unit 
	 */
	public int getUnit(int x, int y) {
		return units[x][y];
	}
	
	/**
	 * Set the unit at the given location
	 * 
	 * @param x The x coordinate of the location where the unit should be set
	 * @param y The y coordinate of the location where the unit should be set
	 * @param unit The ID of the unit to be placed on the map, or 0 to clear the unit at the
	 * given location
	 */
	public void setUnit(int x, int y, int unit) {
		units[x][y] = unit;
	}
	
	/**
	 * @see TileBasedMap#pathFinderVisited(int, int)
	 */
	public void pathFinderVisited(int x, int y) {
		visited[x][y] = true;
	}

	/**
	 * @see TileBasedMap#blocked(PathFindingContext, int, int)
	 */
	@Override
	public boolean blocked(PathFindingContext context, int tx, int ty) {
		// if theres a unit at the location, then it's blocked
		if (getUnit(tx,ty) != 0) {
			return true;
		}
			
		int unit = ((UnitMover) context.getMover()).getType();
				
		// planes can move anywhere
		if (unit == PLANE) {
			return false;
		}
		// tanks can only move across grass
		if (unit == TANK) {
			return getTerrain(tx, ty) != GRASS;
		}
		// boats can only move across water
		if (unit == BOAT) {
			return getTerrain(tx, ty) != WATER;
		}
				
		// unknown unit so everything blocks
		return true;
	}

	/**
	 * Render the map with specific scroll
	 * 
	 */
	public void render(Graphics g, GameContainer container, int decalX, int decalY) {
		g.translate(-(decalX % tileWidth), -(decalY % tileWidth));
		this.render(0,0, decalX/tileWidth, decalY/tileWidth, ((decalX + container.getWidth()) / tileWidth) + 1, ((decalY + container.getHeight()) / tileWidth) + 1);
		g.translate(decalX % tileWidth, decalY % tileWidth);
	}
	
	/**
	 * @see TileBasedMap#getCost(PathFindingContext, int, int, int, int)
	 */
	@Override
	public float getCost(PathFindingContext context, int tx, int ty) {
		return 1;
	}

	@Override
	public int getHeightInTiles() {
		return height;
	}

	@Override
	public int getWidthInTiles() {
		return width;
	}
	
	public int getWidthInPixel() {
		return width * tileWidth;
	}

	public int getHeightInPixel() {
		return height * tileHeight;
	}
}
