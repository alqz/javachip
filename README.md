# Java Chip

A corpus of 40,000 well-formed Java source files (.java) on GitHub.

All files can be parsed into an AST, and can be read safely using UTF-8 encoding.

## Method

This corpus was compiled on 2018 May 16.

These were obtained from [GitHub Java Corpus](http://groups.inf.ed.ac.uk/cup/javaGithub/), a large corpus of .java files from GitHub.

From there, I selected mostly randomly 40,000 files that parse using the Java 8 parser, [javalang](https://github.com/c2nes/javalang).

The 40,000 files were additionally shuffled.